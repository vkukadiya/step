Feature: option to autoplay all videos in a Ladder
As a DrupalLadder visitor
I can choose to play all video segments in a Ladder
So that I get a tour of all steps with video.

Feature: expand all steps; highlight as its video plays
As a visitor who has chosen to have all video segments autoplay
I can see all steps fully expanded and I can see the step highlighted as its video plays
So that I can more easily follow the sequence of instructions.

Feature: pause 20 sec on steps with no video
As a visitor who has chosen to have all video segments autoplay
I can see the default message about missing video
So that I can more easily follow the sequence of instructions.
