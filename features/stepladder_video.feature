Feature: confirmed documentation
  As a documentarian,
  I can maintain what I know to be canonical instruction sets
  So that people who need to get things done (including me, when I need a reminder) always have confirmed information available for their own specific scenarios.

@opensource_ariadne
Feature: POSSE [1]
  As an author,
  I can publish my work on my own site/blog
  So that my site's reputation can grow as a result of my contribution to the DrupalLadder

@a11y
Feature: "play all" button; sync steps (aka topic headers) to video progress
  As a viewer [2] of video clips,
  I can click the "play all clips" button and see
    a) only video clips (clips may cut out parts of a video or combine multiple videos)
    b) video clips in the order of the listed steps (steps may not follow the order presented in a video)
    c) the corresponding steps change from inactive to active as the video reaches each given topic
  So that I get a condensed introduction to the steps because:
    - the written steps limit what is shown during video playback
    - authors of steps have initial control over video(s) played for the viewer
    - viewers see only selected parts of video(s)
    - viewers can follow progress of steps as each is highlighted.

Feature: "play" button
  As a viewer [2] of video clips,
  I can click the "play" button and limit playback only to the active video clip, with the video always pausing at the specified endtime
  So that I can easily review individual steps, replaying a step as often as needed and using the video progress bar to scrub to a specific spot.

@platform02sprint01
Feature: click step (aka  topic header listing) to seek to related point in video
  As a viewer [2] of video clips,
  I can click an instructional step to view just the related clip
  So that I can easily play just the clip that interests me and forego the rest.

@platform02sprint01
Feature: individualize a Ladder per user choices
  As a viewer of a Ladder,
  I can select filters according to my requirements
  So that instruction sets show steps relevant to my chosen OS, toolchain components, etc.

@platform02sprint01
Feature: create a Ladder (aka topic header listing)
  As an author of a Ladder,
  I can add an instructional step (aka step or topic header), and capture video id, seekto time and pause (aka end) time
  So that I can guide viewers to key clips within a video.

@platform02sprint01
Feature: reuse Ladder steps
  As an author of a Ladder
  I can reuse an existing step from any given ladder
  So that improvements to a step in one Ladder can be applied to each instance.

Feature: nest instructions
  As an author of a Ladder,
  I can nest substep to whatever level is required for successful completion of large tasks
  So that I can present a broad perspective when steps are collapsed, and great detail as a viewer expands each nested layer expand.

Feature: propose an improvement to an existing step
@include propose_improvement_to_step.feature

Feature: up/down vote proposed improvement to an existing step
@include features/vote_improvement_up-down.feature

Feature: default to most highly voted revision of a step
  As a first-time viewer of a Ladder (alt: logged in as a team member)
  I can see all the most highly voted revisions of each step (alt: by other members of my team)
  So that my first encounter best reflects the wisdom of the community

Feature: discuss improvement to an existing Ladder or step
  As a viewer of proposed improvements to a Ladder or step,
  I can post comments and vote on others' comments
  So that I can request and/or provide clarification regarding the proposal.

Feature: receive updates but don't adopt revision of a step
  As a viewer of a step that has a proposed improvement,
  I can review the proposal and choose just to receive updates without registering a vote
  So that I can make a decision at a future point.

Feature: poll resources for changes
  As a content maintainer,
  I can review a queue of changes to resources
  So that I can easily determine what content needs to be updated

Feature: automatically test content
  As a content maintainer,
  I can review a dashboard of content tests
  So that I can easily determine what content needs to be updated

/**
 * Discussion:
 *
 * [1] POSSE - publish first on your own site then syndicate elsewhere;
 *   mentioned by Dries in blog post https://dri.es/to-pesos-or-to-posse
 *
 * [2] viewers include people using:
 * - screen, keyboard, mouse and speakers/headphones
 * - touchscreen (laptop, tablet, smartphone) and speakers/headphones
 * - either of the above with captions replacing/augmenting audio
 * - screenreader (esp. tab/gesture navigation & aria) and speakers/headphones
 *
 */

/**
 * Specifications:
 *
 * - build iteratively and collaboratively (emphatically not a project where
 *   'you build what you see')
 * - use core whenever it meets the requirement; if a design/story needs
 *   adjustment to use core, make the adjustment in collaboration with
 *   developer; examples:
 *   - core layouts vs paragraphs
 *   - core media with oembed vs contrib, or directly including youtube iframe.
 *
 * - contribute drupalladder distribution
 * - version content with git and include in drupal.org repository
 * - CI, including ongoing applicability of content
 * - theme components (pref. patternlab) & styleguide
 */
