Feature: interact with ladder
As a blog author
I can include a ladder
So that readers can follow steps using a widget on my site

As a blog reader
(not mvp) I can save my interactions with a ladder
So that the blog author can follow what I have done and where I get stuck

As a blog author
I can create a variation on a ladder for a specific reader
So that we can establish a mentorship and I can give individualized assistance

Feature: authorize a ladder
As a ladder author who is trusted (like the feature on Drupal.org)
I can select any ladder to include in my blog
