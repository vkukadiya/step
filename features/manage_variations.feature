Feature: Ladders show popular variation by default
  As a visitor seeing a Ladder for the first time
  I can review the most popular version by default
  So that I can start with the most broadly adopted approach

Feature: Ladders serve as consistent references
  As a visitor returning to a Ladder with which I've previously interacted
  I can continue to see it in the same state as I last saw it
  So that I can use the Ladder as a reference whenever I need, and if something suddenly no longer works I do not need to wonder if the instruction quality degraded

Feature: Ladders & Steps indicate when they have available variations
  As a (first time or returning) visitor to a Ladder
  I can see when someone has created a variation
  So that I can determine whether to adopt the new variation for future reference or keep my current variation

Feature: List some of the criteria people use to determine which ‘variation’ gets their attention on stack overflow
  highly voted
  up to date
  complete buy brief

Feature: List some diff semantics
  colors indicate changes (this diff approach seems to have 4 colors; do we need more than 2?)

Feature: the core purpose for showing variations
- As a reader of a ladder
  reader can see everything that has changed in any given variation (including things that are in a collapsed parent, differences in order, etc.) 
  show a single ladder variation that contains many changes of many types (say more than 6, maybe even 20 additions/removals of entire steps with children, change of order within child)
  So that reader can understand how individual changes affect the entire ladder

- As a reader of a ladder
  Reader can see everything that has changed in any given variation
  So that Reader can understand how individual changes affect the entire ladder.

Feature: what we currently have is acceptable for the short term
- As a reader of a ladder
  Reader can see the specific words or steps that have changed
Benefit: So that reader can quickly compare differences


List some of the criteria people use to determine which ‘variation’ gets their attention on stack overflow
- highly voted
- up to date
- complete buy brief

List some diff semantics
- colors indicate changes (this diff approach seems to have 4 colors; do we need more than 2?)

As a reader of a ladder
I can see everything that has changed in any given variation (including things that are in a collapsed parent, differences in order, etc.) << Kay’s suggestion: show a single ladder variation that contains many changes of many types (say more than 6, maybe even 20 --additions/removals of entire steps with children, change of order within child ...)
So that I can understand how individual changes affect the entire ladder

As a reader of a ladder
I can see everything that has changed in any given variation
So that I can understand how individual changes affect the entire ladder

Who: As a reader of a ladder
Action: I can see the specific words or steps that have changed
Benefit: So that I can quickly compare differences

First scenario in a sequence of scenarios:
With a ladder with 5 steps
A user changes the 2nd step
All 5 steps are cloned with the user as author

Second:
Another user wants to adopt that change
The entire ladder becomes their preferred variation

Third:
Several users make various changes to all 5 steps
A new user reviews all changes and wants a mix of the variations
The user can adopt author ABC’s changes to steps 1 & 3, and I want author DEF’s change to steps 4&5? (mvp)

Fourth:
Several users make various changes to the 1st step
A new user reviews them and wants title from author ABC and video from author DEF (not mvp)

- proposed test: Feature: variations are non-destructive
      - proposed test: Feature: users can traverse relationships in any direction (revisions <-> originals, children <->)
      - proposed test: Feature: users can see ‘familiar’ variations first
        - I see a variation that I’ve seen before
      - proposed test: Feature: users can see ‘unfamiliar’ variations when
        - it’s popular (‘likes’ as with stackoverflow quality assessment votes, frequently visited)
        - it’s by the original author (reasonable? Not sure)
        - it’s recent
/*
  bug - variation unexpectedly replaces what I used before
  As a returning visitor to a Ladder
  I can no longer follow the instructions and have the outcomes I had before, even though nothing has changed ouside the instructions
  So that I believe the instruction quality degraded
*/
