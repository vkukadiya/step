/*
see also Feature: reuse Ladder steps in ./stepladder_video.feature
*/

Feature: build a long-term plan as a ladder

As a builder of extended plans (creating a ladder of ladders, of sorts)
I can see existing content related to words and tags from the content I am creating
So that I can include existing steps that have been shown help users succeed
(Example of related functionality: on stackexchange, titles of existing questions appear as you type the subject of your own question; on StepLadder the proposal is to populate a pool of likely steps and tags the same way)

Feature: discover related content and tags

As a builder of extended plans
I can add a step and see related tags and steps
So that I have a way to discover existing content to reuse

/**
 * discussion
 *
 * notions about tags:
 * as the author creates steps and adds tags to the steps, popular tags should appear by default
 * each interaction
 *
 * notions about steps:
 *
 * data that may help delineate context:
 * capture indications that a user has completed a step (e.g. simple confirmations
 * like asking whether a Ladder was helpful; noting whether users move on to a
 * dependent step; noting how many Ladders include a step; noting how often a
 * step is visited and/or updated)
*/

as a visitor landing on my first ladder
I can select relevant tags
so that the ladder is reduced
--
as a ladder follower looking through the steps for the first time
I can watch all steps play straight through without further interaction from me
so that I can fold my arms, sit back and get the whole picture
--
as a ladder follower looking through the steps for the second time
I can watch an individual step on its own as many times as I want
so that I can spend as much time as I need in the granular details of an individual step
--
as a ladder follower
I can play other parts of a video on an individual step that are not part of the captured clip
so that I can take review the video author's presentation more broadly
--
as a content author
I can feel confident that my work will be saved and still be there next time I come
so that I can stay enthusiastic about writing materials
--
as a content archiver
I have a clear process for exporting content
so that variations can be preserved in an external repository to be restored/re-imported/audited in the future as needed

- display modes
      - “jump to” child ladder detail page from nested ladder (and see lists of available parents on that child as way to return to parent detail page from child?)
      - authoring (may need more UX work before ready) 
      - new media sourcing
      - what security considerations do we need to account for to use other video source and to add audio (e.g. podcasts)?

expand story: if a step is ‘acquired’ by a user who is not the author, and someone makes a change, a variation is saved vs a change to the original step

- As a ladder reader
  reader can see all substeps as child steps and never see them in the body
  So that reader can filter according to my personal needs; alter them individually

- As a ladder reader
  reader can see video clips (typically lasting about 5 seconds) that correspond to a specific instruction
  So that reader can filter according to my personal needs; alter them individually

Suggestions: improving the current ladder :
- add steps for installing Drupal locally using Lando
- don’t link to the old version and don’t copy-paste indiscriminately from the old version, so that we can show how the new version is intended to work
