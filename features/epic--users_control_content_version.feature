Epic: Users control their own content versions

Feature: default to popular content versions
  As a user viewing content I have not written or altered
  I first see the most popular version of the content
  So that I increase the likelihood I'm using the best available version of the instructions.

Feature: adopt immutable content
  As a user who has made a change to a piece of content (in doing so I become a 'content adopter')
  I can return to my piece of content in the future and see it again unchanged
  So that I can curate sets of instructions that I can repeat confidently, having confirmed the outcomes they produce.

Feature: adopt a specific version
  As a user
  I can review variations on a piece of content (changes introduced by any user)
  So that I can 'adopt' the version that I find works best.
