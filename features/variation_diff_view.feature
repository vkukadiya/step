Feature: allow users to compare difference between variations.

-- As a ladder follower, I should be having the ability to:
    -- view the ladder variations and compare two variations 
    -- understand the exact differences/changes made between two variations.