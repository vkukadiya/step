Feature: up/down vote proposed improvement to an existing step
  As a viewer of a step that has a proposed improvement,
  I can review the proposal and vote it up or down
  So that
    a) others benefit from my evaluation of the proposal
    b) if I return to refollow the steps in the future, the related Ladder(s) reflect my previous evaluation

Scenario: accept proposed improvement
  Given I am viewing a step that has a proposed improvement
  And I click the 'review alternative' button for that step
  And I vote the proposal up
  Then the upvoted version of the step replaces the previous version in my personal copy of the Ladder
