Feature: improve an existing step
  As a viewer of a Ladder,
  I can improve any step
  So that Ladders reflect my understanding of how tasks should be completed.

Scenario: add revision to a step
  Given I am viewing a step
  And I click the 'edit' button for that step
  And I make changes to the step and click 'save'
  Then a new version of the step is available to other users.
