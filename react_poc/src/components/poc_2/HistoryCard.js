import React, { Component } from "react";
import { Card, CardHeader, CardFooter, CardBody, Input, CardText,Button, UncontrolledButtonDropdown, DropdownMenu, DropdownItem, DropdownToggle } from "reactstrap";

import * as moment from 'moment';

import Collapse, { Panel } from 'rc-collapse';
require('rc-collapse/assets/index.css');

import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

const header = (ele,type) => {

  return (
    <strong className={type}>{ele}</strong>
  )
}

const grid = 8;



export default class HistoryCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isHideVideo: true,
      base:this.props.base,
      data:this.props.data,
    };
    this.temp = null
    this.items=[];
  }

  clickCheckBox=(event,item)=>{
    this.setState({[item.stepId]: !this.state[item.stepId],item:item})
    
    // if(item.hasOwnProperty('children') && item.children !== undefined && item.children.length >0){
    //   item.children.forEach(element => {
    //     if(!this.state[item.stepId] ==  true){
    //       this.setState({[element.stepId] :true})
    //       this.items.push(element)
    //     } 
    //   });
    // } 
    // if(!this.state[item.stepId] ==  true){
    //   this.items.push(item);
    // }else if(!this.state[item.stepId] ==  false){
    //    this.items.filter(element => {
    //     if (element.stepId == item.stepId) {
    //       delete this.items[element];
    //     }
    //   });
    // }
    // this.items=this.remove_duplicates(this.items);
  }

  remove_duplicates=(arr)=>{
    let s = new Set(arr);
    let it = s.values();
    return Array.from(it);
  }

    id2List = {
      droppable: 'items',
      droppable2: 'selected'
    };

  getList = id => this.state[this.id2List[id]];

  onDragEnd=(result)=>{
    console.log("result child",result)
  }

  onDragEndd=(res)=>{
    console.log("res",res)
  }

  dispPanel = (ele) => {
    let css='';
      if(ele instanceof Array){
        return(
          ele.map((item,i)=>(
        
            <Droppable droppableId={this.state.base == true ? "base"+item.id:"variation"+item.id+this.props.dropId}>
            {(provided, snapshot) => (
                <div
                    ref={provided.innerRef}
                    // style={getListStyle(snapshot.isDraggingOver)}
                    >
            {this.state.base ==  false && (item.deleted == true || item.added == true || item.updateAdd ==  true || item.updateDelete == true) || this.state.base == true?
           
            <Draggable
              key={item.id}
              draggableId={item.id}
              index={i}>
              {(provided, snapshot) => (
                  <div
                      ref={provided.innerRef}
                      {...provided.draggableProps}
                      {...provided.dragHandleProps}
                      // style={getItemStyle(
                      //     snapshot.isDragging,
                      //     provided.draggableProps.style
                      // 
                      >
                      {this.props.base == false || (this.props.base == true && (item.deleted == false || item.deleted == undefined))?
            
                        <Collapse>
                          {/* {this.state.base ==  false && (item.deleted == true || item.added == true || item.updateAdd ==  true || item.updateDelete == true) ?<Input type="checkbox" checked={this.state[item.stepId]} onChange={(event)=>this.clickCheckBox(event,item)}/>:''}  */}
                          <Panel 
                            className={
                              this.state.base == false ?
                              item.updateAdd || item.added?'card-inner-pannel addedline': item.updateDelete || item.deleted?'card-inner-pannel deletedline':"card-inner-pannel":''
                            }
                            key={i}
                            header={header(item.title,
                            css= this.state.base == false ? item.updateAdd || item.added?'diffadd': item.updateDelete || item.deleted?'diffchange':" ":''
                          )}>
                              {item.children? item.children !== undefined ?
                                // <Collapse>
                                <DragDropContext onDragEnd={this.onDragEnd}>
                                {this.dispPanel(item.children)}
                                 </DragDropContext> 
                                // </Collapse>
                                :'':''}
                            </Panel>
                        </Collapse>
                        :''
                      }
                  </div>
              )}
            </Draggable>
          :
          this.props.base == false || (this.props.base == true && (item.deleted == false || item.deleted == undefined))?
            <Collapse>
              {this.state.base ==  false && (item.deleted == true || item.added == true || item.updateAdd ==  true || item.updateDelete == true) ?<Input type="checkbox" checked={this.state[item.stepId]} onChange={(event)=>this.clickCheckBox(event,item)}/>:''} 
              <Panel 
                className={
                  item.updateAdd || item.added?'card-inner-pannel addedline': item.updateDelete || item.deleted?'card-inner-pannel deletedline':"card-inner-pannel"
                }
                key={i}
                header={header(item.title,
                css=  item.updateAdd || item.added?'diffadd': item.updateDelete || item.deleted?'diffchange':" "
              )}>
                  {item.children? item.children !== undefined ?
                    
                      this.dispPanel(item.children)
                   
                    :'':''}
                </Panel>
            </Collapse>
            
            :''}
            {provided.placeholder}
                    </div>
                )}
            </Droppable>
        ))
        )
      }
  }

  subElement = (ele) => {
    let temp = false
    if (ele.hasOwnProperty('children')) {
      ele.children.map((element) => {
        if (element.stepId == JSON.parse(localStorage.getItem("revisionsInfo")).stepId) {
          temp = element
        } else {
          let t = this.subElement(element)
          if(t) {
            temp = t
          }
        }
      })
    }
    return temp
  }

  handleClick=()=>{
    this.props.showLadder();
  }
  onAdoptEntire=(data)=>{
    this.props.adoptEntire(data);
    // this.setState({base:true})
  }

  onAdoptChecked=()=>{
    
    let {item}=this.state
    if(this.state[item.stepId] ==  true){
      this.props.adoptChecked(item);
      this.setState({[item.stepId]: !this.state[item.stepId]})
    } 
  }

  Compare=(data)=>{
    this.props.compare(data);
  }

  render() {
    const { title, backgroundColor, borderColor, data, onSelectCompare, changeVersionValue, version } = this.props;
    return (
      <Card style={{ backgroundColor, borderColor, marginBottom: 10 }}>
        <CardHeader>
           <div className="clearfix" >
           <div className="float-left" onClick={this.handleClick}>
            <strong >{title}</strong>
           </div>
           <div>
            {this.props.base == false?
             <Button className="ml-5" onClick={() =>this.onAdoptEntire(this.state.data)}> Adopt </Button>
            :''}
           
           </div>
          </div> 
        </CardHeader>
        <CardBody id="card-view-body">
        
            {/* <Droppable droppableId={this.props.dropId}>
                {(provided, snapshot) => (
                    <div
                        ref={provided.innerRef}
                        // style={getListStyle(snapshot.isDraggingOver)}
                        > */}
                         {this.dispPanel(this.state.data["children"])}
                        {/* {provided.placeholder}
                    </div>
                )}
            </Droppable> */}
        </CardBody>

        <CardFooter>
          <CardText style={{ float: "left", marginTop: 10 }}>Updated at: {moment.unix(this.props.updatedTime).format("DD/MM/YYYY")}</CardText>
          {this.props.base == false?
             <Button className="ml-5" onClick={() =>this.Compare(this.state.data)}> Compare </Button>
          :''}
        </CardFooter>
      </Card>
    );
  }
}
