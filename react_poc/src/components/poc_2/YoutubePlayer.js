import React from 'react'
import YouTube from "@u-wave/react-youtube";

const YoutubePlayer = ({video, autoplay, startSeconds, endSeconds, onReady, }, ...rest ) => {
  return (
    <div>
       <YouTube
            video={video}
            autoplay={autoplay}
            showRelatedVideos={false}
            startSeconds={startSeconds}
            endSeconds={endSeconds}
            width={"100%"}
            height={350}
            onReady={onReady}
            {...rest}
        />
    </div>
  )
}

export default YoutubePlayer;
