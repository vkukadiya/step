import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

class FormModal extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      loading:''
    }
    
  }

 
  render() {
      const { modal, toggleModal, type, onSubmit, nodeData, isLoading } = this.props;


      return (
      <div>
        <Modal isOpen={modal} toggle={toggleModal} >
          <ModalHeader toggle={toggleModal}>{type} </ModalHeader>
          <ModalBody>
           {type}... Comming soon!!!
          </ModalBody>
          <ModalFooter>
            <Button className="button-edit" onClick={toggleModal}>OK</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default FormModal;