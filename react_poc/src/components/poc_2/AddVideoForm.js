import React, { Component } from "react";
import Request from "../../common/request";
import YouTube from "@u-wave/react-youtube";
import { Form, FormGroup, Label, Input, FormFeedback, Row, Col, Button, FormText } from "reactstrap";
import constant from '../../common/constant';
import moment from 'moment';
import LoginForm from './LoginForm';

const ID = document.getElementById(constant.class_id).getAttribute(constant.data_id);
const USER_ID = document.getElementById(constant.class_id).getAttribute(constant.user_id);;

export default class AddVideoForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: "",
      videotitle:'',
      pauseSeconds: '00:00:00',
      startSeconds: '00:00:00',
      description:"",
      video:false,
      videoUrl: "",
      videoLoaded:false,
      focused: '',
      suggestions: [],
      suggestTags: [],
      isStartGraterEnd: false,
      validate: {
        titleState: "",
        urlState: "",
        startSecondsState: "",
        pauseSecondsState: "",
        codeTitleState:"",
      },
      isValidUrl: false,
      isParent: false,
      isShowSuggetion: false,
      isChangeTitle: true,
      tags: [],
      isSubmitted: false,
      code_snippet: [
        {
          code_snippet: ""
        }
      ],
      code_Text: [
        {
          value: ""
        }
      ],
      codeValue: "",
      codeTitle:"",
      codeText:"",
      images: [],
      editor: null,
      autoplay:true,
      isPlaying: false,
      isPlay:false,
      showError:false,
      playButtonClick:false
    };
    this.submitForm = this.submitForm.bind(this);
  }

  request = new Request();

  componentWillMount() {
    this.request.getTags().then(res => {
      this.setState({ suggestTags: res })
    }).catch(err => {
    })
  }

  componentWillReceiveProps(nextProp) {
    if (nextProp.type == 'edit') {
      this.setState({
        pauseSeconds: this.secondsToTime(nextProp.nodeData.pauseSeconds).value,
        startSeconds: this.secondsToTime(nextProp.nodeData.startSeconds).value,
        videoUrl: ((nextProp.nodeData.videoId)&& (nextProp.nodeData.videoId.youtubeId == '' || nextProp.nodeData.videoId.youtubeId == null)) ? '' :(nextProp.nodeData.videoId)? `https://www.youtube.com/watch?v=${nextProp.nodeData.videoId.youtubeId}`:'',
        tags: nextProp.nodeData.tags,
        nodeData: nextProp.nodeData,
        videoLoaded :((nextProp.nodeData.videoId)&&(nextProp.nodeData.videoId.youtubeId !== '' || nextProp.nodeData.videoId.youtubeId !== null))  ? true : false,
      })
      if(nextProp.nodeData.videoId){
        this.checkVideoUrl(`https://www.youtube.com/watch?v=${nextProp.nodeData.videoId.youtubeId}`)
      }
      
      this.forceUpdate()
    } else if (nextProp.type == 'add' && this.props.type == 'edit') {
      this.setState({
        title: "",
        pauseSeconds: '00:00:00',
        startSeconds: '00:00:00',
        videoUrl: "",
        focused: '',
        suggestions: [],
        suggestTags: [],
        isStartGraterEnd: false,
        validate: {
          titleState: "",
          urlState: "",
          startSecondsState: "",
          pauseSecondsState: ""
        },
        isValidUrl: false,
        isParent: false,
        isShowSuggetion: false,
        isChangeTitle: true,
        tags: [],
        nodeData: {},
        images: [],
        code_snippet: [{
          code_snippet: ''
        }],
        codeValue: '',
        codeText:''
      })
    }

  }

  componentDidMount() {
    if (this.props.type == 'edit') {
      this.setState({
        pauseSeconds: this.secondsToTime(this.props.nodeData.pauseSeconds).value,
        startSeconds: this.secondsToTime(this.props.nodeData.startSeconds).value,
        videoUrl: ((this.props.nodeData.videoId)&& (this.props.nodeData.videoId.youtubeId == '' || this.props.nodeData.videoId.youtubeId == null)) ? '' :(this.props.nodeData.videoId)? `https://www.youtube.com/watch?v=${this.props.nodeData.videoId.youtubeId}`:'',
        tags: this.props.nodeData.tags,
        nodeData: this.props.nodeData,
      })
      if(this.props.nodeData.videoId){
        this.checkVideoUrl(`https://www.youtube.com/watch?v=${this.props.nodeData.videoId.youtubeId}`)
      } 
    }
  }

  validateForm = () => {
    let isValid = false;
    const { validate } = this.state;

    const NumericRex = /\b(2[0-3]|[01]?[0-9]):([0-5]?[0-9]):([0-5]?[0-9])\b/;

    if (this.state.videoUrl != "") {
      if (this.checkVideoUrl(this.state.videoUrl)) {
        validate.urlState = "has-success";
        this.setState({ isValidUrl: true });
        isValid = true;
      } 
    } else if (this.state.videoUrl == "") {
      validate.urlState = "";
      isValid = true;
    }

    if (this.state.video) {
      if (this.state.startSeconds != "") {
        if (NumericRex.test(this.state.startSeconds)) {
          validate.startSecondsState = "has-success";
          isValid = true;
        } else {
          validate.startSecondsState = "has-danger";
          isValid = false;
        }
      } else {
        validate.startSecondsState = "has-danger";
        isValid = false;
      }
    }

    this.setState({ validate });
    return isValid;
  };

  checkVideoUrl = (url, type = "submit") => {
    const UrlRex = /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})$/;
    const { validate } = this.state;

    if (url !== '') {
        this.setState({ isValidUrl: true });
        if (type == "onChange") {
          validate.urlState = "has-success";
          let that = this;
          setTimeout(function () {
            that.loadVideo(that.state.event);
          }, 3000);
       
        return true;
      }
    }else if(url == ''){
      this.setState({video:false,pauseSeconds: '00:00:00',
      startSeconds: '00:00:00'});
    }
  }

  handleChange = async event => {
    const { target } = event;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const { name } = target;
    await this.setState({
      [name]: value
    });
  };

  secondsToTime = (sec) => {
    if (sec !== undefined) {
      var hours = Math.floor(sec / 3600);
      (hours >= 1) ? sec = sec - (hours * 3600) : hours = '00';
      var min = Math.floor(sec / 60);
      (min >= 1) ? sec = sec - (min * 60) : min = '00';
      (sec < 1) ? sec = '00' : void 0;

      (min.toString().length == 1) ? min = '0' + min : void 0;
      (sec.toString().length == 1) ? sec = '0' + sec : void 0;
      var obj = { time: { h: hours, m: min, s: sec }, value: `${hours}:${min}:${sec}` };
      return obj;
    } else {
      var obj = { time: { h: '00', m: '00', s: '00' }, value: `${'00'}:${'00'}:${'00'}` };
      return obj;
    }
  }

  submitForm(e) {
    e.preventDefault();
    let pId = this.props.nodeData.parent == ID && (this.props.type == 'add')  ? true :false;
    if (this.validateForm() && !this.props.isLoading) {
      const { title, pauseSeconds, startSeconds, videoUrl, tags, isParent, isValidUrl,description, code_snippet, images,code_Text, codeText, codeTitle, codeValue,videotitle } = this.state;
      const { onSubmit, type } = this.props;     
      let data = {}
      if (type == 'edit') {
        data = this.state.nodeData
        if(this.state.video){
          data.pauseSeconds = parseInt(moment.duration(pauseSeconds).asSeconds())
          data.startSeconds = parseInt(moment.duration(startSeconds).asSeconds())
        }
        if (isValidUrl && this.state.videoUrl !== "") {
          if(!data.videoId){
            data.videoId={}
          }
          data.videoId.youtubeId = this.getYoutubeID(videoUrl)
          data.videoUrl = videoUrl
          this.state.event.target.pauseVideo();
        } else {
          data.videoId = { id: null, youtubeId: null }
        }
        data.haveVideoUrl = isValidUrl;
        data.videoTitle=videotitle
      } else {
        if(this.state.video && this.state.videoUrl !== ""){
          data.pauseSeconds = parseInt(moment.duration(pauseSeconds).asSeconds())
          data.startSeconds = parseInt(moment.duration(startSeconds).asSeconds())
        }
        if (isValidUrl && this.state.videoUrl !== "") {
          data.videoId = { youtubeId: this.getYoutubeID(videoUrl) };
          data.videoUrl = videoUrl;
          this.state.event.target.pauseVideo();
        }
        data.dislikes = 0;
        data.is_reviewed = false;
        data.like = [];
        data.likes = 0;
        data.user_id = USER_ID;
        data.vote_click = null;
        data.vote_status = false;
        data.haveVideoUrl = isValidUrl;
        data.videoTitle=videotitle
        data.isParent = this.props.nodeData.parent == 0?true:isParent
        data.isSubmitted = true
      }
      onSubmit(data, type,pId)
    } else {
    }
  }

  handleDelete(i) {
    const tags = this.state.tags.slice(0);
    tags.splice(i, 1);
    this.setState({ tags });
  }

  handleAddition(tag) {
    if (this.state.suggestTags.find(item => tag.name == item.name) != undefined && this.state.tags.find(item => tag.name == item.name) == undefined) {
      let tags = [].concat(this.state.tags, tag);
      this.setState({ tags });
    } else if (this.state.tags.find(item => tag.name == item.name) == undefined) {
      let tags = [].concat(this.state.tags, tag);
      this.setState({ tags });
    }
  }

  onPlayerStateChange = (event) => {
    this.setState({ event });
    this.setState({ videotitle: event.target.getVideoData().title})
    if (event.target.getPlayerState() == 1) {
      this.setState({ isPlay: true, isPlaying: true });
    }
    if (this.props.type != 'edit' && this.state.isChangeTitle) {
      if (event.target.getPlayerState() == 1 && this.state.focused == "") {
        this.setState({ videotitle: event.target.getVideoData().title, startSeconds: "00:00:00",
         pauseSeconds: this.secondsToTime(Math.floor(event.target.getDuration() - 5)).value })
      }

      let time = Math.floor(this.state.event.target.getCurrentTime());

      if (this.state.focused == "start" && this.state.playButtonClick !== true) {
        this.setState({ startSeconds: this.secondsToTime(time).value });
      } else if (this.state.focused == "end" && this.state.playButtonClick !== true) {
        this.setState({ pauseSeconds: this.secondsToTime(time).value });
      }
    }
  }

  play=async ()=>{
    this.setState({playButtonClick:true})
        let video=this.getYoutubeID(this.state.videoUrl);
        let that = this;
        let interval = await setInterval(function() {
          if (that.state.isReadyVideo) {
            clearInterval(interval);
            that.state.event.target.loadVideoById({
              videoId:video,
              startSeconds:moment.duration(that.state.startSeconds).asSeconds(),
              endSeconds: moment.duration(that.state.pauseSeconds).asSeconds()
            });
          }
        }, 1000);
  }

  onPlayerReady = (event) => {
    this.setState({ event, isReadyVideo: true });
    this.loadVideo(event);
  }

  getYoutubeID = (url) => {
    if(url.match(/(^|=|\/)([0-9A-Za-z_-]{11})(\/|&|$|\?|#)/) !== null){
      this.setState({video:true})
      return url.match(/(^|=|\/)([0-9A-Za-z_-]{11})(\/|&|$|\?|#)/)[2]
    }else{
      this.setState({video:false})
      return ""
    }
    
  }

  loadVideo = (event) => {
    if (this.state.isValidUrl && this.state.videoUrl !== "") {
      event.target.loadVideoById({ videoId: this.getYoutubeID(this.state.videoUrl) });
    }
  }

  getCurrentTime = (type) => {
    let time = Math.floor(this.state.event.target.getCurrentTime());
    if (type == "start") {
      this.setState({ focused: "start", startSeconds: this.secondsToTime(time).value });
    } else {
      this.setState({ focused: "end", pauseSeconds: this.secondsToTime(time).value });
    }

  }


  onChange = (event, { newValue }) => {
    this.setState({
      title: newValue
    });
  };

  onSuggestionsFetchRequested = ({ value }) => {
    const inputValue = value.trim().toLowerCase();
    this.request.searchByTitle(inputValue).then(res => {
      this.setState({
        suggestions: res
      });
    }).catch(err => {
      console.log(err)
    })
  };

  // Autosuggest will call this function every time you need to clear suggestions.
  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };

  clickOnTags = (tag) => {
    this.setState({ isValidUrl: false, isShowSuggetion: true, })
  }

  onSuggestionSelected = (event, { suggestion, suggestionValue, suggestionIndex, sectionIndex, method }) => {
    this.setState({
      title: suggestion.title,
      videoUrl: suggestion.field_media_oembed_video,
      isValidUrl: true,
      tags: suggestion.field_tags_export.filter(item => { item.name = item.title; return true }),
      suggestions: [],
      isChangeTitle: false
    })
    this.state.event.target.loadVideoById({ videoId: this.getYoutubeID(suggestion.youtubeId) });

  };

  gallery = (event) => {

    if (event.target.files) {
      let images = [];
      [].forEach.call(event.target.files, (file) => {
        if (/\.(jpe?g|png|gif)$/i.test(file.name)) {
          var reader = new FileReader();
          reader.addEventListener("load", function () {
            images.push({
              name: file.name,
              url: reader.result
            });
          }, false);
          reader.readAsDataURL(file);
        }
      });
      this.setState({
        images: images
      })
    }

  }

 
  render() {
    const { title, suggestions } = this.state;
    let { codeValue,codeText } = this.state;
    if (codeValue == 0 || codeValue == null) {
      codeValue = "";
    }

    if(codeText == 0 || codeText ==  null){
      codeText="";
    }
    const inputProps = {
      placeholder: "Video Title",
      value: title,
      onChange: this.onChange
    };

    let loading = localStorage.getItem('ajs_token') === undefined ? "false" : localStorage.getItem("ajs_token") === "false" ? "false" : "true";

    if(loading =='false'){
      return (
        <LoginForm />
      )
    }

    return (
      <div>
        <div className="container">
          <div className="row">
            <div className="col-md-12 instruction">
              <Form className="form" onSubmit={e => this.submitForm(e)}>
                {(this.props.nodeData.parent == ID && (this.props.type == 'add'))  && (
                  <FormGroup row>
                    <Label for="isParent" sm={4}>
                      Is Parent
                    </Label>
                    <Col sm={8}>
                      <Input
                        type="checkbox"
                        bsSize="sm"
                        name="isParent"
                        id="is-Parent"
                        onChange={e => {
                          this.setState({
                            isParent: !this.state.isParent
                          });
                        }}
                        style={{marginLeft:"-44%"}}
                      />
                    </Col>
                  </FormGroup>
                )}
                <FormGroup row>
                  <Label for="Url" sm={4}>
                    Video URL
                  </Label>
                  <Col sm={8}>
                    <Input
                     className="video-form"
                      type="text"
                      name="videoUrl"
                      id="video-url"
                      placeholder="Video URL"
                      value={this.state.videoUrl}
                      valid={this.state.videoUrl !== "" ?this.state.validate.urlState === "has-success":''}
                      onChange={e => {
                        this.checkVideoUrl(e.target.value, "onChange");
                        this.handleChange(e);
                      }}
                      style={{borderRadius:"3em"}}
                    />
                  </Col>
                </FormGroup>
                {this.state.videoUrl !== "" && (
                  <YouTube
                    autoplay={false}
                    showRelatedVideos={false}
                    width={'100%'}
                    height={350}
                    startSeconds={parseInt(this.state.startsSeconds)}
                    endSeconds={parseInt(this.state.pauseSeconds)}
                    onReady={event => this.onPlayerReady(event)}
                    onStateChange={event => this.onPlayerStateChange(event)} />
                )}
                {this.state.video && (
                  <div>
                    <FormGroup row>
                      <Label for="StartTime" sm={4}>
                        Video Start time
                  </Label>
                      <Col sm={8}>
                        <Row>
                          <Col sm={!this.state.isReadyVideo ? 12 : 4}>
                            <Input
                              type="text"
                              name="startSeconds"
                              id="video-start-time"
                              placeholder="Seconds"
                              value={this.state.startSeconds}
                              valid={this.state.validate.startSecondsState === "has-success"}
                              invalid={this.state.validate.startSecondsState === "has-danger"}
                              onFocus={e => { this.setState({ focused: "start" }) }}
                              onChange={e => {
                                this.handleChange(e);
                              }}
                            />
                            <FormFeedback>Please enter time in only HH:MM:SS format.</FormFeedback>
                          </Col>
                          {this.state.isReadyVideo && (

                            <Col sm={7}>
                              <Button onClick={() => this.getCurrentTime("start")}>Capture current time</Button>
                            </Col>
                          )}
                        </Row>
                      </Col>
                    </FormGroup>

                    <FormGroup row>
                      <Label for="EndTime" sm={4}>
                        Video End time
                  </Label>
                      <Col sm={8}>
                        <Row>
                          <Col sm={!this.state.isReadyVideo ? 12 : 5}>
                            <Input
                              type="text"
                              name="pauseSeconds"
                              id="video-start-time"
                              placeholder="Seconds"
                              value={this.state.pauseSeconds}
                              valid={this.state.validate.pauseSecondsState === "has-success"}
                              invalid={this.state.validate.pauseSecondsState === "has-danger"}
                              onFocus={e => { this.setState({ focused: "end" }) }}
                              //onBlur={e => {this.setState({focused:""})}}
                              onChange={e => {
                                this.handleChange(e);
                              }}
                            />
                          </Col>
                          {this.state.isReadyVideo && (
                            <Col sm={7}>
                              <Button onClick={() => this.getCurrentTime("end")}>Capture current time</Button>
                            </Col>
                          )}
                        </Row>
                      </Col>
                    </FormGroup>
                    {this.state.isReadyVideo && (
                    <div>
                      <Row>
                      <Col sm={5}>
                      </Col>
                        <Col sm={7} className="mb-3">
                        <Button onClick={() => this.play()}>Play</Button>
                        </Col>
                      </Row>
                    </div>
                    )}
                  </div>
                  
                )}
                <FormGroup row>
                  <Label for="Submit" sm={4}></Label>
                  <Col sm={8}>
                    <Button color={this.props.isLoading ? "danger" : "primary"} block >{this.props.isLoading ? "Please wait" : "Submit"}</Button>
                  </Col>
                </FormGroup>
              </Form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
