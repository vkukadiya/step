import React, { Component } from "react";;
import constant from '../../common/constant';
import config from '../../common/config';

const ID = document.getElementById("react-step-ladders").getAttribute("data-nid");
const URL = document.getElementById(constant.class_id).getAttribute(constant.data_url);

export default class LoginForm extends Component {
 
  getApiUrl = ()=>{
    if(URL=='http://localhost'){
      return config.API_URL
    }else{
      return URL
    }
  }

  render() {


    return (
      
        <div className="container">
            <h4>Thanks for the feedback!</h4><br/>
            <h6>Setting up an account is free and open to everyone. Set up your account today and earn extra privileges like posting, commenting and voting.</h6><br/>
             <a href={`${this.getApiUrl()}user/login`} style={{textDecoration:'none'}} >
             <b>Login</b></a> or
             <a href={`${this.getApiUrl()}user/register`} style={{textDecoration:'none'}} > <b> Register</b></a><br/>
      </div>
    );
  }
}