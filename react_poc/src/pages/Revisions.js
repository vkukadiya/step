import React, { Component } from "react";
import "react-toggle/style.css";
import { Collapse, Navbar, Form, FormGroup, Label, Input, NavbarBrand, Nav, NavItem, Button, DropdownToggle, DropdownMenu, DropdownItem,UncontrolledButtonDropdown } from "reactstrap";
import Request from "../common/request";
import HistoryCard from "../components/poc_2/HistoryCard";
import Pagination from "react-js-pagination";
import * as moment from 'moment';
import {baseLadder,variationLadders} from '../common/Dummy'
 
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import Compare from "./Comapre";

const reorder = (list, startIndex, endIndex) => {
    console.log("start",startIndex,endIndex)
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
};



export default class Revisions extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            dropdownOpen: false,
            allItems:this.props.Items,
            activePage: 1,
            isCheckedCompare: false,
            isCheckedPopular: false,
            isCheckedRecent: false,
            dropdownText: 'Change version',
            dropdownOpen: false,
            mode:'Popular',
            data:[],
            base:[],
            versionName: localStorage.getItem('versionName'),
            compare:false
        };
        this.toggle = this.toggle.bind(this);
        this.version = localStorage.getItem('version');
        this.myItem=[];
        // this.chunkArray(variationLadders,2)
    }
    request = new Request();

    componentDidMount=()=>{
       
    }

    reorder = (list, startIndex, endIndex) => {
        console.log("start",startIndex,endIndex)
        let data=[]
        const result = Array.from(list);
        let array=this.request.treeToArray(result);
        let endEle
       
        array.map((item,i)=>{

            
            if(startIndex.droppableId.substr(4) == item.id){
                if(array[i].parent == "13376"){
                    console.log("ele",endEle)
                    let [removed] = result.splice(startIndex.index, 1);
                    removed["modified_id"]=removed["id"];
                    removed["id"]=Math.random().toString(36).substr(2,8);
                    result.splice(endIndex.index, 0, removed);
                    let {base}=this.state;
                    base["children"]=result;
                    this.setState({base:base})
                    localStorage.setItem("baseItem",JSON.stringify(base))
                }
                else{
                    array=this.request.removeChildKey(array);
                    array[i].parent = result[endIndex.index].parent;
                    array[i]["modified_id"]=array[i]["id"];
                    array[i]["id"]=Math.random().toString(36).substr(2,8);
                    
                    // array=this.request.remove_duplicates(array);
                    this.request.arrayToTree(array).then(res=>{
                        console.log("res",res);
                        // data=res;
                        // console.log("data",data)
                       let {base}=this.state;
                       base["children"]=res;
                       this.setState({base:base})
                       localStorage.setItem("baseItem",JSON.stringify(base))
                    })
                }
            }
        })
    }
        // const [removed] = result.splice(startIndex, 1);
        // result.splice(endIndex, 0, removed);


    move = async (source, destination, droppableSource, droppableDestination) => {
        const sourceClone = Array.from(source);
        const destClone = Array.from(destination);
        const [removed] = sourceClone.splice(droppableSource.index, 1);
        let count = 0;
        let {base}=this.state;
        let ele=source[droppableSource.index]
        let array=this.request.treeToArray(base["children"]);
        await array.map((item,i)=>{
            if(source[droppableSource.index]["stepId"] === item.stepId){
                count++;
                // source[droppableSource.index]["modified_id"]=source[droppableSource.index]["id"]
                ele["modified_id"]=ele["id"];
                ele["id"]=Math.random().toString(36).substr(2,8);
                ele.parent = destClone[droppableDestination.index].parent
                array[i]=ele; 
                
                this.request.arrayToTree(array).then(res=>{
                    base["children"]=res
                   
                    // localStorage.setItem("baseItem",JSON.stringify(base))
                    this.setState({base:base});
                })
            }
        })
        if(count == 0){
            source[droppableSource.index]["modified_id"]=source[droppableSource.index]["id"]
            source[droppableSource.index]["id"]=Math.random().toString(36).substr(2,8);
            destClone.splice(droppableDestination.index, 0, removed);
            base["children"]=destClone;
            this.setState({base:base});
        }
        source[droppableSource.index]["stepId"]
        destClone.splice(droppableDestination.index, 0, removed);
    
        const result = {};
        result[droppableSource.droppableId] = sourceClone;
        result[droppableDestination.droppableId] = destClone;
        
        
        return result;
    };

    onModeChange = mode => {
        this.props.onModeChange(mode);
        this.props.onMyClick();
    };

    componentWillMount() {
        let data=JSON.parse(localStorage.getItem("baseItem"));
        if(data == "" || data == null || data == undefined){
            console.log("data",baseLadder)
            this.setState({base:baseLadder[0]})
        }else{
            this.setState({base:data});
        }
        let items=JSON.parse(localStorage.getItem("Variations"));
        if(items == "" || items == null || items == undefined){
            let variation=variationLadders
            this.setState({data:variation.slice(0,2)});
        }else{
            this.setState({data:items.slice(0,2)});
        }
    }

    handlePageChange = pageNumber => {
        this.setState({ activePage: pageNumber ,data:this.myItem[pageNumber-1]});
    };

    chunkArray = (myArray, chunk_size) => {
        var index = 0;
        var arrayLength = myArray.length;
        var tempArray = [];
        for (index = 0; index < arrayLength; index += chunk_size) {
            var myChunk = myArray.slice(index, index + chunk_size);
            tempArray.push(myChunk);
        }
        this.myItem = tempArray;
        return tempArray;
    };

    createMarkup = html => {
        return { __html: html };
    };

    clickCheckBox = (type) => {
        if(type == "compare"){
            this.setState({ isCheckedCompare: !this.state.isChecked });
        }else if(type == "popular"){
            this.setState({ isCheckedPopular: !this.state.isChecked });
        }else if (type == "recent"){
            this.setState({ isCheckedRecent: !this.state.isChecked });
        }
        
    };

    redirectToCompare = (node) => {
        this.props.history.push({
            pathname: `/compare`,
            state: { detail: node }
        })
    }
    displayLadder=(res)=>{
        this.request.ladderResponse(res).then(response=>{
            this.props.onModeChange("tree");
            this.props.ladder(response,"revision")
        })
    }

    adoptEntire=(data)=>{
        let {base}=this.state
        console.log("data",data)
        base["children"]=data["children"]
        localStorage.setItem("baseItem",JSON.stringify(base))
        this.setState({base:base})
    }

    adoptChecked= async (data)=>{
        let {base}=this.state
        let count=0;
        let array=this.request.treeToArray(base["children"]);
        await array.map((ele,i)=>{
            if(data.stepId == ele.stepId){
                count++;
                if(data.deleted == true){
                    if(array[i].children !== null && array[i].children !== undefined){
                        array[i].children.map((item,j)=>{
                            array.map((element,index)=>{
                                if(item.stepId == element.stepId){
                                    delete array[index]
                                }
                            })
                        })
                        delete array[i]
                        this.request.arrayToTree(array).then(res=>{
                            base["children"]=res
                            localStorage.setItem("baseItem",JSON.stringify(base))
                            this.setState({base:base});
                        })
                    }
                }else{
                    array[i]=data;
                    this.request.arrayToTree(array).then(res=>{
                        base["children"]=res
                        localStorage.setItem("baseItem",JSON.stringify(base))
                        this.setState({base:base});
                    })
                }
            }
        })
        if(count == 0){
            array.push(data);
            this.request.arrayToTree(array).then(res=>{
                base["children"]=res
                localStorage.setItem("baseItem",JSON.stringify(base))
                this.setState({base:base});
            })
        }
    }

    compare=(data)=>{
        console.log("data>>>>>>>>>>",data);
        this.setState({compare:true,compareData:data})
        
    }

    renderComparisonView =()=> {
        const TotalPage = this.myItem.length;
        const { activePage } = this.state;
        return (
            <DragDropContext onDragEnd={this.onDragEnd}>
                <div className="history-wrap">
                    <div className="row card-view">
                    <div className="col card-current-version">
                        <HistoryCard 
                            dropId="base"
                            base={true}
                            data={this.state.base} 
                            title={this.state.base.title} 
                            // showLadder={()=>this.displayLadder(this.state.baseItem)}
                            // onAdopt={()=>this.adoptLadder(this.state.baseItem)}
                            updatedTime={this.state.base.last_updated}
                        />
                    </div>
                    <div className="history-table-wrap">
                    <div  className="table-wrap">
                            {this.state.data.map((ele,i)=>( 
                                <div className="col card-current-version" key={i}>
                                    <HistoryCard 
                                        dropId={i}
                                        base={false}
                                        data={ele} 
                                        title={ele.title} 
                                        showLadder={()=>this.displayLadder(ele)}
                                        onAdopt={()=>this.adoptLadder(ele)}
                                        updatedTime={ele.last_updated}
                                        adoptEntire={this.adoptEntire}
                                        adoptChecked={this.adoptChecked}
                                        compare={this.compare}
                                    />
                                </div>
                                
                            ))}
                        </div>
                        </div>
                    </div>
                    

                    {this.state.data.length == variationLadders.length?
                    '':<Button color="secondary" onClick={this.loadmore}>Load More</Button>
                    }
                </div>
            </DragDropContext>
        );
    };
    toggle() {
        this.setState({
          dropdownOpen: !this.state.dropdownOpen
        });
    }

    loadmore=()=>{
        let {data}=this.state;
        let variation=variationLadders;
        let length=data.length
        let totalItems=variationLadders.length
        if(length+3 <= totalItems){
            this.setState({data:this.state.data.concat(variation.slice(length,length+3))})
        }else{
            this.setState({data:this.state.data.concat(variation.slice(length))})
        }
    }


  sortFunction=(a,b)=>{  

    var dateA =  moment.unix(a[0].last_updated).format("DD/MM/YYYY");
    var dateB = moment.unix(b[0].last_updated).format("DD/MM/YYYY");
        if(dateA < dateB){
            return 1
        }else if(dateA > dateB){
            return -1
        }else if(dateA == dateB){
            return moment.unix(a[0].last_updated).format("HH:mm:ss")<moment.unix(b[0].last_updated).format("HH:mm:ss")?1:-1
        }
    };

    onChangeMode = (mode) => {
        if(mode == "Popular"){
            this.chunkArray(this.state.popularItems,3)
            this.setState({ mode });
        }else if(mode == "Recent"){
            this.chunkArray(this.state.recentItems,3)
            this.setState({ mode });
        }

    };

    adoptLadder=data=>{
        this.props.adopt(data);
    }

    getList=(id)=>{
        console.log("id",id)
        if(id === "base"){
            let data= this.state.base;
            return data["children"]
        }else{
            let ID= id.substring(17)
            let data= this.state.data;
            return data[ID]["children"]
        }
    }

    onDragEnd = result => {
        const { source, destination } = result;

        // dropped outside the list
        // if (!destination) {
        //     return;
        // }
        console.log(">>>>Result",result)

        if (source.droppableId.substring(0,4) === destination.droppableId.substring(0,4) && source.droppableId.substring(0,4) === "base") {
            const items = this.reorder(
                this.getList("base"),
                source,
                destination
            );
                let {base} = this.state;
                // base["children"]=items;
                localStorage.setItem("baseItem",JSON.stringify(base))
                // this.setState({base});
        } else if(destination.droppableId.substring(0,4) === "base"){
            const element=this.move(
                this.getList(source.droppableId),
                this.getList("base"),
                source,
                destination
            );
            // let {base}=this.state;
            // base["children"]=result;
            // console.log("base",base)
            
            
            // base["children"]=result.base;
            // // data[(source.droppableId).substring(9)]["children"]= result[source.droppableId]
            
            // // localStorage.setItem("Variations",JSON.stringify(data))
            // this.setState({
            //     base
            //     // data
            // });
        }
    };

    render() {
        const { isChecked } = this.state;
        // console.log(">>>>>>>>>>render",this.state.base)
        return (
            this.state.compare === false?
            <div>
                <div className="container-fluid tutorial-nav">
                    <Navbar color="dark" dark expand="md">
                        <NavbarBrand href="/"> </NavbarBrand>

                        <Collapse navbar>
                            <span style={{ color: "#FFF", marginLeft: 20, fontSize: 25 }}>{this.props.Title.title}</span>
                            <Nav className="ml-auto" navbar>
                                <Button onClick={() => this.onModeChange("tree")}>Go back </Button>

                                <NavItem />
                            </Nav>
                        </Collapse>
                    </Navbar>
                </div>
                <div className="row">
                    <div className="col-md-4">
                        <div className="checkbox-view">
                            <FormGroup check>
                                <Label check>
                                    <Input size="50" checked={isChecked} onChange={()=>this.clickCheckBox("compare")} type="checkbox" /> Always compare with my version
                                </Label>
                            </FormGroup>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="dropdown-view">
                            <Form inline>
                                <FormGroup row>
                                    <Button color="secondary" onClick={this.changeVersion}>Submit</Button>
                                </FormGroup>
                            </Form>
                        </div>
                    </div>
                    <div className="col-md-4 ">
                        <div className="dropdown-view">
                            <UncontrolledButtonDropdown
                            isOpen={this.state.dropdownOpen}
                            toggle={this.toggle}
                            >
                            <DropdownToggle caret className="button-dropdown"  >
                            {this.state.mode === "Popular" ? 'Popular' : this.state.mode === "Recent" ? "Recent" : ''}
                            {/* Instructional steps Mode */}
                            </DropdownToggle>
                            <DropdownMenu right>
                            {this.state.mode !== "Popular" && (
                            <DropdownItem onClick={() => this.onChangeMode("Popular")} style={{cursor:"pointer"}} >
                            Popular
                            </DropdownItem>
                            )}
                            {this.state.mode !== "Recent" && (
                            <DropdownItem onClick={() => this.onChangeMode("Recent")} style={{cursor:"pointer"}} >
                            Recent
                            </DropdownItem>
                            )}
                            </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </div>
                    </div>
                </div>
                {this.renderComparisonView()}
                </div>:
                this.state.compare === true ?
                 <Compare  data={this.state.compareData} base={this.state.base}/>
                 :''
            
        );
    }
}
