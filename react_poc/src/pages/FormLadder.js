import React from "react";
import { FormGroup, Input, Label, FormFeedback } from "reactstrap";

const FormLadder = (props) => {

    return (
        <div>
            <FormGroup>
                <Label for={`${props.id}title`}>Title</Label>
                <Input
                    type="text"
                    name="title"
                    id={`${props.id}title`}
                    value={props.data.title}
                    onChange={props.onChange}
                    onBlur={props.onBlur}
                    placeholder="Enter ladder title"
                    style={{marginLeft:"0px"}} />
                <FormFeedback>Title filed is required</FormFeedback>
            </FormGroup>
            <FormGroup>
                <Label for={`${props.id}description`}>Description</Label>
                <Input
                    rows="5"
                    type="textarea"
                    name="description"
                    id={`${props.id}description`}
                    value={props.data.description[0].value}
                    onChange={props.onChange}
                    onBlur={props.onBlur}
                    placeholder="Enter ladder description" />
                <FormFeedback>Description filed is required</FormFeedback>
            </FormGroup>
        </div>
    )

}

export default FormLadder;
