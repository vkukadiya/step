import React, { Component } from "react";
import {parseDiff, Diff, Hunk, } from 'react-diff-view';
import {diffLines, formatLines} from 'unidiff';
import 'react-diff-view/style/index.css';
import {  Button} from "reactstrap";


    const EMPTY_HUNKS = [];
    // const oldText = base;
    // const newText = data;
    // const [{type, hunks}, setDiff] = useState('');
    // const updateDiffText = useCallback(() => {
    //     const diffText = formatLines(diffLines(oldText.title, newText.title), {context: 3});
    //     const [diff] = parseDiff(diffText, {nearbySequences: 'zip'});
    //     setDiff(diff)
    // }, [oldText.value, newText.value, setDiff]);

export default class Compare extends Component {
    
    constructor(props){
        super(props);
        this.state={
        oldText:this.props.base,
        newText:this.props.data,
        files:[],
        type:''
        }
    }
    
    
    componentWillMount =() => {
       
        let files=[];
        let old_length=this.state.oldText["children"].length;
        let new_length=this.state.newText["children"].length
        let length = old_length > new_length ?old_length:new_length
        console.log("here",length)
        for(let i=0 ; i <length ;i++ ){
            
            console.log("here",length)
            const diffText = formatLines(diffLines(this.state.newText["children"][i]["title"],this.state.oldText["children"][i]["title"] ), {context: 3});
            const temp = parseDiff(diffText, {nearbySequences: 'zip'});
            
            console.log("temp",temp)
            files.push(temp);
        }
        
        this.setState({files:files})
    }

    render(){
        const{hunks}=this.state;
        console.log("in compare",hunks)
        return (
            <div className="diff-view">
           {/* <Button color="secondary" onClick={this.updateDiffText}>DIFF</Button> */}
           {this.state.files.map((item,i)=>(
            //    item[0]["hunks"].length !== 0 ?
               <Diff viewType="split" diffType={item[0]["type"]}hunks={item[0]["hunks"] || EMPTY_HUNKS}>
               {hunks => hunks.map(hunk => <Hunk key={hunk.content} hunk={hunk} />)}
                </Diff>
           ))}
            
            </div>
        );
    }
};


// export default class Compare extends Component {
    
   
//     constructor(props){
//         super(props);
//         console.log("in constructor")
//     }

//     render(){
//         console.log("in compare")
//         const {data}=this.props
//         return (
//             files.map(renderFile)
//         );

//     }
// }