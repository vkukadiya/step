import React, { Component } from "react";
import ReactDOM from "react-dom";
import "../index.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "../App.css";
import YouTube from "@u-wave/react-youtube";
import Gallery from "react-grid-gallery";
import "react-toggle/style.css";
import Request from "../common/request";
import classnames from "classnames";
import { Spinner } from 'reactstrap';
import {
  FaCheck,
  FaCheckDouble,
  FaEdit,
  FaThumbsUp,
  FaThumbsDown,
} from "react-icons/fa";
import {
  Button,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledButtonDropdown
} from "reactstrap";
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Row,
  Col,
  Input,
} from "reactstrap";

import SortableTree, {
  toggleExpandedForAll,
  addNodeUnderParent,
  removeNodeAtPath,
  getNodeAtPath,
  changeNodeAtPath,
  map as mapTree,
  find,
  getVisibleNodeCount,
  walk,
} from "react-sortable-tree";
import {
  MdEdit,
  MdAdd,
} from "react-icons/md";
import "react-sortable-tree/style.css";
import TutorialMode from "./TutorialMode";
import ExpositoryMode from "./ExpositoryMode";
import LoginForm from "../components/poc_2/LoginForm";
import AddVideoForm from "../components/poc_2/AddVideoForm";
import History from "./History";
import Revisions from "./Revisions";
import AddLadder from "./AddLadder";
import constant from "../common/constant";
import * as moment from 'moment';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import PresentationMode from "./PresentationMode";

const ReactTags = require("react-tag-autocomplete");

const maxDepth = 5;
const MAX_LENGTH = 50;

const USER_ID = document.getElementById(constant.class_id).getAttribute(constant.user_id);
const ID = document.getElementById(constant.class_id).getAttribute(constant.data_id);
export default class POC_2 extends Component {
  stopPlayAt = null;
  stopPlayTimer;
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      event: {},
      queueVideos: [],
      isPlay: false,
      isVideoEnd: false,
      isPlayAll: false,
      activeVideoIndex: 0,
      isPlaying: false,
      isSearch: false,
      items: [],
      tags: [],
      suggestions: [],
      searchText: "",
      isReadyApp: false,
      isHaveVideo: true,
      isReadyVideo: false,
      isLastVideo: false,
      isNoVideo: true,
      isShowModal: false,
      modalType: "",
      searchString: "",
      searchFocusIndex: 0,
      searchFoundCount: null,
      nodeData: {},
      isLoading: false,
      disableDrag: false,
      loadingReviewBtn: false,
      loadingVoteBtn: false,
      reviewed_id: "",
      upVote: 110,
      downVote: 50,
      stepUpVote: 10,
      stepDownVote: 50,
      isVoted: "",
      isStepVoted: "",
      treeTitle: {},
      mode: "tree",
      activeTab: "2",
      name: "CodeMirror",
      code: "",
      dropdownOpen: false,
      viewStatus: "video",
      edit:false,
      ladderTitle:'',
      activeTitle:false,
      titleId:'',
      ladderVideo:{},
      mouseleave:"leave",
      modal: false,
      key:'',
      addLadder: {
        parentID: null,
        children: null
      },
      rowImage:[],
      image_res:'',
      code_snippet_res:'',
      video_res:'',
      defaultTab:'Popular',
      revisions:[],
      loadtype:'',
      description:'',
      type:'',
      editField:false,
      stepId:'',
      codesamples:'',
      helptext:'',
      codetitle:'',
      images:[],
      rowInfo:[],
      suggestTags:[],
      showError:false,
      base:[],
      counter:20,
      manual:false,
      click:''
    };
    this.toggle = this.toggle.bind(this);
  }
  request = new Request();
  activeVideo = {};
  tempQueue = [];
  draggedItem = {};
  dropOnItem = {};
  json=[]
  children={}
  int=''
  componentWillMount() {
    this.request.getTags().then(res => {
      this.setState({ suggestTags: res })
    }).catch(err => {
      console.log("err",err);
    })
  }

  generateJSON=(data)=>{
    let image=[];
    let tag=[];
    if(data.hasOwnProperty("tags")){
      if(data.tags.length !== 0){
        data.tags.forEach(element=>{
          tag.push({"id": element.id})
        })
      }
    }
    
    
    if(data.hasOwnProperty("images")){
      data.images.forEach(item=>{
        image.push({"target_id" :item.mid})
      })
    }
    if(data.hasOwnProperty("children")){
      if(data.children !== undefined && data.children.length !==0){
        data.children.map((child,i)=>{
          let result=this.generateJSON(child)
          data.children[i]=result;
        })
      }
    }
    this.json={
      "id": data.stepId,
      "author": data.author,
      "title": data.title,
      "remote_video":data.videoId? data.videoId.youtubeId !== null && data.videoId.youtubeId !== "" ?
      {
        "id": data.videoId.id !== null && data.videoId.id !== "" && data.videoId.id !== undefined ? data.videoId.id :"",
        "is_new": 1,
        "title": data.videoTitle,
        "url": data.videoUrl
      }:
      data.videoId?data.videoId.id !== null && data.videoId.id !== "" && data.videoId.id !== undefined ?
      {
          "id": data.videoId.id,
          "title": data.videoId.title,
          "url": data.videoId.url
      }:
      []:[]:[],
      "start_time": this.convertSecond(data.startSeconds).replace('NaN',0),
      "end_time": this.convertSecond(data.pauseSeconds).replace('NaN',0),
      "tags":tag,
      "parent": data.parent,
      "description": data.description.length >0 ?data.description[0].value:[],
      "likes": data.likes,
      "code_snippet": data.code_snippet,
      "screen_shots":image,
      "children":data.children,
    }
    return this.json
  }


  subElement = (items, to) => {
    if (items.hasOwnProperty("children")) {
      items.children.map((element, index) => {
        if (element.stepId in to) {
          items.children[index] = to[element.stepId];
        }
        this.subElement(element, to);
      });
    }
    return items;
  };

  onMyClick = () => {
    this._isMounted = true;
    let id = document
      .getElementById("react-step-ladders")
      .getAttribute("data-nid");
    this.request.getVideoData().then(res => {
      if (this._isMounted) {
        this.setState({ patentID: id });
        if (res.error) {
          this.setState({ error: "No data found." });
        } else {
          if(res.tree[0]["children"]!==undefined){
          localStorage.setItem(
            "tempItems",
            JSON.stringify(res.tree[0]["children"])
          );
          this.tempQueue = res.queueVideos;
          localStorage.setItem(
            "mainNode",
            JSON.stringify(res.tree[0])
          );
          this.setState({
            activeVideo: res.tree[0],
            treeTitle: res.tree[0],
            items: res.tree[0]["children"],
            suggestions: res.suggestions,
            queueVideos: res.queueVideos,
            ladderTitle:res.tree[0].title,
            activeTitle:true
          });
          this.activeVideo = res.tree[0];
          this.children=res.tree[0];
         
          
          this.activeVideo.videoId.youtubeId == null
            ? this.setState({ isNoVideo: true })
            : this.setState({ isNoVideo: false });
          const spinner = document.getElementById("spinner");
          this.setState({ isReadyApp: true });
          if (spinner && !spinner.hasAttribute("hidden")) {
            spinner.setAttribute("hidden", "true");
            this.setState({ isReadyApp: true });
          }
          this.setActiveVideo(this.activeVideo);
          }
          else{
            localStorage.setItem(
              "tempItems",
              JSON.stringify(res.tree[0])
            );
            localStorage.setItem(
              "mainNode",
              JSON.stringify(res.tree[0])
            );
            this.tempQueue = res.queueVideos;
          
            this.setState({
              activeVideo: res.tree[0],
              treeTitle: res.tree[0],
              items: [],
              suggestions: res.suggestions,
              queueVideos: res.queueVideos,
              ladderTitle:res.tree[0].title,
              activeTitle:true
            });
            this.activeVideo = res.tree[0];
            this.children=res.tree[0];
            this.setState({ isReadyApp: true });
          }
        }

        if (
          localStorage.getItem("replaceRevisions") != "" &&
          localStorage.getItem("replaceRevisions") != null &&
          localStorage.getItem("replaceRevisions") != "undefined"
        ) {
          let replaceRevisions = JSON.parse(
            localStorage.getItem("replaceRevisions")
          );
          let to = [];
          replaceRevisions.map(element => {
            to[element.stepId] = element;
          });
          this.setState({
            items: this.state.items.map(element => {
              element = this.subElement(element, to);
              if (element.stepId in to) {
                return to[element.stepId];
              }
              return element;
            })
          });
        }
      }
    });
  };

  adoptLadder=(item)=>{
    this.request.doPost('rest-api/ladder_adapt',{
      "id":ID,
      "adapted_ladder_id":item.stepId
    }).then(res=>{
      this.request.ladderResponse(item).then(response=>{
        this.onModeChange("tree");
        this.loadLadder(response,'')
      })
    })
  }

  loadLadder=(res,type)=>{
    this.setState({loadtype:type})
    this._isMounted = true;
    let id = document
      .getElementById("react-step-ladders")
      .getAttribute("data-nid");
      if (this._isMounted) {
        this.setState({ patentID: id });
        if (res.error) {
          this.setState({ error: "No data found." });
        } else {
          if(res.tree[0]["children"]!==undefined){
          localStorage.setItem(
            "tempItems",
            JSON.stringify(res.tree[0]["children"])
          );
          this.tempQueue = res.queueVideos;
          localStorage.setItem(
            "mainNode",
            JSON.stringify(res.tree[0])
          );

          this.setState({
            activeVideo: res.tree[0],
            treeTitle: res.tree[0],
            items: res.tree[0]["children"],
            suggestions: res.suggestions,
            queueVideos: res.queueVideos,
            ladderTitle:res.tree[0].title,
            activeTitle:true
          });
          this.activeVideo = res.tree[0];
          this.children=res.tree[0];
         
          
          this.activeVideo.videoId.youtubeId == null
            ? this.setState({ isNoVideo: true })
            : this.setState({ isNoVideo: false });
          const spinner = document.getElementById("spinner");
          this.setState({ isReadyApp: true });
          if (spinner && !spinner.hasAttribute("hidden")) {
            spinner.setAttribute("hidden", "true");
            this.setState({ isReadyApp: true });
          }
          this.setActiveVideo(this.activeVideo);
          }
          else{
            localStorage.setItem(
              "tempItems",
              JSON.stringify(res.tree[0])
            );
            localStorage.setItem(
              "mainNode",
              JSON.stringify(res.tree[0])
            );
            this.tempQueue = res.queueVideos;
          
            this.setState({
              activeVideo: res.tree[0],
              treeTitle: res.tree[0],
              items: [],
              suggestions: res.suggestions,
              queueVideos: res.queueVideos,
              ladderTitle:res.tree[0].title,
              activeTitle:true
            });
            this.activeVideo = res.tree[0];
            this.children=res.tree[0];
            this.setState({ isReadyApp: true });
          }
        }
        if (
          localStorage.getItem("replaceRevisions") != "" &&
          localStorage.getItem("replaceRevisions") != null &&
          localStorage.getItem("replaceRevisions") != "undefined"
        ) {
          let replaceRevisions = JSON.parse(
            localStorage.getItem("replaceRevisions")
          );
          let to = [];
          replaceRevisions.map(element => {
            to[element.stepId] = element;
          });
          this.setState({
            items: this.state.items.map(element => {
              element = this.subElement(element, to);
              if (element.stepId in to) {
                return to[element.stepId];
              }
              return element;
            })
          });
        }   
      }
  }

  sortFunction=(a,b)=>{  

    var dateA =  moment.unix(a[0].last_updated).format("DD/MM/YYYY");
    var dateB = moment.unix(b[0].last_updated).format("DD/MM/YYYY");
        if(dateA < dateB){
            return 1
        }else if(dateA > dateB){
            return -1
        }else if(dateA == dateB){
            return moment.unix(a[0].last_updated).format("HH:mm:ss")<moment.unix(b[0].last_updated).format("HH:mm:ss")?1:-1
        }
  };

  getRevisions=()=>{
   
      this.setState({mode:'revisions'})
  }

  componentDidMount=()=> {
    localStorage.getItem('ajs_token' === undefined) ? console.log("token not found") : (localStorage.getItem('ajs_token') === "true" && this.state.edit==="true" ? this.setState({disableDrag:false}) : this.setState({disableDrag:true}))
    this.onMyClick();
    
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  toggleTab = tab => {
    this.setState({viewStatus:'video',activeTab:tab})
    if(tab!=="2" && this.state.activeVideo.videoId.youtubeId!=='' && this.state.activeVideo.videoId.youtubeId!==null){
      this.state.event.target.pauseVideo();
    }
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
        viewStatus:'video'
      });
    }
  };

  handleTreeOnChange = items => {
    localStorage.setItem(
      "tempItems",JSON.stringify(items));
    this.setState({ items });
    this.update(items);
  };

  handleSearchOnChange = e => {
    this.setState({
      searchString: e.target.value
    });
  };

  toggleNodeExpansion = expanded => {
    this.setState(prevState => ({
      items: toggleExpandedForAll({
        items: prevState.items,
        expanded
      })
    }));
  };

  onClickRow = data => {
   
    this.setState({manual:true,click:'',activeVideoIndex:0,counter:20})
    clearInterval(this.int);
    let { node, path, treeIndex } = data;
    if(data.node === undefined){
      node=data;
    }
    this.setActiveVideo(node);
    this.setState({ activeVideo: node, viewStatus: "video",activeTitle:false });
    this.removeActiveTitle();
    if(this.state.activeTab !== "1" && node.videoId.youtubeId){
      this.state.event.target.pauseVideo();
    }
    else{     
    if (node.videoId && node.videoId.youtubeId) {
      let that = this;
        let interval = setInterval(function() {
          if (that.state.isReadyVideo) {
            clearInterval(interval);
            that.state.event.target.loadVideoById({
              videoId: node.videoId.youtubeId,
              startSeconds: node.startSeconds,
              endSeconds: node.pauseSeconds
            });
          }
        }, 1000);
    }
  }
  };

  removeActiveTitle = () => {
    this.setState(prevState => ({
      treeTitle: Object.keys(prevState.treeTitle).reduce((object, key) => {
        if (key == "isActive") {
          prevState.treeTitle[key] = false;
        }
        return prevState.treeTitle;
      }, {})
    }));
  };

  playTitle = data => {
    this.removeActiveAll();
    data.isActive = true;
    this.setState({ activeVideo: data, treeTitle: data });
    if (data.videoId.youtubeId) {
      this.state.event.target.loadVideoById({
        videoId: data.videoId.youtubeId,
        startSeconds: data.startSeconds,
        endSeconds: data.pauseSeconds
      });
    }
  };

  onPlayerStateChange = event => {
    this.setState({ event });

    if (event.target.getPlayerState() == 1) {
      this.setState({ isPlay: true, isPlaying: true });
    }
  
    if(event.target.getPlayerState() == 0 && this.state.click === "play all"){
      console.log("for continue");
      this.continueVideo();
    }
    if( this.state.manual){
      this.setState({manual:false})
    }
    var time, rate, remainingTime, continueFrom;
    clearTimeout(this.stopPlayTimer);
    if (
      this.state.activeVideo.pauseSeconds >
      Math.floor(event.target.getDuration())
    ) {
      continueFrom = Math.floor(event.target.getDuration() - 1);
    } else {
      continueFrom = this.state.activeVideo.pauseSeconds;
    }

    // if (event.data === event.target.getPlayerState()) {
      
    //   time = event.target.getCurrentTime();
    //   if (time + 0.4 < continueFrom) {
    //     rate = event.target.getPlaybackRate();
    //     remainingTime = (continueFrom - time) / rate;
    //     console.log("playerrrrrrrrrrr")
    //     this.stopPlayTimer = setTimeout(
    //       this.continueVideo,
    //       remainingTime * 1000
    //     );
    //   }
    // }
  };

  removeActiveAll = () => {
    this.setActiveVideo(this.state.activeVideo, "remove");
  };

  setActiveVideo = (activeVideo, flag = "") => {
    this.setState({title:activeVideo.title})
    let getNodeKey = ({ node: object, treeIndex: number }) => {
      return number;
    };
    let newTree = [];
    walk({
      treeData: this.state.items,
      getNodeKey,
      ignoreCollapsed: false,
      callback: ({ node }) => {
        if (activeVideo.stepId == node.stepId) {
          if (flag == "remove") {
            node.isActive = false;
          } else {
            node.isActive = true;
            this.setState({ activeVideo: node });
          }
          newTree.push(node);
        } else {
          node.isActive = false;
          newTree.push(node);
        }
      }
    });
    this.setNewTreeData(this.state.items);
  };

  setNewTreeData = items => {
    this.request.arrayTarrayToTree = array => {
      return new Promise((resolve, reject) => {
        const tree = arrayToTree(array, {
          parentProperty: "parent",
          childrenProperty: "children",
          customID: "stepId"
        });
        resolve(tree);
      });
    };
    this.request.arrayToTree(items).then(res => {
      this.setState({ items: res });
    });
  };

  continueVideo = () => {
    this.setState({counter:20})
    let currentVideo = this.state.queueVideos[this.state.activeVideoIndex];
    let nextVideo = this.state.queueVideos[this.state.activeVideoIndex + 1];
    let lastVideo = this.state.queueVideos[this.state.queueVideos.length - 1];
    console.log("in continue",this.state.activeVideoIndex);
    if (
      this.state.isPlayAll &&
      this.state.isPlay &&
      !this.isPlaying &&
      !this.state.isLastVideo&& !this.state.manual
    ) {
      this.setState({
        activeVideoIndex: this.state.activeVideoIndex + 1,
        activeVideo: nextVideo
      });

      this.setActiveVideo(nextVideo);

      if (nextVideo.videoId.youtubeId == null || nextVideo.videoId.youtubeId == "" ) {
        this.setState({ isHaveVideo: false, isReadyVideo: false });
        let that = this;
        
          if (nextVideo.stepId == lastVideo.stepId) {
            that.setState({ isLastVideo: true, isPlay: false });
          }else{
            this.setState({click:"no video"})
            this.int=setInterval(() => {
              if(this.state.counter === 0){
                clearInterval(this.int)
                that.continueVideo();
                this.setState({counter:20})
              }else{
                this.setState({counter:this.state.counter-1})
              }
            }, 1000);
            // setTimeout(function() {
            //   that.continueVideo();
            // }, 20000);
          }
          
      } else {
        if(currentVideo.videoId.youtubeId == null || currentVideo.videoId.youtubeId == ""){
          this.setState({click:"play all"})
        }else{
          this.setState({click:''})
        }
        this.setState({ isHaveVideo: true, isNoVideo: false, });
        let that = this;
        let interval = setInterval(async()=> {
          if (that.state.isReadyVideo) {
            clearInterval(interval);
            await that.state.event.target.loadVideoById({
              videoId: nextVideo.videoId.youtubeId,
              startSeconds: nextVideo.startSeconds,
              endSeconds: nextVideo.pauseSeconds
            });
            if (nextVideo.stepId == lastVideo.stepId) {
              this.setState({ isLastVideo: true, isPlay: false,click:"" });
            }
          }
        }, 1000);
        
          if(currentVideo.videoId.youtubeId !== null || currentVideo.videoId.youtubeId !== ""){
            setTimeout(()=>{
            this.setState({click:"play all"})
          },2000)
          }
        
      }
    } else {
      this.state.event.target.stopVideo();
      this.setState({ isPlay: false, isVideoEnd: true });
    }
  };

  onPlayerReady = event => {
    this.setState({ event, isReadyVideo: true });
  };

  onPlayerVideoEnd = event => {
    this.setState({
      isPlay: false,
      isVideoEnd: true,
      isPlaying: false,
      manual:false
    });
  };

  handleDelete=async(i)=> {
    let tag=[];
    tag = this.state.tags.filter(function(ele){
       return ele != i;
   });
    await this.setState({tags:this.state.tags.filter((ele)=>{
       return ele != i;
      
       })})
     this.handleTag(tag);
  }

  handleTag=async tag=>{
    await this.setState({tags:tag})
     this.filterValues(this.state.tags)
   }

  handleAddition=async (tag)=> {
    {this.state.tags.length>0 && this.state.tags.map((ele,i)=>{
      if(ele.name===tag){
        this.handleDelete(ele)
      }
    })}
    let tags = [].concat(this.state.tags, {name:tag});
    tags = tags.filter(
      (elem, index, self) =>
        self.findIndex(t => {
          return t === elem
        }) === index
    )
    this.handleTag(tags);
  }
  

  update=(item)=>{
    let tempItems=this.request.treeToArray(JSON.parse(localStorage.getItem("tempItems")));
    let count=0;
    let items=this.request.treeToArray(item);
    let length=items.length;
    items.map((element,i)=>{
      count++;
      tempItems.map((temp,j)=>{
        if(element.stepId == temp.stepId){
          temp.expanded=element.expanded
        }
      })
      if(count == length){
        this.request.arrayToTree(tempItems).then(res=>{
          localStorage.setItem(
            "tempItems",JSON.stringify(res));
        }) 
      }
    })
  }

  filterValues = (tags = this.state.tags, text = this.state.searchText) => {
    
    this.request
    .filterByTags(tags, text, this.state.items)
      // .filterByTags(tags, text, JSON.parse(localStorage.getItem("tempItems")))
      .then(trees => {
        if (this.state.tags.length > 0 || this.state.searchText.length > 0) {
          this.setState({ items: trees });
        } else {
          this.setState({
            isSearch: true,
            items: JSON.parse(localStorage.getItem("tempItems"))
          });
        }
      });
  };


  handleChange(event) {
    this.setState({ searchText: event.target.value, isSearch: true });
    this.filterValues(this.state.tags, event.target.value);
  }

  ladderVideos = async () =>{
    this.removeActiveAll();
    this.setState({activeVideo:this.state.treeTitle,activeTitle:true});
    if (this.state.treeTitle.videoId.youtubeId === "") {
      this.setState({
        isHaveVideo: false,
        isReadyVideo: false,
        viewStatus: "video"
      });
    } 
    else {
      this.setState({
        isHaveVideo: true,
        isNoVideo: false,
        viewStatus: "video"
      });
      let that = this;
      this.setActiveVideo(that.state.treeTitle);
      this.setState({ activeVideo: that.state.treeTitle, viewStatus: 'video',isReadyVideo:true });
      this.removeActiveTitle();
      
  
      if (that.state.treeTitle.videoId.youtubeId) {
        this.state.event.target.loadVideoById({
          videoId: that.state.treeTitle.videoId.youtubeId,
          startSeconds: that.state.treeTitle.startSeconds,
          endSeconds: that.state.treeTitle.pauseSeconds
        });
        that.state.event.target.playVideo();
              that.setState({ isPlay: false, viewStatus: "video" });
      }
    }
  }

  onClickTree=(node,key)=>{
    if (node.videoId.youtubeId) {
      let that = this;
        let interval = setInterval(function() {
          if (that.state.isReadyVideo) {
            clearInterval(interval);
            that.state.event.target.loadVideoById({
              videoId: node.videoId.youtubeId,
              startSeconds: node.startSeconds,
              endSeconds: node.pauseSeconds
            });
          }
        }, 1000);
    }
  }

  onClickPlay = async () => {
    await this.toggleTab("1");
    if (!this.state.isPlay) {
      if (this.state.isVideoEnd) {
        if (this.state.activeVideo.videoId.youtubeId == null) {
          this.setState({
            isHaveVideo: false,
            isReadyVideo: false,
            viewStatus: "video"
          });
        } else {
          this.setState({
            isHaveVideo: true,
            isNoVideo: false,
            viewStatus: "video"
          });
        
          let that = this;
          let interval = await setInterval(function() {
            if (that.state.isReadyVideo) {
              clearInterval(interval);
              that.state.event.target.loadVideoById({
                videoId: that.state.activeVideo.videoId.youtubeId,
                startSeconds: that.state.activeVideo.startSeconds,
                endSeconds: that.state.activeVideo.pauseSeconds
              });
            }
          }, 1000);
        }
        this.setState({ isPlay: true, viewStatus: "video" });
      } else {
        if (this.state.activeVideo.videoId.youtubeId == null) {
          this.setState({
            isHaveVideo: false,
            isReadyVideo: false,
            viewStatus: "video"
          });
        } else {
          await this.setState({
            isHaveVideo: true,
            isNoVideo: false,
            viewStatus: "video"
          });
          let that = this;
          let interval = await setInterval(function() {
            if (that.state.isReadyVideo) {
              clearInterval(interval);
              that.state.event.target.playVideo();
              that.setState({ isPlay: false, viewStatus: "video" });
            }
          }, 1000);
        }
      }
    } else {
      await this.setState({ isPlay: false, viewStatus: "video" });
      let that = this;
      let interval = await setInterval(function() {
        if (that.state.isReadyVideo) {
          clearInterval(interval);
          that.state.event.target.pauseVideo();
          that.setState({ isPlay: false, viewStatus: "video" });
        }
      }, 1000);
    }
  };

  onClickPlayAll = async () => {
    console.log("in on click",this.state.click == "")
    
    await this.toggleTab("1");
   
    if(!this.state.manual){
    
      if(this.state.isPlay && this.state.click == "no video" ){
        console.log("here1")
        this.setState({click:"play all"})
        let that = this;
        that.continueVideo();
      }
      else if(!this.state.isPlay && this.state.click == "pause"){
        console.log("here2")
          this.setState({click:"play all"})
        let that = this;
        let interval = await setInterval(function() {
          if (that.state.isReadyVideo) {
            clearInterval(interval);
            that.state.event.target.playVideo();
            that.setState({ isPlay: false, viewStatus: "video" });
          }
        }, 1000);
      }
      else if (!this.state.isPlay || this.state.click == '') {
        console.log("here3")
        let queueVideos = this.request.treeToArray(this.state.items);
        console.log("queue",queueVideos)
        this.setState(prevState => ({
          items: toggleExpandedForAll({
            treeData: prevState.items,
            expanded: true,
            viewStatus: "video"
          })
        }));
        this.setState(
          { queueVideos, isPlayAll: true, viewStatus: "video" },
          async () => {
            
            let nextVideo = this.state.queueVideos[0];
            this.setState({
              isPlay: true,
              activeVideo: nextVideo,
              isLastVideo: false,
              viewStatus: "video",
            });
  
            if (nextVideo.videoId.youtubeId == null ||nextVideo.videoId.youtubeId == "") {
              this.setState({
                isHaveVideo: false,
                isReadyVideo: false,
                viewStatus: "video",
                click:"no video"
              });
              this.int=setInterval(() => {
                if(this.state.counter === 0){
                  clearInterval(this.int)
                  let that = this;
                  that.continueVideo();
                  this.setState({counter:20})
                }else{
                  this.setState({counter:this.state.counter-1})
                }
                
              }, 1000);
             
              // setTimeout(function() {
              //   that.continueVideo();
              // }, 20000);
            } else {
              this.setState({
                isHaveVideo: true,
                isNoVideo: false,
                viewStatus: "video",
                isPlay:true,
              });
              let that = this;
              let interval =await setInterval(async ()=> {
                
                if (that.state.isReadyVideo) {
                  clearInterval(interval);
                  await that.state.event.target.loadVideoById({
                    videoId: nextVideo.videoId.youtubeId,
                    startSeconds: nextVideo.startSeconds,
                    endSeconds: nextVideo.pauseSeconds
                  });
                }
                setTimeout(()=>{
                  that.setState({ click:"play all"})
                },2000)
              }, 1000); 
                 
            }
            this.setActiveVideo(nextVideo);
          }
        );
        
      } else {
        console.log("here4")
        await this.setState({ isPlay: false, viewStatus: "video" ,click:"pause"});
        let that = this;
        let interval = await setInterval(function() {
          if (that.state.isReadyVideo) {
            clearInterval(interval);
            that.state.event.target.pauseVideo();
            that.setState({ isPlay: false, viewStatus: "video" });
          }
        }, 1000);
      }
    }
   
  };

  onClickRowButton = (rowInfo, type) => {
      let node,path,treeIndex;
      if(rowInfo.node){
        node=rowInfo.node
        path=rowInfo.path
        treeIndex=rowInfo.treeIndex
        this.setState({rowImage:rowInfo.node.images});
      }else{
        node=rowInfo
        this.setState({rowImage:rowInfo.images});
      }
   
    if (type == "revisions") {
      localStorage.setItem(
        "revisionsInfo",
        JSON.stringify({
          parent: rowInfo.node.parent,
          stepId: rowInfo.node.stepId
        })
      );
      this.setState({ mode: "revisions" });
    } else if (type == "history") {
      this.setState({ mode: "history" });
    } else if (type == "remove") {
      this.setState(state => ({
        items: removeNodeAtPath({
          treeData: state.items,
          path,
          getNodeKey: ({ treeIndex }) => treeIndex
        })
      }));
    } else if(type == "add"){
      this.setActiveVideo(node)
      let data={}
        data.pauseSeconds =''
        data.startSeconds = ''
        data.videoId = {};
        data.description = []
        data.title = ''
        data.tags = []
        data.code_snippet = []
        data.images = []
        data.parent=node.stepId
        data.author = USER_ID;
        data.likes={like: 0, dislike: 0, type: ""}
        data.isParent=node.stepId == ID?true:false
      this.setState({
        viewStatus: type,
        activeVideo:data
      });
    }
    else {
      this.setActiveVideo(node);
      this.setState({
        viewStatus: type,
        rowInfo,
        nodeData: node
      });
    }
  };

  findAndSetReviewFlag = (activeVideo, id, flag) => {
    let getNodeKey = ({ node: object, treeIndex: number }) => {
      return number;
    };
    let newTree = [];
    walk({
      treeData: this.state.items,
      getNodeKey,
      ignoreCollapsed: false,
      callback: ({ node }) => {
        if (activeVideo.stepId == node.stepId) {
          node.is_reviewed = flag;
          flag ? (node.reviewed_id = id) : null;
          this.setState({ activeVideo: node, loadingReviewBtn: false });
          newTree.push(node);
        } else {
          newTree.push(node);
        }
      }
    });
    this.setNewTreeData(newTree);
  };

  onClickReviewButton = node => {
    this.setState({ loadingReviewBtn: true });
    this.request.reviewSteps(node, "add").then(res => {
      this.findAndSetReviewFlag(this.state.activeVideo, res.id[0].value, true);
    });
    this.setState({
      isHaveVideo: true,
      isNoVideo: false,
      viewStatus: "video"
    });
    let that = this;
    let interval =  setInterval(function() {
      if (that.state.isReadyVideo) {
        clearInterval(interval);
        that.state.event.target.playVideo();
        that.setState({ isPlay: false, viewStatus: "video" });
      }
    }, 1000);
  };
 
  onRemoveReview = node => {
    this.setState({ loadingReviewBtn: true });
    let id = node.hasOwnProperty("reviewed_id")
      ? node.reviewed_id
      : this.state.reviewed_id;
    if (!this.state.loadingReviewBtn) {
      this.request.reviewSteps(node, "remove", id).then(res => {
        this.findAndSetReviewFlag(this.state.activeVideo, "", false);
      });
    }
  };

  onAddLadderSubmitForm = nodeData => {
    this.request
      .addNewLadderNode(nodeData)
      .then(res => {
      })
      .catch(err => {
        console.log("err",err);
      });
  };

  convertSecond = sec => {
    let hours = Math.floor(sec / 3600);
    let minutes = Math.floor((sec / 60) % 60);
    let seconds = sec % 60;
    let time = {};
    hours != undefined ? (hours = hours.toString() + "H") : null;
    minutes != undefined ? (minutes = minutes.toString() + "M") : null;
    seconds != undefined ? (seconds = seconds.toString() + "S") : null;
    return `PT${hours}${minutes}${seconds}`;
  };

  
  remove_duplicates=(arr)=>{
    let s = new Set(arr);
    let it = s.values();
    return Array.from(it);
  }

  onSubmitForm=async (data,type)=>{
    this.json=[];
    let image=[];
    let count=0;
    if(data.hasOwnProperty("images")){
      if(data.images.length !== 0){
        await this.request.imageUpload(data.images[0],data.stepId)
        .then(res=>{
          image.push({"target_id" : res})
        })
        .catch(err=>{
          this.setState({isLoading:false})  
          console.log("err",err);        
        })
      }
    }
    this.generateJSON(this.children);
    if(data.stepId==ID){
      count++;
      let tag=[],image=[];
      if(this.json.tags.length ==0){
        if(data.tags.length !== 0){
          data.tags.forEach(item=>{
            this.json.tags.push({ "is_new": 1,
            "name": item.name})
          })
        }
      }else{
        if(data.tags.length !== 0){
          data.tags.forEach(item=>{
            if(!item.id){
              this.json.tags.push({"name":item.name, "is_new":1})
              count++;
            }
          })
        }else{
        }
      }
        this.json.id=data.stepId;
        this.json.author=data.author;
        this.json.title= data.title !== "" && data.title !== null && data.title !== undefined ?data.title :this.json.title == data.title?this.json.title : this.json.title;
        this.json.remote_video=this.json.remote_video.youtubeId == data.videoId.youtubeId?this.json.remote_video:
        data.videoId.youtubeId?data.videoId.youtubeId !== null || data.videoId.youtubeId !== "" ?
        {"id": data.videoId.id !== null && data.videoId.id !== "" && data.videoId.id !== undefined ? data.videoId.id :"",
          "is_new": 1,
          "title": data.videoTitle,
          "url": data.videoUrl,
        }:this.json.remote_video:[];
        this.json.start_time=this.convertSecond(data.startSeconds).replace('NaN',0);
        this.json.end_time=this.convertSecond(data.pauseSeconds).replace('NaN',0);
        this.json.tags=this.json.tags;
        this.json.parent= data.parent;
        this.json.description=data.description.length >0 ?data.description[0].value:[];
        this.json.likes=data.likes;
        this.json.code_snippet=data.code_snippet;
        this.json.screen_shots=image;
        
        this.request.doPost('rest-api/ladder_revision?_format=json',this.json).then((res)=>{
          this.activeVideo = data;
            this.setState({
              viewStatus: "video",
              isLoading: false,
              editField:false,
              type:'',
              stepId:''
            });
           
            this.updateTree(data, type)
              .then((response)=>{
                this.setState({isLoading:false})
                this.activeVideo = data;
                this.setState({
                  viewStatus: "video",
                  isLoading: false,
                  editField:false,
                  type:'',
                  stepId:''
                });
                this.updateTree(data, type);
              })
              .catch((err)=>{
                console.log("err",err);
                this.setState({isLoading:false})
              })
        }).catch(err=>{
          console.log("err",err);
        })
    }else{
 
      if(data.hasOwnProperty("stepId")&&this.json.hasOwnProperty("children") && this.json.children !== undefined && this.json.children.length !== 0){
          let array=this.request.treeToArray(this.json.children);
        
          array.map((ele,i)=>{
              if(data.stepId==ele.id){
              count++;
    
              if(ele.tags.length ==0){
                if(data.tags.length !== 0){
                  data.tags.forEach(item=>{
                    ele.tags.push({ "is_new": 1,
                    "name": item.name})
                  })
                }
              }else{
                if(data.tags.length !== 0){
                  ele.tags.forEach(element=>{
                    data.tags.forEach(item=>{
                      if(!item.id){
                        ele.tags.push({"name":item.name, "is_new":1})
                      }
                    })
                  })
                }else{
                } 
              }
                array[i].id=data.stepId;
                array[i].author=data.author;
                array[i].title= data.title !== "" && data.title !== null && data.title !== undefined ?data.title :array[i].title == data.title? array[i].title : array[i].title;
                array[i].remote_video=array[i].remote_video.youtubeId == data.videoId.youtubeId?array[i].remote_video:
                data.videoId.youtubeId?data.videoId.youtubeId !== null || data.videoId.youtubeId !== "" ?
                {"id": data.videoId.id !== null && data.videoId.id !== "" && data.videoId.id !== undefined ?        data.videoId.id :"",
                  "is_new": 1,
                  "title": data.videoTitle,
                  "url": data.videoUrl,
                }:array[i].remote_video:[];
                array[i].start_time=this.convertSecond(data.startSeconds).replace('NaN',0);
                array[i].end_time=this.convertSecond(data.pauseSeconds).replace('NaN',0);
                array[i].tags=array[i].tags;
                array[i].parent=data.parent;
                array[i].description=data.description.length >0 ?data.description[0].value:[];
                array[i].likes=data.likes;
                array[i].code_snippet=data.code_snippet;
                array[i].screen_shots=image.length !== 0?image:array[i].screen_shots;
                this.request.jsonarrayToTree(array).then(res=>{
                  this.json.children=res;
                  this.request.doPost('rest-api/ladder_revision?_format=json',this.json).then((response)=>{
                    this.activeVideo = data;
                    this.setState({
                      viewStatus: "video",
                      isLoading: false,
                      editField:false,
                      type:'',
                      stepId:''
                    });
                   
                    this.updateTree(data, type)
                      .then((response)=>{
                        this.setState({isLoading:false})
                        this.activeVideo = data;
                        this.setState({
                          viewStatus: "video",
                          isLoading: false,
                          editField:false,
                          type:'',
                          stepId:''
                        });
                        this.updateTree(data, type);
                      })
                      .catch((err)=>{
                        this.setState({isLoading:false})
                        console.log("err",err);
                      })
                  }).catch((err)=>{
                    console.log("err",err);
                  })
                })
              }
          })
      }else{
        let tags=[],item={};
        if(data.tags.length !== 0){
          data.tags.forEach(item=>{
           tags.push({ "is_new": 1,
            "name": item.name})
          })
        }
        item.id=data.stepId;
        item.author=data.author;
        item.title=data.title !== "" && data.title !== null && data.title !== undefined ?data.title :item.title == data.title?item.title : item.title;
        item.remote_video=data.videoId !== undefined?data.videoId.youtubeId?data.videoId.youtubeId !== null || data.videoId.youtubeId !== "" ?
        { "id": data.videoId.id !== null && data.videoId.id !== "" && data.videoId.id !== undefined ?        data.videoId.id :"",
          "is_new": 1,
          "title": data.videoTitle,
          "url": data.videoUrl,
        }:[]:[]:[];
        item.start_time=this.convertSecond(data.startSeconds).replace('NaN',0);
        item.end_time=this.convertSecond(data.pauseSeconds).replace('NaN',0);
        item.tags=tags;
        item.parent=data.parent;
        item.description=data.description.length >0 ?data.description[0].value:[];
        item.likes=data.likes;
        item.code_snippet=data.code_snippet;
        item.screen_shots=image.length !== 0?image:[];
        if(count !== 1 && this.json.hasOwnProperty("children") && this.json.children !== undefined && this.json.children.length !== 0){
          let array=this.request.treeToArray(this.json.children);
          array.push(item);
          this.request.jsonarrayToTree(array).then(res=>{
            this.json.children=res;
            this.request.doPost('rest-api/ladder_revision?_format=json',this.json).then((response)=>{
              if(response){
                if(!data.hasOwnProperty("stepId")){
                  data.stepId=response.data.nid
                }
                this.activeVideo = data;
                this.setState({
                  viewStatus: "video",
                  isLoading: false,
                  editField:false,
                  type:'',
                  stepId:''
                });
                this.updateTree(data, type)
                  .then((response)=>{
                    this.setState({isLoading:false})
                    this.activeVideo = data;
                    this.setState({
                      viewStatus: "video",
                      isLoading: false,
                      editField:false,
                      type:'',
                      stepId:''
                    });
                    this.updateTree(data, type);
                  })
                  .catch((err)=>{
                    this.setState({isLoading:false})
                    console.log("err",err);
                  })
              }
            }).catch((err)=>{
              console.log("err",err);
            })
          })
      }else{
        this.json.children=[];
        this.json.children.push(item);
        this.request.doPost('rest-api/ladder_revision?_format=json',this.json).then((res)=>{
          this.activeVideo = data;
            this.setState({
              viewStatus: "video",
              isLoading: false,
              editField:false,
              type:'',
              stepId:''
            });
           
            this.updateTree(data, type)
              .then((response)=>{
                this.setState({isLoading:false})
                this.activeVideo = data;
                this.setState({
                  viewStatus: "video",
                  isLoading: false,
                  editField:false,
                  type:'',
                  stepId:''
                });
                this.updateTree(data, type);
              })
              .catch((err)=>{
                this.setState({isLoading:false})
              })
        }).catch(err=>{
          console.log("err",err);
        })
      }
      }
    }
  }

  editApiCall=(data,data1)=>{

    this.request.doPatch('node/'+`${data1.stepId}`+'?_format=json',data)
    .catch(err=>{
      console.log("err",err);
    })
  }

  updateTree = (data, type) => {
    if (data.tags.length > 0) {
      let suggestions = [];
      suggestions = this.state.suggestions;
      data.tags.forEach(element => {
        if (
          !suggestions.some(el => {
            return el.name == element.name;
          })
        ) {
          element.id = suggestions.length + 2;
          suggestions.push(element);
        }
      });
      this.setState({ suggestions });
    }
    let { node, path, treeIndex } = this.state.rowInfo;
    if (data.isParent && type == "add") {
      if(path === undefined){
        path=[];
      }else{
        path.pop();
      }
      if(treeIndex === undefined){
        treeIndex=0;
      }
      let parentNode = getNodeAtPath({
        treeData: this.state.treeData,
        path: path,
        getNodeKey: ({ treeIndex }) => treeIndex,
        ignoreCollapsed: true
      });
      let getNodeKey = ({ node: object, treeIndex: number }) => {
        return number;
      };
      let parentKey = getNodeKey(parentNode);
      if (parentKey == -1) {
        parentKey = null;
      }
      let newTree = addNodeUnderParent({
        treeData: this.state.items,
        newNode: data,
        expandParent: true,
        parentKey: parentKey,
        getNodeKey: ({ treeIndex }) => treeIndex
      });
      this.setState({ items: newTree.treeData });
      localStorage.setItem(
        "tempItems",JSON.stringify(newTree.treeData));
      this.setActiveVideo(data);
      this.children=this.state.treeTitle;
      this.children.children=this.state.items;
    } else if (type == "add") {
      this.setState(state => ({
        items: addNodeUnderParent({
          treeData: state.items,
          parentKey: path !== undefined?path[path.length - 1]:[],
          expandParent: true,
          getNodeKey: ({ treeIndex }) => treeIndex,
          newNode: data
        }).treeData
      }));
      localStorage.setItem(
        "tempItems",JSON.stringify(this.state.items));
        this.children=this.state.treeTitle;
        this.children.children=this.state.items;
      this.setActiveVideo(data);
    } else if (type == "edit") {    
      if(this.state.items.length >0){
        this.setState(state => ({
          items: changeNodeAtPath({
            treeData: state.items,
            path:path !== undefined?path:[],
            getNodeKey: ({ treeIndex }) => treeIndex !== undefined ?treeIndex:0,
            newNode: data
          })
        }));
      }
      this.setActiveVideo(data);
      this.children=this.state.treeTitle;
      this.children.children=this.state.items;
      if(this.state.rowInfo.node){
        let videos = this.request.treeToArray(this.state.items);
        this.request.getSuggestions(videos).then(suggestions => {
          this.setState({ suggestions });
        });
      }else{
        let videos = this.request.treeToArray(this.state.items);
        this.request.getSuggestions(videos).then(suggestions => {
          this.setState({ suggestions });
          if (data.tags.length > 0) {
            let suggestions = [];
            suggestions = this.state.suggestions;
            data.tags.forEach(element => {
              if (
                !suggestions.some(el => {
                  return el.name == element.name;
                })
              ) {
                element.id = suggestions.length + 2;
                suggestions.push(element);
              }
            });
            this.setState({ suggestions });
          }
        
        });
        
      }
    }
  };

  onDropedNode = nodeData => {
    this.setState({ disableDrag: true });
    this.request
      .onDropedNode(nodeData, this.state.dragNode)
      .then(res => {
        this.setState({ disableDrag: false });
      })
      .catch(err => {
        this.setState({ disableDrag: false });
        console.log("err",err);
      });
  };

  findParent = node => {
    this.state.queueVideos.forEach(item => {
      if (node.parent == item.stepId) {
        this.setState({ dragNode: item });
        return false;
      }
    });
  };

  onDragNode = nodeData => {
    localStorage.setItem("OldTree", JSON.stringify(this.state.items));
    if (nodeData.draggedNode != null) {
      this.findParent(nodeData.draggedNode);
    } else {
      this.setState({ dragNode: null });
    }
  };

  clickOnTags = tag => {
    this.setState({ isShowSuggetion: true });
  };

  voteToLadderStep = (node, type) => {
    if (!this.state.loadingVoteBtn) {
      this.setState({ loadingVoteBtn: true });
      this.request
        .voteToLadder(node.stepId, type === "up"?"like":"dislike")
        .then(res => {
          this.updateNodeValue(node, res.data,type === "up"?"like":"dislike");
        })
        .catch(err => {
          console.log("err",err);
        });
    }

  }

  updateNodeValue = (activeVideo, data,type) => {
    let getNodeKey = ({ node: object, treeIndex: number }) => {
      return number;
    };
    let newTree = [];
    walk({
      treeData: this.state.items,
      getNodeKey,
      ignoreCollapsed: false,
      callback: ({ node }) => {
        if (activeVideo.stepId == node.stepId) {
          node.likes["like"]=data.likes;
          node.likes["dislike"]=data.dislikes;
          node.likes["type"]=type;
          this.setState({ activeVideo: node, loadingVoteBtn: false });
          newTree.push(node);
        } else if(activeVideo.stepId == ID){
          const parent=this.state.activeVideo;
          parent.likes["like"]=data.likes;
          parent.likes["dislike"]=data.dislikes;
          parent.likes["type"]=type;
          this.setState({ activeVideo: parent, loadingVoteBtn: false });
          newTree.push(node);
        }
        else {
          newTree.push(node);
        }
      }
    });
    this.setNewTreeData(newTree);
  };

  onModeChange = mode => { 
    this.setState({ mode });
  };

  updateCode(newCode) {
    this.setState({
      code: newCode
    });
  }

  // getImages = activeVideo => {
  //   return activeVideo.images.map(item => ({
  //     src: item.url,
  //     thumbnail: item.url,
  //     thumbnailWidth: 220,
  //     thumbnailHeight: 220,
  //     isSelected: false,
  //     caption: item.name
  //   }));
  // };
  getImages = activeVideo => {
    return activeVideo.images.map(item => ({
      src: item.url,
      thumbnail: item.url,
      thumbnailWidth: 800,
      thumbnailHeight: 800,
      isSelected: false,
      caption: item.name
    }));
  };

  getCode = activeVideo => {
    if (activeVideo.code_snippet.length > 0) {
      return activeVideo.code_snippet[0].value;
    } else {
      return "";
    }
  };

  createMarkup = html => {
    return { __html:html };
  };

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  isLoggedIn = () => {
    return localStorage.getItem("ajs_token") == undefined ? false : true;
  };

  renderRightSideBlock = () => {
    const { viewStatus, activeVideo } = this.state;
    if (viewStatus == "video" || viewStatus == "add") {
      return (
        <div className="video-wrap">
          <TabContent activeTab={this.state.activeTab} style={{ padding: 10 }}>

             <TabPane tabId="2">
              <Row>
                <Col sm="12" id="image">
                <div>
                <b>Title</b> 
                {this.state.edit &&(
                 <MdEdit
                 onClick={() => this.changeTitle("title",activeVideo.title,activeVideo.stepId)}
                 className="icon-edit"
                />
                )}
                {this.state.editField == true && this.state.type == "title" && this.state.stepId == activeVideo.stepId &&(
                  <div>
                    <Input
                      className="video-form "
                        type="text"
                        name="title"
                        id="title"
                        placeholder="title"
                        value={this.state.title}
                        onChange={e => {
                          this.change(e);
                        }}
                        style={{borderRadius:"3em"}}
                      />
                      {this.state.showError == true && (this.state.title == "" || this.state.title == null) ?<div style={{color:"#ff0000"}}> This field is required</div>:''}
                      <div>
                        <Button onClick={()=> this.submit("title",activeVideo)}>Save</Button>
                        <Button onClick={()=>{this.setState({type:'',editField:false,title:'',stepId:'',showError:false})}}>cancel</Button>
                      </div>
                    </div>
                )}
                  {this.state.stepId == activeVideo.stepId ? this.state.type !== "title" &&(
                    <p>{activeVideo.title}</p>
                  ):this.state.stepId !== activeVideo.stepId && ( <p>{activeVideo.title}</p>)}
                </div>

                <div>
                <b>Description</b>
                {this.state.edit &&(
                  <MdEdit
                    onClick={() => this.changeTitle("description",activeVideo.description.length > 0?activeVideo.description[0].value:'',activeVideo.stepId)}
                    className="icon-edit"
                  />
                )}
                 {this.state.editField == true && this.state.type == "description" && this.state.stepId == activeVideo.stepId &&(
                  <div>
                    <Input
                      className="video-form "
                        type="textarea"
                        name="description"
                        id="description"
                        placeholder="Description"
                        value={this.state.description}
                        onChange={e => {
                          this.change(e);
                        }}
                        style={{borderRadius:"3em"}}
                    />
                    <div>
                      <Button onClick={()=> this.submit("description",activeVideo)}>Save</Button>
                      <Button onClick={()=>{this.setState({type:'',editField:false,description:'',stepId:''})}}>cancel</Button>
                    </div>
                  </div>
                  )}

                {this.state.stepId == activeVideo.stepId ? this.state.type !== "description"  &&
                 (activeVideo.children || this.state.treeTitle.description!=='') && 
                 (
                   <div style={{marginTop:20}}>
                        {activeVideo.description && activeVideo.description.length > 0 && (
                            <div
                              dangerouslySetInnerHTML={this.createMarkup(
                                activeVideo.description[0].value
                              )}
                            />
                        )}
                    </div>
                  ):this.state.stepId !== activeVideo.stepId &&
                  (activeVideo.children || this.state.treeTitle.description!=='') && 
                  (
                    <div style={{marginTop:20}}>
                         {activeVideo.description && activeVideo.description.length > 0 && (
                             <div
                               dangerouslySetInnerHTML={this.createMarkup(
                                 activeVideo.description[0].value
                               )}
                             />
                         )}
                     </div>
                   )}
                </div>

                 <div>
                <b>Tags</b> 
                {this.state.edit &&(
                 <MdEdit
                 onClick={() => this.changeTitle("tag",activeVideo.tags,activeVideo.stepId)}
                 className="icon-edit"
                />
                )}
                {this.state.editField == true && this.state.type == "tag" && this.state.stepId == activeVideo.stepId &&(
                  <div>
                     <div className="reactTag">
                      <ReactTags
                        tags={this.state.tags}
                        allowNew={true}
                        addOnBlur={true}
                        suggestions={this.state.suggestTags}
                        placeholder="Add Video tags"
                        handleDelete={this.deleteTag.bind(this)}
                        handleAddition={this.additionTag.bind(this)} 
                        autoresize={false}
                        inputAttributes={{ maxLength: "100%" }}
                      />
                      </div>
                      <div>
                        <Button onClick={()=> this.submit("tag",activeVideo)}>Save</Button>
                        <Button onClick={()=>{this.setState({type:'',editField:false,tags:'',stepId:''})}}>cancel</Button>
                      </div>
                    </div>
                )}
                  {this.state.stepId == activeVideo.stepId ? this.state.type !== "tag" &&(
                    activeVideo.tags.map((item,i)=>(
                      <p key={i}>{item.name}</p>
                    ))
                  ):this.state.stepId !== activeVideo.stepId && ( activeVideo.tags.map((item,i)=>(
                    <p key={i}>{item.name}</p>
                  )))}
                </div>
                </Col>
              </Row>
            </TabPane> 

            <TabPane tabId="1">
              <Row>
                <Col>
                <div>
                  <b>Video</b>
                  {this.state.edit &&(
                  <MdEdit
                  onClick={() => this.changeTitle("video",activeVideo.stepId == ID ?this.state.treeTitle : this.state.rowInfo,activeVideo.stepId)}
                  className="icon-edit"
                  />)}
                
                  {this.state.activeVideo.videoId?this.state.activeVideo.videoId.youtubeId ? (

                      <div>
                      <p><b>Video URL:</b> {activeVideo.videoUrl?activeVideo.videoUrl:activeVideo.videoId.url }</p>
                      <p><b>Video Start time:</b> {activeVideo.startSeconds?parseInt(activeVideo.startSeconds):''}</p>
                      <p><b>Video End time:</b> {activeVideo.pauseSeconds?parseInt(activeVideo.pauseSeconds):''}</p>
                      <YouTube
                        video={
                          this.activeVideo.videoId
                            ? this.activeVideo.videoId.youtubeId
                            : this.activeVideo.videoId
                        }
                        autoplay={false}
                        showRelatedVideos={false}
                        startSeconds={parseInt(this.activeVideo.startSeconds)}
                        endSeconds={parseInt(this.activeVideo.pauseSeconds)}
                        width={"100%"}
                        height={470}
                        opts={innerWidth="100%"}
                        onReady={event => this.onPlayerReady(event)}
                        onStateChange={event => this.onPlayerStateChange(event)}
                      />
                      </div>
                    ):<div><b>No video found.</b></div>:<div><b>No video found.</b></div>}
                     {(activeVideo.videoId && activeVideo.videoId.youtubeId !== '') && (
                    <div className="desc-block">
                      <div className="video-title-block">
                    {activeVideo.videoId && activeVideo.videoId.youtubeId!=='' && activeVideo.videoId.youtubeId!=null && (
                        <div className="rst__rowToolbar">
                          <div className="rst__toolbarButton">
                            <div className="buttons-wrapper">
                              
                              <Button
                                onClick={() =>
                                  this.voteToLadderStep(activeVideo, "up")
                                }
                                color="white"
                              >
                              <FaThumbsUp
                                color="white"
                                className={` ${activeVideo.likes["type"] == "like" && activeVideo.likes["like"] > 0   ? "like-icon" : "like-icon-hover"}`} 
                              />
                              </Button> 
                              <span className="" >{activeVideo.likes["like"]}</span>
                              <Button
                                onClick={() =>
                                  this.voteToLadderStep(activeVideo, "down")
                                }
                                color="white" background-color="white" border="none"
                              >
                                <FaThumbsDown
                                  color="white"
                                  className={`btn btn-white ${activeVideo.likes["type"] == "dislike" && activeVideo.likes["dislike"] > 0 ? "like-icon" : "like-icon-hover"}`}
                                />
                              </Button>
                             
                              <span className="">{activeVideo.likes["dislike"]}</span>

                              {activeVideo.is_reviewed ? (
                                <Button className="button-watch"  >
                                <FaCheckDouble
                                  color="white" 
                                  className="pr-1"
                                />
                                Watched
                              </Button>
                              
                                 ) : (
                                  <Button className="button-watch watch-icon-deactive" onClick={() => this.onClickReviewButton(activeVideo)} >
                                  <FaCheck
                                    color="white"
                                    className="watch-icon-deactive"
                                  />
                                  Watch
                                </Button>
                              )}
                            </div>
                          </div>
                        </div>
                          )}
                      </div>
                  
                    </div>
                  )}
                </div>
                </Col>
              </Row>
            </TabPane>

            <TabPane tabId="3">
              <Row style={{ padding: 0 }}>
              <Col sm="12">
              
              <div>
                <div>
                <b>Code Sample</b>
                  {this.state.edit &&(
                  <MdEdit
                  onClick={() => this.changeTitle("codetitle",activeVideo.code_snippet,activeVideo.stepId)}
                  className="icon-edit"
                  />)}
                  {this.state.editField == true && this.state.type == "codetitle" && this.state.stepId == activeVideo.stepId &&(
                  <div>
                     <Input
                     className="video-form "
                      type="text"
                      name="codetitle"
                      id="video-url"
                      placeholder="Code Snippet Title"
                      value={this.state.codetitle}
                      onChange={e => {
                        this.change(e);
                      }}
                      style={{borderRadius:"3em"}}
                      
                      /> 
                    <div>
                      <Button onClick={()=> this.submit("codetitle",activeVideo)}>Save</Button>
                      <Button onClick={()=>{this.setState({type:'',editField:false,codetitle:'',stepId:''})}}>cancel</Button>
                    </div>
                  </div>
                  )}

                </div>
                
                {this.state.stepId == activeVideo.stepId ? this.state.type !== "codetitle" && (
                   <div className="code-sample-wrap"> 
                   {activeVideo.code_snippet.length > 0 ? 
                     activeVideo.code_snippet.map((ele,i)=>{
                       return(
                         <div key={i}>
                           <div className="code-header">
                               {ele.title}
                           </div>
                           <div
                            dangerouslySetInnerHTML={this.createMarkup(
                              ele.help_text
                            )}
                            >
                          </div>
                          <div 
                         dangerouslySetInnerHTML={this.createMarkup(
                          ele.code_snippet
                        )}
                          >
                        </div>
                         </div>
                         
                       )
                     })
                      :<b>No code samples found.</b>
                       }
                   </div>
                ):this.state.stepId !== activeVideo.stepId  && (
                  <div className="code-sample-wrap"> 
                  {activeVideo.code_snippet.length > 0 ? 
                    activeVideo.code_snippet.map((ele,i)=>{
                      return(
                        <div key={i}>
                           <div className="code-header">
                               {ele.title}
                           </div>
                           <div
                            dangerouslySetInnerHTML={this.createMarkup(
                              ele.help_text
                            )}
                            >
                          </div>
                          <div 
                         dangerouslySetInnerHTML={this.createMarkup(
                          ele.code_snippet
                        )}
                          >
                        </div>
                        </div>
                      )
                    })
                     :<b>No code samples found.</b>
                      }
                  </div>
               )}
              </div>

               {/* <div>
                <div>
                <b>Code Samples</b>
                  {this.state.edit &&(
                  <MdEdit
                  onClick={() => this.changeTitle("codesamples",activeVideo.code_snippet,activeVideo.stepId)}
                  className="icon-edit"
                  />)}
                  {this.state.editField == true && this.state.type == "codesamples" && this.state.stepId == activeVideo.stepId &&(
                  <div>
                    <CKEditor
                    editor={ ClassicEditor }
                    data={this.state.codesamples}
                    onChange={ ( event, editor ) => {
                        const data = editor.getData();
                        this.setState({codesamples:data})
                    } }
                    />
                    <div>
                      <Button onClick={()=> this.submit("codesamples",activeVideo)}>Save</Button>
                      <Button onClick={()=>{this.setState({type:'',editField:false,codesamples:'',stepId:''})}}>cancel</Button>
                    </div>
                  </div>
                  )}

                </div>
                
                {this.state.stepId == activeVideo.stepId ? this.state.type !== "codesamples" && (
                   <div className="code-sample-wrap"> 
                   {activeVideo.code_snippet.length > 0 ? 
                     activeVideo.code_snippet.map((ele,i)=>{
                       return(
                         <div key={i}>
                           <div 
                              dangerouslySetInnerHTML={this.createMarkup(
                               ele.code_snippet
                             )}
                           >
                           </div>
                         </div>
                       )
                     })
                      :<b>No code samples found.</b>
                       }
                   </div>
                ):this.state.stepId !== activeVideo.stepId && (
                  <div className="code-sample-wrap"> 
                  {activeVideo.code_snippet.length > 0 ? 
                    activeVideo.code_snippet.map((ele,i)=>{
                      return(
                        <div key={i}>
                          <div 
                             dangerouslySetInnerHTML={this.createMarkup(
                              ele.code_snippet
                            )}
                          >
                          </div>
                        </div>
                      )
                    })
                     :<b>No code samples found.</b>
                      }
                  </div>
               )}
              </div> */}
              <div>
                <div>
                  <b>Help Text</b>
                  {this.state.edit &&(
                  <MdEdit
                  onClick={() => this.changeTitle("helptext",activeVideo.code_snippet,activeVideo.stepId)}
                  className="icon-edit"
                  />)}
                  {this.state.editField == true && this.state.type == "helptext" && this.state.stepId == activeVideo.stepId &&(
                  <div>
                     <CKEditor
                      editor={ClassicEditor}
                      data={this.state.helptext}
                      onChange={(event, editor) => {
                        this.setState({
                          helptext: editor.getData(),
                        })
                      }}
                      onBlur={editor => {
                      }}
                      onFocus={editor => {
                      }}
                    />
                    <div>
                      <Button onClick={()=> this.submit("helptext",activeVideo)}>Save</Button>
                      <Button onClick={()=>{this.setState({type:'',editField:false,helptext:'',stepId:''})}}>cancel</Button>
                    </div>
                  </div>
                  )}
                </div>
                  
                {this.state.stepId == activeVideo.stepId ? this.state.type !== "helptext" && (
                  <div className="code-sample-wrap"> 
                  {activeVideo.code_snippet.length > 0 ? 
                    activeVideo.code_snippet.map((ele,i)=>{
                      return(
                        <div key={i}>
                          <div
                          dangerouslySetInnerHTML={this.createMarkup(
                            ele.help_text
                          )}
                          >
                          </div>
                        </div>
                      )
                    })
                     :<b>No code samples found.</b>
                      }
                  </div>
                ):this.state.stepId !== activeVideo.stepId  && (
                  <div className="code-sample-wrap"> 
                  {activeVideo.code_snippet.length > 0 ? 
                    activeVideo.code_snippet.map((ele,i)=>{
                      return(
                        <div key={i}>
                          <div
                          dangerouslySetInnerHTML={this.createMarkup(
                            ele.help_text
                          )}
                          >
                          </div>
                        </div>
                      )
                    })
                     :<b>No code samples found.</b>
                      }
                  </div>
                )}
              </div> 

                </Col>
              </Row>
            </TabPane>  

            <TabPane tabId="4">
              <Row>
                <Col sm="12" id="image">
                <div>
                  <b>Image</b>
                  {this.state.edit &&(
                  <MdEdit
                  onClick={() => this.changeTitle("image",activeVideo,activeVideo.stepId)}
                  className="icon-edit"
                  />)}
                  {this.state.editField == true && this.state.type == "image" && this.state.stepId == activeVideo.stepId &&(
                    <div>
                   <Input type="file" name="gallery[]" id="gallery" accept="image/*" onChange={this.gallery} multiple={true} />
                   {this.state.showError == true && this.state.images.length == 0  ?<div style={{color:"#ff0000"}}> Please choose image</div>:''}
                   <div>
                   <Button onClick={()=> this.submit("image",activeVideo)}>Save</Button>
                   <Button onClick={()=>{this.setState({type:'',editField:false,images:[],stepId:'',showError:false})}}>cancel</Button>
                  </div>
                  </div>
                   )}
                   {this.state.stepId == activeVideo.stepId ? this.state.type !== "image" &&(
                    activeVideo.images.length > 0 ? 
                      <Gallery images={this.getImages(activeVideo)} />
                    :<div><b>No snapshots found.</b></div>):this.state.stepId !== activeVideo.stepId && (
                      activeVideo.images.length > 0 ? 
                      <Gallery images={this.getImages(activeVideo)} />
                    :<div><b>No snapshots found.</b></div>
                    )}
                  </div>
                </Col>
              </Row>
            </TabPane>
          </TabContent>
        </div>
      );
    } else if (this.isLoggedIn()) {
      if (viewStatus == "edit") {
        return (
          <AddVideoForm
            onSubmit={this.onSubmitForm}
            nodeData={this.state.nodeData}
            type={"edit"}
            isLoading={this.state.isLoading}
          />
        );
      }
    }
     else {
      return <LoginForm />;
    }
  };


  gallery = (event,activeVideo) => {

    if (event.target.files) {
      let images = [];
      [].forEach.call(event.target.files, (file) => {
        if (/\.(jpe?g|png|gif)$/i.test(file.name)) {
          var reader = new FileReader();
          reader.addEventListener("load", function () {
            images.push({
              name: file.name,
              url: reader.result
            });
          }, false);
          reader.readAsDataURL(file);
        }
      });
      this.setState({
        images: images
      })
    }

  }

  change=async event=>{
    const { target } = event;
    const value =  target.value;
    const { name } = target;
    await this.setState({
      [name]: value
    });
  }

  changeTitle=(type,value,id)=>{
    this.setState({editField:true,type:type,stepId:id});
    if(type == "description"){
      this.setState({description:value})
    }else if(type == "title"){
      this.setState({title:value})
    }else if(type == "video"){
      this.onClickRowButton(value,"edit")
    }else if(type == "codesamples"){
      this.setState({codesamples:value.length>0 && value[0].hasOwnProperty('code_snippet') &&  value[0].hasOwnProperty('code_snippet')!==undefined ?value[0].code_snippet:''})
    }else if(type == "helptext"){
      this.setState({helptext:value.length>0 && value[0].hasOwnProperty('help_text') &&  value[0].hasOwnProperty('help_text')!==undefined ?value[0].help_text:''})
    }else if(type == "codetitle"){
      this.setState({codetitle:value.length>0 && value[0].hasOwnProperty('title') &&  value[0].hasOwnProperty('title')!==undefined ?value[0].title:''})
    }else if(type == "tag"){
      this.setState({tags: value});
    }
  }

  submit=(type,activeVideo)=>{
    if(type == "title"){
      if(this.state.title!== null && this.state.title !== ""){
        activeVideo.title=this.state.title;
        this.setState({showError:false})
        this.onSubmitForm(activeVideo,this.state.viewStatus == "add"?"add":"edit")
      }else{
        this.setState({showError:true})
      }
    }else if(type == "description"){
      activeVideo.description=this.state.description==""?[]:[{value:this.state.description}];
      this.onSubmitForm(activeVideo,this.state.viewStatus == "add"?"add":"edit")
    }else if(type == "codesamples"){
      activeVideo.code_snippet = [
        {
          code_snippet:this.state.codesamples,
        }
      ]
      this.onSubmitForm(activeVideo,this.state.viewStatus == "add"?"add":"edit")
    }else if(type == "helptext"){
      activeVideo.code_snippet = [
        {
          help_text:this.state.helptext,
        }
      ]
      this.onSubmitForm(activeVideo,this.state.viewStatus == "add"?"add":"edit")
    }else if(type == "codetitle"){
      activeVideo.code_snippet = [
        {
          title:this.state.codetitle
        }
      ]
      this.onSubmitForm(activeVideo,this.state.viewStatus == "add"?"add":"edit")
    }else if(type == "image"){
      if(this.state.images.length > 0){
        activeVideo.images=this.state.images;
        this.setState({showError:false})
        this.onSubmitForm(activeVideo,this.state.viewStatus == "add"?"add":"edit")
      }else{
        this.setState({showError:true})
      }
      
    }else if (type == "tag"){
      activeVideo.tags = this.state.tags
      this.onSubmitForm(activeVideo,this.state.viewStatus == "add"?"add":"edit")
    }

  }

  deleteTag(i) {
    const tags = this.state.tags.slice(0);
    tags.splice(i, 1);
    this.setState({ tags });
  }

  additionTag(tag) {
    if (this.state.suggestTags.find(item => tag.name == item.name) != undefined && this.state.tags.find(item => tag.name == item.name) == undefined) {
      let tags = [].concat(this.state.tags, tag);
      this.setState({ tags });
    } else if (this.state.tags.find(item => tag.name == item.name) == undefined) {
      let tags = [].concat(this.state.tags, tag);
      this.setState({ tags });
    }
  }


  addLadder = () => {
    this.onModeChange("addLadder");
  };

  treeButton = async () => {
   await this.setState({edit:!this.state.edit})

    localStorage.getItem('ajs_token' === undefined) ? console.log("token not found") : (localStorage.getItem('ajs_token') === "true"&&this.state.edit===true ? this.setState({disableDrag:false,edit:true}) : this.setState({disableDrag:true}))
  }

  entermouse=async (event,stepTitle,title)=>{
    if(event=="enter")
    {
     await this.setState({mouseleave:"enter",titleId:title})
    }
    else{
     await this.setState({mouseleave:"leave",titleId:''})
    }
  }

  toggleModal=()=> {
    this.setState({
      isShowModal: !this.state.isShowModal
    });
  }

  eventChange=(event)=>{
    this.setState({event});
  }

  toggleTabs=(tab)=>{
    this.setState({defaultTab:tab})
  }

  render() {
    const {
      isPlay,
      isReadyApp,
      isHaveVideo,
      viewStatus,
      items,
      searchString,
      searchFocusIndex,
      searchFoundCount,
      disableDrag,
      upVote,
      downVote,
      isVoted,
      stepDownVote,
      stepUpVote,
      isStepVoted,
      treeTitle,
      activeVideo,
      mode,
      defaultTab
    } = this.state;
    const count = getVisibleNodeCount({ treeData: this.state.items });
    
   return(
    <div id="react-step-ladders" className="container-fluid wrapper">
      <div>
      {isReadyApp === false ? 
      <div style={{marginLeft:"50%"}}>
        <Spinner color="primary" />
        </div>:
        <div>
        <Row>
          {/* <Col>
            { mode !== "addLadder" ?
              <Button className="button-play" onClick={() => this.addLadder()}>
              Add New Ladder
            </Button> : null }
          </Col> */}
          <Col>
            <Button
                  className="button-play"
                  onClick={() => this.onClickPlayAll()}
                  disabled={!isReadyApp}
            >
              {isPlay&& this.state.click  === "no video"&& !this.state.manual ? "Next" :!isPlay && this.state.click === "pause" && !this.state.manual? "Resume": isPlay&& this.state.click === "play all"&& !this.state.manual ? "PAUSE" : "Play All"}
              {/* {isPlay&& this.state.click  === "no video"&& !this.state.manual ? "Next" :!isPlay && this.state.click&& !this.state.manual === "pause" ? "Resume": isPlay&& this.state.click === "play all"&& !this.state.manual ? "PAUSE" : !isPlay  ?"Play All": "STOP"} */}
            </Button>
            <div className="mt-4">
            {isPlay&& this.state.click === "no video" ?this.state.counter<10?
             '00:0'+this.state.counter:
             '00:'+this.state.counter:''}
            </div> 
          </Col>
  
          <Col  className="radio-button-wrap">
            {this.state.suggestions.length>0 && mode !== "html" && mode !== "addLadder"?
              this.state.suggestions.map((item, i) =>(
                  <div className="radio-button" key={i}>                
                    <div className="custom-control custom-checkbox header-checkbox">
                      <input type="checkbox" className="custom-control-input" id={`checkbox${i.toString()}`}
                        onChange={()=>this.handleAddition(item.name)}
                      />
                      <label className="custom-control-label drupal-button" outline
                      key={item.id}
                      onClick={() => this.clickOnTags(item.name)}
                      color="light" htmlFor={`checkbox${i.toString()}`}>
                        {item.name}
                      </label>
                    </div>
                  </div>
                  ))
            :''}
          </Col>
          <Col   className="text-right">
          { mode !== "addLadder" ?
          
            <UncontrolledButtonDropdown
              isOpen={this.state.dropdownOpen}
              toggle={this.toggle}
            >
              <DropdownToggle caret className="button-dropdown"  >
              {mode === "tree" ? 'Instructional steps Mode' : mode === "html" ? "Expository Mode" : mode === "lecture" ? "Lecture Mode":mode === "presentation" ? "Presentation Mode":''}
              </DropdownToggle>
              <DropdownMenu right>
                {mode !== "tree" && (
                  <DropdownItem onClick={() => this.onModeChange("tree")} style={{cursor:"pointer"}} >
                    Instructional steps Mode
                  </DropdownItem>
                )}
                {mode !== "html" && (
                  <DropdownItem onClick={() => this.onModeChange("html")} style={{cursor:"pointer"}} >
                    Expository Mode
                  </DropdownItem>
                )}
                {mode !== "lecture" &&(
                  <DropdownItem onClick={() => this.onModeChange("lecture")} style={{cursor:"pointer"}} >
                    Lecture Mode
                  </DropdownItem>
                )}
                {mode !== "presentation" &&(
                  <DropdownItem onClick={() => this.onModeChange("presentation")} style={{cursor:"pointer"}} >
                    Presentation Mode
                  </DropdownItem>
                )}
              </DropdownMenu>
            </UncontrolledButtonDropdown>
          :''}
        </Col>
        </Row>


        {this.state.error && (
          <h3>No records found!!!</h3>
        )}

        {mode === "html" && (
          <ExpositoryMode
          Title={treeTitle}
          Items={this.state.treeTitle}
          Videos={this.state.queueVideos}
          mode="html"
          onModeChange={this.onModeChange}
          id="2"
          onclick={this.onClickTree}
          eventChange={this.eventChange}
        />
        )}
        {mode === "history" && (
          <History
          Title={treeTitle}
          Items={this.state.queueVideos}
          mode="history"
          onModeChange={this.onModeChange}
          onMyClick={this.onMyClick}
        />
        )}

        {mode === "revisions" && (
          <Revisions
          Title={treeTitle}
          Items={this.state.revisions}
          mode="revisions"
          onModeChange={this.onModeChange}
          onMyClick={this.onMyClick}
          ladder={this.loadLadder}
          adopt={this.adoptLadder}
          base={this.state.base}
        />
        )}

        {mode === "addLadder" && (
           <AddLadder
           onModeChange={this.onModeChange}
           onMyClick={this.onMyClick}
           onAddLadderSubmitForm={this.onAddLadderSubmitForm}
         />
        )}


        {mode === "lecture" && (
          <TutorialMode
          Title={treeTitle}
          TreeItems={this.state.items}
          Items={this.state.queueVideos}
          mode="lecture"
          onModeChange={this.onModeChange}
          id="1"
        />
        )}

        {mode === "presentation" && (
          <PresentationMode
          Title={treeTitle}
          TreeItems={this.state.items}
          Items={this.state.queueVideos}
          mode="presentation"
          onModeChange={this.onModeChange}
          id="1"
        />
        )}



        {mode === "tree" && isReadyApp && (
          
          <div className="video-content">
          
          {!isReadyApp ? 
            <div className="">
            </div>
          : (
          <div className="nav-section ">
            <Row className="align-items-center">
             
              <Col lg="6">
              <Row>
                <Col>

                </Col>
                <Col>
                  { localStorage.getItem('ajs_token')==="true" ? 
                  <button className={this.state.edit?"button-edit":"button-edit-hover"}  onClick={()=>this.treeButton()} >
                    <FaEdit color="white" className="edit-icon" />
                    Edit
                  </button>
                  :''}
                </Col>
                {/* <Col>
                  {this.state.treeTitle.revision_exist ?
                    <button className='button-edit-hover' onClick={()=>this.getRevisions()} >Revisions</button>
                  :this.state.loadtype == 'revision' ? <button className='button-edit-hover' onClick={()=>this.getRevisions()} >Back to Revisions</button>:''}
                </Col> */}
              </Row>
              </Col>
             
              <Col lg="6">
                <div className="topnav">
                  <Nav tabs >
                  <NavItem>
                      <NavLink
                        style={{ cursor: "pointer" }}
                        
                        className={classnames({
                          activeRow: this.state.activeTab === "2"
                        })}
                        onClick={() => {
                          this.toggleTab("2");
                        }}
                      >
                        Ladder Info
                      </NavLink>
                  </NavItem> 

                    <NavItem>
                      <NavLink
                        style={{ cursor: "pointer" }}
                        
                        className={classnames({
                          activeRow: this.state.activeTab === "1"
                        })}
                        onClick={() => {
                          this.toggleTab("1");
                        }}
                      >
                        Video
                      </NavLink>
                    </NavItem>
                      <NavItem>
                        <NavLink
                          style={{ cursor: "pointer" }}
                          className={classnames({
                            activeRow: this.state.activeTab === "4"
                          })}
                          onClick={() => {
                            this.toggleTab("4");
                          }}
                        >
                          Screenshots
                        </NavLink>
                      </NavItem>
                        <NavItem>
                          <NavLink
                            style={{ cursor: "pointer" }}
                            className={classnames({
                              activeRow: this.state.activeTab === "3"
                            })}
                            onClick={() => {
                              this.toggleTab("3");
                            }}
                          >
                            Code samples
                          </NavLink>
                        </NavItem> 
                  </Nav>
                </div>
              </Col>
              
            </Row>
          </div>
          )}
          
        {isReadyApp && (
          <Row className="tabs-wrapper">
          
            
            <Col lg="6">
            <div className={this.state.ladderTitle  ? "shadow bg-white rounded shadow1 header-ladder"  :""} style={{marginLeft:80}}>
              <h6 className="small-header pt-3 " className={this.state.activeTitle?"active-row":''}  onClick={()=>this.ladderVideos()} style={{cursor:'pointer'}}>
              <strong>{this.state.treeTitle.title}</strong>
               </h6>
               {this.state.edit ? 
                        <React.Fragment>
                        <MdAdd
                          className
                          onClick={() => this.onClickRowButton(this.state.treeTitle, "add")}
                          className="icon-add"
                        />
                        </React.Fragment>
                :""
                } 
               </div>
              <div >
            {this.state.treeTitle.children || this.state.items.length>0? 
                <SortableTree
                  ref={component => {
                    this.treeComponent = component;
                  }}
                  style={{ height: count * 62 }}
                  treeData={items}
                  rowHeight={60}
                  rowWidth={100}
                  scaffoldBlockPxWidth={40}
                  onChange={this.handleTreeOnChange}
                  maxDepth={maxDepth}
                  className="video-tree"
                  searchQuery={searchString}
                  onMoveNode={node => this.onDropedNode(node)}
                  onDragStateChanged={node => this.onDragNode(node)}
                  searchFocusOffset={searchFocusIndex}
                  canDrag={!disableDrag}
                  canDrop={({ nextParent }) =>
                    !nextParent || !nextParent.noChildren
                  }
                  searchFinishCallback={matches =>
                    this.setState({
                      searchFoundCount: matches.length,
                      searchFocusIndex:
                        matches.length > 0
                          ? searchFocusIndex % matches.length
                          : 0
                    })
                  }
                  isVirtualized={true}
                  generateNodeProps={rowInfo => ({
                    title: (
                      
                      <span
                        className={rowInfo.node.isActive ? "active-row" : ""}
                      >
                        <div
                          className="main_content"
                          id={`node-${rowInfo.node.stepId}`}
                                                   
                        >
                        <div
                         onMouseEnter={()=>this.entermouse("enter",`node-${rowInfo.node.stepId}`,rowInfo.node.stepId)}
                         onMouseLeave={()=>this.entermouse("leave")}
                        >

                        {this.state.mouseleave==="enter" && rowInfo.node.stepId===this.state.titleId ?
                        rowInfo.node.title
                        :
                          <div>{rowInfo.node.title.substring(0,MAX_LENGTH)}
                          {rowInfo.node.title.length>MAX_LENGTH 
                          ?'...':'' }
                          </div> }
                        </div>
                        </div>
                      </span>
                    ),
                    subtitle: rowInfo.node.url,
                    buttons: [
                      
                      <div className={rowInfo.node.isActive ? "edit-video-form active-row" : "edit-video-form"}>
                       {this.state.edit ? 
                        <div className="active-icons" style={{width:"80px"}}>
                        <MdAdd
                          className
                          onClick={() => this.onClickRowButton(rowInfo, "add")}
                          className="icon-add"
                        />
                        </div>
                          : ""} 
                      </div>
                    ],
                    onClick: event => {
                      this.setState({rowInfo:rowInfo})
                      if (
                        event.target.className.animVal == undefined &&
                        event.target.className.baseVal === undefined
                      ) {
                        if (
                          event.target.className.includes(
                            "rst__collapseButton"
                          ) ||
                          event.target.className.includes(
                            "rst__expandButton"
                          ) ||
                          event.target.className.includes("rst__moveHandle")
                        ) {
                        } else {
                          this.onClickRow(rowInfo);
                        }
                      }
                    }
                  })}
                />
                  :''}
              </div>
            </Col>
            <Col lg="6" className="video-col-wrap">
              {this.renderRightSideBlock()}
            </Col>
          </Row>
          )}
        </div>

        )}
      </div>
      }
      </div>
      </div>
   );
   }
  }
ReactDOM.render(<POC_2 />, document.getElementById("react-step-ladders"));