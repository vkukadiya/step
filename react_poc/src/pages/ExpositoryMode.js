import React, { Component } from "react";
import "react-toggle/style.css";
import {Row, Col} from "reactstrap";
import Request from "../common/request";
import YouTube from "@u-wave/react-youtube";
import Gallery from "react-grid-gallery";
import {
  FaCheck,
  FaCheckDouble,
  FaPen,
  FaEdit,
  FaSearch,
  FaThumbsUp,
  FaThumbsDown
} from "react-icons/fa";

export default class ExpositoryMode extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dropdownOpen: false,
      allItem: this.props.Items,
      activeVideo:this.props.Items,
      event: {},
      data:'',
      autoPlay:false,
      count:1
    };
    this.toggle = this.toggle.bind(this);
  }
  request = new Request();

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  onModeChange = mode => {
    this.setState({ mode });
    this.props.onModeChange(mode);
  };

  onclick=(item,key,event)=>{
    this.setState({event});
    if (item.videoId.youtubeId) {
        let that = this;
          let interval = setInterval(function() {
              clearInterval(interval);
              that.event.target.loadVideoById({
                videoId: node.videoId.youtubeId,
                startSeconds: node.startSeconds,
                endSeconds: node.pauseSeconds
              });
          }, 1000);
        }
  }

  createMarkup = html => {
    return { __html: html };
  };

  getImages = activeVideo => {
    return activeVideo.images.map(item => ({
      src: item.url,
      thumbnail: item.url,
      thumbnailWidth: 220,
      thumbnailHeight: 220,
      isSelected: false,
      caption: item.name
    }));
  };

  onPlayerReady=event=>{
    this.setState({ event, isReadyVideo: true });
  }

  onPlayerStateChange=event=>{
    if(event.target.getPlayerState() == 1){
      if(this.state.data === '' || this.state.data === undefined || this.state.data === null){
        this.setState({data:event.target.getPlayerState(),event:event})
      }else if(this.state.data == 0 ){         
          this.setState({data:event.target.getPlayerState(),event:event,autoplay:true})
      }else if(this.state.data == 1 && this.state.autoplay==true){
          this.setState({data:event.target.getPlayerState(),event:event,autoplay:false})
      }else if(this.state.data == 1 && this.state.autoplay==false){
        this.state.event.target.pauseVideo();
        this.setState({data:event.target.getPlayerState(),event:event,autoplay:false})
      }else{
        this.state.event.target.pauseVideo();
        this.setState({data:event.target.getPlayerState(),event:event,autoplay:false})
      }
    }else if(event.target.getPlayerState() == 0){
        this.setState({data:event.target.getPlayerState(),event:event,autoPlay:true})
    }
  }
  renderData=(ele)=>{
    return(
      <div className="container pl-5">
      <Row>
        <Col sm="6">
        <div className="data-header">
              Description
            </div>
          {ele.description.length >0?
            <div className=""
            dangerouslySetInnerHTML={this.createMarkup(
              ele.description[0].value
            )}/>
          :'No Description Found'
          }
        </Col>
        <Col sm="6">
        <div className="data-header">
          Video
        </div>
        {
            ele.videoId.youtubeId?(
              <YouTube
                    video={
                      ele.videoId
                        ? ele.videoId.youtubeId
                        : ele.videoId
                    }
                    autoplay={false}
                    showRelatedVideos={false}
                    width={"100%"}
                    height={470}
                    opts={innerWidth="100%"}
                    onReady={event => this.onPlayerReady(event)}
                    onStateChange={event => this.onPlayerStateChange(event)}
                  />
            ):'No Video Found'
          }
        </Col>
      </Row>
      <Row>
        <Col sm="6">
          <div className="data-header">
            Code Samples
          </div>
          {ele.code_snippet.length > 0 ? 
              ele.code_snippet.map((element,i)=>{
                return(
                  <div key={i}>
                    <div className="code-header">
                      <strong>
                        {element.title}
                      </strong>
                    </div>
                    <div>
                      {element.code_snippet}
                    </div>
                    <div>
                      {element.help_text}
                    </div>
                  </div>
                )
              })
            :'No Code Samples Found'}
        </Col>
        <Col sm="6">
          <div className="data-header">
            Sceenshot
          </div>
          {ele.images.length > 0 ? (
            <Gallery images={this.getImages(ele)} />
          ):'No Snapshot Found'}
        </Col>
      </Row>
    </div>
    )
  }

  renderTitle=(counter,title)=>{
    if(counter == 1){
      return(
        <h2>#{title}</h2>
      )
    }else if(counter == 2){
      return(
        <h2>#{title}</h2>
      )
    }else if(counter ==3){
      return(
        <h3>#{title}</h3>
      )
    }else if(counter ==4){
      return(
        <h4>#{title}</h4>
      )
    }else if(counter ==5){
      return(
        <h5>#{title}</h5>
      )
    }else if(counter ==6){
      return(
        <h6>#{title}</h6>
      )
    }
  }

  renderChildrens=(allItem,counter)=>{
    if(counter === undefined){
      counter=2;
    }
    let count=counter
    if(allItem!==undefined){
      return(
        <div className="container" >
          {allItem.map((ele,i)=>(
            <div key={i} className="container pt-3">
                 {this.renderTitle(counter,ele.title)}
                  {this.renderData(ele)}
                  {ele.children!== undefined?
                        <div className="container">
                          {this.renderChildrens(ele.children,count+1)}
                        </div>
                    :''}
            </div>
          ))}
        </div> 
      )
    }
  }
   

  render() {
    const { allItem } = this.state;
    let counter=1;
      return (
        
      <div className="video-content">
        {this.props.id!=="1"?
        <div>
          <div className="card-view pt-5 pl-3 pr-3" style={{marginLeft:'0px'}}>
            <div className="container">
              {this.renderTitle(1,allItem.title)}
              {this.renderData(allItem)}
              {this.renderChildrens(allItem.children)}
            </div>
        </div>
        </div>
       :
       <div style={{margin:"200px auto", display:'table'}}>

       <p className="text">Coming Soon <span className="a">.</span> <span className="b">.</span> <span className="c">.</span> </p>

          <div className="haut">
            <div className="chap"></div>
          </div>
          <div className="bas">
            <div className="lim">
            <div className="les"></div>
            </div>
          </div>
          <div id="smoke">
            <span className="s0"></span>
            <span className="s1"></span>
            <span className="s2"></span>
            <span className="s3"></span>
            <span className="s4"></span>
            <span className="s5"></span>
            <span className="s6"></span>
            <span className="s7"></span>
            <span className="s8"></span>
            <span className="s9"></span>
          </div>


       <div>
       </div>
       </div>
  }
      </div>
    );
  }
}
