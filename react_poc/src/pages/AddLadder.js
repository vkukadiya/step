import React, { Component } from "react";
import {  Collapse, Navbar, NavbarBrand, Nav, Button, Form, FormGroup } from "reactstrap";
import FormLadder from "./FormLadder";

export default class AddLadder extends Component {

    constructor(props) {
        super(props)
        this.state = {
            versionModified: true,
            stepId: "78",
            title: "",
            videoId: {
                id: null,
                youtubeId: ""
            },
            startSeconds: 12,
            pauseSeconds: 19,
            tags: [],
            parent: "0",
            description: [
                {
                    value: "",
                    summary: "",
                    format: "basic_html"
                }
            ],
            like: [],
            code_snippet: [],
            images: [],
            is_reviewed: false,
            children: [{
                versionModified: false,
                stepId: 1,
                title: "",
                videoId: {
                    id: "211",
                    youtubeId: "PnzfCj7N9dY"
                },
                startSeconds: 0,
                pauseSeconds: 657,
                tags: [],
                parent: "78",
                description: [
                    {
                        value: "",
                        summary: "",
                        format: "basic_html"
                    }
                ],
                like: [
                    {
                        likes: "0",
                        dislikes: "0",
                        clicked_by: null
                    }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false,
                children: []
            }]
        }
    }

    onModeChange = mode => {
        this.props.onModeChange(mode);
        this.props.onMyClick();
    };

    onChange = (event) => {
        if (event.target.id.replace(/[^0-9]/g, '') === '') {
            if (event.target.name == "description") {
                this.setState({
                    [event.target.name]: [{
                        value: event.target.value,
                        summary: "",
                        format: "basic_html"
                    }]
                })
            } else {
                this.setState({
                    [event.target.name]: event.target.value
                })
            }
        } else {
            const newStateContent = this.state.children
            if (event.target.name == "description") {
                newStateContent[event.target.id.replace(/[^0-9]/g, '')][event.target.name] = [{
                    value: event.target.value,
                    summary: "",
                    format: "basic_html"
                }]
            } else {
                newStateContent[event.target.id.replace(/[^0-9]/g, '')][event.target.name] = event.target.value
            }
            this.setState({
                children: newStateContent
            });
        }
    }

    onBlur = (event) => {
        this.validation([{
            id: event.target.id,
            value: event.target.value
        }])
    }

    validation = (fileds) => {
        fileds.map((element) => {
            if (element.value === "") {
                if (document.getElementById(element.id).classList.contains('is-valid')) {
                    document.getElementById(element.id).classList.remove('is-valid')
                }
                if (!document.getElementById(element.id).classList.contains('is-invalid')) {
                    document.getElementById(element.id).classList.add('is-invalid')
                }
            } else {
                if (document.getElementById(element.id).classList.contains('is-invalid')) {
                    document.getElementById(element.id).classList.remove('is-invalid')
                }
                if (!document.getElementById(element.id).classList.contains('is-valid')) {
                    document.getElementById(element.id).classList.add('is-valid')
                }
            }
        })
    }

    formSubmit = event => {
        event.preventDefault()
        let fileds = [
            {
                id: 'title',
                value: this.state.title
            },
            {
                id: 'description',
                value: this.state.description[0].value
            }
        ]
        this.validation(fileds)
        if (document.getElementsByClassName("is-invalid").length === 0) {
            let ladderList = {
                parent: this.state,
            };
            this.props.onAddLadderSubmitForm({
                parent: this.state,
            })
            this.onModeChange("tree");
        }
    }

    render() {
        return (
            <div>
                <div className="container-fluid tutorial-nav">
                    <Navbar color="dark" dark expand="md">
                        <NavbarBrand href="/"> </NavbarBrand>
                        <Collapse navbar>
                            <span style={{ color: "#FFF", marginLeft: 20, fontSize: 25 }}>Add New Ladder</span>
                            <Nav className="ml-auto" navbar>
                                <Button onClick={() => this.onModeChange("tree")}>Go back </Button>
                            </Nav>
                        </Collapse>
                    </Navbar>
                </div>
                <div className="container p-5">
                    <Form onSubmit={this.formSubmit}>
                        <FormLadder id={""} data={this.state} onChange={this.onChange} onBlur={this.onBlur} />
                        <FormGroup>
                            <Button type="submit">Submit</Button>
                        </FormGroup>
                    </Form>
                </div>
            </div>
        )
    }
}
