import React, { Component } from "react";
import "react-toggle/style.css";
import { Collapse, Navbar, Form, FormGroup, Label, Input, NavbarBrand, Nav, NavItem, Button} from "reactstrap";
import Request from "../common/request";
import HistoryCard from "../components/poc_2/HistoryCard";
import { CompareData } from '../common/Dummy'
import Pagination from "react-js-pagination";

export default class History extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dropdownOpen: false,
      allItem: this.props.Items,
      activePage: 1,
      isChecked: false,
      dropdownText: 'Change version',
      versionName: localStorage.getItem('versionName')
    };
    this.toggle = this.toggle.bind(this);
    this.version = null;
  }
  request = new Request();

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  onModeChange = mode => {
    this.setState({ mode });
    this.props.onModeChange(mode);
    this.props.onMyClick();
  };

  componentWillMount() {
    this.setState({ Item: this.chunkArray(CompareData, 3, 'all'), PreservedItem: this.chunkArray(CompareData, 3, 'PreservedItem') });
  }

  handlePageChange = pageNumber => {
    this.setState({ activePage: pageNumber });
  };

  chunkArray = (myArray, chunk_size, type) => {
    var index = 0;
    var arrayLength = myArray.length;
    var tempArray = [];
    for (index = 0; index < arrayLength; index += chunk_size) {
      var myChunk = myArray.slice(index, index + chunk_size);
      if (type == 'all') {
        tempArray.push(myChunk);
      } else {
        myChunk[0] = myArray[0];
        tempArray.push(myChunk);
      }
    }
    return tempArray;
  };

  createMarkup = html => {
    return { __html: html };
  };

  clickCheckBox = e => {
    this.setState({ isChecked: !this.state.isChecked });
  };

  redirectToCompare = (node) => {
    this.props.history.push({
      pathname: `/compare`,
      state: { detail: node }
    })
  }

  renderComparisonView = isChecked => {
    let Item = []
    if (isChecked) {
      Item = this.state.PreservedItem
    } else {
      Item = this.state.Item
    }
    const TotalPage = Item.length;
    const { activePage } = this.state;

    return (
      <div className="history-wrap">
        <div className="row card-view">
          {Item[activePage - 1] != undefined && Item[activePage - 1][0] != undefined && (
            <div className="col-sm-4 card-current-version">
              <HistoryCard version={this.version} changeVersionValue={this.changeVersionValue} onSelectCompare={this.redirectToCompare} data={Item[activePage - 1][0]} title={Item[activePage - 1][0].title} backgroundColor="rgba(248,187,208 ,1)" borderColor="#333" />
            </div>
          )}
          {Item[activePage - 1] != undefined && Item[activePage - 1][1] != undefined && (
            <div className="col-sm-4 card-current-version">
              <HistoryCard version={this.version} changeVersionValue={this.changeVersionValue} onSelectCompare={this.redirectToCompare} data={Item[activePage - 1][1]} title={Item[activePage - 1][1].title} backgroundColor="rgba(248,187,208 ,1)" borderColor="#333" />
            </div>
          )}
          {Item[activePage - 1] != undefined && Item[activePage - 1][2] != undefined && (
            <div className="col-sm-4 card-current-version">
              <HistoryCard version={this.version} changeVersionValue={this.changeVersionValue} onSelectCompare={this.redirectToCompare} data={Item[activePage - 1][2]} title={Item[activePage - 1][2].title} backgroundColor="rgba(248,187,208 ,1)" borderColor="#333" />
            </div>
          )}
        </div>
        <Pagination
          activePage={this.state.activePage}
          itemsCountPerPage={1}
          totalItemsCount={TotalPage}
          pageRangeDisplayed={5}
          onChange={this.handlePageChange}
          innerClass="custom-pagination"
          itemClass="page-box"
          linkClass="page-link"
          activeClass="page-active"
        />
      </div>
    );
  };

  changeVersionValue = (stepId, title) => {
    this.version = stepId;
    this.setState({
      versionName: title
    });
  }

  changeVersion = () => {
    if (this.version != null) {
      localStorage.setItem('version', this.version);
    }
    this.onModeChange("tree");
  }

  render() {
    const { isChecked } = this.state;
    return (
      <div>
        <div className="container-fluid tutorial-nav">
          <Navbar color="dark" dark expand="md">
            <NavbarBrand href="/"> </NavbarBrand>

            <Collapse navbar>
              <span style={{ color: "#FFF", marginLeft: 20, fontSize: 25 }}>{this.props.Title.title}</span>
              <Nav className="ml-auto" navbar>
                <Button onClick={() => this.onModeChange("tree")}>Go back </Button>

                <NavItem />
              </Nav>
            </Collapse>
          </Navbar>
        </div>
        <div className="row">
          <div className="col-md-6">
            <div className="checkbox-view">
              <FormGroup check>
                <Label check>
                  <Input size="50" checked={isChecked} onChange={this.clickCheckBox} type="checkbox" /> Always compare with my version
                </Label>
              </FormGroup>
            </div>
          </div>
          <div className="col-md-6">
            <div className="dropdown-view">
              <Form inline>
                <FormGroup row>
                  <Label className="pr-2">{this.state.versionName}</Label>
                  <Button color="secondary" onClick={this.changeVersion}>Submit</Button>
                </FormGroup>
              </Form>
            </div>
          </div>
        </div>
        {this.renderComparisonView(isChecked)}
      </div>
    );
  }
}
