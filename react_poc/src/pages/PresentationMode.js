import React, { Component } from "react";
import { Button, Input, ButtonGroup,Row,Col } from "reactstrap";
import YouTube from "@u-wave/react-youtube";
import "react-toggle/style.css";
import { Carousel, CarouselItem} from "reactstrap";

import SortableTree, {
  map as mapTree,
  walk
} from "react-sortable-tree";
import Request from "../common/request";

import "react-sortable-tree/style.css"; // This only needs to be imported once in your app

export default class PresentationMode extends Component {
  stopPlayAt = null;
  stopPlayTimer;

  constructor(props) {
    super(props);
    this.state = {
      activeIndex: 0,
      sidebarOpen: true,
      isOpen: false,
      docked: true,
      baconIsReady: true,
      dropdownOpen: false,
      mode: "tree",
      data: [],
      treeData: this.props.TreeItems,
      activeImageIndex:0
    };
  }
  request = new Request();
  componentDidMount() {
  }

  onModeChange = mode => {
    this.setState({ mode });
    this.props.onModeChange(mode)
  };

  handleBaconChange = () => {
    this.setState({ baconIsReady: !this.state.baconIsReady });
  };

  onExiting = () => {
    this.animating = true;
  };

  onExited = () => {
    this.animating = false;
  };

  next = async () => {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === this.props.Items.length - 1 ? this.props.Items.length - 1 : this.state.activeIndex + 1;
    await this.state.event.target.stopVideo();
    await this.setState({ activeIndex: nextIndex });
    this.setActiveVideo(this.props.Items[this.state.activeIndex]);
  };

  previous = async () => {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? this.props.Items.length - 1 : this.state.activeIndex - 1;
    await this.state.event.target.stopVideo();
    await this.setState({ activeIndex: nextIndex });
    this.setActiveVideo(this.props.Items[this.state.activeIndex]);
  };

  goToIndex = async newIndex => {
    if (this.animating) return;
    await this.setState({ activeIndex: newIndex });
    this.setActiveVideo(this.props.Items[this.state.activeIndex]);
  };

  toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  };


  toggleBtn = () => {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }
  onSetSidebarOpen = () => {
    this.setState({ sidebarOpen: !this.state.sidebarOpen, docked: !this.state.docked });
  };

  setActiveVideo = activeVideo => {
    let getNodeKey = ({ node: object, treeIndex: number }) => {
      return number;
    };
    let newTree = [];
    walk({
      treeData: this.state.treeData,
      getNodeKey,
      ignoreCollapsed: false,
      callback: ({ node }) => {
        if (activeVideo.stepId == node.stepId) {
          node.isActive = true;
          node.expanded = true;
          newTree.push(node);
        } else {
          node.isActive = false;
          newTree.push(node);
        }
      }
    });
    this.setNewTreeData(newTree);
  };

  setNewTreeData = items => {
    this.request.arrayToTree(items).then(res => {
      this.setState({ treeData: res });
    });
  };

  onPlayerReady = event => {
    this.setState({ event });
  };

  onPlayerStateChange = event => {
    this.setState({ event });
  };

  onClickRow = rowInfo => {
    this.setActiveVideo(rowInfo.node);
    this.props.Items.map((item, i) => {
      if (rowInfo.node.stepId === item.stepId) this.goToIndex(i);
    });
  };

  createMarkup = html => {
    return { __html: html };
  };

  nextImage=(image)=>{
    if (this.imageanimating) return;
    const nextIndex = this.state.activeImageIndex === image.length - 1 ? 0 : this.state.activeImageIndex + 1;
    this.setState({activeImageIndex:nextIndex});
  }

  previousImage = (image) => {
    if (this.imageanimating) return;
    const nextIndex = this.state.activeImageIndex === 0 ? image.length - 1 : this.state.activeImageIndex - 1;
    this.setState({activeImageIndex:nextIndex});
  }

  goToIndex = (newIndex) => {
    if (this.imageanimating) return;
    this.setState({activeImageIndex:newIndex});
  }

  onExitingImage=()=>{
    this.imageanimating = true
  }

  onExitedImage=()=>{
    this.imageanimating = false
  }

  render() {
    const { activeIndex} = this.state;

    const slides = this.props.Items.map((item, i) => {
      return (
        <CarouselItem onExiting={this.onExiting} onExited={this.onExited} key={i} >
          <div className="instruction-block p-3 mb-5 bg-light rounded rounded" style={{ width: "100%", margin: "0 auto", height: "auto" }}>
            <div style={{ padding: 30 }}>
              <div style={{ textAlign: "center",color:"black" }}>
                <b style={{fontSize:"2rem"}}> {item.title}</b>
              </div>
              <div className="mt-5">
                  <Row>
                      <Col>
                      <div style={{color:"black", height:"50vh"}}>
                        <b>Video</b>
                        {item.videoId.youtubeId != "" ? (
                            <YouTube
                                video={item.videoId ? item.videoId.youtubeId : item.videoId}
                                autoplay={false}
                                showRelatedVideos={false}
                                width={"100%"}
                                height={350}
                                onReady={event => this.onPlayerReady(event)}
                                onStateChange={event => this.onPlayerStateChange(event)}
                            />
                            ) : (
                                <div>No Video found.</div>
                        )}
                      </div>
                      
                      <div style={{color:"black",height:"50vh", width:"50vw"}}>
                      <b>Image</b>
                
                      {item.images.length >0 ? 
                       <div>
                        <Carousel
                              activeIndex={this.state.activeImageIndex}
                              next={()=>this.nextImage(item.images)}
                              previous={()=>this.previousImage(item.images)}
                        >
                        {/* <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={goToIndex} /> */}
                        
                          {item.images.map((img,j)=>(
                            <CarouselItem
                            onExiting={this.onExitingImage} 
                            onExited={this.onExitedImage}
                            key={j}
                            >
                            <img src={img.url} alt={img.name} />
                            </CarouselItem>
                          ))}
                       
                        </Carousel>
                        </div>
                         :<div>No snapshots found.</div>}
                      </div>
                      </Col>
                      <Col>
                      <div style={{color:"black",height:"50vh"}}>
                        <b>Description</b>
                        {item.description != "" ? <div dangerouslySetInnerHTML={this.createMarkup(item.description[0].value)} /> : <div>No Description found.</div>}
                      </div>
                       <div style={{color:"black",height:"50vh"}}>
                        <b>Code Samples</b>
                       {item.code_snippet.length > 0 ? 
                            item.code_snippet.map((ele,i)=>{
                            return(
                                <div key={i}>
                                    <div className="code-header">
                                        {ele.title}
                                    </div>
                                    <div
                                    dangerouslySetInnerHTML={this.createMarkup(
                                        ele.help_text
                                    )}
                                    >
                                    </div>
                                    <div 
                                    dangerouslySetInnerHTML={this.createMarkup(
                                    ele.code_snippet
                                    )}
                                    >
                                    </div>
                                </div>)
                            })
                            :<div>No code samples found.</div>
                        }
                       </div>
                      </Col>
                  </Row>
              </div>
              
            </div>
          </div>
        </CarouselItem>
      );
    });

    return (
      <div className="video-content">

        <div className="container-fluid tutorial-nav">
        </div>
        <div className="coming-soon-left">{this.props.Title.title}</div>
        {this.props.Items.length>0 && (
        <div className="container-fluid">
          <div className="row tutorial-block">
            <div className="col-sm-12">
              <div />

              <div style={{ width: "100%", margin: "3% auto auto 0", display: "inline-block" }}>
                <Carousel activeIndex={activeIndex} next={this.next} previous={this.previous} interval={false} AutoPlay={false}>
                  {slides}
                </Carousel>
              </div>
              <div style={{ margin: "0 auto", display: "table" }}>
                {this.state.activeIndex !== 0 ? <Button onClick={this.previous}>Prev</Button> : null} &nbsp;
                {this.state.activeIndex !== this.props.Items.length - 1 ? <Button onClick={this.next}>Next</Button> : null}
              </div>
            </div>
          </div>
        </div>
    )}
      </div>
    );
  }
}
