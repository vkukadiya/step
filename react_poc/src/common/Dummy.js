const CompareData = [
    {
      title:"Current Version",
      data: {
        versionModified: true,
        stepId: "78",
        title: "Drupal NL-ladder",
        videoId: {
          id: null,
          youtubeId: ""
        },
        startSeconds: 12,
        pauseSeconds: 19,
        tags: [],
        parent: "0",
        description: [
          {
            value: "\u003Cp\u003EHulp bij de opleiding van Nederlandstalige Drupal websitebouwers, niveau beginners\u003C/p\u003E\r\n",
            summary: "",
            format: "basic_html"
          }
        ],
        like: [],
        code_snippet: [],
        images: [],
        is_reviewed: false,
        children: [
          {
            versionModified: true,
            stepId: "77",
            title: "Les 1: Inleiding en situering van Drupal",
            videoId: {
              id: "241",
              youtubeId: "LdF2RcelRg0"
            },
            startSeconds: 1,
            pauseSeconds: 4,
            tags: [
              {
                id: "1",
                name: "Drupal 7"
              },
              {
                id: "61",
                name: "Drupal 8"
              }
            ],
            parent: "78",
            description: [
              {
                value:
                  "\u003Ch2\u003EWhat is Lorem Ipsum?\u003C/h2\u003E\r\n\r\n\u003Cp\u003E\u003Cstrong\u003ELorem Ipsum\u003C/strong\u003E\u0026nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\u0027s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\u003C/p\u003E\r\n\r\n\u003Ch2\u003EWhy do we use it?\u003C/h2\u003E\r\n\r\n\u003Cp\u003EIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \u0027Content here, content here\u0027, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \u0027lorem ipsum\u0027 will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\u003C/p\u003E\r\n\r\n\u003Cp\u003E\u0026nbsp;\u003C/p\u003E\r\n\r\n\u003Ch2\u003E\u003Cstrong\u003EWhere does it come from?\u003C/strong\u003E\u003C/h2\u003E\r\n\r\n\u003Cp\u003EContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \u0022de Finibus Bonorum et Malorum\u0022 (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \u0022Lorem ipsum dolor sit amet..\u0022, comes from a line in section 1.10.32.\u003C/p\u003E\r\n\r\n\u003Cp\u003EThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \u0022de Finibus Bonorum et Malorum\u0022 by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.\u003C/p\u003E\r\n",
                summary: "",
                format: "basic_html"
              }
            ],
            like: [
              {
                likes: "1",
                dislikes: "0",
                clicked_by: "{\u002212\u0022:\u0022like\u0022,\u0022default\u0022:\u0022default\u0022}",
                action: null
              }
            ],
            code_snippet: [
              {
                value: "<h1>What is Lorem Ipsum?</h1>" + 
                      "\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>"
              }
            ],
            images: [
              {
                name: "bhavin",
                url: "https://images.pexels.com/photos/248797/pexels-photo-248797.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
              }
            ],
            is_reviewed: true,
            reviewed_id: "31",
            children: [
              {
                versionModified: true,
                stepId: "83",
                title: "iPhone 10 Years Later: The phone that almost wasn\u0027t",
                videoId: {
                  id: "27",
                  youtubeId: "FfXuxiO_Iqg"
                },
                startSeconds: "",
                pauseSeconds: 2,
                tags: [],
                parent: "77",
                description: [
                  {
                    value:
                      "\u003Ch3\u003EThe standard Lorem Ipsum passage, used since the 1500s\u003C/h3\u003E\r\n\r\n\u003Cp\u003E\u0022Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\u0022\u003C/p\u003E\r\n\r\n\u003Ch3\u003ESection 1.10.32 of \u0022de Finibus Bonorum et Malorum\u0022, written by Cicero in 45 BC\u003C/h3\u003E\r\n\r\n\u003Cp\u003E\u0022Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\u0022\u003C/p\u003E\r\n\r\n\u003Ch3\u003E1914 translation by H. Rackham\u003C/h3\u003E\r\n\r\n\u003Cp\u003E\u0022But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?\u0022\u003C/p\u003E\r\n",
                    summary: "",
                    format: "basic_html"
                  }
                ],
                like: [],
                code_snippet: [],
                images: [],
                is_reviewed: false,
                children: [
                  {
                    versionModified: true,
                    stepId: "88",
                    title: "iPhone \u2014 Hear what you read \u2014 Apple",
                    videoId: {
                      id: "29",
                      youtubeId: "2VA2H9XKyi4"
                    },
                    startSeconds: 1,
                    pauseSeconds: 12,
                    tags: [],
                    parent: "83",
                    description: [],
                    like: [
                      {
                        likes: "1",
                        dislikes: "1",
                        clicked_by: "{\u00221\u0022:\u0022like\u0022,\u002212\u0022:\u0022dislike\u0022,\u0022default\u0022:\u0022default\u0022}",
                        action: null
                      }
                    ],
                    code_snippet: [],
                    images: [],
                    is_reviewed: false
                  }
                ],
                expanded: false
              },
              {
                versionModified: false,
                stepId: "241",
                title: "doloremque laudantium, totam rem aperiam,",
                videoId: {
                  id: "206",
                  youtubeId: "2VA2H9XKyi4"
                },
                startSeconds: 0,
                pauseSeconds: 0,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: false,
                stepId: "251",
                title: "Why Do Electronics Die?",
                videoId: {
                  id: "261",
                  youtubeId: "iOVg62_DUYU"
                },
                startSeconds: 0,
                pauseSeconds: 348,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: false,
                stepId: "256",
                title: "How Does Fast Charging Work?",
                videoId: {
                  id: "176",
                  youtubeId: "Zsct0T5V03M"
                },
                startSeconds: 0,
                pauseSeconds: 266,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: true,
                stepId: "261",
                title: "Microsoft is Infuriating.",
                videoId: {
                  id: "181",
                  youtubeId: "PnzfCj7N9dY"
                },
                startSeconds: 0,
                pauseSeconds: 657,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: false,
                stepId: "266",
                title: "Microsoft is Infuriating new.",
                videoId: {
                  id: "226",
                  youtubeId: "PnzfCj7N9dY"
                },
                startSeconds: 0,
                pauseSeconds: 657,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: true,
                stepId: "286",
                title: "Amazing FREE Mac Utilities You Must Download!",
                videoId: {
                  id: null,
                  youtubeId: ""
                },
                startSeconds: "",
                pauseSeconds: "",
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: false,
                stepId: "301",
                title: "Not Everyone Should Code",
                videoId: {
                  id: "246",
                  youtubeId: "EFwa5Owp0-k"
                },
                startSeconds: 0,
                pauseSeconds: 521,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: true,
                stepId: "306",
                title: "The Worst Software Engineering Advice I\u0027ve Ever Seen",
                videoId: {
                  id: "251",
                  youtubeId: "Qo52KfvdUUY"
                },
                startSeconds: 0,
                pauseSeconds: 740,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: false,
                stepId: "311",
                title: "The Truth About Wireless Charging",
                videoId: {
                  id: "256",
                  youtubeId: "iOVg62_DUYU"
                },
                startSeconds: 0,
                pauseSeconds: 559,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              }
            ],
            expanded: false
          },
          {
            versionModified: false,
            stepId: "76",
            title: "Les 2: Installatie van Drupal",
            videoId: {
              id: null,
              youtubeId: ""
            },
            startSeconds: 23,
            pauseSeconds: 39,
            tags: [],
            parent: "78",
            description: [],
            like: [],
            code_snippet: [],
            images: [],
            is_reviewed: false,
            children: [
              {
                versionModified: false,
                stepId: "316",
                title: "The Future Of Software Development",
                videoId: {
                  id: "266",
                  youtubeId: "KwxnmeJsiQQ"
                },
                startSeconds: 0,
                pauseSeconds: 5,
                tags: [
                  {
                    id: "1",
                    name: "Drupal 7"
                  }
                ],
                parent: "76",
                description: [],
                like: [
                  {
                    likes: "1",
                    dislikes: "0",
                    clicked_by: "{\u00221\u0022:\u0022like\u0022,\u0022default\u0022:\u0022default\u0022}",
                    action: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: true,
                reviewed_id: "51"
              }
            ],
            expanded: false
          },
          {
            versionModified: true,
            stepId: "231",
            title: "Lorem ipsum dolor sit amet",
            videoId: {
              id: "201",
              youtubeId: "a2Y79QR-yKQ"
            },
            startSeconds: 0,
            pauseSeconds: 3,
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: true,
            reviewed_id: "36",
            children: [
              {
                versionModified: true,
                stepId: "236",
                title: "Sed ut perspiciatis unde omnis",
                videoId: {
                  id: null,
                  youtubeId: ""
                },
                startSeconds: "",
                pauseSeconds: "",
                tags: [],
                parent: "231",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "1",
                    clicked_by: "{\u002212\u0022:\u0022dislike\u0022,\u0022default\u0022:\u0022default\u0022}",
                    action: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: true,
                reviewed_id: "41"
              }
            ],
            expanded: false
          },
          {
            versionModified: true,
            stepId: "246",
            title: "Microsoft is Infuriating.",
            videoId: {
              id: "211",
              youtubeId: "PnzfCj7N9dY"
            },
            startSeconds: 0,
            pauseSeconds: 657,
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          },
          {
            versionModified: false,
            stepId: "271",
            title: "iPhone \u2014 Hear what you read \u2014 Apple",
            videoId: {
              id: "216",
              youtubeId: "2VA2H9XKyi4"
            },
            startSeconds: 0,
            pauseSeconds: 10,
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          },
          {
            versionModified: false,
            stepId: "276",
            title: "includes is not a function",
            videoId: {
              id: "221",
              youtubeId: "PnzfCj7N9dY"
            },
            startSeconds: 0,
            pauseSeconds: 657,
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          },
          {
            versionModified: false,
            stepId: "281",
            title: "recipient window\u0027s origin",
            videoId: {
              id: null,
              youtubeId: ""
            },
            startSeconds: "",
            pauseSeconds: "",
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          },
          {
            versionModified: true,
            stepId: "291",
            title: "iPhone \u2014 Hear what you read \u2014 Apple",
            videoId: {
              id: "231",
              youtubeId: "2VA2H9XKyi4"
            },
            startSeconds: 0,
            pauseSeconds: 10,
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "1",
                dislikes: "0",
                clicked_by: "{\u002212\u0022:\u0022like\u0022,\u0022default\u0022:\u0022default\u0022}",
                action: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: true,
            reviewed_id: "46"
          },
          {
            versionModified: true,
            stepId: "296",
            title: "xxxxxx",
            videoId: {
              id: null,
              youtubeId: ""
            },
            startSeconds: 0,
            pauseSeconds: 1694,
            tags: [
              {
                id: "66",
                name: "Drupal introduction"
              }
            ],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          }
        ],
        expanded: false
      }
    },
    {
      title:"Updated Version 1",
      data: {
        versionModified: true,
        stepId: "79",
        title: "D8 Multilingual Initiative",
        videoId: {
          id: null,
          youtubeId: ""
        },
        startSeconds: 12,
        pauseSeconds: 19,
        tags: [],
        parent: "0",
        description: [
          {
            value: "This ladder is an outline and the lessons need to be filled out.",
            summary: "",
            format: "basic_html"
          }
        ],
        like: [],
        code_snippet: [],
        images: [],
        is_reviewed: false,
        children: [
          {
            versionModified: false,
            stepId: "77",
            title: "Install Drupal in a non-English language",
            videoId: {
              id: "241",
              youtubeId: "LdF2RcelRg0"
            },
            startSeconds: 1,
            pauseSeconds: 4,
            tags: [
              {
                id: "1",
                name: "Drupal 7"
              },
              {
                id: "61",
                name: "Drupal 8"
              }
            ],
            parent: "78",
            description: [
              {
                value:
                  "This lesson will teach you how add a non-English language to your Drupal site, and use that language for the installation process.",
                summary: "",
                format: "basic_html"
              }
            ],
            like: [
              {
                likes: "1",
                dislikes: "0",
                clicked_by: "{\u002212\u0022:\u0022like\u0022,\u0022default\u0022:\u0022default\u0022}",
                action: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: true,
            reviewed_id: "31",
            children: [
              {
                versionModified: false,
                stepId: "83",
                title: "Translate interface from Drupal UI",
                videoId: {
                  id: "27",
                  youtubeId: "FfXuxiO_Iqg"
                },
                startSeconds: "",
                pauseSeconds: 2,
                tags: [],
                parent: "77",
                description: [],
                like: [],
                code_snippet: [],
                images: [],
                is_reviewed: false,
                children: [
                  {
                    versionModified: false,
                    stepId: "88",
                    title: "Add more languages and enable language switching",
                    videoId: {
                      id: "29",
                      youtubeId: "2VA2H9XKyi4"
                    },
                    startSeconds: 1,
                    pauseSeconds: 12,
                    tags: [],
                    parent: "83",
                    description: [],
                    like: [
                      {
                        likes: "1",
                        dislikes: "1",
                        clicked_by: "{\u00221\u0022:\u0022like\u0022,\u002212\u0022:\u0022dislike\u0022,\u0022default\u0022:\u0022default\u0022}",
                        action: null
                      }
                    ],
                    code_snippet: [],
                    images: [],
                    is_reviewed: false
                  }
                ],
                expanded: false
              },
              {
                versionModified: true,
                stepId: "241",
                title: "Make properties translatable",
                videoId: {
                  id: "206",
                  youtubeId: "2VA2H9XKyi4"
                },
                startSeconds: 0,
                pauseSeconds: 0,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: false,
                stepId: "251",
                title: "Automate process of pulling translation files down from localize.drupal.org",
                videoId: {
                  id: "261",
                  youtubeId: "iOVg62_DUYU"
                },
                startSeconds: 0,
                pauseSeconds: 348,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: false,
                stepId: "256",
                title: "Make nodes field-translatable",
                videoId: {
                  id: "176",
                  youtubeId: "Zsct0T5V03M"
                },
                startSeconds: 0,
                pauseSeconds: 266,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: true,
                stepId: "261",
                title: "Make properties translatable",
                videoId: {
                  id: "181",
                  youtubeId: "PnzfCj7N9dY"
                },
                startSeconds: 0,
                pauseSeconds: 657,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: false,
                stepId: "266",
                title: "Make UI for fields to be configured as translatable",
                videoId: {
                  id: "226",
                  youtubeId: "PnzfCj7N9dY"
                },
                startSeconds: 0,
                pauseSeconds: 657,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: true,
                stepId: "286",
                title: "Use t() function in php",
                videoId: {
                  id: null,
                  youtubeId: ""
                },
                startSeconds: "",
                pauseSeconds: "",
                tags: [],
                parent: "77",
                description: [
                  {
                    value: "Drupal's t() function is used to allow text to be translated. This is very helpful in the i18n of your Drupal site, so in this lesson you will learn how to use the t() function for PHP.",
                    summary: "",
                    format: "basic_html"
                  }
                ],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: false,
                stepId: "301",
                title: "Use the t() function in JavaScript",
                videoId: {
                  id: "246",
                  youtubeId: "EFwa5Owp0-k"
                },
                startSeconds: 0,
                pauseSeconds: 521,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: false,
                stepId: "306",
                title: "Block Visibility on a per-language Basis",
                videoId: {
                  id: "251",
                  youtubeId: "Qo52KfvdUUY"
                },
                startSeconds: 0,
                pauseSeconds: 740,
                tags: [],
                parent: "77",
                description: [
                  {
                    value: "Sometimes you want some blocks to only show for specific languages. This lesson shows you how to enable/disable blocks on a per-language basis.",
                    summary: "",
                    format: "basic_html"
                  }
                ],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: true,
                stepId: "311",
                title: "Work on a core issue",
                videoId: {
                  id: "256",
                  youtubeId: "iOVg62_DUYU"
                },
                startSeconds: 0,
                pauseSeconds: 559,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              }
            ],
            expanded: false
          },
          {
            versionModified: false,
            stepId: "76",
            title: "Find a core system that interests you and learn about it",
            videoId: {
              id: null,
              youtubeId: ""
            },
            startSeconds: 23,
            pauseSeconds: 39,
            tags: [],
            parent: "78",
            description: [
              {
                value: "DrupalLadder.org also has a growing library of lessons for learning about Drupal core. Check out these lessons by clicking the Lessons tab, and selecting Core Project type in the filter",
                summary: "",
                format: "basic_html"
              }
            ],
            like: [],
            code_snippet: [],
            images: [],
            is_reviewed: false,
            children: [
              {
                versionModified: false,
                stepId: "316",
                title: "Mobile Initiative",
                videoId: {
                  id: "266",
                  youtubeId: "KwxnmeJsiQQ"
                },
                startSeconds: 0,
                pauseSeconds: 5,
                tags: [
                  {
                    id: "1",
                    name: "Drupal 7"
                  }
                ],
                parent: "76",
                description: [
                  {
                    value: "To make Drupal a leading mobile CMS platform, the top issues we’ll need to address ",
                    summary: "",
                    format: "basic_html"
                  }
                ],
                like: [
                  {
                    likes: "1",
                    dislikes: "0",
                    clicked_by: "{\u00221\u0022:\u0022like\u0022,\u0022default\u0022:\u0022default\u0022}",
                    action: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: true,
                reviewed_id: "51"
              }
            ],
            expanded: false
          },
          {
            versionModified: true,
            stepId: "231",
            title: "Web Services and Context Core Initiative",
            videoId: {
              id: "201",
              youtubeId: "a2Y79QR-yKQ"
            },
            startSeconds: 0,
            pauseSeconds: 3,
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: true,
            reviewed_id: "36",
            children: [
              {
                versionModified: false,
                stepId: "236",
                title: "Design Initiative",
                videoId: {
                  id: null,
                  youtubeId: ""
                },
                startSeconds: "",
                pauseSeconds: "",
                tags: [],
                parent: "231",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "1",
                    clicked_by: "{\u002212\u0022:\u0022dislike\u0022,\u0022default\u0022:\u0022default\u0022}",
                    action: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: true,
                reviewed_id: "41"
              }
            ],
            expanded: false
          },
          {
            versionModified: true,
            stepId: "246",
            title: "HTML5 Initiative.",
            videoId: {
              id: "211",
              youtubeId: "PnzfCj7N9dY"
            },
            startSeconds: 0,
            pauseSeconds: 657,
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          },
          {
            versionModified: false,
            stepId: "271",
            title: "Ajax framework",
            videoId: {
              id: "216",
              youtubeId: "2VA2H9XKyi4"
            },
            startSeconds: 0,
            pauseSeconds: 10,
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          },
          {
            versionModified: true,
            stepId: "276",
            title: "cache.inc",
            videoId: {
              id: "221",
              youtubeId: "PnzfCj7N9dY"
            },
            startSeconds: 0,
            pauseSeconds: 657,
            tags: [],
            parent: "78",
            description: [
              {
                value: "Same filename and directory in other branches Functions and interfaces for cache handling.",
                summary: "",
                format: "basic_html"
              }
            ],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          },
          {
            versionModified: false,
            stepId: "281",
            title: "Database abstraction layer",
            videoId: {
              id: null,
              youtubeId: ""
            },
            startSeconds: "",
            pauseSeconds: "",
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          },
          {
            versionModified: false,
            stepId: "291",
            title: "system.module",
            videoId: {
              id: "231",
              youtubeId: "2VA2H9XKyi4"
            },
            startSeconds: 0,
            pauseSeconds: 10,
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "1",
                dislikes: "0",
                clicked_by: "{\u002212\u0022:\u0022like\u0022,\u0022default\u0022:\u0022default\u0022}",
                action: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: true,
            reviewed_id: "46"
          },
          {
            versionModified: true,
            stepId: "296",
            title: "Install Git",
            videoId: {
              id: null,
              youtubeId: ""
            },
            startSeconds: 0,
            pauseSeconds: 1694,
            tags: [
              {
                id: "66",
                name: "Drupal introduction"
              }
            ],
            parent: "78",
            description: [
              {
                value: "Install Git on your personal computer",
                summary: "",
                format: "basic_html"
              }
            ],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          }
        ],
        expanded: false
      }
    },
    {
      title:"Updated Version 2",
      data: {
        versionModified: true,
        stepId: "80",
        title: "Test patches",
        videoId: {
          id: null,
          youtubeId: ""
        },
        startSeconds: 12,
        pauseSeconds: 19,
        tags: [],
        parent: "0",
        description: [
          {
            value: "Test patches submitted to drupal.org for issues awaiting review.",
            summary: "",
            format: "basic_html"
          }
        ],
        like: [],
        code_snippet: [],
        images: [],
        is_reviewed: false,
        children: [
          {
            versionModified: true,
            stepId: "77",
            title: "Getting started in the issue queue",
            videoId: {
              id: "241",
              youtubeId: "LdF2RcelRg0"
            },
            startSeconds: 1,
            pauseSeconds: 4,
            tags: [
              {
                id: "1",
                name: "Drupal 7"
              },
              {
                id: "61",
                name: "Drupal 8"
              }
            ],
            parent: "78",
            description: [
              {
                value:
                  "By the end of this exercise you will know how to post and contribute to issues in the Drupal issue queue.",
                summary: "",
                format: "basic_html"
              }
            ],
            like: [
              {
                likes: "1",
                dislikes: "0",
                clicked_by: "{\u002212\u0022:\u0022like\u0022,\u0022default\u0022:\u0022default\u0022}",
                action: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: true,
            reviewed_id: "31",
            children: [
              {
                versionModified: false,
                stepId: "83",
                title: "Re-roll patches to apply cleanly to the latest version of Drupal",
                videoId: {
                  id: "27",
                  youtubeId: "FfXuxiO_Iqg"
                },
                startSeconds: "",
                pauseSeconds: 2,
                tags: [],
                parent: "77",
                description: [
                  {
                    value:
                      "Learn to update or re-roll patches to apply to a newer version of Drupal",
                    summary: "",
                    format: "basic_html"
                  }
                ],
                like: [],
                code_snippet: [],
                images: [],
                is_reviewed: false,
                children: [
                  {
                    versionModified: false,
                    stepId: "88",
                    title: "Find a core system that interests you and learn about it",
                    videoId: {
                      id: "29",
                      youtubeId: "2VA2H9XKyi4"
                    },
                    startSeconds: 1,
                    pauseSeconds: 12,
                    tags: [],
                    parent: "83",
                    description: [],
                    like: [
                      {
                        likes: "1",
                        dislikes: "1",
                        clicked_by: "{\u00221\u0022:\u0022like\u0022,\u002212\u0022:\u0022dislike\u0022,\u0022default\u0022:\u0022default\u0022}",
                        action: null
                      }
                    ],
                    code_snippet: [],
                    images: [],
                    is_reviewed: false
                  }
                ],
                expanded: false
              },
              {
                versionModified: true,
                stepId: "241",
                title: "Create or Join an issue team to work on an issue in the queue",
                videoId: {
                  id: "206",
                  youtubeId: "2VA2H9XKyi4"
                },
                startSeconds: 0,
                pauseSeconds: 0,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: false,
                stepId: "251",
                title: "Work on a core issue",
                videoId: {
                  id: "261",
                  youtubeId: "iOVg62_DUYU"
                },
                startSeconds: 0,
                pauseSeconds: 348,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: true,
                stepId: "256",
                title: "Write tests",
                videoId: {
                  id: "176",
                  youtubeId: "Zsct0T5V03M"
                },
                startSeconds: 0,
                pauseSeconds: 266,
                tags: [],
                parent: "77",
                description: [
                  {
                    value: "Learn to write an automated test for a Drupal core bug.",
                    summary: "",
                    format: "basic_html"
                  }
                ],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: false,
                stepId: "261",
                title: "Review and revise patches",
                videoId: {
                  id: "181",
                  youtubeId: "PnzfCj7N9dY"
                },
                startSeconds: 0,
                pauseSeconds: 657,
                tags: [],
                parent: "77",
                description: [
                  {
                    value: "Learn to review and revise patches submitted to the drupal.org issue queue.",
                    summary: "",
                    format: "basic_html"
                  }
                ],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: true,
                stepId: "266",
                title: "Schema.org ladder.",
                videoId: {
                  id: "226",
                  youtubeId: "PnzfCj7N9dY"
                },
                startSeconds: 0,
                pauseSeconds: 657,
                tags: [],
                parent: "77",
                description: [
                  {
                    value: "Learn how to setup schema.org on your site and benefit from improved search results on Google.",
                    summary: "",
                    format: "basic_html"
                  }
                ],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: true,
                stepId: "286",
                title: "Create schema.org fields for affiliation and job title",
                videoId: {
                  id: null,
                  youtubeId: ""
                },
                startSeconds: "",
                pauseSeconds: "",
                tags: [],
                parent: "77",
                description: [
                  {
                    value: "Complete the affiliation and job title properties.",
                    summary: "",
                    format: "basic_html"
                  }
                ],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: false,
                stepId: "301",
                title: "Create a schema.org field 'image'",
                videoId: {
                  id: "246",
                  youtubeId: "EFwa5Owp0-k"
                },
                startSeconds: 0,
                pauseSeconds: 521,
                tags: [],
                parent: "77",
                description: [
                  {
                    value: "you will learn how to create a field with the appropriate schema.org mappings",
                    summary: "",
                    format: "basic_html"
                  }
                ],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: false,
                stepId: "306",
                title: "Create a schema.org content type",
                videoId: {
                  id: "251",
                  youtubeId: "Qo52KfvdUUY"
                },
                startSeconds: 0,
                pauseSeconds: 740,
                tags: [],
                parent: "77",
                description: [
                  {
                    value: "you will learn how to create a content type with schema.org mappings",
                    summary: "",
                    format: "basic_html"
                  }
                ],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: false,
                stepId: "311",
                title: "Install the schema.org module",
                videoId: {
                  id: "256",
                  youtubeId: "iOVg62_DUYU"
                },
                startSeconds: 0,
                pauseSeconds: 559,
                tags: [],
                parent: "77",
                description: [
                  {
                    value: "you will learn how to enable the schema.org module",
                    summary: "",
                    format: "basic_html"
                  } 
                ],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              }
            ],
            expanded: false
          },
          {
            versionModified: false,
            stepId: "76",
            title: "User module",
            videoId: {
              id: null,
              youtubeId: ""
            },
            startSeconds: 23,
            pauseSeconds: 39,
            tags: [],
            parent: "78",
            description: [
              {
                value: "The user module manages all user account functions in a Drupal site.",
                summary: "",
                format: "basic_html"
              } 
            ],
            like: [],
            code_snippet: [],
            images: [],
            is_reviewed: false,
            children: [
              {
                versionModified: false,
                stepId: "316",
                title: "Determine who can create accounts (visitors or administrators)",
                videoId: {
                  id: "266",
                  youtubeId: "KwxnmeJsiQQ"
                },
                startSeconds: 0,
                pauseSeconds: 5,
                tags: [
                  {
                    id: "1",
                    name: "Drupal 7"
                  }
                ],
                parent: "76",
                description: [],
                like: [
                  {
                    likes: "1",
                    dislikes: "0",
                    clicked_by: "{\u00221\u0022:\u0022like\u0022,\u0022default\u0022:\u0022default\u0022}",
                    action: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: true,
                reviewed_id: "51"
              }
            ],
            expanded: false
          },
          {
            versionModified: false,
            stepId: "231",
            title: "Modify user email templates",
            videoId: {
              id: "201",
              youtubeId: "a2Y79QR-yKQ"
            },
            startSeconds: 0,
            pauseSeconds: 3,
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: true,
            reviewed_id: "36",
            children: [
              {
                versionModified: false,
                stepId: "236",
                title: "User Registration Notification and hook_mail()",
                videoId: {
                  id: null,
                  youtubeId: ""
                },
                startSeconds: "",
                pauseSeconds: "",
                tags: [],
                parent: "231",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "1",
                    clicked_by: "{\u002212\u0022:\u0022dislike\u0022,\u0022default\u0022:\u0022default\u0022}",
                    action: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: true,
                reviewed_id: "41"
              }
            ],
            expanded: false
          },
          {
            versionModified: true,
            stepId: "246",
            title: "Create roles to manage content",
            videoId: {
              id: "211",
              youtubeId: "PnzfCj7N9dY"
            },
            startSeconds: 0,
            pauseSeconds: 657,
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          },
          {
            versionModified: true,
            stepId: "271",
            title: "Fields and Entities Ladder",
            videoId: {
              id: "216",
              youtubeId: "2VA2H9XKyi4"
            },
            startSeconds: 0,
            pauseSeconds: 10,
            tags: [],
            parent: "78",
            description: [
              {
                value: "This curriculum will provide an introduction to fields and entities.",
                summary: "",
                format: "basic_html"
              } 
            ],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          },
          {
            versionModified: true,
            stepId: "276",
            title: "Customizing Display on the Node View",
            videoId: {
              id: "221",
              youtubeId: "PnzfCj7N9dY"
            },
            startSeconds: 0,
            pauseSeconds: 657,
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          },
          {
            versionModified: true,
            stepId: "281",
            title: "Using a Custom Content Type",
            videoId: {
              id: null,
              youtubeId: ""
            },
            startSeconds: "",
            pauseSeconds: "",
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          },
          {
            versionModified: false,
            stepId: "291",
            title: "Fields vs. Content Types",
            videoId: {
              id: "231",
              youtubeId: "2VA2H9XKyi4"
            },
            startSeconds: 0,
            pauseSeconds: 10,
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "1",
                dislikes: "0",
                clicked_by: "{\u002212\u0022:\u0022like\u0022,\u0022default\u0022:\u0022default\u0022}",
                action: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: true,
            reviewed_id: "46"
          },
          {
            versionModified: true,
            stepId: "296",
            title: "Types of fields",
            videoId: {
              id: null,
              youtubeId: ""
            },
            startSeconds: 0,
            pauseSeconds: 1694,
            tags: [
              {
                id: "66",
                name: "Drupal introduction"
              }
            ],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          }
        ],
        expanded: false
      }
    },
    {
      title:"Updated Version 3",
      data: {
        versionModified: true,
        stepId: "81",
        title: "Fields and Entities Ladder",
        videoId: {
          id: null,
          youtubeId: ""
        },
        startSeconds: 12,
        pauseSeconds: 19,
        tags: [],
        parent: "0",
        description: [
          {
            value: "This curriculum will provide an introduction to fields and entities.",
            summary: "",
            format: "basic_html"
          }
        ],
        like: [],
        code_snippet: [],
        images: [],
        is_reviewed: false,
        children: [
          {
            versionModified: false,
            stepId: "77",
            title: "Introduction to fields",
            videoId: {
              id: "241",
              youtubeId: "LdF2RcelRg0"
            },
            startSeconds: 1,
            pauseSeconds: 4,
            tags: [
              {
                id: "1",
                name: "Drupal 7"
              },
              {
                id: "61",
                name: "Drupal 8"
              }
            ],
            parent: "78",
            description: [
              {
                value:
                  "This lesson briefly describes the usage and components of fields on a content type.",
                summary: "",
                format: "basic_html"
              }
            ],
            like: [
              {
                likes: "1",
                dislikes: "0",
                clicked_by: "{\u002212\u0022:\u0022like\u0022,\u0022default\u0022:\u0022default\u0022}",
                action: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: true,
            reviewed_id: "31",
            children: [
              {
                versionModified: true,
                stepId: "83",
                title: "Using a Custom Content Type",
                videoId: {
                  id: "27",
                  youtubeId: "FfXuxiO_Iqg"
                },
                startSeconds: "",
                pauseSeconds: 2,
                tags: [],
                parent: "77",
                description: [
                  {
                    value:
                      "This lesson will teach you how to use a custom content type.",
                    summary: "",
                    format: "basic_html"
                  }
                ],
                like: [],
                code_snippet: [],
                images: [],
                is_reviewed: false,
                children: [
                  {
                    versionModified: true,
                    stepId: "88",
                    title: "Open source your project on GitHub",
                    videoId: {
                      id: "29",
                      youtubeId: "2VA2H9XKyi4"
                    },
                    startSeconds: 1,
                    pauseSeconds: 12,
                    tags: [],
                    parent: "83",
                    description: [],
                    like: [
                      {
                        likes: "1",
                        dislikes: "1",
                        clicked_by: "{\u00221\u0022:\u0022like\u0022,\u002212\u0022:\u0022dislike\u0022,\u0022default\u0022:\u0022default\u0022}",
                        action: null
                      }
                    ],
                    code_snippet: [],
                    images: [],
                    is_reviewed: false
                  }
                ],
                expanded: false
              },
              {
                versionModified: false,
                stepId: "241",
                title: "Share your project with selected users on GitHub",
                videoId: {
                  id: "206",
                  youtubeId: "2VA2H9XKyi4"
                },
                startSeconds: 0,
                pauseSeconds: 0,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: true,
                stepId: "251",
                title: "Get your own gov project code reviewed",
                videoId: {
                  id: "261",
                  youtubeId: "iOVg62_DUYU"
                },
                startSeconds: 0,
                pauseSeconds: 348,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: true,
                stepId: "256",
                title: "Contribute your project to Drupal by releasing and maintaining it on drupal.org",
                videoId: {
                  id: "176",
                  youtubeId: "Zsct0T5V03M"
                },
                startSeconds: 0,
                pauseSeconds: 266,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: false,
                stepId: "261",
                title: "Get comfortable with Drupal",
                videoId: {
                  id: "181",
                  youtubeId: "PnzfCj7N9dY"
                },
                startSeconds: 0,
                pauseSeconds: 657,
                tags: [],
                parent: "77",
                description: [
                  {
                    value: "Add content, taxonomy, etc. Configure settings and blocks. Add fields to entities.",
                    summary: "",
                    format: "basic_html"
                  } 
                ],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: true,
                stepId: "266",
                title: "Installing a new module in Drupal 8",
                videoId: {
                  id: "226",
                  youtubeId: "PnzfCj7N9dY"
                },
                startSeconds: 0,
                pauseSeconds: 657,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: false,
                stepId: "286",
                title: "Creating files directory, settings and services file",
                videoId: {
                  id: null,
                  youtubeId: ""
                },
                startSeconds: "",
                pauseSeconds: "",
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: true,
                stepId: "301",
                title: "Starting with Drush",
                videoId: {
                  id: "246",
                  youtubeId: "EFwa5Owp0-k"
                },
                startSeconds: 0,
                pauseSeconds: 521,
                tags: [],
                parent: "77",
                description: [
                  {
                    value: "In this ladder we'll learn to use Drush and how it can make our lives easier.",
                    summary: "",
                    format: "basic_html"
                  } 
                ],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: false,
                stepId: "306",
                title: "Custom Drush commands",
                videoId: {
                  id: "251",
                  youtubeId: "Qo52KfvdUUY"
                },
                startSeconds: 0,
                pauseSeconds: 740,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: true,
                stepId: "311",
                title: "Migrate your site using Drush",
                videoId: {
                  id: "256",
                  youtubeId: "iOVg62_DUYU"
                },
                startSeconds: 0,
                pauseSeconds: 559,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              }
            ],
            expanded: false
          },
          {
            versionModified: true,
            stepId: "76",
            title: "Drush commands for performing basic things",
            videoId: {
              id: null,
              youtubeId: ""
            },
            startSeconds: 23,
            pauseSeconds: 39,
            tags: [],
            parent: "78",
            description: [
              {
                value: "This tutorial will teach you to do some basic things on your Drupal site using Drush",
                summary: "",
                format: "basic_html"
              } 
            ],
            like: [],
            code_snippet: [],
            images: [],
            is_reviewed: false,
            children: [
              {
                versionModified: false,
                stepId: "316",
                title: "Backup and Restore using Drush",
                videoId: {
                  id: "266",
                  youtubeId: "KwxnmeJsiQQ"
                },
                startSeconds: 0,
                pauseSeconds: 5,
                tags: [
                  {
                    id: "1",
                    name: "Drupal 7"
                  }
                ],
                parent: "76",
                description: [
                  {
                    value: "This lesson teaches how to Backup and Restore your site data",
                    summary: "",
                    format: "basic_html"
                  } 
                ],
                like: [
                  {
                    likes: "1",
                    dislikes: "0",
                    clicked_by: "{\u00221\u0022:\u0022like\u0022,\u0022default\u0022:\u0022default\u0022}",
                    action: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: true,
                reviewed_id: "51"
              }
            ],
            expanded: false
          },
          {
            versionModified: true,
            stepId: "231",
            title: "Updating core, modules and database using Drush",
            videoId: {
              id: "201",
              youtubeId: "a2Y79QR-yKQ"
            },
            startSeconds: 0,
            pauseSeconds: 3,
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: true,
            reviewed_id: "36",
            children: [
              {
                versionModified: true,
                stepId: "236",
                title: "Using Sql and devel-generate commands for Drush",
                videoId: {
                  id: null,
                  youtubeId: ""
                },
                startSeconds: "",
                pauseSeconds: "",
                tags: [],
                parent: "231",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "1",
                    clicked_by: "{\u002212\u0022:\u0022dislike\u0022,\u0022default\u0022:\u0022default\u0022}",
                    action: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: true,
                reviewed_id: "41"
              }
            ],
            expanded: false
          },
          {
            versionModified: true,
            stepId: "246",
            title: "Clean up HTML output for Drupal 8",
            videoId: {
              id: "211",
              youtubeId: "PnzfCj7N9dY"
            },
            startSeconds: 0,
            pauseSeconds: 657,
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          },
          {
            versionModified: false,
            stepId: "271",
            title: "Manually test a theme function to twig template conversion",
            videoId: {
              id: "216",
              youtubeId: "2VA2H9XKyi4"
            },
            startSeconds: 0,
            pauseSeconds: 10,
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          },
          {
            versionModified: true,
            stepId: "276",
            title: "Review and revise Twig patches",
            videoId: {
              id: "221",
              youtubeId: "PnzfCj7N9dY"
            },
            startSeconds: 0,
            pauseSeconds: 657,
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          },
          {
            versionModified: false,
            stepId: "281",
            title: "Re-roll Twig patches",
            videoId: {
              id: null,
              youtubeId: ""
            },
            startSeconds: "",
            pauseSeconds: "",
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          },
          {
            versionModified: true,
            stepId: "291",
            title: "Convert a PHP template file into a Twig template",
            videoId: {
              id: "231",
              youtubeId: "2VA2H9XKyi4"
            },
            startSeconds: 0,
            pauseSeconds: 10,
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "1",
                dislikes: "0",
                clicked_by: "{\u002212\u0022:\u0022like\u0022,\u0022default\u0022:\u0022default\u0022}",
                action: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: true,
            reviewed_id: "46"
          },
          {
            versionModified: true,
            stepId: "296",
            title: "Convert a theme function into a Twig template",
            videoId: {
              id: null,
              youtubeId: ""
            },
            startSeconds: 0,
            pauseSeconds: 1694,
            tags: [
              {
                id: "66",
                name: "Drupal introduction"
              }
            ],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          }
        ],
        expanded: false
      }
    },
    {
      title:"Updated Version 4",
      data: {
        versionModified: true,
        stepId: "82",
        title: "D8 Twig theme system",
        videoId: {
          id: null,
          youtubeId: ""
        },
        startSeconds: 12,
        pauseSeconds: 19,
        tags: [],
        parent: "0",
        description: [
          {
            value: "This ladder leads through contributing to the Twig in Core project for Drupal 8. It assumes that you have completed at least the first 5 steps of the main Drupal core ladder, and are familiar with Drupal 7 core theme templates and functions.",
            summary: "",
            format: "basic_html"
          }
        ],
        like: [],
        code_snippet: [],
        images: [],
        is_reviewed: false,
        children: [
          {
            versionModified: false,
            stepId: "77",
            title: "Consolidate duplicate or similar templates",
            videoId: {
              id: "241",
              youtubeId: "LdF2RcelRg0"
            },
            startSeconds: 1,
            pauseSeconds: 4,
            tags: [
              {
                id: "1",
                name: "Drupal 7"
              },
              {
                id: "61",
                name: "Drupal 8"
              }
            ],
            parent: "78",
            description: [
              {
                value:
                  "In this lesson you will consolidate templates in Drupal core.",
                summary: "",
                format: "basic_html"
              }
            ],
            like: [
              {
                likes: "1",
                dislikes: "0",
                clicked_by: "{\u002212\u0022:\u0022like\u0022,\u0022default\u0022:\u0022default\u0022}",
                action: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: true,
            reviewed_id: "31",
            children: [
              {
                versionModified: true,
                stepId: "83",
                title: "Benchmark a Twig template conversion",
                videoId: {
                  id: "27",
                  youtubeId: "FfXuxiO_Iqg"
                },
                startSeconds: "",
                pauseSeconds: 2,
                tags: [],
                parent: "77",
                description: [
                  {
                    value:
                      "In this lesson you will test the performance implications of a specific twig template conversion",
                    summary: "",
                    format: "basic_html"
                  }
                ],
                like: [],
                code_snippet: [],
                images: [],
                is_reviewed: false,
                children: [
                  {
                    versionModified: false,
                    stepId: "88",
                    title: "Re-roll Twig patches",
                    videoId: {
                      id: "29",
                      youtubeId: "2VA2H9XKyi4"
                    },
                    startSeconds: 1,
                    pauseSeconds: 12,
                    tags: [],
                    parent: "83",
                    description: [
                      {
                        value:
                          "Learn to update or re-roll patches to apply to a newer version of Drupal",
                        summary: "",
                        format: "basic_html"
                      }
                    ],
                    like: [
                      {
                        likes: "1",
                        dislikes: "1",
                        clicked_by: "{\u00221\u0022:\u0022like\u0022,\u002212\u0022:\u0022dislike\u0022,\u0022default\u0022:\u0022default\u0022}",
                        action: null
                      }
                    ],
                    code_snippet: [],
                    images: [],
                    is_reviewed: false
                  }
                ],
                expanded: false
              },
              {
                versionModified: true,
                stepId: "241",
                title: "Getting started with Drupal for GCI Students",
                videoId: {
                  id: "206",
                  youtubeId: "2VA2H9XKyi4"
                },
                startSeconds: 0,
                pauseSeconds: 0,
                tags: [],
                parent: "77",
                description: [
                  {
                    value:
                      "In this ladder we'll talk about various resources and materials that will help you to get started to contributing to Drupal for Google code in quickly.",
                    summary: "",
                    format: "basic_html"
                  }
                ],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: true,
                stepId: "251",
                title: "Some important things",
                videoId: {
                  id: "261",
                  youtubeId: "iOVg62_DUYU"
                },
                startSeconds: 0,
                pauseSeconds: 348,
                tags: [],
                parent: "77",
                description: [
                  {
                    value:
                      "In this final lesson, we'll learn some tips and more resources",
                    summary: "",
                    format: "basic_html"
                  }
                ],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: true,
                stepId: "256",
                title: "What to do now? (Important resources)",
                videoId: {
                  id: "176",
                  youtubeId: "Zsct0T5V03M"
                },
                startSeconds: 0,
                pauseSeconds: 266,
                tags: [],
                parent: "77",
                description: [
                  {
                    value:
                      "You would learn about various places from where you can learn about Drupal",
                    summary: "",
                    format: "basic_html"
                  }
                ],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: false,
                stepId: "261",
                title: "Test patches",
                videoId: {
                  id: "181",
                  youtubeId: "PnzfCj7N9dY"
                },
                startSeconds: 0,
                pauseSeconds: 657,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: true,
                stepId: "266",
                title: "Getting started in the issue queue",
                videoId: {
                  id: "226",
                  youtubeId: "PnzfCj7N9dY"
                },
                startSeconds: 0,
                pauseSeconds: 657,
                tags: [],
                parent: "77",
                description: [
                  {
                    value:
                      "By the end of this exercise you will know how to post and contribute to issues in the Drupal issue queue.",
                    summary: "",
                    format: "basic_html"
                  }
                ],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: true,
                stepId: "286",
                title: "Introduction to the GSoC",
                videoId: {
                  id: null,
                  youtubeId: ""
                },
                startSeconds: "",
                pauseSeconds: "",
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: false,
                stepId: "301",
                title: "Install Drupal 8 Locally",
                videoId: {
                  id: "246",
                  youtubeId: "EFwa5Owp0-k"
                },
                startSeconds: 0,
                pauseSeconds: 521,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: true,
                stepId: "306",
                title: "Installing Drush on Windows",
                videoId: {
                  id: "251",
                  youtubeId: "Qo52KfvdUUY"
                },
                startSeconds: 0,
                pauseSeconds: 740,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: false,
                stepId: "311",
                title: "Getting Started with Drupal for GSoC Students",
                videoId: {
                  id: "256",
                  youtubeId: "iOVg62_DUYU"
                },
                startSeconds: 0,
                pauseSeconds: 559,
                tags: [],
                parent: "77",
                description: [
                  {
                    value:
                      "This ladder is designed to give an introduction to participating in the Google Summer of Code with Drupal.",
                    summary: "",
                    format: "basic_html"
                  }
                ],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              }
            ],
            expanded: false
          },
          {
            versionModified: false,
            stepId: "76",
            title: "Installing Drupal 8 on Arch linux using LAMP stack",
            videoId: {
              id: null,
              youtubeId: ""
            },
            startSeconds: 23,
            pauseSeconds: 39,
            tags: [],
            parent: "78",
            description: [],
            like: [],
            code_snippet: [],
            images: [],
            is_reviewed: false,
            children: [
              {
                versionModified: true,
                stepId: "316",
                title: "Document Root and AllowOverride in Apache's config for Arch Linux",
                videoId: {
                  id: "266",
                  youtubeId: "KwxnmeJsiQQ"
                },
                startSeconds: 0,
                pauseSeconds: 5,
                tags: [
                  {
                    id: "1",
                    name: "Drupal 7"
                  }
                ],
                parent: "76",
                description: [],
                like: [
                  {
                    likes: "1",
                    dislikes: "0",
                    clicked_by: "{\u00221\u0022:\u0022like\u0022,\u0022default\u0022:\u0022default\u0022}",
                    action: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: true,
                reviewed_id: "51"
              }
            ],
            expanded: false
          },
          {
            versionModified: true,
            stepId: "231",
            title: "Installing PHP on Arch Linux",
            videoId: {
              id: "201",
              youtubeId: "a2Y79QR-yKQ"
            },
            startSeconds: 0,
            pauseSeconds: 3,
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: true,
            reviewed_id: "36",
            children: [
              {
                versionModified: false,
                stepId: "236",
                title: "Installing MySQL on Arch Linux",
                videoId: {
                  id: null,
                  youtubeId: ""
                },
                startSeconds: "",
                pauseSeconds: "",
                tags: [],
                parent: "231",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "1",
                    clicked_by: "{\u002212\u0022:\u0022dislike\u0022,\u0022default\u0022:\u0022default\u0022}",
                    action: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: true,
                reviewed_id: "41"
              }
            ],
            expanded: false
          },
          {
            versionModified: false,
            stepId: "246",
            title: "Installing Apache on Arch-Linux",
            videoId: {
              id: "211",
              youtubeId: "PnzfCj7N9dY"
            },
            startSeconds: 0,
            pauseSeconds: 657,
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          },
          {
            versionModified: true,
            stepId: "271",
            title: "Installing a new module in Drupal 8",
            videoId: {
              id: "216",
              youtubeId: "2VA2H9XKyi4"
            },
            startSeconds: 0,
            pauseSeconds: 10,
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          },
          {
            versionModified: true,
            stepId: "276",
            title: "GCI mentor guide",
            videoId: {
              id: "221",
              youtubeId: "PnzfCj7N9dY"
            },
            startSeconds: 0,
            pauseSeconds: 657,
            tags: [],
            parent: "78",
            description: [
              {
                value:
                  "This guide would help Drupal mentors to get started for GCI easily and quickly",
                summary: "",
                format: "basic_html"
              }
            ],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          },
          {
            versionModified: false,
            stepId: "281",
            title: "Guidelines for Mentors",
            videoId: {
              id: null,
              youtubeId: ""
            },
            startSeconds: "",
            pauseSeconds: "",
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          },
          {
            versionModified: false,
            stepId: "291",
            title: "Guide to Melange for mentors",
            videoId: {
              id: "231",
              youtubeId: "2VA2H9XKyi4"
            },
            startSeconds: 0,
            pauseSeconds: 10,
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "1",
                dislikes: "0",
                clicked_by: "{\u002212\u0022:\u0022like\u0022,\u0022default\u0022:\u0022default\u0022}",
                action: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: true,
            reviewed_id: "46"
          },
          {
            versionModified: true,
            stepId: "296",
            title: "How GCI works?",
            videoId: {
              id: null,
              youtubeId: ""
            },
            startSeconds: 0,
            pauseSeconds: 1694,
            tags: [
              {
                id: "66",
                name: "Drupal introduction"
              }
            ],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          }
        ],
        expanded: false
      }
    },
    {
      title:"Updated Version 5",
      data: {
        versionModified: true,
        stepId: "83",
        title: "GSoC Mentor guide",
        videoId: {
          id: null,
          youtubeId: ""
        },
        startSeconds: 12,
        pauseSeconds: 19,
        tags: [],
        parent: "0",
        description: [
          {
            value: "Get started with GSoC as a mentor!",
            summary: "",
            format: "basic_html"
          }
        ],
        like: [],
        code_snippet: [],
        images: [],
        is_reviewed: false,
        children: [
          {
            versionModified: false,
            stepId: "77",
            title: "Introduction to the GSoC",
            videoId: {
              id: "241",
              youtubeId: "LdF2RcelRg0"
            },
            startSeconds: 1,
            pauseSeconds: 4,
            tags: [
              {
                id: "1",
                name: "Drupal 7"
              },
              {
                id: "61",
                name: "Drupal 8"
              }
            ],
            parent: "78",
            description: [
              {
                value:
                  "This lesson gives information about contributing to Drupal in the Google Summer of Code (GSoC).",
                summary: "",
                format: "basic_html"
              }
            ],
            like: [
              {
                likes: "1",
                dislikes: "0",
                clicked_by: "{\u002212\u0022:\u0022like\u0022,\u0022default\u0022:\u0022default\u0022}",
                action: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: true,
            reviewed_id: "31",
            children: [
              {
                versionModified: false,
                stepId: "83",
                title: "Managing students with Virtual SCRUM meetings",
                videoId: {
                  id: "27",
                  youtubeId: "FfXuxiO_Iqg"
                },
                startSeconds: "",
                pauseSeconds: 2,
                tags: [],
                parent: "77",
                description: [
                  {
                    value:
                      "This lesson teaches the basics of how students and mentors should communicated during the GSoC.",
                    summary: "",
                    format: "basic_html"
                  }
                ],
                like: [],
                code_snippet: [],
                images: [],
                is_reviewed: false,
                children: [
                  {
                    versionModified: true,
                    stepId: "88",
                    title: "Student/Mentor Conflict Resolution",
                    videoId: {
                      id: "29",
                      youtubeId: "2VA2H9XKyi4"
                    },
                    startSeconds: 1,
                    pauseSeconds: 12,
                    tags: [],
                    parent: "83",
                    description: [],
                    like: [
                      {
                        likes: "1",
                        dislikes: "1",
                        clicked_by: "{\u00221\u0022:\u0022like\u0022,\u002212\u0022:\u0022dislike\u0022,\u0022default\u0022:\u0022default\u0022}",
                        action: null
                      }
                    ],
                    code_snippet: [],
                    images: [],
                    is_reviewed: false
                  }
                ],
                expanded: false
              },
              {
                versionModified: true,
                stepId: "241",
                title: "How midterms/finals work",
                videoId: {
                  id: "206",
                  youtubeId: "2VA2H9XKyi4"
                },
                startSeconds: 0,
                pauseSeconds: 0,
                tags: [],
                parent: "77",
                description: [
                  {
                    value: "This lesson helps you to understand how midterms/finals work and what is the procedure that you need to follow in order to make it things well.",
                    summary: "",
                    format: "basic_html"
                  }
                ],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: true,
                stepId: "251",
                title: "Introduction to GCI",
                videoId: {
                  id: "261",
                  youtubeId: "iOVg62_DUYU"
                },
                startSeconds: 0,
                pauseSeconds: 348,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: true,
                stepId: "256",
                title: "Ladder to Site-building",
                videoId: {
                  id: "176",
                  youtubeId: "Zsct0T5V03M"
                },
                startSeconds: 0,
                pauseSeconds: 266,
                tags: [],
                parent: "77",
                description: [
                  {
                    value: "Start from the bottom of the Ladder",
                    summary: "",
                    format: "basic_html"
                  }
                ],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: false,
                stepId: "261",
                title: "Build views",
                videoId: {
                  id: "181",
                  youtubeId: "PnzfCj7N9dY"
                },
                startSeconds: 0,
                pauseSeconds: 657,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: true,
                stepId: "266",
                title: "follow other Drupallers",
                videoId: {
                  id: "226",
                  youtubeId: "PnzfCj7N9dY"
                },
                startSeconds: 0,
                pauseSeconds: 657,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: false,
                stepId: "286",
                title: "Join drupal groups",
                videoId: {
                  id: null,
                  youtubeId: ""
                },
                startSeconds: "",
                pauseSeconds: "",
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: true,
                stepId: "301",
                title: "Join IRC channels",
                videoId: {
                  id: "246",
                  youtubeId: "EFwa5Owp0-k"
                },
                startSeconds: 0,
                pauseSeconds: 521,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: true,
                stepId: "306",
                title: "Join D.O(Drupal.org)",
                videoId: {
                  id: "251",
                  youtubeId: "Qo52KfvdUUY"
                },
                startSeconds: 0,
                pauseSeconds: 740,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              },
              {
                versionModified: false,
                stepId: "311",
                title: "Add multilingual functionality",
                videoId: {
                  id: "256",
                  youtubeId: "iOVg62_DUYU"
                },
                startSeconds: 0,
                pauseSeconds: 559,
                tags: [],
                parent: "77",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "0",
                    clicked_by: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: false
              }
            ],
            expanded: false
          },
          {
            versionModified: true,
            stepId: "76",
            title: "Twig security",
            videoId: {
              id: null,
              youtubeId: ""
            },
            startSeconds: 23,
            pauseSeconds: 39,
            tags: [],
            parent: "78",
            description: [],
            like: [],
            code_snippet: [],
            images: [],
            is_reviewed: false,
            children: [
              {
                versionModified: true,
                stepId: "316",
                title: "add slideshow",
                videoId: {
                  id: "266",
                  youtubeId: "KwxnmeJsiQQ"
                },
                startSeconds: 0,
                pauseSeconds: 5,
                tags: [
                  {
                    id: "1",
                    name: "Drupal 7"
                  }
                ],
                parent: "76",
                description: [],
                like: [
                  {
                    likes: "1",
                    dislikes: "0",
                    clicked_by: "{\u00221\u0022:\u0022like\u0022,\u0022default\u0022:\u0022default\u0022}",
                    action: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: true,
                reviewed_id: "51"
              }
            ],
            expanded: false
          },
          {
            versionModified: true,
            stepId: "231",
            title: "Les 3: Rondleiding en installatie van Drupal",
            videoId: {
              id: "201",
              youtubeId: "a2Y79QR-yKQ"
            },
            startSeconds: 0,
            pauseSeconds: 3,
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: true,
            reviewed_id: "36",
            children: [
              {
                versionModified: false,
                stepId: "236",
                title: "Beheerderstools",
                videoId: {
                  id: null,
                  youtubeId: ""
                },
                startSeconds: "",
                pauseSeconds: "",
                tags: [],
                parent: "231",
                description: [],
                like: [
                  {
                    likes: "0",
                    dislikes: "1",
                    clicked_by: "{\u002212\u0022:\u0022dislike\u0022,\u0022default\u0022:\u0022default\u0022}",
                    action: null
                  }
                ],
                code_snippet: [],
                images: [],
                is_reviewed: true,
                reviewed_id: "41"
              }
            ],
            expanded: false
          },
          {
            versionModified: true,
            stepId: "246",
            title: "Les 4: Basisfunctionaliteiten van Drupal",
            videoId: {
              id: "211",
              youtubeId: "PnzfCj7N9dY"
            },
            startSeconds: 0,
            pauseSeconds: 657,
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          },
          {
            versionModified: false,
            stepId: "271",
            title: "Modules",
            videoId: {
              id: "216",
              youtubeId: "2VA2H9XKyi4"
            },
            startSeconds: 0,
            pauseSeconds: 10,
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          },
          {
            versionModified: false,
            stepId: "276",
            title: "Les 5: Inhoud toevoegen",
            videoId: {
              id: "221",
              youtubeId: "PnzfCj7N9dY"
            },
            startSeconds: 0,
            pauseSeconds: 657,
            tags: [],
            parent: "78",
            description: [
              {
                value: "In dit hoofdstuk leer je inhoud toevoegen aan je website",
                summary: "",
                format: "basic_html"
              }
            ],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          },
          {
            versionModified: true,
            stepId: "281",
            title: "Inhoud aanmaken",
            videoId: {
              id: null,
              youtubeId: ""
            },
            startSeconds: "",
            pauseSeconds: "",
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          },
          {
            versionModified: true,
            stepId: "291",
            title: "Artikel",
            videoId: {
              id: "231",
              youtubeId: "2VA2H9XKyi4"
            },
            startSeconds: 0,
            pauseSeconds: 10,
            tags: [],
            parent: "78",
            description: [],
            like: [
              {
                likes: "1",
                dislikes: "0",
                clicked_by: "{\u002212\u0022:\u0022like\u0022,\u0022default\u0022:\u0022default\u0022}",
                action: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: true,
            reviewed_id: "46"
          },
          {
            versionModified: true,
            stepId: "296",
            title: "Pagina",
            videoId: {
              id: null,
              youtubeId: ""
            },
            startSeconds: 0,
            pauseSeconds: 1694,
            tags: [
              {
                id: "66",
                name: "Drupal introduction"
              }
            ],
            parent: "78",
            description: [],
            like: [
              {
                likes: "0",
                dislikes: "0",
                clicked_by: null
              }
            ],
            code_snippet: [],
            images: [],
            is_reviewed: false
          }
        ],
        expanded: false
      }
    }
  ];

  const baseLadder=[
    {
      "stepId":"13376",
      "id" : Math.random().toString(36).substr(2,8),
      "author":"1",
      "title":"make a hot, delicious drink",
      "videoId":{
        "id":null,
        "title":"",
        "youtubeId":"",
        "url":""
      },
      "startSeconds":"",
      "pauseSeconds":"",
      "tags":[{
        "id":"1421",
        "name":"indian masala tea"
      }],
      "parent":"0",
      "description":[],
      "likes":{
        "like":0,
        "dislike":0,
        "type":""
      },
      "code_snippet":[{
        "title":null,
        "code_snippet":null,
        "help_text":null
      }],
      "images":[],
      "score":null,
      "last_updated":"1574253401",
      "revision_exist":true,
      "is_reviewed":false,
      "children":[
        {
        "stepId":
        "13381",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"boil 1\/2 cup pure water",
        "videoId":{
          "id":"3436",
          "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc","url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
        },
        "startSeconds":"",
        "pauseSeconds":"",
        "tags":[{
          "id":"1421",
          "name":"indian masala tea"
        }],
        "parent":"13376",
        "description":[],
        "likes":{
          "like":0,
          "dislike":0,
          "type":""
        },
        "code_snippet":[],
        "images":[],
        "score":null,
        "last_updated":"1574252156",
        "is_reviewed":false
      },
      {
        "stepId":"13386",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add the major flavors to the boiling water",
        "videoId":{
          "id":null,
          "title":"",
          "youtubeId":"",
          "url":""
        },
        "startSeconds":"",
        "pauseSeconds":"",
        "tags":[],
        "parent":"13376",
        "description":[],
        "likes":{
          "like":0,
          "dislike":0,
          "type":""
        },
        "code_snippet":[],
        "images":[],
        "score":null,
        "last_updated":"1574252667",
        "is_reviewed":false,
        "children":[
          {
          "stepId":"13391",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"add 1 tablespoon tea leaves\t",
          "videoId":{
            "id":"3441",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc","url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
          },
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{
            "id":"1421",
            "name":"indian masala tea"
          }],
          "parent":"13386",
          "description":[],
          "likes":{
            "like":0,
            "dislike":0,
            "type":""
          },
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252341",
          "is_reviewed":false
        },
        {
          "stepId":"13396",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"add 1 tablespoon ground coffee",
          "videoId":{
            "id":"3446",
            "title":"@Boiling Coffee using sand  :|#Made on hot sand |Like Turkish Cofee || Apple Street Food","youtubeId":"7WonfypKDpM",
            "url":"https:\/\/www.youtube.com\/watch?v=7WonfypKDpM"
          },
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{
            "id":"1426",
            "name":"sand coffee"
          }],
          "parent":"13386",
          "description":[],
          "likes":{
            "like":0,
            "dislike":0,
            "type":""
          },
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252482",
          "is_reviewed":false
        },
        {
          "stepId":"13401",
          "author":"1",
          "id" : Math.random().toString(36).substr(2,8),
          "title":"add 1 tablespoon sugar",
          "videoId":{
            "id":"3451",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
            "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"},
            "startSeconds":"",
            "pauseSeconds":"",
            "tags":[{
              "id":"1421",
              "name":"indian masala tea"
            }],
            "parent":"13386",
            "description":[],
            "likes":{"like":0,"dislike":0,"type":""},
            "code_snippet":[],
            "images":[],
            "score":null,
            "last_updated":"1574252576",
            "is_reviewed":false
          },
          {
            "stepId":"13406",
            "author":"1",
            "id" : Math.random().toString(36).substr(2,8),
            "title":"grate a thumb-sized piece of ginger into boiling tea",
            "videoId":{
              "id":"3456",
              "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
              "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
            },
            "startSeconds":7,
            "pauseSeconds":11,
            "tags":[{"id":"1421","name":"indian masala tea"}],
            "parent":"13386",
            "description":[],
            "likes":{"like":0,"dislike":0,"type":""},
            "code_snippet":[],
            "images":[],
            "score":null,
            "last_updated":"1574252653",
            "is_reviewed":false}],
            "expanded":false
          },
          {
            "stepId":"13411",
            "author":"1",
            "id" : Math.random().toString(36).substr(2,8),
            "title":"meld the flavors",
            "videoId":{"id":null,"title":"","youtubeId":"","url":""},
            "startSeconds":"",
            "pauseSeconds":"",
            "tags":[],
            "parent":"13376",
            "description":[],
            "likes":{"like":0,"dislike":0,"type":""},
            "code_snippet":[],
            "images":[],
            "score":null,
            "last_updated":"1574253401",
            "is_reviewed":false,
            "children":[
              {
              "stepId":"13436",
              "id" : Math.random().toString(36).substr(2,8),
              "author":"1",
              "title":"let mixture boil additional 2 minutes",
              "videoId":{
                "id":"3466",
                "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
                "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
              },
              "startSeconds":11,
              "pauseSeconds":15,
              "tags":[],
              "parent":"13411",
              "description":[],
              "likes":{"like":0,"dislike":0,"type":""},
              "code_snippet":[],
              "images":[],
              "score":null,
              "last_updated":"1574253212",
              "is_reviewed":false
            },
            {
              "stepId":"13441",
              "id" : Math.random().toString(36).substr(2,8),
              "author":"1",
              "title":"add 1\/2 cup milk to boiling tea \u0026 continue boiling",
              "videoId":
              {
                "id":"3471",
                "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
                "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
              },
              "startSeconds":29,
              "pauseSeconds":34,
              "tags":[{"id":"1421","name":"indian masala tea"}],
              "parent":"13411",
              "description":[],
              "likes":{"like":0,"dislike":0,"type":""},
              "code_snippet":[],
              "images":[],
              "score":null,
              "last_updated":"1574253336",
              "is_reviewed":false
            }],
            "expanded":false
          },
          {
            "stepId":"13416",
            "author":"1",
            "id" : Math.random().toString(36).substr(2,8),
            "title":"sprinkle 3\/4 teaspoon tea masala",
            "videoId":{"id":null,"title":"","youtubeId":"","url":""},
            "startSeconds":"",
            "pauseSeconds":"",
            "tags":[{"id":"1421","name":"indian masala tea"}],
            "parent":"13376",
            "description":[],
            "likes":{"like":0,"dislike":0,"type":""},
            "code_snippet":[],
            "images":[],
            "score":null,
            "last_updated":"1574252791",
            "is_reviewed":false
          },
          {
            "stepId":"13421",
            "author":"1",
            "id" : Math.random().toString(36).substr(2,8),
            "title":"continue boiling until foam builds a few inches",
            "videoId":{
              "id":"3461",
              "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
              "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
            },
            "startSeconds":32,
            "pauseSeconds":37,
            "tags":[{"id":"1421","name":"indian masala tea"}],
            "parent":"13376",
            "description":[],
            "likes":{"like":0,"dislike":0,"type":""},
            "code_snippet":[],
            "images":[],
            "score":null,
            "last_updated":"1574252856",
            "is_reviewed":false
          },
          {
            "stepId":"13426",
            "author":"1",
            "id" : Math.random().toString(36).substr(2,8),
            "title":"lower heat to a gentle boil for 1 more minute",
            "videoId":{"id":null,"title":"","youtubeId":"","url":""},
            "startSeconds":"",
            "pauseSeconds":"",
            "tags":[{"id":"1421","name":"indian masala tea"}],
            "parent":"13376",
            "description":[],
            "likes":{"like":0,"dislike":0,"type":""},
            "code_snippet":[],
            "images":[],
            "score":null,
            "last_updated":"1574252992",
            "is_reviewed":false
          },
        ],
          "expanded":false
    }
  ]

const variationLadders=[
  {
    "stepId":"13376",
    "id" : Math.random().toString(36).substr(2,8),
    "author":"1",
    "title":"make a hot, delicious drink",
    "videoId":{
      "id":null,
      "title":"",
      "youtubeId":"",
      "url":""
    },
    "startSeconds":"",
    "pauseSeconds":"",
    "tags":[{
      "id":"1421",
      "name":"indian masala tea"
    }],
    "parent":"0",
    "description":[],
    "likes":{
      "like":0,
      "dislike":0,
      "type":""
    },
    "code_snippet":[{
      "title":null,
      "code_snippet":null,
      "help_text":null
    }],
    "images":[],
    "score":null,
    "last_updated":"1574253401",
    "revision_exist":true,
    "is_reviewed":false,
    "children":[
      {
      "stepId":
      "13381",
      "id" : Math.random().toString(36).substr(2,8),
      "author":"1",
      "title":"boil 1\/2 cup pure water",
      "videoId":{
        "id":"3436",
        "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc","url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
      },
      "startSeconds":"",
      "pauseSeconds":"",
      "tags":[{
        "id":"1421",
        "name":"indian masala tea"
      }],
      "parent":"13376",
      "description":[],
      "likes":{
        "like":0,
        "dislike":0,
        "type":""
      },
      "code_snippet":[],
      "images":[],
      "score":null,
      "last_updated":"1574252156",
      "is_reviewed":false
    },
    {
      "stepId":"13386",
      "id" : Math.random().toString(36).substr(2,8),
      "author":"1",
      "title":"add the major flavors to the boiling water",
      "videoId":{
        "id":null,
        "title":"",
        "youtubeId":"",
        "url":""
      },
      "startSeconds":"",
      "pauseSeconds":"",
      "tags":[],
      "parent":"13376",
      "description":[],
      "likes":{
        "like":0,
        "dislike":0,
        "type":""
      },
      "code_snippet":[],
      "images":[],
      "score":null,
      "last_updated":"1574252667",
      "is_reviewed":false,
      "children":[
        {
        "stepId":"13391",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add 1 tablespoon tea leaves........\t",
        "videoId":{
          "id":"3441",
          "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc","url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
        },
        "startSeconds":"",
        "pauseSeconds":"",
        "tags":[{
          "id":"1421",
          "name":"indian masala tea"
        }],
        "parent":"13386",
        "description":[],
        "likes":{
          "like":0,
          "dislike":0,
          "type":""
        },
        "code_snippet":[],
        "images":[],
        "score":null,
        "last_updated":"1574252341",
        "updateAdd":true,
        "is_reviewed":false
      },
      {
        "stepId":"13396",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add 1 tablespoon ground coffee",
        "videoId":{
          "id":"3446",
          "title":"@Boiling Coffee using sand  :|#Made on hot sand |Like Turkish Cofee || Apple Street Food","youtubeId":"7WonfypKDpM",
          "url":"https:\/\/www.youtube.com\/watch?v=7WonfypKDpM"
        },
        "startSeconds":"",
        "pauseSeconds":"",
        "tags":[{
          "id":"1426",
          "name":"sand coffee"
        }],
        "parent":"13386",
        "description":[],
        "likes":{
          "like":0,
          "dislike":0,
          "type":""
        },
        "code_snippet":[],
        "images":[],
        "score":null,
        "last_updated":"1574252482",
        "is_reviewed":false
      },
      {
        "stepId":"13401",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add 1 tablespoon sugar",
        "videoId":{
          "id":"3451",
          "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
          "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{
            "id":"1421",
            "name":"indian masala tea"
          }],
          "parent":"13386",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252576",
          "is_reviewed":false
        },
        {
          "stepId":"13406",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"grate a thumb-sized piece of ginger into boiling tea",
          "videoId":{
            "id":"3456",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
            "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
          },
          "startSeconds":7,
          "pauseSeconds":11,
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13386",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252653",
          "is_reviewed":false}],
          "expanded":false
        },
        {
          "stepId":"13411",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"meld the flavors",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574253401",
          "is_reviewed":false,
          "children":[
            {
            "stepId":"13436",
            "id" : Math.random().toString(36).substr(2,8),
            "author":"1",
            "title":"let mixture boil additional 2 minutes",
            "videoId":{
              "id":"3466",
              "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
              "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
            },
            "startSeconds":11,
            "pauseSeconds":15,
            "tags":[],
            "parent":"13411",
            "description":[],
            "likes":{"like":0,"dislike":0,"type":""},
            "code_snippet":[],
            "images":[],
            "score":null,
            "last_updated":"1574253212",
            "is_reviewed":false
          },
          {
            "stepId":"13441",
            "id" : Math.random().toString(36).substr(2,8),
            "author":"1",
            "title":"add 1\/2 cup milk to boiling tea \u0026 continue boiling",
            "videoId":
            {
              "id":"3471",
              "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
              "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
            },
            "startSeconds":29,
            "pauseSeconds":34,
            "tags":[{"id":"1421","name":"indian masala tea"}],
            "parent":"13411",
            "description":[],
            "likes":{"like":0,"dislike":0,"type":""},
            "code_snippet":[],
            "images":[],
            "score":null,
            "last_updated":"1574253336",
            "is_reviewed":false
          }],
          "expanded":false
        },
        {
          "stepId":"13416",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"sprinkle 3\/4 teaspoon tea masala",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252791",
          "is_reviewed":false
        },
        {
          "stepId":"13421",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"continue boiling until foam builds a few inches",
          "videoId":{
            "id":"3461",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
            "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
          },
          "startSeconds":32,
          "pauseSeconds":37,
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252856",
          "is_reviewed":false
        },
        {
          "stepId":"13426",
          "author":"1",
          "id" : Math.random().toString(36).substr(2,8),
          "title":"lower heat to a gentle boil for 1 more minute",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252992",
          "is_reviewed":false
        },
        
      ],
        "expanded":false
  },
  {
    "stepId":"13376",
    "id" : Math.random().toString(36).substr(2,8),
    "author":"1",
    "title":"make a hot, delicious drink",
    "videoId":{
      "id":null,
      "title":"",
      "youtubeId":"",
      "url":""
    },
    "startSeconds":"",
    "pauseSeconds":"",
    "tags":[{
      "id":"1421",
      "name":"indian masala tea"
    }],
    "parent":"0",
    "description":[],
    "likes":{
      "like":0,
      "dislike":0,
      "type":""
    },
    "code_snippet":[{
      "title":null,
      "code_snippet":null,
      "help_text":null
    }],
    "images":[],
    "score":null,
    "last_updated":"1574253401",
    "revision_exist":true,
    "is_reviewed":false,
    "children":[
      {
      "stepId":
      "13381",
      "id" : Math.random().toString(36).substr(2,8),
      "author":"1",
      "title":"boil 1\/2 cup water",
      "videoId":{
        "id":"3436",
        "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc","url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
      },
      "startSeconds":"",
      "pauseSeconds":"",
      "tags":[],
      "parent":"13376",
      "description":[],
      "likes":{
        "like":0,
        "dislike":0,
        "type":""
      },
      "code_snippet":[],
      "images":[],
      "score":null,
      "last_updated":"1574252156",
      "is_reviewed":false,
      "updateDelete":true
    },
    {
      "stepId":"13386",
      "id" : Math.random().toString(36).substr(2,8),
      "author":"1",
      "title":"add the major flavors to the boiling water",
      "videoId":{
        "id":null,
        "title":"",
        "youtubeId":"",
        "url":""
      },
      "startSeconds":"",
      "pauseSeconds":"",
      "tags":[],
      "parent":"13376",
      "description":[],
      "likes":{
        "like":0,
        "dislike":0,
        "type":""
      },
      "code_snippet":[],
      "images":[],
      "score":null,
      "last_updated":"1574252667",
      "is_reviewed":false,
      "children":[
        {
        "stepId":"13391",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add 1 tablespoon tea leaves........\t",
        "videoId":{
          "id":"3441",
          "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc","url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
        },
        "startSeconds":"",
        "pauseSeconds":"",
        "tags":[{
          "id":"1421",
          "name":"indian masala tea"
        }],
        "parent":"13386",
        "description":[],
        "likes":{
          "like":0,
          "dislike":0,
          "type":""
        },
        "code_snippet":[],
        "images":[],
        "score":null,
        "last_updated":"1574252341",
        "is_reviewed":false
      },
      {
        "stepId":"13396",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add 1 tablespoon ground coffee",
        "videoId":{
          "id":"3446",
          "title":"@Boiling Coffee using sand  :|#Made on hot sand |Like Turkish Cofee || Apple Street Food","youtubeId":"7WonfypKDpM",
          "url":"https:\/\/www.youtube.com\/watch?v=7WonfypKDpM"
        },
        "startSeconds":"",
        "pauseSeconds":"",
        "tags":[{
          "id":"1426",
          "name":"sand coffee"
        }],
        "parent":"13386",
        "description":[],
        "likes":{
          "like":0,
          "dislike":0,
          "type":""
        },
        "code_snippet":[],
        "images":[],
        "score":null,
        "last_updated":"1574252482",
        "is_reviewed":false
      },
      {
        "stepId":"13401",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add 1 tablespoon sugar",
        "videoId":{
          "id":"3451",
          "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
          "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{
            "id":"1421",
            "name":"indian masala tea"
          }],
          "parent":"13386",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252576",
          "is_reviewed":false
        },
        {
          "stepId":"13406",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"grate a thumb-sized piece of ginger into boiling tea",
          "videoId":{
            "id":"3456",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
            "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
          },
          "startSeconds":7,
          "pauseSeconds":11,
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13386",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252653",
          "is_reviewed":false}],
          "expanded":false
        },
        {
          "stepId":"13411",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"meld the flavors",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574253401",
          "is_reviewed":false,
          "children":[
            {
            "stepId":"13436",
            "id" : Math.random().toString(36).substr(2,8),
            "author":"1",
            "title":"let mixture boil additional 2 minutes",
            "videoId":{
              "id":"3466",
              "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
              "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
            },
            "startSeconds":11,
            "pauseSeconds":15,
            "tags":[],
            "parent":"13411",
            "description":[],
            "likes":{"like":0,"dislike":0,"type":""},
            "code_snippet":[],
            "images":[],
            "score":null,
            "last_updated":"1574253212",
            "is_reviewed":false
          },
          {
            "stepId":"13441",
            "id" : Math.random().toString(36).substr(2,8),
            "author":"1",
            "title":"add 1\/2 cup milk to boiling tea \u0026 continue boiling",
            "videoId":
            {
              "id":"3471",
              "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
              "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
            },
            "startSeconds":29,
            "pauseSeconds":34,
            "tags":[{"id":"1421","name":"indian masala tea"}],
            "parent":"13411",
            "description":[],
            "likes":{"like":0,"dislike":0,"type":""},
            "code_snippet":[],
            "images":[],
            "score":null,
            "last_updated":"1574253336",
            "is_reviewed":false
          }],
          "expanded":false
        },
        {
          "stepId":"13416",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"sprinkle 3\/4 teaspoon tea masala",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252791",
          "is_reviewed":false
        },
        {
          "stepId":"13421",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"continue boiling until foam builds a few inches",
          "videoId":{
            "id":"3461",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
            "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
          },
          "startSeconds":32,
          "pauseSeconds":37,
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252856",
          "is_reviewed":false
        },
        {
          "stepId":"13426",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"lower heat to a gentle boil for 1 more minute",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252992",
          "is_reviewed":false
        },
      ],
    "expanded":false,
  },
  {
    "stepId":"13376",
    "id" : Math.random().toString(36).substr(2,8),
    "author":"1",
    "title":"make a hot, delicious drink",
    "videoId":{
      "id":null,
      "title":"",
      "youtubeId":"",
      "url":""
    },
    "startSeconds":"",
    "pauseSeconds":"",
    "tags":[{
      "id":"1421",
      "name":"indian masala tea"
    }],
    "parent":"0",
    "description":[],
    "likes":{
      "like":0,
      "dislike":0,
      "type":""
    },
    "code_snippet":[{
      "title":null,
      "code_snippet":null,
      "help_text":null
    }],
    "images":[],
    "score":null,
    "last_updated":"1574253401",
    "revision_exist":true,
    "is_reviewed":false,
    "children":[
      {
      "stepId":
      "13381",
      "id" : Math.random().toString(36).substr(2,8),
      "author":"1",
      "title":"boil 1\/2 cup water",
      "videoId":{
        "id":"3436",
        "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc","url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
      },
      "startSeconds":"",
      "pauseSeconds":"",
      "tags":[{
        "id":"1421",
        "name":"indian masala tea"
      }],
      "parent":"13376",
      "description":[],
      "likes":{
        "like":0,
        "dislike":0,
        "type":""
      },
      "code_snippet":[],
      "images":[],
      "score":null,
      "last_updated":"1574252156",
      "is_reviewed":false
      },
      {
        "stepId":"13386",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add the major flavors to the boiling water",
        "videoId":{
          "id":null,
          "title":"",
          "youtubeId":"",
          "url":""
        },
        "startSeconds":"",
        "pauseSeconds":"",
        "tags":[],
        "parent":"13376",
        "description":[],
        "likes":{
          "like":0,
          "dislike":0,
          "type":""
        },
        "code_snippet":[],
        "images":[],
        "score":null,
        "last_updated":"1574252667",
        "is_reviewed":false,
        "children":[
          {
          "stepId":"13391",
          "author":"1",
          "title":"add 1 tablespoon tea leaves........\t",
          "videoId":{
            "id":"3441",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc","url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
          },
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{
            "id":"1421",
            "name":"indian masala tea"
          }],
          "parent":"13386",
          "description":[],
          "likes":{
            "like":0,
            "dislike":0,
            "type":""
          },
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252341",
          "is_reviewed":false
        },
        {
          "stepId":"13396",
          "author":"1",
          "title":"add 1 tablespoon ground coffee",
          "videoId":{
            "id":"3446",
            "title":"@Boiling Coffee using sand  :|#Made on hot sand |Like Turkish Cofee || Apple Street Food","youtubeId":"7WonfypKDpM",
            "url":"https:\/\/www.youtube.com\/watch?v=7WonfypKDpM"
          },
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{
            "id":"1426",
            "name":"sand coffee"
          }],
          "parent":"13386",
          "description":[],
          "likes":{
            "like":0,
            "dislike":0,
            "type":""
          },
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252482",
          "is_reviewed":false
        },
        {
          "stepId":"13401",
          "author":"1",
          "title":"add 1 tablespoon sugar",
          "videoId":{
            "id":"3451",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
            "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"},
            "startSeconds":"",
            "pauseSeconds":"",
            "tags":[{
              "id":"1421",
              "name":"indian masala tea"
            }],
            "parent":"13386",
            "description":[],
            "likes":{"like":0,"dislike":0,"type":""},
            "code_snippet":[],
            "images":[],
            "score":null,
            "last_updated":"1574252576",
            "is_reviewed":false
          },
          {
            "stepId":"13406",
            "author":"1",
            "title":"grate a thumb-sized piece of ginger into boiling tea",
            "videoId":{
              "id":"3456",
              "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
              "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
            },
            "startSeconds":7,
            "pauseSeconds":11,
            "tags":[{"id":"1421","name":"indian masala tea"}],
            "parent":"13386",
            "description":[],
            "likes":{"like":0,"dislike":0,"type":""},
            "code_snippet":[],
            "images":[],
            "score":null,
            "last_updated":"1574252653",
            "is_reviewed":false}],
            "expanded":false
      },
      {
        "stepId":"13411",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"meld the flavors",
        "videoId":{"id":null,"title":"","youtubeId":"","url":""},
        "startSeconds":"",
        "pauseSeconds":"",
        "tags":[],
        "parent":"13376",
        "description":[],
        "likes":{"like":0,"dislike":0,"type":""},
        "code_snippet":[],
        "images":[],
        "score":null,
        "last_updated":"1574253401",
        "is_reviewed":false,
        "children":[
          {
          "stepId":"13436",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"let mixture boil additional 2 minutes",
          "videoId":{
            "id":"3466",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
            "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
          },
          "startSeconds":11,
          "pauseSeconds":15,
          "tags":[],
          "parent":"13411",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574253212",
          "is_reviewed":false
        },
        {
          "stepId":"13441",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"add 1\/2 cup milk to boiling tea \u0026 continue boiling",
          "videoId":
          {
            "id":"3471",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
            "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
          },
          "startSeconds":29,
          "pauseSeconds":34,
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13411",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574253336",
          "is_reviewed":false
        }],
        "expanded":false,
        "deleted":true,
      },
        {
          "stepId":"13416",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"sprinkle 3\/4 teaspoon tea masala",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252791",
          "is_reviewed":false
        },
        {
          "stepId":"13421",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"continue boiling until foam builds a few inches",
          "videoId":{
            "id":"3461",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
            "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
          },
          "startSeconds":32,
          "pauseSeconds":37,
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252856",
          "is_reviewed":false
        },
        {
          "stepId":"13426",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"lower heat to a gentle boil for 1 more minute",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252992",
          "is_reviewed":false
        },
        ],
        "expanded":false
  },
  {
    "stepId":"13376",
    "id" : Math.random().toString(36).substr(2,8),
    "author":"1",
    "title":"make a hot, delicious drink",
    "videoId":{
      "id":null,
      "title":"",
      "youtubeId":"",
      "url":""
    },
    "startSeconds":"",
    "pauseSeconds":"",
    "tags":[{
      "id":"1421",
      "name":"indian masala tea"
    }],
    "parent":"0",
    "description":[],
    "likes":{
      "like":0,
      "dislike":0,
      "type":""
    },
    "code_snippet":[{
      "title":null,
      "code_snippet":null,
      "help_text":null
    }],
    "images":[],
    "score":null,
    "last_updated":"1574253401",
    "revision_exist":true,
    "is_reviewed":false,
    "children":[
      {
      "stepId":
      "13381",
      "id" : Math.random().toString(36).substr(2,8),
      "author":"1",
      "title":"boil 1\/2 cup water",
      "videoId":{
        "id":"3436",
        "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc","url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
      },
      "startSeconds":"",
      "pauseSeconds":"",
      "tags":[{
        "id":"1421",
        "name":"indian masala tea"
      }],
      "parent":"13376",
      "description":[],
      "likes":{
        "like":0,
        "dislike":0,
        "type":""
      },
      "code_snippet":[],
      "images":[],
      "score":null,
      "last_updated":"1574252156",
      "is_reviewed":false
    },
    {
      "stepId":"13386",
      "id" : Math.random().toString(36).substr(2,8),
      "author":"1",
      "title":"add the major flavors to the boiling water",
      "videoId":{
        "id":null,
        "title":"",
        "youtubeId":"",
        "url":""
      },
      "startSeconds":"",
      "pauseSeconds":"",
      "tags":[],
      "parent":"13376",
      "description":[],
      "likes":{
        "like":0,
        "dislike":0,
        "type":""
      },
      "code_snippet":[],
      "images":[],
      "score":null,
      "last_updated":"1574252667",
      "is_reviewed":false,
      "children":[
        {
        "stepId":"13391",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add 1 tablespoon tea leaves........\t",
        "videoId":{
          "id":"3441",
          "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc","url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
        },
        "startSeconds":"",
        "pauseSeconds":"",
        "tags":[{
          "id":"1421",
          "name":"indian masala tea"
        }],
        "parent":"13386",
        "description":[],
        "likes":{
          "like":0,
          "dislike":0,
          "type":""
        },
        "code_snippet":[],
        "images":[],
        "score":null,
        "last_updated":"1574252341",
        "is_reviewed":false
      },
      {
        "stepId":"13396",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add 1 tablespoon ground coffee",
        "videoId":{
          "id":"3446",
          "title":"@Boiling Coffee using sand  :|#Made on hot sand |Like Turkish Cofee || Apple Street Food","youtubeId":"7WonfypKDpM",
          "url":"https:\/\/www.youtube.com\/watch?v=7WonfypKDpM"
        },
        "startSeconds":"",
        "pauseSeconds":"",
        "tags":[{
          "id":"1426",
          "name":"sand coffee"
        }],
        "parent":"13386",
        "description":[],
        "likes":{
          "like":0,
          "dislike":0,
          "type":""
        },
        "code_snippet":[],
        "images":[],
        "score":null,
        "last_updated":"1574252482",
        "is_reviewed":false
      },
      {
        "stepId":"13401",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add 1 tablespoon sugar",
        "videoId":{
          "id":"3451",
          "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
          "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{
            "id":"1421",
            "name":"indian masala tea"
          }],
          "parent":"13386",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252576",
          "is_reviewed":false
        },
        {
          "stepId":"13406",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"grate a thumb-sized piece of ginger into boiling tea",
          "videoId":{
            "id":"3456",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
            "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
          },
          "startSeconds":7,
          "pauseSeconds":11,
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13386",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252653",
          "is_reviewed":false}],
          "expanded":false
        },
        {
          "stepId":"13416",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"sprinkle 3\/4 teaspoon tea masala",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252791",
          "is_reviewed":false
        },
        {
          "stepId":"13421",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"continue boiling until foam builds a few inches",
          "videoId":{
            "id":"3461",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
            "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
          },
          "startSeconds":32,
          "pauseSeconds":37,
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252856",
          "is_reviewed":false
        },
        {
          "stepId":"13426",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"lower heat to a gentle boil for 1 more minute",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252992",
          "is_reviewed":false
        },
        {
          "stepId":"13431",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"pour through a strainer and enjoy!",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574253063",
          "is_reviewed":false,
          "added":true
        }],
        "expanded":false
  },
  {
    "stepId":"13376",
    "id" : Math.random().toString(36).substr(2,8),
    "author":"1",
    "title":"make a hot, delicious drink",
    "videoId":{
      "id":null,
      "title":"",
      "youtubeId":"",
      "url":""
    },
    "startSeconds":"",
    "pauseSeconds":"",
    "tags":[{
      "id":"1421",
      "name":"indian masala tea"
    }],
    "parent":"0",
    "description":[],
    "likes":{
      "like":0,
      "dislike":0,
      "type":""
    },
    "code_snippet":[{
      "title":null,
      "code_snippet":null,
      "help_text":null
    }],
    "images":[],
    "score":null,
    "last_updated":"1574253401",
    "revision_exist":true,
    "is_reviewed":false,
    "children":[
      {
      "stepId":
      "13381",
      "id" : Math.random().toString(36).substr(2,8),
      "author":"1",
      "title":"boil 1\/2 cup water",
      "videoId":{
        "id":"3436",
        "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc","url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
      },
      "startSeconds":"",
      "pauseSeconds":"",
      "tags":[{
        "id":"1421",
        "name":"indian masala tea"
      }],
      "parent":"13376",
      "description":[],
      "likes":{
        "like":0,
        "dislike":0,
        "type":""
      },
      "code_snippet":[],
      "images":[],
      "score":null,
      "last_updated":"1574252156",
      "is_reviewed":false
    },
    {
      "stepId":"13386",
      "id" : Math.random().toString(36).substr(2,8),
      "author":"1",
      "title":"add the major flavors to the boiling water",
      "videoId":{
        "id":null,
        "title":"",
        "youtubeId":"",
        "url":""
      },
      "startSeconds":"",
      "pauseSeconds":"",
      "tags":[],
      "parent":"13376",
      "description":[],
      "likes":{
        "like":0,
        "dislike":0,
        "type":""
      },
      "code_snippet":[],
      "images":[],
      "score":null,
      "last_updated":"1574252667",
      "is_reviewed":false,
      "children":[
        {
        "stepId":"13391",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add 1 tablespoon tea leaves........\t",
        "videoId":{
          "id":"3441",
          "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc","url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
        },
        "startSeconds":"",
        "pauseSeconds":"",
        "tags":[{
          "id":"1421",
          "name":"indian masala tea"
        }],
        "parent":"13386",
        "description":[],
        "likes":{
          "like":0,
          "dislike":0,
          "type":""
        },
        "code_snippet":[],
        "images":[],
        "score":null,
        "last_updated":"1574252341",
        "is_reviewed":false
      },
      {
        "stepId":"13396",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add 1 tablespoon ground coffee",
        "videoId":{
          "id":"3446",
          "title":"@Boiling Coffee using sand  :|#Made on hot sand |Like Turkish Cofee || Apple Street Food","youtubeId":"7WonfypKDpM",
          "url":"https:\/\/www.youtube.com\/watch?v=7WonfypKDpM"
        },
        "startSeconds":"",
        "pauseSeconds":"",
        "tags":[{
          "id":"1426",
          "name":"sand coffee"
        }],
        "parent":"13386",
        "description":[],
        "likes":{
          "like":0,
          "dislike":0,
          "type":""
        },
        "code_snippet":[],
        "images":[],
        "score":null,
        "last_updated":"1574252482",
        "is_reviewed":false
      },
      {
        "stepId":"13401",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add 1 tablespoon sugar",
        "videoId":{
          "id":"3451",
          "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
          "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{
            "id":"1421",
            "name":"indian masala tea"
          }],
          "parent":"13386",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252576",
          "is_reviewed":false
        },
        {
          "stepId":"13406",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"grate a thumb-sized piece of ginger into boiling tea",
          "videoId":{
            "id":"3456",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
            "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
          },
          "startSeconds":7,
          "pauseSeconds":11,
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13386",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252653",
          "is_reviewed":false}],
          "expanded":false
        },
        {
          "stepId":"13416",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"sprinkle 3\/4 teaspoon tea masala",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252791",
          "is_reviewed":false
        },
        {
          "stepId":"13421",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"continue boiling until foam builds a few inches",
          "videoId":{
            "id":"3461",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
            "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
          },
          "startSeconds":32,
          "pauseSeconds":37,
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252856",
          "is_reviewed":false
        },
        {
          "stepId":"13426",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"lower heat to a gentle boil for 1 more minute",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252992",
          "is_reviewed":false
        },
        {
          "stepId":"13431",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"pour through a strainer and enjoy!",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574253063",
          "is_reviewed":false,
        }
      ],
        "expanded":false
  }
]

export {
   baseLadder,variationLadders
}