export default {
    class_id: "react-step-ladders",
    data_id: "data-nid",
    user_id: "data-uid",
    data_url: "data-url",
    auth_token: "session/token?_format=json",
    get_node: "rest-api/ladder/",
    update_node: "entity/node?_format=json",
    add_node: "entity/node?_format=json",
    add_video: "entity/media?_format=json",
    add_file: "entity/file?_format=hal_json",
    add_media: "entity/media?_format=hal_json",
    get_tags: "rest-api/tags?_format=hal_json",
    add_tag: "entity/taxonomy_term?_format=hal_json",
    review_step: "entity/flagging?_format=json",
    search_title: 'rest-api/steps-search?_format=json&title=',
    user_login: 'user/login?_format=json',
    ladder_revision:'rest-api/ladder_revision?_format=json'
}