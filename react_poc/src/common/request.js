import React, { Component } from "react";

import config from "./config";
import constant from "./constant";
var arrayToTree = require("array-to-tree");
import axios from "axios";

import SortableTree, { toggleExpandedForAll, addNodeUnderParent, removeNodeAtPath, getNodeAtPath, changeNodeAtPath } from "react-sortable-tree";
import { CompareData } from "./Dummy";
import { FaGalacticSenate } from "react-icons/fa";

let stack = [];
let filteredArray=[]
let ID = document.getElementById(constant.class_id).getAttribute(constant.data_id)

const USER_ID = document.getElementById(constant.class_id).getAttribute(constant.user_id);
const URL = document.getElementById(constant.class_id).getAttribute(constant.data_url);

export default class Request {

  constructor() {
    this.temp = []
  }


  getApiUrl = () => {
    if (URL == 'http://localhost') {
      return config.API_URL
    } else {
      return URL
    }
  }

  serverRequest = requesrParam => {
    return new Promise((resolve, reject) => {
      axios(requesrParam)
        .then(function (response) {
          resolve(response);
        })
        .catch(function (error) {
          console.log('ERROR >> "', error);
          reject(error);
        });
    });
  };

  login = () => {
    return new Promise((resolve,reject)=>{
      let requestParam = {
        method: "POST",
        url:this.getApiUrl()+"user/login?_format=json",
        data:{
          "name": "admin",
          "pass": "testing"
          
        },
        headers:{
          "Content-Type":"application/json"
        }
      }
      this.serverRequest(requestParam)
      .then(res=>{
        console.log("res",res.data);
        resolve({data:{X_CSRF_token:res.data.csrf_token}});
      })
    })
  }

  veryfyToken = () => {
    return new Promise((resolve, reject) => {
      let requestParam = {
        method: "GET",
        url: this.getApiUrl() + constant.auth_token,  
      };
      this.serverRequest(requestParam)
        .then(res => {
            resolve({ data: { X_CSRF_token: res.data } });
        })
        .catch(err => reject(err));
    });
  };

  doPost = (api_slug, data, content_type = "application/json") => {
    return new Promise((resolve, reject) => {
      this.veryfyToken()
        .then(res => {
          let requestParam = {
            method: "POST",
            url: this.getApiUrl() + api_slug,
            data: data,
            headers: {
              Authorization: "basic am9obm55OnRlc3Rpbmc=",
              "X-CSRF-Token": res.data.X_CSRF_token,
              "Content-Type": content_type
            }
          };
          this.serverRequest(requestParam)
            .then(res => {
              resolve(res);
            })
            .catch(err => {
              console.log('ERROR >> "', err);
              reject(err);
            });
        })
        .catch(err => {
          console.log('ERROR >> "', err);
          reject(err);
        });
    });
  };

  doGet = api_slug => {
    return new Promise((resolve, reject) => {
      this.veryfyToken()
        .then(res => {
          let requestParam = {
            method: "GET",
            url: this.getApiUrl() + api_slug,
            headers: {
              Authorization: "Basic YWRtaW46dGVzdGluZw==",
              "X-CSRF-Token": res.data.X_CSRF_token
            }
          };
          this.serverRequest(requestParam)
            .then(res => {
              resolve(res);
            })
            .catch(err => {
              console.log('ERROR >> "', err);
              reject(err);
            });
        })
        .catch(err => {
          console.log('ERROR >> "', err);
          reject(err);
        });
    });
  };

  doPatch = (api_slug, data) => {
    return new Promise((resolve, reject) => {
      this.veryfyToken()
        .then(res => {
          let requestParam = {
            method: "PATCH",
            url: this.getApiUrl() + api_slug,
            data: data,
            headers: {
              Authorization: "basic am9obm55OnRlc3Rpbmc=",
              "X-CSRF-Token": res.data.X_CSRF_token
            }
          };
          this.serverRequest(requestParam)
            .then(res => {
              resolve(res);
            })
            .catch(err => {
              console.log('ERROR >> "', err);
              reject(err);
            });
        })
        .catch(err => {
          console.log('ERROR >> "', err);
          reject(err);
        });
    });
  };

  doDelete = (api_slug) => {
    return new Promise((resolve, reject) => {
      this.veryfyToken()
        .then(res => {
          let requestParam = {
            method: "DELETE",
            url: this.getApiUrl() + api_slug,
            headers: {
              Authorization: "basic am9obm55OnRlc3Rpbmc=",
              "X-CSRF-Token": res.data.X_CSRF_token
            }
          };
          this.serverRequest(requestParam)
            .then(res => {
              resolve(res);
            })
            .catch(err => {
              console.log('ERROR >> "', err);
              reject(err);
            });
        })
        .catch(err => {
          console.log('ERROR >> "', err);
          reject(err);
        });
    });
  };

  convertSecond = sec => {
    let hours = Math.floor(sec / 3600);
    let minutes = Math.floor((sec / 60) % 60);
    let seconds = sec % 60;
    let time = {};
    hours != undefined ? (hours = hours.toString() + "H") : null;
    minutes != undefined ? (minutes = minutes.toString() + "M") : null;
    seconds != undefined ? (seconds = seconds.toString() + "S") : null;
    return `PT${hours}${minutes}${seconds}`;
  };

  addTags = tags => {
    return new Promise((resolve, reject) => {
      if (tags.length > 0) {
        let newTags = [];
        this.veryfyToken()
          .then(res => {
            let requestParam = {
              method: "POST",
              url: this.getApiUrl() + constant.add_tag,
              headers: {
                Authorization: "basic am9obm55OnRlc3Rpbmc=",
                "X-CSRF-Token": res.data.X_CSRF_token
              }
            };
            let promises = [];
            tags.forEach(item => {
              if (!item.hasOwnProperty("tid") && !item.hasOwnProperty("id")) {
                let TagData = { vid: [{ target_id: "tags" }], name: { value: item.name } };
                requestParam.data = TagData;
                promises.push(axios(requestParam));
              } else {
                item.hasOwnProperty("tid") ? newTags.push(item.tid) : newTags.push(item.id)
              }
            });
            if (promises.length > 0) {
              axios
                .all(promises)
                .then(async results => {
                  let AllTags = [...new Set(newTags.concat(results.map(response => response.data.tid[0].value.toString())))];
                  resolve(AllTags);
                })
                .catch(err => {
                  reject(err);
                });
            } else {
              resolve(newTags);
            }

          })
          .catch(err => {
            reject(err);
          });
      } else {
        resolve([]);
      }
    });
  };


  addVideo = (VideoData, data) => {
    return new Promise((resolve, reject) => {
        this.doPost(constant.add_video, VideoData)
          .then(res => {
            resolve(res)
          }).catch(err => {
            reject(err)
          })
    });
  }

  addImage=(imageData,data)=>{
    return new Promise((resolve,reject)=>{
      this.doPost(constant.add_file,imageData, "application/hal+json")
      .then(file_res => {
        let fid = file_res.data.fid[0].value
        this.doPost(constant.add_media, {
          field_media_image: [
            {
              target_id: fid,
              alt: "image",
              target_type: "file",
              url: "/file/" + fid
            }
          ],
          name: [
            {
              "value": data.images[0].name
            }
          ],
          bundle: "image"
        }).then(media_res => {
          resolve(media_res)
        })
      }).catch(err=>{
        reject(err);
      })
    })
  }


  imageUpload = (image, stepId) => {
    return new Promise((resolve, reject) => {
      this.doPost(constant.add_file, {
        _links: {
          type: {
            href: this.getApiUrl() + "rest/type/file/image"
          }
        },
        filename: [{
          value: image.name
        }],
        data: [{
          value: image.url.split(",")[1]
        }]
      }, "application/hal+json").then(file_res => {
        let fid = file_res.data.fid[0].value
        this.doPost(constant.add_media, {
          field_media_image: [
            {
              target_id: fid,
              alt: "image",
              target_type: "file",
              url: "/file/" + fid
            }
          ],
          name: [
            {
              "value": image.name
            }
          ],
          bundle: "image"
        }).then(media_res => {
          let mid = media_res.data.mid[0].value
          this.doPatch("node/" + stepId + "?_format=json", {
            type: [{
              target_id: "ladder"
            }],
            field_image_er: [{
              target_id: mid,
              target_type: "media",
              url: "/media/" + mid
            }]
          })
            .then(res => {
              resolve(mid);
            })
            .catch(err => {
              reject(err);
            });
        }).catch(err => {
          reject(err)
        })
      }).catch(err => {
        reject(err)
      })
    })
  }

  addNewLadderNode = (nodeData) => {
    return new Promise((resolve, reject) => {
      let parent = null
      this.doPost(constant.add_node, {
        title: [{ value: nodeData.parent.title }],
        type: [{ target_id: "ladder" }],
        field_is_ladder: [{ value: true }]
      })
        .then(result => {
          parent = result.data.nid[0].value
          localStorage.setItem('data-nid', parent);
          this.doPost(constant.add_node, {
            title: [{ value: nodeData.child[0].title }],
            type: [{ target_id: "ladder" }],
            field_is_ladder: [{ value: true }]
          })
            .then(result => {
              this.doPatch("node/" + parent + "?_format=json", {
                type: [{ target_id: "ladder" }],
                field_steps_er: [{ target_id: result.data.nid[0].value }]
              })
                .then(res => {
                  resolve(res);
                })
                .catch(err => {
                  console.log('ERROR >> "', err);
                  reject(err);
                });
            })
            .catch(err => {
              reject(err);
            });
        })
        .catch(err => {
          reject(err);
        });
    })
  }

  addNewNode = (rowInfo, data, items) => {
    let startFrom = this.convertSecond(data.startSeconds).replace('NaN',0);
    let endFrom = this.convertSecond(data.pauseSeconds).replace('NaN',0);
    let imageData,codeSnippetData,VideoData
    return new Promise((resolve, reject) => {
      let  node, parentNode, path ;
      if(rowInfo.node){
        node=rowInfo.node;
        parentNode=rowInfo.parentNode;
        path=rowInfo.path;
      }else{
        node=rowInfo;
      }
      let nodeData={
        "type": [{ target_id: "ladder" }],
        "title": [{ value: data.title }],
        "body": { value: data.description.length>0?data.description[0].value:'' },
        "field_is_ladder": [{ value: true }],
        "field_start_time":[{ value: startFrom }],
        "field_end_time":[{ value: endFrom }],
      }
      let tags=[],code_snippet_res=[],video_res=[],image_res=[];
      this.addTags(data.tags).then(res=>{
        if (res.length > 0) {
          res.forEach(tag => {
            tags.push({ target_id: tag });
          })
          nodeData.field_tags=tags
        }
      }).catch(err => {
        reject(err);
      });
        if(data.images.length >0){
          this.addImage(imageData={
            _links: {
              type: {
                href: this.getApiUrl() + "rest/type/file/image"
              }
            },
            filename: [{
              value: data.images[0].name
            }],
            data: [{
              value: data.images[0].url.split(",")[1]
            }]
          },data).then(res=>{
            image_res=res;
            nodeData.field_image_er= [{target_id: res.data.mid[0].value}]
            if(data.code_snippet.length>0){
              this.doPost('entity/paragraph?_format=json',codeSnippetData={
          
                "field_code_snippet":  [
                  {
                    
                    "value":  data.code_snippet[0].code_snippet
                  }
                ],
                "field_help_text": [
                  {
                    "value":data.code_snippet[0].help_text
                  }
                ],
                 "field_title": [
                  {
                    "value": data.code_snippet[0].title
          
                  }
                ],
                "type": "code_snippet"
              }).then(res=>{
                code_snippet_res=res;
                nodeData.field_code_snippet=[{target_id: res.data.id[0].value, target_revision_id:res.data.revision_id[0].value}]
                if(data.videoUrl){
                  this.addVideo( VideoData = {
                    field_media_oembed_video: [{ value: data.videoUrl }],
                    name: [{ value: data.videoTitle ?data.videoTitle:data.title }],
                    bundle: "remote_video",
                  },data).then(res=>{
                    video_res=res
                    nodeData.field_remote_video_er = [{ target_id: res.data.mid[0].value }]
                    this.addAPICall(nodeData,data,parentNode,node,path,items).then(res=>{
                      resolve(res);
                    }).catch(err=>{
                      reject(err)
                    })
                  }).catch(err=>{
                    console.log("video err",err);
                  })
                }else{
                  this.addAPICall(nodeData,data,parentNode,node,path,items).then(res=>{
                    resolve(res);
                  }).catch(err=>{
                    reject(err)
                  })
                }
              }).catch(err=>{
                console.log("code snippet err",err)
              })
            }else{
              if(data.videoUrl){
                this.addVideo( VideoData = {
                  field_media_oembed_video: [{ value: data.videoUrl }],
                  name: [{ value: data.videoTitle ?data.videoTitle:data.title }],
                  bundle: "remote_video",
                },data).then(res=>{
                  video_res=res
                  nodeData.field_remote_video_er = [{ target_id: res.data.mid[0].value }]
                  this.addAPICall(nodeData,data,parentNode,node,path,items).then(res=>{
                    resolve(res);
                  }).catch(err=>{
                    reject(err)
                  })
                }).catch(err=>{
                  console.log("video err",err);
                })
              }else{
                this.addAPICall(nodeData,data,parentNode,node,path,items).then(res=>{
                  resolve(res)
                }).catch(err=>{
                  reject(err)
                })
              }
            }
          }).catch(err=>{
            console.log("imageERR",err);
          })
        }else if(data.code_snippet.length>0){
          this.doPost('entity/paragraph?_format=json',codeSnippetData={
      
            "field_code_snippet":  [
              {
                
                "value":  data.code_snippet[0].code_snippet
              }
            ],
            "field_help_text": [
              {
                "value":data.code_snippet[0].help_text
              }
            ],
             "field_title": [
              {
                "value": data.code_snippet[0].title
      
              }
            ],
            "type": "code_snippet"
          }).then(res=>{
            code_snippet_res=res;
            nodeData.field_code_snippet=[{target_id: res.data.id[0].value, target_revision_id:res.data.revision_id[0].value}]
            if(data.videoUrl){
              this.addVideo( VideoData = {
                field_media_oembed_video: [{ value: data.videoUrl }],
                name: [{ value: data.videoTitle ?data.videoTitle:data.title }],
                bundle: "remote_video",
              },data).then(res=>{
                video_res=res
                nodeData.field_remote_video_er = [{ target_id: res.data.mid[0].value }]
                this.addAPICall(nodeData,data,parentNode,node,path,items).then(res=>{
                  resolve(res)
                }).catch(err=>{
                  reject(err)
                })
              }).catch(err=>{
                console.log("video err",err);
              })
            }else{
              this.addAPICall(nodeData,data,parentNode,node,path,items).then(res=>{
                resolve(res)
              }).catch(err=>{
                reject(err)
              })
            }
          }).catch(err=>{
            console.log("code snippet err",err)
          })
        }else if(data.videoUrl){
          this.addVideo( VideoData = {
            field_media_oembed_video: [{ value: data.videoUrl }],
            name: [{ value: data.videoTitle ?data.videoTitle:data.title }],
            bundle: "remote_video",
          },data).then(res=>{
            video_res=res
            nodeData.field_remote_video_er = [{ target_id: res.data.mid[0].value }]
            this.addAPICall(nodeData,data,parentNode,node,path,items).then(res=>{
              resolve(res);
            }).catch(err=>{
              reject(err)
            })
          }).catch(err=>{
            console.log("video err",err);
          })
        }else{
          this.addAPICall(nodeData,data,parentNode,node,path,items).then(res=>{
            resolve(res);
          }).catch(err=>{
            reject(err)
          })
        }
    })
      
    
  };

  addAPICall=(nodeData,data,parentNode,node,path,items)=>{
    return new Promise((resolve,reject)=>{
      this.doPost(constant.add_node, nodeData)
      .then(result => {
        let parentID = "";
        parentNode == null ? (parentID = node.stepId) : (parentID = parentNode.stepId);
        if(path === undefined){
          parentID = node.stepId;
        }
        if(path !== undefined){
          if (path.length > 1 ) {
            parentID = node.stepId;
          }
        }
        
        data.isParent ? (parentID = ID) : null;

        let treeData = {
          type: [{ target_id: "ladder" }]
        };
        let field_steps_er = [];
        if (node.hasOwnProperty("children") && !data.isParent) {
          node.children.forEach(item => {
            field_steps_er.push({ target_id: item.stepId });
          });
        } else if (items.length > 0 && data.isParent) {
          items.forEach(item => {
            field_steps_er.push({ target_id: item.stepId });
          });
        }

        field_steps_er.push({ target_id: result.data.nid[0].value });
        treeData.field_steps_er = field_steps_er;
        this.doPatch("node/" + parentID + "?_format=json", treeData)
          .then(res => {
            resolve({ node_id: result.data.nid[0].value, parent_id: res.data.nid[0].value });
          })
          .catch(err => {
            console.log('ERROR >> "', err);
            reject(err);
          });
      })
      .catch(err => {
        console.log('ERROR >> "', err);
        reject(err);
      });
    })
  }
  voteToLadder = (stepId, type) => {
    return new Promise((resolve, reject) => {
      this.doPost("like_and_dislike/node/"+type+"/"+stepId,{}).then((res)=>{
        resolve(res)
      }).catch(err=>{
        console.log("err",err);
        reject(err);
      })
    });
  };


  editNode = (rowInfo, data) => {
    let startFrom = this.convertSecond(data.startSeconds);
    let endFrom = this.convertSecond(data.pauseSeconds);
    let codeSnippet1 = []
    let { node, parentNode } = rowInfo;
    if(data.code_snippet.length>0){
      codeSnippet1 = [{value:data.code_snippet[0].code_snippet}]
    }
    return new Promise((resolve, reject) => {
      this.addTags(data.tags).then(res => {
       
        let nodeData = {
          type: [{ target_id: "ladder" }],
          title: [{ value: data.title }],
          body: { value: data.description[0].value, format: "full_html"},
          field_is_ladder: [{ value: true }],
          field_start_time: [{ value: startFrom }],
          field_end_time: [{ value: endFrom }],
          field_code_snippet: code_snippet[0].code_snippet,
        };

        let field_tags = [];
       
        if (res.length > 0) {
          res.forEach(tag => {
            field_tags.push({ target_id: tag });
          });
          nodeData.field_tags = field_tags;
        } else {
          nodeData.field_tags = field_tags;
        }
        if(data.images.length>0){
          this.imageUpload(data.images[0],data.stepId)
          .then(res=>console.log("image>>",res))
          .catch(err=>console.log("image>>",err))
        }
        if (node.videoId.youtubeId != data.videoId.youtubeId || node.videoId.youtubeId != null) {
          let VideoData = {
            field_media_oembed_video: [{ value: data.videoUrl }],
            name: [{ value: data.title }],
            bundle: "remote_video"
          };
          this.doPost(constant.add_video, VideoData)
            .then(res => {
              nodeData.field_remote_video_er = [{ target_id: res.data.mid[0].value }];
              this.doPatch("node/" + node.stepId + "?_format=json", nodeData)
                .then(res => {
                  resolve(res);
                })
                .catch(err => {
                  console.log('ERROR >> "', err);
                  reject(err);
                });
            })
            .catch(err => {
              console.log('ERROR >> "', err);
              reject(err);
            });
        } else {
          if (node.videoId.id == null && !nodeData.hasOwnProperty('field_remote_video_er')) {
            nodeData.field_remote_video_er = [];
          }

          this.doPatch("node/" + node.stepId + "?_format=json", nodeData)
            .then(res => {
              resolve(res);
            })
            .catch(err => {
              console.log('ERROR >> "', err);
              reject(err);
            });
        }
      }).catch(err => reject(err));
    });
  };

  replaceRevisions = (data, replaceRevisionsInfo) => {
    let temp = false;
    data.children.map((element) => {
      if (element.parent == data.stepId && element.stepId == replaceRevisionsInfo.stepId) {
        temp = element
      }
    });
    return temp
  }

  myReplaceRevisions = (data, replaceRevisionsInfo) => {
    if (data.hasOwnProperty('children')) {
      data.children.map((element) => {
        if (element.stepId == replaceRevisionsInfo.stepId && element.parent == replaceRevisionsInfo.parent) {
          this.temp = element
        }
        this.myReplaceRevisions(element, replaceRevisionsInfo)
      })
    }
  }

  getVideoData = () => {
    let that = this;
    return new Promise((resolve, reject) => {
      this.doGet(constant.get_node + ID)
        .then(res => {
          if (Array.isArray(res.data)) {
            let suggestions = [];
            if(Array.isArray(res.data[0].children) && (res.data[0].children !== undefined )){
              res.data[0].tags.forEach(element=>{
                if (
                  !suggestions.some(el => {
                    return el.name == element.name;
                  })
                ) {
                  suggestions.push(element);
                }
              })
            let queueVideos = that.treeToArray(res.data[0].children);
           
            let VideoList = []
            queueVideos.forEach(element => {
              VideoList.push(this.addVoteKeys(element))
              element.tags.forEach(element => {
                if (
                  !suggestions.some(el => {
                    return el.name == element.name;
                  })
                ) {
                  suggestions.push(element);
                }
              });
            });
            this.arrayToTree(VideoList).then(result => {
              let treeData = []
              let tree = this.addVoteKeys(res.data[0])
              tree.children = result;
              treeData.push(tree);
              let response = {
                tree: treeData,
                suggestions,
                queueVideos: VideoList
              };
              resolve(response);
            })
            }
            else{
                let suggestions = [];
                let treeData = [];
                res.data[0].tags.forEach(element=>{
                  if (
                    !suggestions.some(el => {
                      return el.name == element.name;
                    })
                  ) {
                    suggestions.push(element);
                  }
                })
                let tree = this.addVoteKeys(res.data[0])
                treeData.push(tree);
                let response = {
                  tree: treeData,
                  suggestions:suggestions,
                  queueVideos: []
                };
                resolve(response);
              
            }
          } else {
            resolve({ error: "No records found!!!" });
          }
        })
        .catch(err => {
          console.log('ERROR >>', err);
          reject(err);
        });
    });
  };

  getSuggestions = (videos) => {
    return new Promise((resolve, reject) => {
      let suggestions = [];
      videos.forEach(element => {
        element.tags.forEach(element => {
          if (
            !suggestions.some(el => {
              return el.name == element.name;
            })
          ) {
            suggestions.push(element);
          }
        });
      });
      resolve(suggestions);
    });
  }
  

  addVoteKeys = (element) => {
    return element;
  }

  onDropedNode = (Info, data) => {
    let { nextParentNode, nextPath, nextTreeIndex, node, path, prevPath, prevTreeIndex, treeData, treeIndex } = Info;
    let temp_prev = JSON.parse(localStorage.getItem("prevPath"));
    let old_Tree = JSON.parse(localStorage.getItem("OldTree"));
    let mainNode= JSON.parse(localStorage.getItem("mainNode"));
    prevPath.length > 1 ? prevPath.pop() : null;
    let ownParentNode = null;
    let p_id = "";
    ownParentNode = getNodeAtPath({
      treeData: old_Tree,
      path: prevPath,
      getNodeKey: ({ treeIndex }) => treeIndex,
      ignoreCollapsed: true
    });
    if(node.parent == ID){
      ownParentNode={
        "node":mainNode,
      }
    }
    if (node.stepId == ownParentNode.node.stepId) {
      p_id = ID;
    } else {
      p_id = ownParentNode.node.stepId;
    }

    let removeNodeData = {
      type: [{ target_id: "ladder" }]
    };

    let fields_steps_er = [];
    if (p_id == ID) {
      treeData.forEach(item => {
        fields_steps_er.push({ target_id: item.stepId });
      });
    } else {
      ownParentNode.node.children.forEach(item => {
        if (node.stepId != item.stepId) {
          fields_steps_er.push({ target_id: item.stepId });
        }
      });
    }
    removeNodeData.field_steps_er = fields_steps_er;
    let parentID = "";
    if (nextParentNode == null) {
      parentID = ID;
    } else {
      parentID = nextParentNode.stepId;
    }
    let updateNodeData = {
      type: [{ target_id: "ladder" }]
    };
    let field_steps_er = [];
    if (parentID == ID) {
      treeData.forEach(item => {
        field_steps_er.push({ target_id: item.stepId });
      });
    } else if (nextParentNode != null && nextParentNode.hasOwnProperty("children")) {
      nextParentNode.children.forEach(item => {
        field_steps_er.push({ target_id: item.stepId });
      });
    } else {
      field_steps_er = [];
    }
    updateNodeData.field_steps_er = field_steps_er;
    return new Promise((resolve, reject) => {
      this.doPatch("node/" + p_id + "?_format=json", removeNodeData)
        .then(res => {
          this.doPatch("node/" + parentID + "?_format=json", updateNodeData)
            .then(res => {
              console.log("DROPPEDNODE:",res)
              resolve(res);
            })
            .catch(err => {
              console.log('ERROR >> "', err);
              reject(err);
            });
        })
        .catch(err => {
          console.log('ERROR >> "', err);
          reject(err);
        });
    });
  };

  jsonarrayToTree=array=>{
    return new Promise((resolve, reject) => {
      const tree = arrayToTree(array, {
        parentProperty: "parent",
        childrenProperty: "children",
        customID: "id"
      });
      resolve(tree);
    });
  }


  arrayToTree = array => {
    return new Promise((resolve, reject) => {
      const tree = arrayToTree(array, {
        parentProperty: "parent",
        childrenProperty: "children",
        customID: "stepId"
      });
      resolve(tree);
    });
  };
  addParentKey=(item,array)=>{
    let temp;
   array.forEach(element=>{
    temp=element;
     if(item.parent == ID){
     }else{
       if(item.parent == element.stepId){
         filteredArray.push(element)
         this.removeItem(array,element)
         this.addParentKey(temp,array); 
       }
     }
   })
   return filteredArray
 }

 filterByTags = (tags, text, tree) => {
   return new Promise((resolve, reject) => {
     let array = [];
     array = this.treeToArray(tree);
     array = this.removeChildKey(array);
     let filteredTag = [];
     array.forEach(item => {
         if (tags.length > 0) {
           item.tags.forEach(tag => {
             tags.forEach(findTag => {
               if (tag.name == findTag.name && item.title.toLowerCase().indexOf(text.toLowerCase()) != -1) {
                 filteredTag.push(item);
               }
             });
           });
         }
     });
     let temparray=[];
     if(filteredTag.length>0){
       let temp=filteredTag
       let result=[];
       temp.forEach(item=>{
         filteredArray=[]
         result=this.addParentKey(item,array)
         if(result.length >0){
          result.forEach(res=>{
            temparray.push(res);
          })
          temparray.push(item);
         }else{
           temparray.push(item);
         }
       })
     }
     temparray=this.remove_duplicates(temparray)
     this.arrayToTree(temparray).then(tree => {
       resolve(tree);
     });
   });
 };

  remove_duplicates=(arr)=>{
    let s = new Set(arr);
    let it = s.values();
    return Array.from(it);
  }

 removeItem=(array, item)=>{
    for(var i in array){
        if(array[i]==item){
            array.splice(i,1);
            break;
        }
    }
}

  getTags = () => {
    return new Promise((resolve, reject) => {
      this.doGet(constant.get_tags)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          console.log('ERROR >> "', err);
          reject(err);
        });
    });
  };

  searchByTitle = (title) => {
    return new Promise((resolve, reject) => {
      this.doGet(constant.search_title + title)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          console.log('ERROR >> "', err);
          reject(err);
        });
    });
  };

  reviewSteps = (node, type = '', id = '') => {
    return new Promise((resolve, reject) => {

      let reviewData = {
        entity_id: [parseInt(node.stepId)],
        flag_id: [{
          target_id: "bookmark",
          target_type: "flag"
        }],
        entity_type: [{ value: "node" }],
        uid: [{ target_id: 0 }]
      }

      if (type == 'add') {
        this.doPost(constant.review_step, reviewData)
          .then(res => {
            resolve(res.data);
          })
          .catch(err => {
            console.log('ERROR >> "', err);
            reject(err);
          });
      } else {
        this.doDelete(`entity/flagging/${id}?_format=json`)
          .then(res => {
            resolve(res.data);
          })
          .catch(err => {
            console.log('ERROR >> "', err);
            reject(err);
          });
      }
    });
  }

  removeChildKey = array => {
    return array.filter(item => {
      if (item.children || item.parent == ID) {
        item.isParent = true;
        delete item["children"];
      }
      return true;
    });
  };

  treeToArray = root => {
    stack = [];
    root.map((data, index) => {
      if (typeof data.videoId !== null) {
        stack.push(data);
        if (typeof data.children !== null) {
          this.childIds(data.children);
        }
      }
    });
    return stack;
  };

  childIds = children => {
    if (typeof children !== "undefined" && children !== null) {
      {
        children.map((subchild, index) => {
          if (subchild.videoId !== null) {
            stack.push(subchild);
            if (typeof subchild.children !== "undefined") {
              this.childIds(subchild.children);
            }
          }
        });
      }
    }
  };

  ladderResponse=(res)=>{
    return new Promise((resolve,reject)=>{
      let that = this;
        let suggestions = [];
        if(Array.isArray(res.children) && (res.children !== undefined )){
            res.tags.forEach(element=>{
            if (
                !suggestions.some(el => {
                return el.name == element.name;
                })
            ) {
                suggestions.push(element);
            }
            })
        let queueVideos = that.treeToArray(res.children);
        
        let VideoList = []
        queueVideos.forEach(element => {
            VideoList.push(this.addVoteKeys(element))
            element.tags.forEach(element => {
            if (
                !suggestions.some(el => {
                return el.name == element.name;
                })
            ) {
                suggestions.push(element);
            }
            });
        });
        this.arrayToTree(VideoList).then(result => {
            let treeData = []
            let tree = this.addVoteKeys(res)
            tree.children = result;
            treeData.push(tree);
            let response = {
            tree: treeData,
            suggestions,
            queueVideos: VideoList
            };
            resolve(response)
        })
        }
        else{
            let suggestions = [];
            let treeData = [];
            res.tags.forEach(element=>{
                if (
                !suggestions.some(el => {
                    return el.name == element.name;
                })
                ) {
                suggestions.push(element);
                }
            })
            let tree = this.addVoteKeys(res)
            treeData.push(tree);
            let response = {
                tree: treeData,
                suggestions:suggestions,
                queueVideos: []
            };
            resolve(response)
        }
    })
  }

}
