import React, { Component } from "react";
import "./App.css";
import YouTube from "@u-wave/react-youtube";

import Header from "./components/header";
import Footer from "./components/footer"

import { Button } from 'reactstrap';

import ListGroupCollapse from "./components/ListGroupCollapse";


export default class App extends Component {
  stopPlayAt = null;
  stopPlayTimer;

  constructor(props) {
    super(props);
    this.state = {
      collapse: false,
      collapseArr:[],
      selected:null,
      toggleIndex:1,
      event: "",
      sec: 0,
      active: "",
      isPlay:true,
      isPlayAll:false,
      collapseIndex:1

    };
  }


  ITEM_DATA = [
    {
      title: "Download & run composer installer",
      startAt: 0,
      items: [
        {
          name: "Download from the official source",
          startAt: 0,
          endAt:10
        },
        {
          name: "Locate Composer-Setup.exe on your computer & double-click it",
          startAt: 10,
          endAt:30
        },
        {
          name: "When prompted provide path to php executable",
          startAt: 30,
          endAt:39
        }
      ]
    },
    {
      title: "Confirm composer is installed globally",
      startAt: 39,
      items: [
        {
          name: "Using Command or PowerShell, type composer and press enter",
          startAt: 39,
          endAt:42
        },
        {
          name: "Locate Composer-Setup.exe on your computer & double-click it",
          startAt: 42,
          endAt:45
        },
        {
          name: "When prompted provide path to php executable",
          startAt: 45,
          endAt:60
        }
      ]
    },
    {
      title: "Install CGR to manage globally required composer packages",
      startAt: 60,
      items: [
        {
          name: "Using Command or PowerShell, type composer and press enter",
          startAt: 60,
          endAt:90
        }
      ]
    },
    {
      title: "Create a new Drupal 8 project",
      startAt: 90,
      items: [
        {
          name: "Using Command or PowerShell, type composer and press enter",
          startAt: 90,
          endAt:150
        }
      ]
    },
    {
      title: "Add a contributed module to your project",
      startAt: 150,
      items: [
        {
          name: "Using Command or PowerShell, type composer and press enter",
          startAt: 150,
          endAt:100000
        }
      ]
    }
  ];


  onCollapseToggle = (index) => {
    //this.setState(prevState => { collapseArr: prevState.collapseArr.push(index.toString()) })
  }

  onClickTitle = (startAt,selected) => {
    if(this.state.selected === selected){
      this.setState({ collapse: !this.state.collapse, selected });
    }else{
      this.setState({ collapse: true, selected });
    }
      this.goTo(startAt);
  }

  goTo = (sec) => {
    this.state.event.target.seekTo(sec);
    this.state.event.target.playVideo();
    this.setState({ active: sec, isPlayAll:false });
  };

  onPlayerStateChange = event => {
    this.setState({ event});
    console.log(event)

    this.setState({active:this.stopPlayAt});

    // if( event.target.getCurrentTime()+1 >= this.stopPlayAt){

    // }

    this.setState({collapse:true, selected:this.state.collapseIndex});

    if(event.target.getPlayerState() === 1){



      this.setState({ isPlay:false});
    }else if (event.target.getPlayerState() === 2){
      this.setState({ isPlay:true, currTime:event.target.getCurrentTime()});


      console.log("this.state.currTime",this.state.currTime);
      console.log("this.stopPlayAt",this.stopPlayAt);

    }

    this.setStopPoint(event.target);

    if(event.target.getCurrentTime() === 0){
      this.setState({ active:0});
    }
    var time, rate, remainingTime;
    clearTimeout(this.stopPlayAllTimer);
    clearTimeout(this.stopPlayTimer);
    if (event.data === event.target.getPlayerState()) {
      time = event.target.getCurrentTime();
      if (time + 0.4 < this.stopPlayAt) {
        rate = event.target.getPlaybackRate();
        remainingTime = (this.stopPlayAt - time) / rate;
        this.stopPlayAllTimer = setTimeout(this.playVideo, remainingTime * 1000);
        this.stopPlayTimer = setTimeout(this.pauseVideo, remainingTime * 1000);
      }
    }
  };

  pauseVideo = () => {
    if(!this.state.isPlayAll){
      this.state.event.target.pauseVideo();
    }
  };

  playVideo = () => {
    if(this.state.isPlayAll){
      this.setStopPoint(this.state.event.target);
      this.setState({ collapse:true ,selected:this.state.collapseIndex});
      this.setState({ active: this.stopPlayAt, });
      var time, rate, remainingTime;
      clearTimeout(this.stopPlayAllTimer);
      if (this.state.event.data === this.state.event.target.getPlayerState()) {
        time = this.state.event.target.getCurrentTime();
        if (time + 0.4 < this.stopPlayAt) {
          rate = this.state.event.target.getPlaybackRate();
          remainingTime = (this.stopPlayAt - time) / rate;
          console.log("remainingTime",this.stopPlayAt );
          this.stopPlayAllTimer = setTimeout(this.playVideo, remainingTime * 1000);
        }
      }
    }
  };

  onClickPlayAll = () => {
    this.setState({collapse:true , selected:this.state.collapseIndex});

    this.setState({ isPlayAll:true});
    this.stopPlayAt = this.state.event.target.getDuration();
    if (this.state.event.target.getPlayerState() === 1 || this.state.event.target.getPlayerState() === 2 ) {
      this.setState({ isPlayAll:false});
      this.state.event.target.stopVideo();
    } else {
      this.state.event.target.seekTo(0);
      this.state.event.target.playVideo();
    }
  };

  onClickPlay = (selected) => {

    this.setState({ selected:this.state.collapseIndex});


    if (this.state.event.target.getPlayerState() === 1) {
      this.state.event.target.pauseVideo();
      if(!this.state.isPlayAll){
        this.setState({collapse:true })
      }
    } else {
      this.state.event.target.playVideo();
      if(!this.state.isPlayAll){
        this.setState({collapse:true,selected:selected == null ? 1 : selected})
      }
    }
  }

  setStopPoint = event => {
    let flag = true;
    this.ITEM_DATA.forEach((data,index,arr) => {

      if(data.startAt <= this.stopPlayAt) {
        this.setState({collapseIndex:index+1})
      }


      if (flag) {
        let temp = true;
        data.items.forEach(item => {
          if (index === arr.length - 1){
            this.stopPlayAt = event.getDuration();
          }
          if (temp && item.startAt > Math.floor(event.getCurrentTime()) + 1) {
            flag = false;
            temp = false;
            this.stopPlayAt = item.startAt;
            this.setState({ active:  item.startAt });
          } else { return false; }
        });
      } else { return false; }
    });
  };
  componentDidMount(){
  }

  render() {
    const { active, selected, isPlay, isPlayAll,collapseIndex } = this.state;

    return (
      <div>
        <div className="container">
          <div className="row">
            <div className="col-md-12 instruction">
                <h3 className="mt-5">steps to install & use Composer with Drupal 8</h3>
                <div className="row mt-5 collapse-block">
                  <div className="col-md-6">
                  {this.ITEM_DATA.map((data, index) =>
                    <ListGroupCollapse
                      key={index}
                      sourceData={data.items}
                      selected={selected}
                      titleSeries={index+1}
                      startAt={data.startAt}
                      itemTitle={data.title}
                      onClickItem={this.goTo}
                      onClickTitle={this.onClickTitle}
                      onCollapseToggle={this.onCollapseToggle}
                      collepsed={this.state.collapse}
                      active={parseInt(active)}
                      activeItemColor="blue"
                      defaultItemColor="#666"
                      listViewClass="listView"
                      collapseIndex={collapseIndex}
                       />
                  )}

                  </div>
                  <div className="col-md-6 ">
                     <YouTube
                        ref={(ref) => this.emailInputRef = ref}
                        video="fsVL_xrYO0w"
                        autoplay={false}
                        showRelatedVideos={false}
                        controls={false}
                        width={'100%'}
                        height={350}
                        startSeconds={parseInt(this.state.sec)}
                        onReady={event => this.setState({ event})}
                        onStateChange={event => this.onPlayerStateChange(event)} />

                        <div className="video-btn" >
                          <Button color={"success"} onClick={()=>this.onClickPlay(selected)}>
                           {isPlay ? "Play" : "Pause"}
                          </Button>
                          <Button color={"success"} onClick={()=>this.onClickPlayAll()}>
                            {isPlayAll ? "STOP" : (!isPlay ? "STOP":"Play All")}
                          </Button>
                        </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
