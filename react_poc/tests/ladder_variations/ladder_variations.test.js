
/*
Reference:https://gitlab.com/onso-labs/step/blob/master/features/manage_variations.feature
Objectives: 
     As a ladder follower, I can
      - see several variations and compare their diffs (proposed: multiple columns with highlighted additions/subtractions/moves)
      - request even more variations (proposed: load more button)

*/


import constant from "../../src/common/config";

module.exports = {
    tags: ['variation-diff'],

    'beforeEach': (browser, done) => {
      browser.url(constant.TEST_URL, () => {
            browser.useCss().expect.element('body').to.be.present.before(1000);
            done();
        });
        browser
        .url(constant.TEST_URL)
        .execute(function() {
          window.localStorage.setItem('ajs_token', true);
          return true;
        }, [], function(result) {
          this.assert.ok(result.value);
        })
        
    },
    "Test that 'load more' button will load more variations if available.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-edit-hover",function(){
            browser.elements("css selector",'div .button-edit-hover',function(items){
                browser.elementIdClick(items.value[1][Object.keys(items.value[1])[0]],function(){
                    this.pause(10000);
                    browser.waitForElementVisible(".navbar-nav button",function(){
                        browser.getText(".navbar-nav button",function(text){
                            this.assert.equal(text.value,"GO BACK");
                        })
                        browser.click(".navbar-nav button",function(){
                            this.pause(1000);
                            browser.waitForElementVisible("div .button-dropdown ",function(){
                                browser.getText("div .button-dropdown ",function(result){
                                    this.assert.equal(result.value, "INSTRUCTIONAL STEPS MODE")
                                })
                            })
                        })
                    })
                })
            })   
        })
    },

    "Test that each variation will show difference with red or green color from base ladder.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-edit-hover",function(){
            browser.elements("css selector",'div .button-edit-hover',function(items){
                browser.elementIdClick(items.value[1][Object.keys(items.value[1])[0]],function(){
                    this.pause(10000);
                    browser.waitForElementVisible(".navbar-nav button",function(){
                        browser.getText(".navbar-nav button",function(text){
                            this.assert.equal(text.value,"GO BACK");
                        })
                        browser.click(".navbar-nav button",function(){
                            this.pause(1000);
                            browser.waitForElementVisible("div .button-dropdown ",function(){
                                browser.getText("div .button-dropdown ",function(result){
                                    this.assert.equal(result.value, "INSTRUCTIONAL STEPS MODE")
                                })
                            })
                        })
                    })
                })
            })   
        })
    },

    "Test that each variation will show specific changes in child/parent node.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-edit-hover",function(){
            browser.elements("css selector",'div .button-edit-hover',function(items){
                browser.elementIdClick(items.value[1][Object.keys(items.value[1])[0]],function(){
                    this.pause(10000);
                    browser.waitForElementVisible(".navbar-nav button",function(){
                        browser.getText(".navbar-nav button",function(text){
                            this.assert.equal(text.value,"GO BACK");
                        })
                        browser.click(".navbar-nav button",function(){
                            this.pause(1000);
                            browser.waitForElementVisible("div .button-dropdown ",function(){
                                browser.getText("div .button-dropdown ",function(result){
                                    this.assert.equal(result.value, "INSTRUCTIONAL STEPS MODE")
                                })
                            })
                        })
                    })
                })
            })   
        })
    },

    "Test that in update-add case only the added content will be higlighted with the green color.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-edit-hover",function(){
            browser.elements("css selector",'div .button-edit-hover',function(items){
                browser.elementIdClick(items.value[1][Object.keys(items.value[1])[0]],function(){
                    this.pause(10000);
                    browser.waitForElementVisible(".navbar-nav button",function(){
                        browser.getText(".navbar-nav button",function(text){
                            this.assert.equal(text.value,"GO BACK");
                        })
                        browser.click(".navbar-nav button",function(){
                            this.pause(1000);
                            browser.waitForElementVisible("div .button-dropdown ",function(){
                                browser.getText("div .button-dropdown ",function(result){
                                    this.assert.equal(result.value, "INSTRUCTIONAL STEPS MODE")
                                })
                            })
                        })
                    })
                })
            })   
        })
    },
    
    "Test that in update-delete case only the deleted content will be higlighted with the red color.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-edit-hover",function(){
            browser.elements("css selector",'div .button-edit-hover',function(items){
                browser.elementIdClick(items.value[1][Object.keys(items.value[1])[0]],function(){
                    this.pause(10000);
                    browser.waitForElementVisible(".navbar-nav button",function(){
                        browser.getText(".navbar-nav button",function(text){
                            this.assert.equal(text.value,"GO BACK");
                        })
                        browser.click(".navbar-nav button",function(){
                            this.pause(1000);
                            browser.waitForElementVisible("div .button-dropdown ",function(){
                                browser.getText("div .button-dropdown ",function(result){
                                    this.assert.equal(result.value, "INSTRUCTIONAL STEPS MODE")
                                })
                            })
                        })
                    })
                })
            })   
        })
    },

    "Test that in update case added specific content will be higlighted with the green and deleted content will be highlighted with the red color.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-edit-hover",function(){
            browser.elements("css selector",'div .button-edit-hover',function(items){
                browser.elementIdClick(items.value[1][Object.keys(items.value[1])[0]],function(){
                    this.pause(10000);
                    browser.waitForElementVisible(".navbar-nav button",function(){
                        browser.getText(".navbar-nav button",function(text){
                            this.assert.equal(text.value,"GO BACK");
                        })
                        browser.click(".navbar-nav button",function(){
                            this.pause(1000);
                            browser.waitForElementVisible("div .button-dropdown ",function(){
                                browser.getText("div .button-dropdown ",function(result){
                                    this.assert.equal(result.value, "INSTRUCTIONAL STEPS MODE")
                                })
                            })
                        })
                    })
                })
            })   
        })
    },

    "Test that if whole node or one step is deleted then in the variation whole node's title will be highlighted in red color.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-edit-hover",function(){
            browser.elements("css selector",'div .button-edit-hover',function(items){
                browser.elementIdClick(items.value[1][Object.keys(items.value[1])[0]],function(){
                    this.pause(10000);
                    browser.waitForElementVisible(".navbar-nav button",function(){
                        browser.getText(".navbar-nav button",function(text){
                            this.assert.equal(text.value,"GO BACK");
                        })
                        browser.click(".navbar-nav button",function(){
                            this.pause(1000);
                            browser.waitForElementVisible("div .button-dropdown ",function(){
                                browser.getText("div .button-dropdown ",function(result){
                                    this.assert.equal(result.value, "INSTRUCTIONAL STEPS MODE")
                                })
                            })
                        })
                    })
                })
            })   
        })
    },

    "Test that if whole node or one step is added then in the variation whole node's title will be highlighted in green color.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-edit-hover",function(){
            browser.elements("css selector",'div .button-edit-hover',function(items){
                browser.elementIdClick(items.value[1][Object.keys(items.value[1])[0]],function(){
                    this.pause(10000);
                    browser.waitForElementVisible(".navbar-nav button",function(){
                        browser.getText(".navbar-nav button",function(text){
                            this.assert.equal(text.value,"GO BACK");
                        })
                        browser.click(".navbar-nav button",function(){
                            this.pause(1000);
                            browser.waitForElementVisible("div .button-dropdown ",function(){
                                browser.getText("div .button-dropdown ",function(result){
                                    this.assert.equal(result.value, "INSTRUCTIONAL STEPS MODE")
                                })
                            })
                        })
                    })
                })
            })   
        })
    },

    "Test that only the modified/added/deleted node/step can be dragged.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-edit-hover",function(){
            browser.elements("css selector",'div .button-edit-hover',function(items){
                browser.elementIdClick(items.value[1][Object.keys(items.value[1])[0]],function(){
                    this.pause(10000);
                    browser.waitForElementVisible(".navbar-nav button",function(){
                        browser.getText(".navbar-nav button",function(text){
                            this.assert.equal(text.value,"GO BACK");
                        })
                        browser.click(".navbar-nav button",function(){
                            this.pause(1000);
                            browser.waitForElementVisible("div .button-dropdown ",function(){
                                browser.getText("div .button-dropdown ",function(result){
                                    this.assert.equal(result.value, "INSTRUCTIONAL STEPS MODE")
                                })
                            })
                        })
                    })
                })
            })   
        })
    },

    "Test that only the modified/added/deleted node/step can be dropped into only the base ladder.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-edit-hover",function(){
            browser.elements("css selector",'div .button-edit-hover',function(items){
                browser.elementIdClick(items.value[1][Object.keys(items.value[1])[0]],function(){
                    this.pause(10000);
                    browser.waitForElementVisible(".navbar-nav button",function(){
                        browser.getText(".navbar-nav button",function(text){
                            this.assert.equal(text.value,"GO BACK");
                        })
                        browser.click(".navbar-nav button",function(){
                            this.pause(1000);
                            browser.waitForElementVisible("div .button-dropdown ",function(){
                                browser.getText("div .button-dropdown ",function(result){
                                    this.assert.equal(result.value, "INSTRUCTIONAL STEPS MODE")
                                })
                            })
                        })
                    })
                })
            })   
        })
    },

    "Test that the base ladder will always stick to the right corner of the page.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-edit-hover",function(){
            browser.elements("css selector",'div .button-edit-hover',function(items){
                browser.elementIdClick(items.value[1][Object.keys(items.value[1])[0]],function(){
                    this.pause(10000);
                    browser.waitForElementVisible(".navbar-nav button",function(){
                        browser.getText(".navbar-nav button",function(text){
                            this.assert.equal(text.value,"GO BACK");
                        })
                        browser.click(".navbar-nav button",function(){
                            this.pause(1000);
                            browser.waitForElementVisible("div .button-dropdown ",function(){
                                browser.getText("div .button-dropdown ",function(result){
                                    this.assert.equal(result.value, "INSTRUCTIONAL STEPS MODE")
                                })
                            })
                        })
                    })
                })
            })   
        })
    },

    "Test that the variation ladders will be scrolled horizontally.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-edit-hover",function(){
            browser.elements("css selector",'div .button-edit-hover',function(items){
                browser.elementIdClick(items.value[1][Object.keys(items.value[1])[0]],function(){
                    this.pause(10000);
                    browser.waitForElementVisible(".navbar-nav button",function(){
                        browser.getText(".navbar-nav button",function(text){
                            this.assert.equal(text.value,"GO BACK");
                        })
                        browser.click(".navbar-nav button",function(){
                            this.pause(1000);
                            browser.waitForElementVisible("div .button-dropdown ",function(){
                                browser.getText("div .button-dropdown ",function(result){
                                    this.assert.equal(result.value, "INSTRUCTIONAL STEPS MODE")
                                })
                            })
                        })
                    })
                })
            })   
        })
    },

    "Test that on the click of node/step if it is collapsed then it'll expand it.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-edit-hover",function(){
            browser.elements("css selector",'div .button-edit-hover',function(items){
                browser.elementIdClick(items.value[1][Object.keys(items.value[1])[0]],function(){
                    this.pause(10000);
                    browser.waitForElementVisible(".navbar-nav button",function(){
                        browser.getText(".navbar-nav button",function(text){
                            this.assert.equal(text.value,"GO BACK");
                        })
                        browser.click(".navbar-nav button",function(){
                            this.pause(1000);
                            browser.waitForElementVisible("div .button-dropdown ",function(){
                                browser.getText("div .button-dropdown ",function(result){
                                    this.assert.equal(result.value, "INSTRUCTIONAL STEPS MODE")
                                })
                            })
                        })
                    })
                })
            })   
        })
    },

    "Test that on the click of node/step if it is expanded then it'll collapse it.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-edit-hover",function(){
            browser.elements("css selector",'div .button-edit-hover',function(items){
                browser.elementIdClick(items.value[1][Object.keys(items.value[1])[0]],function(){
                    this.pause(10000);
                    browser.waitForElementVisible(".navbar-nav button",function(){
                        browser.getText(".navbar-nav button",function(text){
                            this.assert.equal(text.value,"GO BACK");
                        })
                        browser.click(".navbar-nav button",function(){
                            this.pause(1000);
                            browser.waitForElementVisible("div .button-dropdown ",function(){
                                browser.getText("div .button-dropdown ",function(result){
                                    this.assert.equal(result.value, "INSTRUCTIONAL STEPS MODE")
                                })
                            })
                        })
                    })
                })
            })   
        })
    },


    'after': function(browser) {
        browser
          .perform(() => {
            console.log('[perform]: Finished Test:', browser.currentTest.name);
          })
          .end();
      },
}