/*
Reference:https://gitlab.com/onso-labs/step/blob/master/features/manage_variations.feature
Objectives: 
    As a ladder follower, I can
      - review alternative versions of the ladder

*/

import constant from "../../src/common/config";

module.exports = {
    tags: ['variation-review'],

    'beforeEach': (browser, done) => {
      browser.url(constant.TEST_URL, () => {
            browser.useCss().expect.element('body').to.be.present.before(1000);
            done();
        });
        browser
        .url(constant.TEST_URL)
        .execute(function() {
          window.localStorage.setItem('ajs_token', true);
          return true;
        }, [], function(result) {
          this.assert.ok(result.value);
        })
        
    },

    "Test that user can review alternate versions of the ladder.":(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-edit-hover",function(){
            browser.elements("css selector",'div .button-edit-hover',function(items){
                browser.elementIdClick(items.value[1][Object.keys(items.value[1])[0]],function(){
                    this.pause(10000);
                    browser.waitForElementVisible("div .card-current-version:nth-child(3) .card-header .float-left",function(){
                        browser.click("div .card-current-version:nth-child(3) .card-header .float-left",function(){
                            this.pause(1000);
                            browser.waitForElementVisible("div .button-dropdown ",function(){
                                browser.getText("div .button-dropdown ",function(result){
                                    this.assert.equal(result.value, "INSTRUCTIONAL STEPS MODE")
                                })
                            })
                        })
                    })
                })
            })   
        })
    },


    'after': function(browser) {
        browser
          .perform(() => {
            console.log('[perform]: Finished Test:', browser.currentTest.name);
          })
          .end();
      },
}