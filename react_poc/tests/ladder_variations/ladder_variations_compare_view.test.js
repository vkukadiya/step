
/*
Reference:https://gitlab.com/onso-labs/step/blob/feature_variations_diff/features/variation_diff_view.feature
Objectives: 
   As a follower of the ladder:
    -- I should be able to see the variation differences in side by side format
*/


import constant from "../../src/common/config";

module.exports = {
    tags: ['variation-diff'],

    'beforeEach': (browser, done) => {
      browser.url(constant.TEST_URL, () => {
            browser.useCss().expect.element('body').to.be.present.before(1000);
            done();
        });
        browser
        .url(constant.TEST_URL)
        .execute(function() {
          window.localStorage.setItem('ajs_token', true);
          return true;
        }, [], function(result) {
          this.assert.ok(result.value);
        })
        
    },

    "Test that base ladder will render at left side.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-edit-hover",function(){
            browser.elements("css selector",'div .button-edit-hover',function(items){
                browser.elementIdClick(items.value[1][Object.keys(items.value[1])[0]],function(){
                    this.pause(10000);
                    browser.waitForElementVisible(".navbar-nav button",function(){
                        browser.getText(".navbar-nav button",function(text){
                            this.assert.equal(text.value,"GO BACK");
                        })
                        browser.click(".navbar-nav button",function(){
                            this.pause(1000);
                            browser.waitForElementVisible("div .button-dropdown ",function(){
                                browser.getText("div .button-dropdown ",function(result){
                                    this.assert.equal(result.value, "INSTRUCTIONAL STEPS MODE")
                                })
                            })
                        })
                    })
                })
            })   
        })
    },

    "Test that variation ladder will render at right side.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-edit-hover",function(){
            browser.elements("css selector",'div .button-edit-hover',function(items){
                browser.elementIdClick(items.value[1][Object.keys(items.value[1])[0]],function(){
                    this.pause(10000);
                    browser.waitForElementVisible(".navbar-nav button",function(){
                        browser.getText(".navbar-nav button",function(text){
                            this.assert.equal(text.value,"GO BACK");
                        })
                        browser.click(".navbar-nav button",function(){
                            this.pause(1000);
                            browser.waitForElementVisible("div .button-dropdown ",function(){
                                browser.getText("div .button-dropdown ",function(result){
                                    this.assert.equal(result.value, "INSTRUCTIONAL STEPS MODE")
                                })
                            })
                        })
                    })
                })
            })   
        })
    },
    

    'after': function(browser) {
        browser
          .perform(() => {
            console.log('[perform]: Finished Test:', browser.currentTest.name);
          })
          .end();
      },
}