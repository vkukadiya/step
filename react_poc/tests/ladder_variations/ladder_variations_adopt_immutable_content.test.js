/*
Reference:https://gitlab.com/onso-labs/step/blob/master/features/manage_variations.feature
Objectives: 
    As a ladder follower, I can
    - adopt any of the variation of the ladder.
    - so, that adopt variation will always be displayed at the top

*/

import constant from "../../src/common/config";

module.exports = {
    tags: ['variation-adopted'],

    'beforeEach': (browser, done) => {
      browser.url(constant.TEST_URL, () => {
            browser.useCss().expect.element('body').to.be.present.before(1000);
            done();
        });
        browser
        .url(constant.TEST_URL)
        .execute(function() {
          window.localStorage.setItem('ajs_token', true);
          return true;
        }, [], function(result) {
          this.assert.ok(result.value);
        })
        
    },

    "Test that Go Back button will redirect back to the instructional mode.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-edit-hover",function(){
            browser.elements("css selector",'div .button-edit-hover',function(items){
                browser.elementIdClick(items.value[1][Object.keys(items.value[1])[0]],function(){
                    this.pause(10000);
                    browser.waitForElementVisible(".navbar-nav button",function(){
                        browser.getText(".navbar-nav button",function(text){
                            this.assert.equal(text.value,"GO BACK");
                        })
                        browser.click(".navbar-nav button",function(){
                            this.pause(1000);
                            browser.waitForElementVisible("div .button-dropdown ",function(){
                                browser.getText("div .button-dropdown ",function(result){
                                    this.assert.equal(result.value, "INSTRUCTIONAL STEPS MODE")
                                })
                            })
                        })
                    })
                })
            })   
        })
    },
    
    "Test that each variation card has adopt button for adopting that variation.":(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-edit-hover",function(){
            browser.elements("css selector",'div .button-edit-hover',function(items){
                browser.elementIdClick(items.value[1][Object.keys(items.value[1])[0]],function(){
                    this.pause(10000);
                    browser.waitForElementVisible(".clearfix .btn-secondary",function(){
                        browser.elements("css selector",".clearfix .btn-secondary",function(text){
                            this.verify.ok(text.value.length == 3);
                        })
                    })
                })
            })   
        })
    },

    "Test that adopted variation of the ladder will be displayed at the top.":(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.click('div .button-edit-hover',function(){
            browser.elements("css selector"," div .ladder-btn",function(){
                browser.click('div .icon-edit', function(){
                    browser.elements("css selector", "div .ladder-btn",function(elements){
                        browser.elementIdClick(elements.value[0][Object.keys(elements.value[0])[0]], function() {
                          browser.waitForElementVisible(".card-view .card",function(){
                                browser.elements("css selector",".card-view:first-child .card:first-child .float-left",function(result){
                                    browser.elementIdText(elements.value[0][Object.keys(elements.value[0])[0]],function(text){
                                        this.assert.equal(text.value,"Adopted Version")
                                    })
                                })
                            })
                        });
                    })
                })
            })
        })   
    },

    "Test that collapse menu is visible and it will expand on the click of that in card.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-edit-hover",function(){
            browser.elements("css selector",'div .button-edit-hover',function(items){
                browser.elementIdClick(items.value[1][Object.keys(items.value[1])[0]],function(){
                    this.pause(10000);
                   browser.waitForElementVisible(".card .rc-collapse-header",function(){
                       browser.elements("css selector",".card .rc-collapse-header",function(result){
                           browser.elementIdClick(result.value[1][Object.keys(result.value[1])[0]],function(){
                               browser.elements("css selector",".card .rc-collapse-content-active",function(res){
                                   this.verify.ok(res);
                               })
                           })
                       })
                   })
                })
            })   
        })
    },

    "Test that by clicking 'Popular' from dropdown it'll sort variations based on popularity.":(browser)=>{
        let date;
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-edit-hover",function(){
            browser.elements("css selector",'div .button-edit-hover',function(items){
                browser.elementIdClick(items.value[1][Object.keys(items.value[1])[0]],function(){
                    this.pause(10000);
                    browser.useCss().getText("div .card-current-version:first-child .card-footer p",function(text){
                        date=text.value.substring(12);
                    })
                    browser.waitForElementVisible("div .row .col-md-4:nth-child(3) .dropdown-view",function(){
                        browser.useCss().click("div .row .col-md-4:nth-child(3) .dropdown-view button",function(result){
                            browser.useXpath().click("//*[contains(text(),'Popular')]",function(){
                                this.pause(3000);
                                browser.useCss().getText("div .card-current-version:first-child .card-footer p",function(item){
                                   this.verify.ok(item.value.substring(12) >= date || item.value.substring(12) <= date)
                                })
                            });
                        })
                    })
                })
            })   
        })
    },

    "Test that by clicking 'Recent' from dropdown it'll sort variations based on Recent updation.":(browser)=>{
        let date;
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-edit-hover",function(){
            browser.elements("css selector",'div .button-edit-hover',function(items){
                browser.elementIdClick(items.value[1][Object.keys(items.value[1])[0]],function(){
                    this.pause(10000);
                    browser.useCss().getText("div .card-current-version:first-child .card-footer p",function(text){
                        date=text.value.substring(12);
                    })
                    browser.waitForElementVisible("div .row .col-md-4:nth-child(3) .dropdown-view",function(){
                        browser.useCss().click("div .row .col-md-4:nth-child(3) .dropdown-view button",function(result){
                            browser.useXpath().click("//*[contains(text(),'Recent')]",function(){
                                this.pause(3000);
                                browser.useCss().getText("div .card-current-version:first-child .card-footer p",function(item){
                                   this.verify.ok(item.value.substring(12)>= date)
                                })
                            });
                        })
                    })
                })
            })   
        })
    },

    "Test that by clicking Active option it will show variations of the ladder which are active.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.click('div .button-edit-hover',function(){
            browser.elements("css selector"," div .ladder-btn",function(){
                browser.click('div .icon-edit', function(){
                    browser.elements("css selector", "div .ladder-btn",function(elements){
                        browser.elementIdClick(elements.value[0][Object.keys(elements.value[0])[0]], function() {
                            browser.waitForElementVisible("div .button-dropdown",function(){
                                browser.click("div .button-dropdown",function(){
                                    browser.elements("div .dropdown-item",function(result){
                                        browser.elementIdClick(result.value[2][Object.keys(result.value[2])[0]], function(text) {
                                            this.verify.ok(text);
                                        });
                                    })
                                })
                            })
                        });
                    })
                })
            })
        })   
    },

    'after': function(browser) {
        browser
          .perform(() => {
            console.log('[perform]: Finished Test:', browser.currentTest.name);
          })
          .end();
      },
}