/*
Reference:https://gitlab.com/onso-labs/step/blob/master/features/manage_variations.feature
Objectives: 
    As a ladder follower, I can
      - see variations which are highly voted
      - see variations which are up to date

*/


import constant from "../../src/common/config";

module.exports = {
    tags: ['variation-popular'],

    'beforeEach': (browser, done) => {
      browser.url(constant.TEST_URL, () => {
            browser.useCss().expect.element('body').to.be.present.before(1000);
            done();
        });
        browser
        .url(constant.TEST_URL)
        .execute(function() {
          window.localStorage.setItem('ajs_token', true);
          return true;
        }, [], function(result) {
          this.assert.ok(result.value);
        })
        
    },


    "Test that by clicking 'Popular' from dropdown it'll sort variations based on popularity.":(browser)=>{
        let date;
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-edit-hover",function(){
            browser.elements("css selector",'div .button-edit-hover',function(items){
                browser.elementIdClick(items.value[1][Object.keys(items.value[1])[0]],function(){
                    this.pause(10000);
                    browser.useCss().getText("div .card-current-version:first-child .card-footer p",function(text){
                        date=text.value.substring(12);
                    })
                    browser.waitForElementVisible("div .row .col-md-4:nth-child(3) .dropdown-view",function(){
                        browser.useCss().click("div .row .col-md-4:nth-child(3) .dropdown-view button",function(result){
                            browser.useXpath().click("//*[contains(text(),'Popular')]",function(){
                                this.pause(3000);
                                browser.useCss().getText("div .card-current-version:first-child .card-footer p",function(item){
                                   this.verify.ok(item.value.substring(12) >= date || item.value.substring(12) <= date)
                                })
                            });
                        })
                    })
                })
            })   
        })
    },

    "Test that by clicking 'Recent' from dropdown it'll sort variations based on Recent updation.":(browser)=>{
        let date;
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-edit-hover",function(){
            browser.elements("css selector",'div .button-edit-hover',function(items){
                browser.elementIdClick(items.value[1][Object.keys(items.value[1])[0]],function(){
                    this.pause(10000);
                    browser.useCss().getText("div .card-current-version:first-child .card-footer p",function(text){
                        date=text.value.substring(12);
                    })
                    browser.waitForElementVisible("div .row .col-md-4:nth-child(3) .dropdown-view",function(){
                        browser.useCss().click("div .row .col-md-4:nth-child(3) .dropdown-view button",function(result){
                            browser.useXpath().click("//*[contains(text(),'Recent')]",function(){
                                this.pause(3000);
                                browser.useCss().getText("div .card-current-version:first-child .card-footer p",function(item){
                                   this.verify.ok(item.value.substring(12)>= date)
                                })
                            });
                        })
                    })
                })
            })   
        })
    },

    "Test that by default the most popular variation of the ladder is showing if user has not altered or written content of that ladder.":(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-edit-hover",function(){
            browser.elements("css selector",'div .button-edit-hover',function(items){
                browser.elementIdClick(items.value[1][Object.keys(items.value[1])[0]],function(){
                    this.pause(10000);
                    browser.waitForElementVisible("div .row .col-md-4:nth-child(3) .dropdown-view",function(){
                        browser.useCss().getText("div .row .col-md-4:nth-child(3) .dropdown-view button",function(result){
                           this.assert.equal(result.value,'POPULAR')
                        })
                    })
                })
            })   
        })
    },

    'after': function(browser) {
        browser
          .perform(() => {
            console.log('[perform]: Finished Test:', browser.currentTest.name);
          })
          .end();
      },
}