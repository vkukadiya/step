import constant from "../../src/common/config";

module.exports = {
    tags: ['revision'],

    'beforeEach': (browser, done) => {
      browser.url(constant.TEST_URL, () => {
            browser.useCss().expect.element('body').to.be.present.before(1000);
            done();
        });
        browser
        .url(constant.TEST_URL)
        .execute(function() {
          window.localStorage.setItem('ajs_token', true);
          return true;
        }, [], function(result) {
          this.assert.ok(result.value);
        })
        
    },
    'Test that title of the main ladder is visible.':(browser)=>{ 
        let temp='';
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.useCss().waitForElementVisible('div .active-row', function() {
            this.getText('div .active-row',function(text){
                temp=text.value;
            })
            browser.click('div .button-edit-hover',function(){
                browser.elements("css selector"," div .ladder-btn",function(result){
                    browser.click('div .icon-edit', function(){
                        browser.elements("css selector", "div .ladder-btn",function(elements){
                            browser.elementIdClick(elements.value[0][Object.keys(elements.value[0])[0]], function() {
                                browser.getText(".collapse span",function(title){
                                    this.assert.equal(title.value,temp);
                                })
                            });
                        })
                    })
                })
            })   
        })
    },

    "Test that Go Back button is visible and clickable.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.click('div .button-edit-hover',function(){
            browser.elements("css selector"," div .ladder-btn",function(){
                browser.click('div .icon-edit', function(){
                    browser.elements("css selector", "div .ladder-btn",function(elements){
                        browser.elementIdClick(elements.value[0][Object.keys(elements.value[0])[0]], function() {
                          browser.waitForElementVisible(".navbar-nav button",function(){
                              browser.getText(".navbar-nav button",function(text){
                                  this.assert.equal(text.value,"GO BACK");
                              })
                              browser.click(".navbar-nav button",function(result){
                                  this.verify.ok(result);
                              })
                          })
                        });
                    })
                })
            })
        })   
    },

    "Test that checkbox is clickable and checkbox's title 'Always compare with my version' is loaded.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.click('div .button-edit-hover',function(){
            browser.elements("css selector"," div .ladder-btn",function(result){
                browser.click('div .icon-edit', function(){
                    browser.elements("css selector", "div .ladder-btn",function(elements){
                        browser.elementIdClick(elements.value[0][Object.keys(elements.value[0])[0]], function() {
                           browser.waitForElementVisible("div .form-check-label",function(){
                               browser.getText("div .form-check-label",function(text){
                                   this.assert.equal(text.value,'Always compare with my version');
                               })
                               browser.setValue('.form-check-label input[type=checkbox]', browser.Keys.ENTER,function(item){
                                this.verify.ok(item);
                            });
                           })
                        });
                    })
                })
            })
        })   
    },

    "Test that card of versions is visible in one page.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.click('div .button-edit-hover',function(){
            browser.elements("css selector"," div .ladder-btn",function(){
                browser.click('div .icon-edit', function(){
                    browser.elements("css selector", "div .ladder-btn",function(elements){
                        browser.elementIdClick(elements.value[0][Object.keys(elements.value[0])[0]], function() {
                          browser.waitForElementVisible(".card-view .card",function(){
                             browser.elements("css selector",".card-view .card",function(result){
                                 this.verify.ok(result.value.length >0);
                             })
                          })
                        });
                    })
                })
            })
        })   
    },

    "Test that by default the most popular version of the ladder is showing if user has not altered or written content of that ladder.":(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.click('div .button-edit-hover',function(){
            browser.elements("css selector"," div .ladder-btn",function(){
                browser.click('div .icon-edit', function(){
                    browser.elements("css selector", "div .ladder-btn",function(elements){
                        browser.elementIdClick(elements.value[0][Object.keys(elements.value[0])[0]], function() {
                          browser.waitForElementVisible(".card-view .card",function(){
                             browser.elements("css selector",".card-view:first-child .card:first-child .float-left",function(result){
                                browser.elementIdText(elements.value[0][Object.keys(elements.value[0])[0]],function(text){
                                    this.assert.equal(text.value,"Most Popular Version")
                                })
                             })
                          })
                        });
                    })
                })
            })
        })   
    },

    "Test that if user has visited any of the version of the ladder then it will show that version instead of most popular version.":(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.click('div .button-edit-hover',function(){
            browser.elements("css selector"," div .ladder-btn",function(){
                browser.click('div .icon-edit', function(){
                    browser.elements("css selector", "div .ladder-btn",function(elements){
                        browser.elementIdClick(elements.value[0][Object.keys(elements.value[0])[0]], function() {
                          browser.waitForElementVisible(".card-view .card",function(){
                             browser.elements("css selector",".card-view:first-child .card:first-child .float-left",function(result){
                                browser.elementIdText(elements.value[0][Object.keys(elements.value[0])[0]],function(text){
                                    this.assert.equal(text.value,"Visited Version")
                                })
                             })
                          })
                        });
                    })
                })
            })
        })   
    },

    "Test that each version card has adopt button for adopting that version and it is clickable.":(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.click('div .button-edit-hover',function(){
            browser.elements("css selector"," div .ladder-btn",function(){
                browser.click('div .icon-edit', function(){
                    browser.elements("css selector", "div .ladder-btn",function(elements){
                        browser.elementIdClick(elements.value[0][Object.keys(elements.value[0])[0]], function() {
                          browser.waitForElementVisible(".card-view .card",function(){
                             browser.elements("css selector",".card-view .card .card-header button",function(result){
                                browser.elementIdText(elements.value[0][Object.keys(elements.value[0])[0]],function(text){
                                    this.assert.equal(text.value,"Adopt")
                                })
                                browser.elementIdClick(elements.value[0][Object.keys(elements.value[0])[0]],function(result){
                                    this.verify.ok(result);
                                })
                             })
                          })
                        });
                    })
                })
            })
        })   
    },

    "Test that adopted version of the ladder will be displayed at the top.":(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.click('div .button-edit-hover',function(){
            browser.elements("css selector"," div .ladder-btn",function(){
                browser.click('div .icon-edit', function(){
                    browser.elements("css selector", "div .ladder-btn",function(elements){
                        browser.elementIdClick(elements.value[0][Object.keys(elements.value[0])[0]], function() {
                          browser.waitForElementVisible(".card-view .card",function(){
                             browser.elements("css selector",".card-view:first-child .card:first-child .float-left",function(result){
                                browser.elementIdText(elements.value[0][Object.keys(elements.value[0])[0]],function(text){
                                    this.assert.equal(text.value,"Visited Version")
                                })
                             })
                          })
                        });
                    })
                })
            })
        })   
    },

    "Test that header of the card are visible.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.click('div .button-edit-hover',function(){
            browser.elements("css selector"," div .ladder-btn",function(){
                browser.click('div .icon-edit', function(){
                    browser.elements("css selector", "div .ladder-btn",function(elements){
                        browser.elementIdClick(elements.value[0][Object.keys(elements.value[0])[0]], function() {
                            browser.waitForElementVisible(".card-view .card .card-header",function(){
                                browser.elements("css selector",".card-view .card .card-header",function(result){
                                    this.verify.ok(result.value.length == 3);
                                })
                            })
                        });
                    })
                })
            })
        })   
    },

    "Test that collapse menu is visible and it will expand on the click of that in card.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.click('div .button-edit-hover',function(){
            browser.elements("css selector"," div .ladder-btn",function(){
                browser.click('div .icon-edit', function(){
                    browser.elements("css selector", "div .ladder-btn",function(elements){
                        browser.elementIdClick(elements.value[0][Object.keys(elements.value[0])[0]], function() {
                            browser.waitForElementVisible(".card-view .card .card-body .rc-collapse",function(){
                                browser.elements("css selector",".card-view .card .card-header",function(result){
                                    this.verify.ok(result.value.length == 3);
                                    browser.click(".card-view .card .card-body .rc-collapse-header",function(){
                                        browser.elements("css selector",".card-view .card .card-body  .rc-collapse-content-active",function(item){
                                            this.verify.ok(item.value.length !== 0);
                                        })
                                    })
                                })
                            })
                        });
                    })
                })
            })
        })   
    },

    "Test that updated at is visible in card footer.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.click('div .button-edit-hover',function(){
            browser.elements("css selector"," div .ladder-btn",function(){
                browser.click('div .icon-edit', function(){
                    browser.elements("css selector", "div .ladder-btn",function(elements){
                        browser.elementIdClick(elements.value[0][Object.keys(elements.value[0])[0]], function() {
                            browser.waitForElementVisible(".card-view .card .card-footer",function(){
                                browser.elements("css selector",".card-view .card .card-footer p",function(result){
                                    this.verify.ok(result.value.length == 3);
                                    browser.getText(".card-view .card .card-footer p",function(text){
                                        this.assert.equal(text.value,"Updated at: 2019-04-19")
                                    })
                                })
                            })
                        });
                    })
                })
            })
        })   
    },

    "Test that Compare with button is visible and clickable in card footer.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.click('div .button-edit-hover',function(){
            browser.elements("css selector"," div .ladder-btn",function(){
                browser.click('div .icon-edit', function(){
                    browser.elements("css selector", "div .ladder-btn",function(elements){
                        browser.elementIdClick(elements.value[0][Object.keys(elements.value[0])[0]], function() {
                            browser.waitForElementVisible(".card-view .card .card-footer",function(){
                                browser.elements("css selector",".card-view .card .card-footer .dropdown-toggle",function(result){
                                    this.verify.ok(result.value.length == 3);
                                    browser.getText(".card-view .card .card-footer .dropdown-toggle",function(text){
                                        this.assert.equal(text.value,"COMPARE WITH")
                                    })
                                })
                            })
                        });
                    })
                })
            })
        })   
    },

    "Test that pagination is visible.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.click('div .button-edit-hover',function(){
            browser.elements("css selector"," div .ladder-btn",function(){
                browser.click('div .icon-edit', function(){
                    browser.elements("css selector", "div .ladder-btn",function(elements){
                        browser.elementIdClick(elements.value[0][Object.keys(elements.value[0])[0]], function() {
                            browser.waitForElementVisible("div .custom-pagination",function(){
                                browser.elements("css selector","div .custom-pagination",function(result){
                                    this.verify.ok(result.value.length > 0);
                                })
                            })
                        });
                    })
                })
            })
        })   
    },

    "Test that dropdown menu is visible.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.click('div .button-edit-hover',function(){
            browser.elements("css selector"," div .ladder-btn",function(){
                browser.click('div .icon-edit', function(){
                    browser.elements("css selector", "div .ladder-btn",function(elements){
                        browser.elementIdClick(elements.value[0][Object.keys(elements.value[0])[0]], function() {
                            browser.waitForElementVisible("div .button-dropdown",function(){
                                browser.elements("css selector","div .button-dropdown",function(result){
                                    this.verify.ok(result.value.length > 0);
                                })
                            })
                        });
                    })
                })
            })
        })   
    },

    "Test that dropdown menu has three options 'Popular', 'Oldest', 'Active'.":(browser)=>{ 
        expectedOptions=["Popular","Oldest","Active"];
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.click('div .button-edit-hover',function(){
            browser.elements("css selector"," div .ladder-btn",function(){
                browser.click('div .icon-edit', function(){
                    browser.elements("css selector", "div .ladder-btn",function(elements){
                        browser.elementIdClick(elements.value[0][Object.keys(elements.value[0])[0]], function() {
                            browser.waitForElementVisible("div .button-dropdown",function(){
                                browser.click("div .button-dropdown",function(){
                                    browser.elements("div .dropdown-item",function(result){
                                        for (let index = 0; index < result.value.length; index++) {
                                            browser.elementIdText(result.value[index][Object.keys(result.value[index])[0]], function(text) {
                                                this.assert.equal(text.value, expectedOptions[index]);
                                            });
                                        }
                                    })
                                })
                            })
                        });
                    })
                })
            })
        })   
    },

    "Test that by clicking popular option it will show popular versions of the ladder.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.click('div .button-edit-hover',function(){
            browser.elements("css selector"," div .ladder-btn",function(){
                browser.click('div .icon-edit', function(){
                    browser.elements("css selector", "div .ladder-btn",function(elements){
                        browser.elementIdClick(elements.value[0][Object.keys(elements.value[0])[0]], function() {
                            browser.waitForElementVisible("div .button-dropdown",function(){
                                browser.click("div .button-dropdown",function(){
                                    browser.elements("div .dropdown-item",function(result){
                                        browser.elementIdClick(result.value[0][Object.keys(result.value[0])[0]], function(text) {
                                            this.verify.ok(text);
                                        });
                                    })
                                })
                            })
                        });
                    })
                })
            })
        })   
    },

    "Test that by clicking Oldest option it will sort versions of the ladder from oldest to newest.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.click('div .button-edit-hover',function(){
            browser.elements("css selector"," div .ladder-btn",function(){
                browser.click('div .icon-edit', function(){
                    browser.elements("css selector", "div .ladder-btn",function(elements){
                        browser.elementIdClick(elements.value[0][Object.keys(elements.value[0])[0]], function() {
                            browser.waitForElementVisible("div .button-dropdown",function(){
                                browser.click("div .button-dropdown",function(){
                                    browser.elements("div .dropdown-item",function(result){
                                        browser.elementIdClick(result.value[1][Object.keys(result.value[1])[0]], function(text) {
                                            this.verify.ok(text);
                                        });
                                    })
                                })
                            })
                        });
                    })
                })
            })
        })   
    },

    "Test that by clicking Active option it will show versions of the ladder which are active.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.expect.element('body').to.be.present.before(1000);
        browser.click('div .button-edit-hover',function(){
            browser.elements("css selector"," div .ladder-btn",function(){
                browser.click('div .icon-edit', function(){
                    browser.elements("css selector", "div .ladder-btn",function(elements){
                        browser.elementIdClick(elements.value[0][Object.keys(elements.value[0])[0]], function() {
                            browser.waitForElementVisible("div .button-dropdown",function(){
                                browser.click("div .button-dropdown",function(){
                                    browser.elements("div .dropdown-item",function(result){
                                        browser.elementIdClick(result.value[2][Object.keys(result.value[2   ])[0]], function(text) {
                                            this.verify.ok(text);
                                        });
                                    })
                                })
                            })
                        });
                    })
                })
            })
        })   
    },

    'after': function(browser) {
        browser
          .perform(() => {
            console.log('[perform]: Finished Test:', browser.currentTest.name);
          })
          .end();
      },
}