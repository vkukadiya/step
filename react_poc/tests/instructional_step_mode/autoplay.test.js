/*
Reference:https://gitlab.com/onso-labs/step/blob/master/features/autoplay_videos.feature
Objectives: 
    As a ladder user, I can
    - use play all button for playing all the videos one after other
    - so, that I can see all the parent/children videos in queue

*/

import constant from "../../src/common/config";

module.exports = {
    tags: ['autoplay-video'],

    'beforeEach': (browser, done) => {
      browser.url(constant.TEST_URL, () => {
            browser.useCss().expect.element('body').to.be.present.before(1000);
            done();
        });
        browser
        .url(constant.TEST_URL)
        .execute(function() {
          window.localStorage.setItem('ajs_token', true);
          return true;
        }, [], function(result) {
          this.assert.ok(result.value);
        })
        
    },
    'Test that play all button will play all the videos of parent and children in queue.':(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.useCss().waitForElementVisible('div .button-play', function() {
            browser.click("div .button-play",function(result){
                this.verify.ok(result);
            })
            
        });        
    },

    "Test that if any step has not a video then it will pause for 20 sec before moving to the next video.":(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.useCss().waitForElementVisible('div .button-play', function() {
            browser.click("div .button-play",function(){
                browser.useXpath().click("//*[contains(text(),'VIDEO')]").pause(1000);
                browser.elements("css selector","div .rst__node",function(result){
                    for (let index = 0; index < result.value.length; index++) {
                        browser.elementIdClick(result.value[index][Object.keys(result.value[index])[0]], function() {
                            this.pause(1000);
                            browser.elements("css selector","div .tab-pane:nth-child(2) .col div:nth-child(2) b",function(text){
                                console.log("text",text)
                                if(text.value.length === 1){
                                    this.pause(20000);
                                    this.verify.ok(text);
                                }else if(index === result.value.length){
                                    console.log("All steps have video");
                                }
                            })
                        });
                    }
                })
            })
            
        });      
    },

    'after': function(browser) {
        browser
          .perform(() => {
            console.log('[perform]: Finished Test:', browser.currentTest.name);
          })
          .end();
      },
}