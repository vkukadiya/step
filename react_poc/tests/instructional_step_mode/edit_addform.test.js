/*
Objectives: 
    As an authenticated user of ladder, I can
    - edit button for editing ladder
    - by clicking on that enable editing option for each node/step

*/

import constant from "../../src/common/config";

module.exports = {
    tags: ['form'],

    'beforeEach': (browser, done) => {
      browser.url(constant.TEST_URL, () => {
            browser.useCss().expect.element('body').to.be.present.before(1000);
            done();
        });
        browser
        .url(constant.TEST_URL)
        .execute(function() {
          window.localStorage.setItem('ajs_token', true);
          return true;
        }, [], function(result) {
          this.assert.ok(result.value);
        })
        
    },
    'TBD-015. Test that in edit/add node form when enter youtube video url it’ll show video.':(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.click('div .button-edit-hover',function(){
            browser.click('div .icon-edit', function(){
                browser.elements("css selector", "div .form",function(form){
                    browser.waitForElementVisible('div .form').setValue('input[id=video-url]', ['https://www.youtube.com/watch?v=4Gl9s40vldY'],function(){
                        this.pause(2000);
                        browser.elements("css selector",".form iframe",function(item){
                            console.log("item",item.value.length);
                            this.verify.ok(item.value.length == 1);
                        })
                    });
                })
            })
        })
    },
    
    'TBD-016. Test that in edit/add node form if video is not broken then after video it’ll show start/end time inputs and play button.':(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.click('div .button-edit-hover',function(){
            browser.click('div .icon-edit', function(){
                browser.elements("css selector", "div .form",function(form){
                    browser.waitForElementVisible('div .form').setValue('input[id=video-url]', ['https://www.youtube.com/watch?v=4Gl9s40vldY'],function(){
                        this.pause(2000);
                       browser.elements("css selector",'input[name="startSeconds"]',function(element){
                           this.verify.ok(element.value.length == 1)
                       })
                       browser.elements("css selector",'input[name="pauseSeconds"]',function(item){
                           this.verify.ok(item.value.length == 1)
                       })
                       this.getText(".mb-3 button",function(text){
                           this.assert.equal(text.value,"PLAY");
                       })
                    });
                })
            })
        })
    },

    'TBD-017. Test that by clicking on the play button it’ll play video from start time and stop at end time defined in start/end time inputs.':(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.click('div .button-edit-hover',function(){
            browser.click('div .icon-edit', function(){
                browser.elements("css selector", "div .form",function(){
                    browser.waitForElementVisible('div .form').setValue('input[id=video-url]', ['https://www.youtube.com/watch?v=4Gl9s40vldY'],function(){
                        // browser.clearValue('input[name="pauseSeconds"]');
                        // browser.setValue('input[name="startSeconds"]',['00:00:00'],function(){
                            // browser.setValue('input[name="pauseSeconds"]',['00:00:04'],function(){
                                this.pause(1000);
                                console.log("result");
                                browser.useXpath().click("//*[contains(text(),'Play')]").pause(20000);
                            // })
                        // })
                    });
                })
            })
        })
    },

    'after': function(browser) {
        browser
          .perform(() => {
            console.log('[perform]: Finished Test:', browser.currentTest.name);
          })
          .end();
      },
}