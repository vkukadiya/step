/*
Objectives: 
    As a ladder user, I can
    - see the dropdown menu for different modes
    - by choosing any of option from that will display ladder in that mode 

*/

import constant from "../../src/common/config";

module.exports = {
    tags: ['dropdown-menu'],

    'beforeEach': (browser, done) => {
      browser.url(constant.TEST_URL, () => {
            browser.useCss().expect.element('body').to.be.present.before(1000);
            done();
        });
        browser
        .url(constant.TEST_URL)
        .execute(function() {
          window.localStorage.setItem('ajs_token', true);
          return true;
        }, [], function(result) {
          this.assert.ok(result.value);
        })
    },
    'TBD-001. Test that at the top right side of the page dropdown menu is loaded.':(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.useCss().waitForElementVisible('div .dropdown-toggle', function() {
            browser.click('div .dropdown-toggle',function(){
                browser.elements("css selector",".dropdown-menu-right .dropdown-item",function(result){
                    this.verify.ok(result)
                })
            })
        });        
    },
    'TBD-002. Test that the dropdown menu has three options.':(browser)=>{
        var expectedNavElements = ['Expository Mode', 'Lecture Mode'];
        browser.expect.element('body').to.be.present.before(1000);
        browser.useCss().waitForElementVisible('div .dropdown-toggle', function() {
            this.getText('div .dropdown-toggle',function(text){
                this.assert.equal(text.value,'INSTRUCTIONAL STEPS MODE')
            })
            browser.click('div .dropdown-toggle',function(){
                browser.elements("css selector",".dropdown-menu-right .dropdown-item",function(result){
                    for (let index = 0; index < result.value.length; index++) {
                        browser.elementIdText(result.value[index][Object.keys(result.value[index])[0]], function(text) {
                            this.assert.equal(text.value, expectedNavElements[index]);
                        });
                    }
                })
            })
        });        
    },

    'TBD-026. Test that by clicking on the Expository Mode option from the dropdown it’ll open the expository mode.':(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.useCss().click('div .dropdown-toggle',function(){
            browser.useCss().waitForElementVisible('.dropdown-menu .dropdown-item', function() {
                browser.useXpath().click("//*[contains(text(),'Expository Mode')]").pause(1000);
            });
        })  
    },

    'TBD-033. Test that by clicking on the Lecture Mode option from the dropdown it’ll open the lecture mode.':(browser)=>{
        // browser.expect.element('body').to.be.present.before(1000);
        browser.useCss().click('div .dropdown-toggle',function(){
            browser.useCss().waitForElementVisible('.dropdown-menu .dropdown-item', function() {
                browser.useXpath().click("//*[contains(text(),'Lecture Mode')]").pause(1000);
            });
        })    
    },
    

    'after': function(browser) {
        browser
          .perform(() => {
            console.log('[perform]: Finished Test:', browser.currentTest.name);
          })
          .end();
      },
}