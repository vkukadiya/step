/*
Objectives: 
    As a ladder user, I can
    - see the different filter tags on the top of the ladder
    - by clicking on those tags will filter node/steps of the ladder 

*/

import constant from "../../src/common/config";

module.exports = {
    tags: ['filter-tag'],

    'beforeEach': (browser, done) => {
      browser.url(constant.TEST_URL, () => {
            browser.useCss().expect.element('body').to.be.present.before(1000);
            done();
        });
        browser
        .url(constant.TEST_URL)
        .execute(function() {
            window.localStorage.setItem('ajs_token', true);
            return true;
        }, [], function(result) {
            this.assert.ok(result.value);
        })
    },

    'TBD-003. Test that the filter tags are visible.':(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.useCss().waitForElementVisible('.radio-button-wrap .radio-button', function() {
            browser.elements("css selector",".radio-button-wrap .radio-button",function(result){
                this.verify.ok(result.value.length > 0)
            }) 
        })       
    },

    'TBD-004. Test that the filter tags’ checkboxes are clickable.':(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.useCss().waitForElementVisible(' .radio-button-wrap .radio-button', function() {
            browser.elements("css selector",".radio-button-wrap .radio-button .custom-control-input",function(result){
                for (let index = 0; index < result.value.length; index++) {
                    let type= "checkbox"+index
                    browser.waitForElementVisible('.radio-button-wrap .radio-button').setValue('input[id='+type+']', browser.Keys.ENTER,function(item){
                        this.verify.ok(item);
                    });
                }
            })  
        })     
    },

    // 'TBD-005. Test that by clicking any of the filter tags’ checkbox it will filter the matching parent or parent-children pairs.':(browser)=>{
    //     browser.expect.element('body').to.be.present.before(1000);
    //     browser.elements("css selector",".radio-button-wrap .radio-button .custom-control-input",function(result){
    //         for (let index = 0; index < result.value.length; index++) {
    //             browser.click(".radio-button-wrap .radio-button .custom-control-input",function(item){
    //                 this.assert.equal(item.status,0)
    //             })
    //         }
    //     })   
    // },

    // 'TBD-006. Test that by clicking any of the filter tags’ checkbox it will filter the matching parent or parent-children pairs and the ladders remain collapsed.':(browser)=>{
    //     browser.expect.element('body').to.be.present.before(1000);
    //     browser.elements("css selector",".radio-button-wrap .radio-button .custom-control-input",function(result){
    //         for (let index = 0; index < result.value.length; index++) {
    //             browser.click(".radio-button-wrap .radio-button .custom-control-input",function(item){
    //                 this.assert.equal(item.status,0)
    //             })
    //         }
    //     })   
    // },


    'after': function(browser) {
        browser
          .perform(() => {
            console.log('[perform]: Finished Test:', browser.currentTest.name);
          })
          .end();
      },
}