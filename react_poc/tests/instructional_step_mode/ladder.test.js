/*
Objectives: 
    As a ladder user, I can
    - see the main parent ladder
    - see the information of whole ladder
*/

import constant from "../../src/common/config";

module.exports = {
    tags: ['ladder'],

    'beforeEach': (browser, done) => {
      browser.url(constant.TEST_URL, () => {
            browser.useCss().expect.element('body').to.be.present.before(1000);
            done();
        });
        browser
        .url(constant.TEST_URL)
        .execute(function() {
          window.localStorage.setItem('ajs_token', true);
          return true;
        }, [], function(result) {
          this.assert.ok(result.value);
        })
        
    },
    'TBD-008. Test that Main ladder’s title is loaded.':(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.useCss().waitForElementVisible('div .active-row', function() {
            this.getText('div .active-row',function(text){
                this.verify.ok(text)
            })
            browser.click('div .active-row',function(result){
                this.verify.ok(result);
            })
        });        
    },
    'TBD-009. Test that by clicking on the main ladder’s title it will open its video tab.':(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.click('div .active-row',function(result){
            browser.useCss().waitForElementVisible('li .activeRow',function(){
                this.getText('li .activeRow',function(text){
                    this.assert.equal(text.value, 'VIDEO');
                })
            })
        })       
    },
    'TBD-010. Test that edit button is visible.':(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.useCss().waitForElementVisible('div .button-edit-hover',function(){
            this.getText('div .button-edit-hover',function(text){
                this.assert.equal(text.value, 'EDIT');
            })
        })      
    },
    'TBD-011. Test that edit button is clickable.':(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.useCss().waitForElementVisible('div .button-edit-hover',function(){
            browser.click('div .button-edit-hover',function(result){
                this.verify.ok(result);
            })
        })      
    },
    "TBD-012. Test that by clicking the edit button it’ll show edit and add icons at the right side of the ladder's name.":(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.click('div .button-edit-hover',function(){
            browser.elements("css selector","div .active-icons",function(result){
                this.verify.ok(result.value.length > 0)
            })
        })
       
    },
    "TBD-013. Test that by clicking the edit icon it’ll show the edit form for that particular node.":(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.click('div .button-edit-hover',function(){
            browser.elements("css selector","div .active-icons",function(result){
                for (let index = 0; index < result.value.length; index++) {
                    browser.click('div .icon-edit', function(){
                        browser.elements("css selector", "div .form",function(form){
                            this.verify.ok(form.value.length == 1)
                        })
                    })
                }
            })
        })
    },
    "TBD-014. Test that by clicking the add icon it’ll show the add new node form.":(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.click('div .button-edit-hover',function(){
            browser.elements("css selector","div .active-icons",function(result){
                for (let index = 0; index < result.value.length; index++) {
                    browser.click('div .icon-edit', function(){
                        browser.elements("css selector", "div .form",function(form){
                            this.verify.ok(form.value.length == 1)
                        })
                    })
                }
            })
        })
    },

    'after': function(browser) {
        browser
          .perform(() => {
            console.log('[perform]: Finished Test:', browser.currentTest.name);
          })
          .end();
      },
}