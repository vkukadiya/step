/*
Objectives: 
    As a ladder user, I can
    - see the different tabs named "Ladder Info", "Video", "ScreenShots", "Code Samples" 
    - traverse through those tabs for detailed information about node/step

*/


import constant from "../../src/common/config";

module.exports = {
    tags: ['tabs'],

    'beforeEach': (browser, done) => {
      browser.url(constant.TEST_URL, () => {
            browser.useCss().expect.element('body').to.be.present.before(1000);
            done();
        });
        browser
        .url(constant.TEST_URL)
        .execute(function() {
          window.localStorage.setItem('ajs_token', true);
          return true;
        }, [], function(result) {
          this.assert.ok(result.value);
        })
        
    },
    'TBD-020. Test that in ‘Video’ tab it is showing video if video is added.':(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.useCss().waitForElementVisible('.video-wrap', function() {
            this.asseert.elementPresent('.video-wrap .active');
            // browser.element("css selector",".col iframe",function(result){
            //     if(result.status === 0){
            //         this.verify.ok(result);
            //     }else{
            //         browser.getText(".col",function(text){
            //             console.log(text.value);
            //         })
            //     }
            // })
        });        
    },
    'TBD-021. Test that in ‘Video’ tab it is showing like/dislike icon if video is added.':(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.useCss().waitForElementVisible('.video-wrap .col', function() {
            browser.element("css selector",".col iframe",function(result){
                if(result.value.length >0){
                    this.verify.ok(result);
                    browser.element("css selector",".buttons-wrapper button:first-child",function(element){
                        this.verify.ok(element.value.length >0)
                    })
                    browser.element("css selector",".buttons-wrapper button:nth-child(3)",function(item){
                        this.verify.ok(item.value.length >0)
                    })
                }else{
                    browser.getText(".col",function(text){
                        console.log(text.value);
                    })
                }
            })
        });        
    },

    'TBD-022. Test that in ‘Video’ tab it is showing watch button if video is added.':(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.useCss().waitForElementVisible('.video-wrap .col', function() {
            browser.element("css selector",".col iframe",function(result){
                if(result.value.length >0){
                    this.verify.ok(result);
                    browser.element("css selector",".buttons-wrapper .button-watch",function(element){
                        this.verify.ok(element.value.length >0)
                    })
                }else{
                    browser.getText(".col",function(text){
                        console.log(text.value);
                    })
                }
            })
        });        
    },

    'TBD-023. Test that in ‘Video’ tab it is showing description if added.':(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.useCss().waitForElementVisible('.video-wrap .col', function() {
            browser.getText(".tab-pane .col div:nth-child(3)",function(result){
                if(result.value.length >0){
                    this.verify.ok(result);
                }else{
                   console.log("Description not added")
                }
            })
        });        
    },
    
    'TBD-024. Test that in screenshot tab it is showing image if added.':(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000); 
        browser.waitForElementVisible("div .video-content",function(){
            browser.useXpath().click("//*[contains(text(),'Screenshots')]").pause(1000); 
            browser.useCss().waitForElementVisible("#image",function(){
                // browser.verify.visible("#ReactGridGallery")
                // if(this.useCss().assert.cssClassPresent(".active .row #image div","ReactGridGallery")){
                    browser.element("css selector",'.active .row #ReactGridGallery ',function(result){
                        console.log(">>>>>",result.value);
                        if (result.value && result.value.ELEMENT) {
                            this.verify.ok(result);
                        } else {
                            browser.element("css selector",'.active .row b', function(){
                                browser.getText(".active .row b",function(text){
                                    console.log(text.value);
                                })
                            })
                        }
                    })
                // }
                //  // browser.useCss().waitForElementVisible(".active .row #image div",function(){
            }) 
        }) 
    },

    'TBD-024. Test that in Ladder Information tab it is showing ladder info.':(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000); 
        browser.waitForElementVisible("div .video-content",function(){
            browser.useXpath().click("//*[contains(text(),'Ladder Info')]").pause(1000); 
            browser.useCss().waitForElementVisible("#image",function(){
                // browser.verify.visible("#ReactGridGallery")
                // if(this.useCss().assert.cssClassPresent(".active .row #image div","ReactGridGallery")){
                    // browser.element("css selector",'.active .row #ReactGridGallery ',function(result){
                    //     console.log(">>>>>",result.value);
                    //     if (result.value && result.value.ELEMENT) {
                    //         this.verify.ok(result);
                    //     } else {
                    //         browser.element("css selector",'.active .row b', function(){
                    //             browser.getText(".active .row b",function(text){
                    //                 console.log(text.value);
                    //             })
                    //         })
                    //     }
                    // })
                // }
                //  // browser.useCss().waitForElementVisible(".active .row #image div",function(){
            }) 
        }) 
    },

    'TBD-025. Test that in code samples tab it is showing code title if it is added.':(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .video-content",function(){
            browser.useXpath().click("//*[contains(text(),'CODE SAMPLES')]").pause(1000);  
            browser.waitForElementVisible("div .code-header",function(){
                browser.elements("css selector", "div .code-header",function(result){
                    if(result.value.length > 0){
                        this.verify.ok(result)
                    }else{
                        console.log("code title is not added");
                    }
                })
            })
        })
    },

    'TBD-026. Test that in code samples tab it is showing code snippets if added.':(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .video-content",function(){
            browser.useXpath().click("//*[contains(text(),'CODE SAMPLES')]").pause(1000);  
            browser.waitForElementVisible("div .code-sample-wrap",function(){
                browser.elements("css selector", "div .code-sample-wrap div div:nth-child(2)",function(result){
                    if(result.value.length > 0){
                        this.verify.ok(result)
                    }else{
                        console.log("code snippets is not added");
                    }
                })
            })
        })
    },

    'TBD-027. Test that in code samples tab it is showing help text if added.':(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .video-content",function(){
            browser.useXpath().click("//*[contains(text(),'CODE SAMPLES')]").pause(1000);  
            browser.waitForElementVisible("div .code-sample-wrap",function(){
                browser.elements("css selector", "div .code-sample-wrap div div:nth-child(3)",function(result){
                    if(result.value.length > 0){
                        this.verify.ok(result)
                    }else{
                        console.log("help text is not added");
                    }
                })
            })
        })
    },

    'after': function(browser) {
        browser
          .perform(() => {
            console.log('[perform]: Finished Test:', browser.currentTest.name);
          })
          .end();
      },
}