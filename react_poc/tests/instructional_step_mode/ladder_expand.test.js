/*
Objectives: 
    As a ladder user, I can
    - expand/collapse each and every parent node
    - so, that can see the children node/step information

*/

import constant from "../../src/common/config";

module.exports = {
    tags: ['ladder-expand'],

    'beforeEach': (browser, done) => {
      browser.url(constant.TEST_URL, () => {
            browser.useCss().expect.element('body').to.be.present.before(1000);
            done();
        });
        browser
        .url(constant.TEST_URL)
        .execute(function() {
          window.localStorage.setItem('ajs_token', true);
          return true;
        }, [], function(result) {
          this.assert.ok(result.value);
        })
        
    },
    'TBD-018. Test that each parent node has down arrow icon at the left side of title.':(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.useCss().waitForElementVisible('div .rst__expandButton', function() {
            this.pause(2000);
           browser.element("css selector"," .rst__nodeContent button",function(result){
               if(Object.keys(result.value).length > 0){
                this.verify.ok(Object.keys(result.value).length > 0)
               }
                // for (let index = 0; index < elements.value.length; index++) {
                //     browser.elementIdClick(result.value[index][Object.keys(result.value[index])[0]],function(){
                //         browser.element("css selector"," .rst__nodeContent button",function(item){
                //             if(item.value.length > 0){

                //             }
                //         })
                //     })
                // }
               
               else{
                   console.log("Parent node not exist");
               }
               
           })
        });        
    },
    'TBD-019. Test that by clicking the down arrow icon it’ll show its children nodes.':(browser)=>{ 
        browser.expect.element('body').to.be.present.before(1000);
        browser.useCss().waitForElementVisible('div .rst__expandButton', function() {
           browser.element("css selector",".rst__nodeContent button",function(elements){
            if(Object.keys(elements.value).length > 0){
                browser.click(".rst__nodeContent button",function(){
                    browser.element("css selector","div .rst__lineChildren",function(item){
                             this.verify.ok(Object.keys(item.value).length > 0);
                     })
               })
            }
           })
        });        
    },

    'after': function(browser) {
        browser
          .perform(() => {
            console.log('[perform]: Finished Test:', browser.currentTest.name);
          })
          .end();
      },
}