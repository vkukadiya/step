/*
Objectives: 
    As an authenticated user of the ladder,
    - I can see the new ladder authoring button
    - from that user can author new ladder

*/

import constant from "../../src/common/config";

module.exports = {
    tags: ['add-ladder'],

    'beforeEach': (browser, done) => {
      browser.url(constant.TEST_URL, () => {
            browser.useCss().expect.element('body').to.be.present.before(1000);
            done();
        });
        browser
        .url(constant.TEST_URL)
        .execute(function() {
          window.localStorage.setItem('ajs_token', true);
          return true;
        }, [], function(result) {
          this.assert.ok(result.value);
        })
    },
    'Test that at the top left side of the page add new ladder button is loaded and its is clickable.':(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-play",function(){
            browser.getText("div .button-play",function(text){
                this.assert.equal(text.value,"ADD NEW LADDER")
            })
            browser.click("div .button-play",function(result){
                this.verify.ok(result);
            })
        })
    },

    'Test that in the add new ladder form title "Add New Ladder" is visible.':(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-play",function(){
            browser.click("div .button-play",function(){
               this.pause(1000);
               browser.getText(".collapse span",function(text){
                   this.assert.equal(text.value, "Add New Ladder");
               })
            })
        })
    },

    'Test that in the add new ladder form "GO BACK" button is visible and clickable.':(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-play",function(){
            browser.click("div .button-play",function(){
               this.pause(1000);
               browser.getText("ul button",function(text){
                   this.assert.equal(text.value, "GO BACK");
               })
               browser.click("ul button",function(result){
                   this.verify.ok(result)
               })
            })
        })
    },

    'Test that in the add new ladder form "Title" field is visible and can input values in that.':(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-play",function(){
            browser.click("div .button-play",function(){
               this.pause(1000);
               browser.getText(".form-group:first-child label",function(text){
                   this.assert.equal(text.value, "Title");
               })
               browser.waitForElementVisible(".form-group:first-child").setValue("input[id=title]",["new ladder"]).pause(500).getValue('input[id=title]',function(result){
                this.assert.equal(result.value,"new ladder");
            });
            })
        })
    },

    'Test that in the add new ladder form "Description" field is visible and can input values in that.':(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-play",function(){
            browser.click("div .button-play",function(){
               this.pause(1000);
               browser.getText(".form-group:nth-child(2) label",function(text){
                   this.assert.equal(text.value, "Description");
               })
               browser.waitForElementVisible(".form-group:nth-child(2)").setValue("textarea[id=description]",["new ladder"]).pause(500).getValue('textarea[id=description]',function(result){
                this.assert.equal(result.value,"new ladder");
            });
            })
        })
    },

    'Test that in the add new ladder form if Title fileld is empty and we click "Submit" button the it will show message "Title filed is required".':(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-play",function(){
            browser.click("div .button-play",function(){
               this.pause(1000);
               browser.waitForElementVisible(".form-group:nth-child(2)").setValue("textarea[id=description]",["new ladder"]).pause(500).getValue('textarea[id=description]',function(result){
                this.assert.equal(result.value,"new ladder");
                })
               browser.click(".form-group button",function(result){
                   this.verify.ok(result);
                    browser.getText(".form-group:first-child .invalid-feedback",function(text){
                        this.assert.equal(text.value,"Title filed is required");
                    })
               })
            })
        })
    },

    'Test that in the add new ladder form if Description fileld is empty and we click "Submit" button the it will show message "Description filed is required".':(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-play",function(){
            browser.click("div .button-play",function(){
               this.pause(1000);
               browser.waitForElementVisible(".form-group:first-child").setValue("input[id=title]",["new ladder"]).pause(500).getValue('input[id=title]',function(result){
                this.assert.equal(result.value,"new ladder");
                });
               browser.click(".form-group button",function(result){
                   this.verify.ok(result);
                    browser.getText(".form-group:nth-child(2) .invalid-feedback",function(text){
                        this.assert.equal(text.value,"Description filed is required");
                    })
               })
            })
        })
    },

    'Test that in the add new ladder form "Submit" button is visible and clickable.':(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.waitForElementVisible("div .button-play",function(){
            browser.click("div .button-play",function(){
               this.pause(1000);
               browser.getText(".form-group button",function(text){
                   this.assert.equal(text.value, "SUBMIT");
               })
                browser.waitForElementVisible(".form-group:first-child").setValue("input[id=title]",["new ladder"]).pause(500).getValue('input[id=title]',function(result){
                    this.assert.equal(result.value,"new ladder");
                });
                browser.waitForElementVisible(".form-group:nth-child(2)").setValue("textarea[id=description]",["new ladder"]).pause(500).getValue('textarea[id=description]',function(result){
                this.assert.equal(result.value,"new ladder");
                })
               browser.click(".form-group button",function(result){
                   this.verify.ok(result);
               })
            })
        })
    },

    'after': function(browser) {
        browser
          .perform(() => {
            console.log('[perform]: Finished Test:', browser.currentTest.name);
          })
          .end();
      },
}