/*
Reference:https://gitlab.com/onso-labs/step/blob/master/features/presentation_mode.feature
Objectives:
    As a ladder user, I can
    - use presentation mode
    - so, that I can see all the steps of the ladder in presentation view along with its information

===
Feature planning from 2020-03-11 Wednesday
===

Feature: presentation attendee can follow instructions later
  As an attendee of the First-time contributor sprint
  I can see the instructions on slides while sitting in a room with about 200 other first-timers
  And after the presentation I can refer to the same information in a set of instructions
  So that there is a clear connection between the presentation and the complex steps I need to follow

Feature: presentation attendee can improve instructions; everyone can choose to adopt or ignore the improvement
  As an attendee of the First-time contributor sprint
  I can improve instructions on ladders
  So that other people attending the presentation can see that same improvement

*/

import constant from "../../src/common/config";

module.exports = {
    tags: ['presentation-mode'],

    'beforeEach': (browser, done) => {
      browser.url(constant.TEST_URL, () => {
            browser.useCss().expect.element('body').to.be.present.before(1000);
            done();
        });
        browser
        .url(constant.TEST_URL)
        .execute(function() {
          window.localStorage.setItem('ajs_token', true);
          return true;
        }, [], function(result) {
          this.assert.ok(result.value);
        })

    },
    'Test that name of the parent ladder is showing.':(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.useCss().waitForElementVisible('div .rst__expandButton', function() {
            this.pause(2000);
           browser.element("css selector"," .rst__nodeContent button",function(result){
               if(Object.keys(result.value).length > 0){
                this.verify.ok(Object.keys(result.value).length > 0)
               }
                // for (let index = 0; index < elements.value.length; index++) {
                //     browser.elementIdClick(result.value[index][Object.keys(result.value[index])[0]],function(){
                //         browser.element("css selector"," .rst__nodeContent button",function(item){
                //             if(item.value.length > 0){

                //             }
                //         })
                //     })
                // }



           })
        });
    },
    "Test that each step's title is showing.":(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.useCss().waitForElementVisible('div .rst__expandButton', function() {
           browser.element("css selector",".rst__nodeContent button",function(elements){
            if(Object.keys(elements.value).length > 0){
                browser.click(".rst__nodeContent button",function(){
                    browser.element("css selector","div .rst__lineChildren",function(item){
                             this.verify.ok(Object.keys(item.value).length > 0);
                     })
               })
            }
           })
        });
    },

    "Test that left side of the screen video and for multiple image, image carousel is showing .":(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.useCss().waitForElementVisible('div .rst__expandButton', function() {
           browser.element("css selector",".rst__nodeContent button",function(elements){
            if(Object.keys(elements.value).length > 0){
                browser.click(".rst__nodeContent button",function(){
                    browser.element("css selector","div .rst__lineChildren",function(item){
                             this.verify.ok(Object.keys(item.value).length > 0);
                     })
               })
            }
           })
        });
    },

    "Test that right side of the screen description and code samples' of that step is showing.":(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.useCss().waitForElementVisible('div .rst__expandButton', function() {
           browser.element("css selector",".rst__nodeContent button",function(elements){
            if(Object.keys(elements.value).length > 0){
                browser.click(".rst__nodeContent button",function(){
                    browser.element("css selector","div .rst__lineChildren",function(item){
                             this.verify.ok(Object.keys(item.value).length > 0);
                     })
               })
            }
           })
        });
    },

    "Test that next button will show the next step or children of the ladder.":(browser)=>{
        browser.expect.element('body').to.be.present.before(1000);
        browser.useCss().waitForElementVisible('div .rst__expandButton', function() {
           browser.element("css selector",".rst__nodeContent button",function(elements){
            if(Object.keys(elements.value).length > 0){
                browser.click(".rst__nodeContent button",function(){
                    browser.element("css selector","div .rst__lineChildren",function(item){
                             this.verify.ok(Object.keys(item.value).length > 0);
                     })
               })
            }
           })
        });
    },

    'after': function(browser) {
        browser
          .perform(() => {
            console.log('[perform]: Finished Test:', browser.currentTest.name);
          })
          .end();
      },
}
