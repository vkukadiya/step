var hwp     = require('html-webpack-plugin');
var path    = require('path');

module.exports = {
  //entry: ['whatwg-fetch', './src/pages/Charts.js'],
  entry: {
    // ladder_react: './src/index.js',
    ladder_react: './src/pages/poc_2.js',
    //ladder_react_form: './src/pages/Form.js',
  },
  output: {
    path: __dirname,
    filename: '[name].bundle.js'
  },
  module: {
    loaders: [
      {test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader'},
      { test: /\.css$/, loader: "style-loader!css-loader" },
      {
        test: /\.less$/,
        use: [
            { loader: "style-loader" },
            { loader: "css-loader" },
            { loader: "less-loader" }
        ]
    },
      {
        test: /\.scss$/,
        use: [
          { loader: "style-loader" },
          { loader: "css-loader" },
          { loader: "sass-loader" }
        ] 
      }
    ]
  },
  plugins:[
      new hwp({template:path.join(__dirname, '/src/index.html')})
  ],
  devServer: {
    port: 8081,
    publicPath: '/',
    historyApiFallback: true
  }
};
