# Why this new structure for the DrupalLadder?

This Drupal Ladder concept is a radical rethink. What problem does it aim to solve?
- for people trying to get stuff done (readers of documentation, contributors, team members), it provides
  - compact information sets written specifically for each individual's actual scenario
- for people helping others get up to speed (authors of documentation, team leads, mentors, employers, conference presenters, trainers)
-
