**Drupal-Ladder Roadmap**

Drupal-ladder is drupal based site partially decoupled with reactJS. React Js is used as front-end for rendering ladder page and also user can add/edit any node(Step) of the ladder and drupal 8 is used as a backend for providing APIs for front end rendering.

**Current Features:**

	- User can create new ladder once logged in to the drupal site.
	- User can add children as well while creating the new ladder or any parent from the drupal site.
	- Users can see the ladder step by step and add/edit any new node or can create a ladder from react front-view also.
	- Users can upvote/downvote to any step’s video.
	- Users can add tags, description, snapshot as well as code samples from drupal or from react front view.
	- Users can see a ladder in instructional, expository and also in lecture mode.
	- Users can filter steps by tags.

**Future Expansion:**

	- Planning to make revisions/variations on each edit.
	- Planning to make a new page in which anyone can see all the variations of that particular ladder with changes highlighted.
	- Planning to make the feature of adopting any variation of the ladder.
	- Planning to make two options for adopting ladder whether you can adopt entire variation of that ladder or you can adopt particular node of that variant ladder.
	- Planning to make filtering variation by popular and by recent updation.



