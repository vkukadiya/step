requirements:
  steps are easy to follow during implementation and future reference
  development decisions minimize long-term maintenance effort
  user experience scales to hundreds of thousands of users (separate consideration from performance considerations at launch; anticipate refactors)
  architecture decisions aim for maximum crowdsourcing benefit
  ease of content authorship
  ease of

guiding principles:
  content reusability

entity_reference for steps:
  reusable content
    changes made in one place are available everywhere

vote on revisions:
  individuals can evaluate alternatives and document choices for future reference
  as available solutions evolve, the whole community can share the effort to evaluate

attached view for step nav:


why bootstrap 4 for theming?

why Drupal 8 for cms?
