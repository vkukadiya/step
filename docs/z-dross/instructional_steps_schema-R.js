{
  discussion: [
    "kay's response on Ravi's modification of version 19.01.01", [
      ["brief explanation", [
        "in the following example", [
          "stepId p1 Install Drupal serves as the Ladder; it is the child of stepId p2 Adopt a CMS for your website",
          ""
      ]]]
      ["additional proposed changes", [
        "use childInstructionSets field to set order; the reasoning is that", [
          "for each step to be reusable it needs to have multiple parents",
          "order of child steps is adjusted"
        ],
      ]],
      ["reasons for proposed changes",
        ["requirements would include", [
          "order of child instruction sets",
          "multiple possible parent instruction steps",
          "as discussed Jan 9"]],
      ["versioning", [
        "let's call Ravi's proposed json version 19.02.02)",
        "let's call the following proposed json version 19.02.03"
      ]]
    ]
  ]],
  version: "19.02.03"
}, {
  steps: [{
    stepId: "p1",
    instruction: "Install Drupal",
    videoId: "def123",
    startSeconds: 60,
    pauseSeconds: 65,
    tags: ["Windows 10 Enterprise", "Windows PowerShell", "Mac OSX Mojave", "Terminal", "bash"],
    parentInstructionSets: [],
    childInstructionSets: [],
  }, {
    stepId: "c10",
    instruction: "composer create-project drupal-composer/drupal-project:8.x-dev { some-dir } --stability dev --no-interaction",
    videoId: "KcC8KZ_Ga2M",
    startSeconds: 123,
    pauseSeconds: 132,
    tags: ["Windows 10 Enterprise", "Windows Explorer"],
    parentInstructionSets: [],
    childInstructionSets: [],
    parentStep: "p1"
  }, {
    stepId: "p2",
    instruction: "Adopt a CMS for your website",
    videoId: "Wp6Z2wVyPeY",
    startSeconds: 123,
    pauseSeconds: 132,
    tags: ["Windows 10 Enterprise", "Windows Explorer"],
    parentInstructionSets: [],
    childInstructionSets: [],
    parentStep: ""
  }, {
    stepId: "c2",
    instruction: "React vs. Angular vs. Vue: Which Should You Choose?",
    videoId: "xDhzYQ4VyCw",
    startSeconds: 123,
    pauseSeconds: 132,
    tags: ["Windows 10 Enterprise", "Windows Explorer"],
    parentInstructionSets: [],
    childInstructionSets: [],
    parentStep: "p1"
  }, {
    stepId: "c3",
    instruction: "Becoming a Frontend Developer / Designer in 2019 - Five Step Guide",
    videoId: "9kl30htP1iw",
    startSeconds: 123,
    pauseSeconds: 132,
    tags: ["Windows 10 Enterprise", "Windows Explorer"],
    parentInstructionSets: [],
    childInstructionSets: [],
    parentStep: "p2"
  }]
}
