<!-- from https://www.taniarascia.com/getting-started-with-react/ -->

<!doctype html>
<html>
<head>
  <meta charset="utf-8">

  <title>proposed project schedule</title>
  <script src="https://unpkg.com/react@^16/umd/react.development.js"></script>
  <script src="https://unpkg.com/react-dom@^16/umd/react-dom.development.js"></script>
  <script src="https://unpkg.com/babel-standalone@^6/babel.js"></script>
</head>

<body>

  <div id="root"></div>

  <script type="text/babel">

    class App extends React.Component {
      // render method is the only required method in a class component
      render() {
        return (
          <div>
            <h1>proposed project schedule</h1>
            <Schedule />
          </div>
        );
      }
    }

    // simple component with output nested in main class component
    const Schedule = () => {
      render() {
        const events = [
          {
            "name": "a task to keep on the schedule",
            "date": "Jan 1, 2100"
          }
        ];

        return (
          <div className="container">
            <EventList eventData={events} />
          </div>
        );
      }
    }

    class EventList extends React.Component {
      render() {
        const { eventData } = this.props;

        return (
          <div>
            <h2>dates</h2>
            <Schedule />
          </div>
        );
      }
    }

    // method to render class in html entry point
    ReactDOM.render(<App />, document.getElementById('root'));
  </script>

</body>

<!--
DETAILS OF NESTED COMPONENTS

In Tania's example:
App {
  Table {  // sets characterData={characters}
    TableBody  // sets characterData={characterData}
  }
}

In schedule example:
App {
  Schedule {
    EventList  // sets characterData={characterData}
  }
}
-->

</html>
