as the reader of instructional steps

I can see substeps of any given step

So that I can drill into details of any step that isn't sufficiently clear to me

Eva Open configuration options
write a broad objective
why a broad objective? the broad objective allows more than one solution

write steps for reaching that objective
each step is worded again as a broad objective. why? so that the process, while prescriptive in its presentation, leaves room for any approach to be added/recommended/evaluated.

confirm that the steps display as expected
the order in which steps display can be critical. should something indicate when order is flexible to allow different approaches to put things in different orders?
