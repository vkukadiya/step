- Our primary focus for tomorrow will be to introduce partial decoupling method (Use Drupal driven website and integrate React into it)
- Accommodate React POC into fresh D8 website
- Introduce Play and Play All buttons in the POC (React)
- D8 related POC (video clips api, ticket #4)
- We need to push our POC to Git Lab
- Create a separate ticket for automated testing using Behat
- Our devops needs to find a way for CICD (Our commits to Gitlab should automatically deploy to Acquia instance)
