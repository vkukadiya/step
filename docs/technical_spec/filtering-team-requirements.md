#### Filter in intructional mode team requirements:

#### Feature: Filter
 	Allow moderators to filter ladder via selecting tags,
When user select the tags from the list it will filter the existing ladders which match with the step tags.
After match list it displayed all the parent and Childs/grand child list which is match with the tags.
User can choose more than one tags at a time. List should be display in collapse manner if there is a parent tree is visible.

#### Filter in expository mode team requirements:

#### Feature: Filter

	Allow moderators to filter the ladder's sections by selecting tags,
As moderator select the tag it reflects the list and make new tree sections with match step along with there parents and grand parents steps. User can choose more than one tags at a time.

#### Filter in Lecture mode team requirements:

#### Feature: Filter
	Allow moderators to filter the ladder's sections by selecting tags,
As moderator select the tag it reflects the section start with filter list first steps parents to child order. User can choose more than one tags at a time.