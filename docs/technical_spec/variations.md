Variations explanation:

**TBD-001**: Default Ladder.

**Prerequisite**: `<div>` tag to display default ladder.

**Steps**: The most popular ladder will be displayed as a default ladder.

**Comments**:

**TBD-002**: Popular filter tab.

**Prerequisite**: `<tab>` tag with name popular to display.

**Steps**: Popular filter tab will be displayed on the right side of the screen above the mode dropdown.

**Comments**:

**TBD-003**: Oldest filter tab.

**Prerequisite**: `<tab>` tag with name oldest to display.

**Steps**: Oldest filter tab will be displayed on the right side of the popular filter tab above the mode dropdown.

**Comments**:

**TBD-004**: Popular filter tab click.

**Prerequisite**: `<tab>` tag with name popular to display.

**Steps**: On the click of popular filter tab it will display the list of the variants of the ladder based on the number of likes.

**Comments**:

**TBD-005**: Oldest filter tab click.

**Prerequisite**: `<tab>` tag with name oldest to display.

**Steps**: On the click of oldest filter tab it will display the list of the variants of the ladder based on the time or we can say it’ll sort the variants from oldest to latest.

**Comments**:

**TBD-006**: Listing of variants.

**Prerequisite**: `<div>` tag to display variant list.

**Steps**: Different variants will be displayed in card layout.

**Comments**:

**TBD-007**: Title of variant.

**Prerequisite**: `<CardTitle>` tag to display variant title.

**Steps**: Title of variant will be displayed as a card title.

**Comments**:

**TBD-008**: Details of variant.

**Prerequisite**: `<CardBody>` tag to display variant details.

**Steps**: Details of variant will be displayed as a card body.

**Comments**:

**TBD-009**: On the click of the variants listed in the filter tabs.

**Prerequisite**: List of variants to display in the filter tabs.

**Steps**: By clicking any of the variant listed in the filter tabs it will show the whole ladder of that variant in the instructional mode layout.

**Comments**:

