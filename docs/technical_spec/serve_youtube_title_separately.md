**Serve YouTube title separately explanation**:

**TBD-001**: Serve YouTube title separately

**Prerequisite**: State `youtube_video_title` initilized.

**Steps**: As we entered url in youtube video url’s input box it will serve youtube video’s title as different field.

**Comments**:

**TBD-002**: Store youtube video’s title.

**Prerequisite**: State `youtube_video_title` will be updated as per video url title.

**Steps**: Youtube video’s title will be stored but not displayed on the screen.

**Comments**:

**TBD-003**: Send youtube video’s title to API.

**Prerequisite**: `youtube_video_title` will be set in obeject and will be send in POST API.

**Steps**: Youtube video title will be sent in the API as a field `youtube_video_title` so that it would be stored on the server.

**Comments**: