#### TBD-001. Test that the filter tags are visible.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - The filter tags should be visible.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - Then I can see the filter tags.
#### TBD-002. Test that the filter tags’ checkboxes are clickable.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - The filter tags should be visible.
    - The filter tags’ checkboxes should be clickable.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - Then I can see the filter tags.
    - Then I can click the filter tags’ checkboxes.
#### TBD-003. Test that by clicking any of the filter tags’ checkbox it will filter the matching parent or parent-children pairs.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - The filter tags should be visible.
    - By clicking any of the filter tags’ checkbox it should filter the matching parent or parent-children pairs according to tags.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - Then I can see the filter tags.
    - Then I can click the filter tags’ checkboxes.
    - Then when I click on any of the filter tag it will filter the parent which has matching tag and/or if any of the children has matching tag then it’ll show that children ladder with its parent.

#### TBD-003. Test that by clicking any of the filter tags’ checkbox it will filter the matching parent or parent-children pairs and the ladders remain collapsed.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - The filter tags should be visible.
    - By clicking any of the filter tags’ checkbox it should filter the matching parent or parent-children pairs and the ladders remain collapsed.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - Then I can see the filter tags.
    - Then I can click the filter tags’ checkboxes.
    - Then when I click on any of the filter tag it will filter the parent which has matching tag and/or if any of the children has matching tag then it’ll show that children ladder with its parent and the ladders remain collapsed.
