# Instructional Mode
#### TBD-001. Test that at the top right side of the page dropdown menu is loaded.
- Feature: 
    - The drupal ladder’s instructional steps mode should be loaded.
    - The dropdown should be visible.
- Scenario:
    - Given that I am on drupal ladder’s instructional steps mode.
    - Then I see the dropdown at right top corner.
    - Then I should be able to click on that dropdown.
#### TBD-002. Test that the dropdown menu has three options.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - The dropdown should be visible.
    - The dropdown must have three options named ‘Instructional steps Mode’, ‘Expository Mode’, ‘Lecture Mode’.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - Then I see the dropdown at right top corner.
    - Then I should be able to click on that dropdown.
    - Then I can see the three options named ‘Instructional steps Mode’, ‘Expository Mode’, ‘Lecture Mode’.
#### TBD-003. Test that the filter tags are visible.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - The filter tags should be visible.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - Then I can see the filter tags.
#### TBD-004. Test that the filter tags’ checkboxes are clickable.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - The filter tags should be visible.
    - The filter tags’ checkboxes should be clickable.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - Then I can see the filter tags.
    - Then I can click the filter tags’ checkboxes.
#### TBD-005. Test that by clicking any of the filter tags’ checkbox it will filter the matching parent or parent-children pairs.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - The filter tags should be visible.
    - By clicking any of the filter tags’ checkbox it should filter the matching parent or parent-children pairs according to tags.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - Then I can see the filter tags.
    - Then I can click the filter tags’ checkboxes.
    - Then when I click on any of the filter tag it will filter the parent which has matching tag and/or if any of the children has matching tag then it’ll show that children ladder with its parent.

#### TBD-006. Test that by clicking any of the filter tags’ checkbox it will filter the matching parent or parent-children pairs and not change the state of expanded/collapsed steps.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - The filter tags should be visible.
    - By clicking any of the filter tags’ checkbox it should filter the matching parent or parent-children pairs and the ladders remain collapsed.
- Scenario:
    - Given that I am on drupal ladder’s page
    - And that I have expanded some ladders.
    - Then I can see the filter tags.
    - Then I can click the filter tags’ checkboxes.
    - Then when I click on any of the filter tag it will filter the parent which has matching tag and/or if any of the children has matching tag then it’ll show that children ladder with its parent
    - And the ladders I had prevoiusly expanded remain expanded.
- Reference https://gitlab.com/onso-labs/step/issues/128
#### TBD-007. Test that three tabs named ‘Video’,’Screenshot’, and ‘Code Samples’ are visible.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - The three tabs should be visible.
    - Tabs name should be ‘Video’, ‘Screenshot’ and ‘Code Samples’.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - Then I can see the tabs which names are ‘Video’, ‘Screenshot’, ‘Code Samples’.

#### TBD-008. Test that Main ladder’s title is loaded.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - The main ladder’s title is visible.
    - The main ladder’s title should be clickable.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - Then I can see the main ladder’s title and it is clickable.

#### TBD-009. Test that by clicking on the main ladder’s title it will open its video tab.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - By clicking on the main ladder’s title it must open its video tab.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - Then I can see the video tab when clicked on the title of ladder.

#### TBD-010. Test that edit button is visible.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - The edit button must be visible.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - Then I can see edit button.

#### TBD-011. Test that edit button is clickable.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - The edit button must be visible.
    - The edit button must be clickable.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - Then I can see edit button.
    - Then I can click on the edit button.

#### TBD-012. Test that by clicking the edit button it’ll show edit and add icons at the right side of the ladder's name.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - By clicking the edit button it must show edit and add icons at the right side of the ladder's name.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - By clicking the edit button it is showing edit and add icons at the right side of the ladder's name.

#### TBD-013. Test that by clicking the edit icon it’ll show the edit form for that particular node.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - By clicking the edit icon it must show edit form for that particular node.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - By clicking the edit icon it is showing edit form for that particular node.

#### TBD-014. Test that by clicking the add icon it’ll show the add new node form.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - By clicking the add icon it must show the add new node form.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - By clicking the add icon it is showing the add new node form.

#### TBD-015. Test that in edit/add node form when enter youtube video url it’ll show video.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - In edit/add node form when enter youtube video url it must show video.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - Then I can see video in edit/add node form when entered youtube video url.

#### TBD-016. Test that in edit/add node form if video is not broken then after video it’ll show start/end time inputs and play button.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - In edit/add node form if video is not broken then after video it must show start/end time inputs and play button.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - Then I can see start/end time inputs and play button in edit/add node form if video is not broken.

#### TBD-017. Test that by clicking on the play button it’ll play video from start time and stop at end time defined in start/end time inputs.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - By clicking on the play button it must play video from start time and stop at end time defined in start/end time inputs.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - Then I can see when we click on the play button it’ll play video from start time and stop at end time defined in start/end time inputs.

#### TBD-018. Test that each parent node has down arrow icon at the left side of title.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - Each parent node must have down arrow icon at the left side of title and it must be clickable.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - Each parent node have down arrow icon at the left side of title and it is clickable.

#### TBD-019. Test that by clicking the down arrow icon it’ll show its children nodes.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - By clicking the down arrow icon it must show it’s children nodes.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - By clicking the down arrow icon it is showing it’s children nodes.

#### TBD-020. Test that in ‘Video’ tab it is showing video if video is added.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - In video tab it must show video if video is added.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - I can see the video in video tab if video is added.
#### TBD-021. Test that in ‘Video’ tab it is showing like/dislike icon if video is added.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - In video tab it must like/dislike icon if video is added.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - I can see the like/dislike icon in video tab if video is added.

#### TBD-022. Test that in ‘Video’ tab it is showing watch button if video is added.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - In video tab it must show watch button if video is added.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - I can see watch button in video tab  if video is added.
#### TBD-023. Test that in ‘Video’ tab it is showing description if added.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - In video tab it must show description if it is added.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - I can see the description in video tab  if it is added.

#### TBD-024. Test that in screenshot tab it is showing image if added.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - In screenshot tab it must show image if it is added.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - I can see the image in screenshot tab if it is added.

#### TBD-025. Test that in code samples tab it is showing code title , code snippets and help text if added.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - In code samples tab it must show code title , code snippets and help text if it is added.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - I can see the code title,code snippets and help text in code samples tab if it is added.

#### TBD-026. Test that in code samples tab it is showing code snippets if added.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - In code samples tab it must show code snippets if it is added.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - I can see the code snippets in code samples tab if it is added.
#### TBD-027. Test that in code samples tab it is showing help text if added.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - In code samples tab it must show help text if it is added.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - I can see the help text in code samples tab if it is added.
#### TBD-028. Test that like/dislike icons are clickable.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - Like/dislike Icons must be clickable.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - I can click like/dislike icons.

#### TBD-029. Test that number of likes/dislikes is showing in the video tab.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - Number of likes/dislikes must be shown in the video tab.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - I can see the number of likes/dislikes in video tab.

#### TBD-030. Test that by clicking on the like/dislike icon it’ll increase/decrease number of counts.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - By clicking on the like/dislike icon it must increase/decrease number of counts.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - By clicking on the like/dislike icon it will increase/decrease number of counts.

#### TBD-031. Test that by clicking on the Expository Mode option from the dropdown it’ll open the expository mode.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - By clicking on the Expository Mode option from the dropdown it must open the expository mode.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - By clicking on the Expository Mode option from the dropdown it will open the expository mode.

#### TBD-032. Test that in expository mode main and parent ladders’ title will be displayed in <h1> tag.
- Feature: 
    - The drupal ladder’s expository mode page should be loaded.
    - In expository mode main and parent ladders’ title must be displayed in <h1> tag.
- Scenario:
    - Given that I am on drupal ladder’s expository mode page.
    - In expository mode main and parent ladders’ title will be displayed in <h1> tag.

#### TBD-032. Test that in expository mode children ladders’ title will be displayed in <h3> tag.
- Feature: 
    - The drupal ladder’s expository mode page should be loaded.
    - In expository mode children ladders’ title must be displayed in <h3> tag.
- Scenario:
    - Given that I am on drupal ladder’s expository mode page.
    - In expository mode children ladders’ title will be displayed in <h3> tag.

#### TBD-033. Test that in expository mode every node’s description will be displayed at the left side after its title.
- Feature: 
    - The drupal ladder’s expository mode page should be loaded.
    - In expository mode every node’s description must be displayed at the left side after its title.
- Scenario:
    - Given that I am on drupal ladder’s expository mode page.
    - In expository mode every node’s description is displayed at the left side after its title.

#### TBD-034. Test that in expository mode every node’s video will be displayed at the right side of the description.
- Feature: 
    - The drupal ladder’s expository mode page should be loaded.
    - In expository mode every node’s video must be displayed at the right side of the description.
- Scenario:
    - Given that I am on drupal ladder’s expository mode page.
    - In expository mode every node’s video is displayed at the right side of the description.

#### TBD-035. Test that in expository mode every node’s code samples will be displayed at the left side after the description.
- Feature: 
    - The drupal ladder’s expository mode page should be loaded.
    - In expository mode every node’s code samples must be displayed at the left side after the description.
- Scenario:
    - Given that I am on drupal ladder’s expository mode page.
    - In expository mode every node’s code samples is displayed at the left side after the description.

#### TBD-036. Test that in expository mode every node’s screenshot will be displayed at the right side of the code samples.
- Feature: 
    - The drupal ladder’s expository mode page should be loaded.
    - In expository mode every node’s screenshot must be displayed at the right side of the code samples.
- Scenario:
    - Given that I am on drupal ladder’s expository mode page.
    - In expository mode every node’s screenshot must be displayed at the right side of the code samples.

#### TBD-037. Test that by clicking on the Lecture Mode option from the dropdown it’ll open the lecture mode.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - By clicking on the Lecture Mode option from the dropdown it must open the Lecture mode.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - By clicking on the Lecture Mode option from the dropdown it will open the Lecture mode.

#### TBD-038. Test that in Lecture mode in main header main ladder’s title will be displayed.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - In Lecture mode in main header main ladder’s title must be displayed.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - In Lecture mode in main header main ladder’s title is displayed.

#### TBD-039. Test that in Lecture mode ladder’s title, video and description is displayed if added.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - In Lecture mode ladder’s title, video and description must be displayed if added.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - In Lecture mode ladder’s title, video and description is displayed if added.

#### TBD-040. Test that in Lecture mode next button will be displayed.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - In Lecture mode next button must be displayed.

- Scenario:
    - Given that I am on drupal ladder’s page.
    - In Lecture mode next button is displayed.

#### TBD-041. Test that in Lecture mode by clicking on the next button it will display its children or its sibling.
- Feature: 
    - The drupal ladder’s page should be loaded.
    - In Lecture mode by clicking the next button it must display its children or its sibling.
- Scenario:
    - Given that I am on drupal ladder’s page.
    - In Lecture mode by clicking the next button it is displayed its children or its sibling.









