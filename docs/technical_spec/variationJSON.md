```
 baseLadder=[
    {
      "stepId":"13376",
      "id" : Math.random().toString(36).substr(2,8),
      "author":"1",
      "title":"make a hot, delicious drink",
      "videoId":{
        "id":null,
        "title":"",
        "youtubeId":"",
        "url":""
      },
      "startSeconds":"",
      "pauseSeconds":"",
      "tags":[{
        "id":"1421",
        "name":"indian masala tea"
      }],
      "parent":"0",
      "description":[],
      "likes":{
        "like":0,
        "dislike":0,
        "type":""
      },
      "code_snippet":[{
        "title":null,
        "code_snippet":null,
        "help_text":null
      }],
      "images":[],
      "score":null,
      "last_updated":"1574253401",
      "revision_exist":true,
      "is_reviewed":false,
      "children":[
        {
        "stepId":
        "13381",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"boil 1\/2 cup pure water",
        "videoId":{
          "id":"3436",
          "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc","url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
        },
        "startSeconds":"",
        "pauseSeconds":"",
        "tags":[{
          "id":"1421",
          "name":"indian masala tea"
        }],
        "parent":"13376",
        "description":[],
        "likes":{
          "like":0,
          "dislike":0,
          "type":""
        },
        "code_snippet":[],
        "images":[],
        "score":null,
        "last_updated":"1574252156",
        "is_reviewed":false
      },
      {
        "stepId":"13386",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add the major flavors to the boiling water",
        "videoId":{
          "id":null,
          "title":"",
          "youtubeId":"",
          "url":""
        },
        "startSeconds":"",
        "pauseSeconds":"",
        "tags":[],
        "parent":"13376",
        "description":[],
        "likes":{
          "like":0,
          "dislike":0,
          "type":""
        },
        "code_snippet":[],
        "images":[],
        "score":null,
        "last_updated":"1574252667",
        "is_reviewed":false,
        "children":[
          {
          "stepId":"13391",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"add 1 tablespoon tea leaves\t",
          "videoId":{
            "id":"3441",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc","url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
          },
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{
            "id":"1421",
            "name":"indian masala tea"
          }],
          "parent":"13386",
          "description":[],
          "likes":{
            "like":0,
            "dislike":0,
            "type":""
          },
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252341",
          "is_reviewed":false
        },
        {
          "stepId":"13396",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"add 1 tablespoon ground coffee",
          "videoId":{
            "id":"3446",
            "title":"@Boiling Coffee using sand  :|#Made on hot sand |Like Turkish Cofee || Apple Street Food","youtubeId":"7WonfypKDpM",
            "url":"https:\/\/www.youtube.com\/watch?v=7WonfypKDpM"
          },
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{
            "id":"1426",
            "name":"sand coffee"
          }],
          "parent":"13386",
          "description":[],
          "likes":{
            "like":0,
            "dislike":0,
            "type":""
          },
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252482",
          "is_reviewed":false
        },
        {
          "stepId":"13401",
          "author":"1",
          "id" : Math.random().toString(36).substr(2,8),
          "title":"add 1 tablespoon sugar",
          "videoId":{
            "id":"3451",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
            "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"},
            "startSeconds":"",
            "pauseSeconds":"",
            "tags":[{
              "id":"1421",
              "name":"indian masala tea"
            }],
            "parent":"13386",
            "description":[],
            "likes":{"like":0,"dislike":0,"type":""},
            "code_snippet":[],
            "images":[],
            "score":null,
            "last_updated":"1574252576",
            "is_reviewed":false
          },
          {
            "stepId":"13406",
            "author":"1",
            "id" : Math.random().toString(36).substr(2,8),
            "title":"grate a thumb-sized piece of ginger into boiling tea",
            "videoId":{
              "id":"3456",
              "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
              "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
            },
            "startSeconds":7,
            "pauseSeconds":11,
            "tags":[{"id":"1421","name":"indian masala tea"}],
            "parent":"13386",
            "description":[],
            "likes":{"like":0,"dislike":0,"type":""},
            "code_snippet":[],
            "images":[],
            "score":null,
            "last_updated":"1574252653",
            "is_reviewed":false}],
            "expanded":false
          },
          {
            "stepId":"13411",
            "author":"1",
            "id" : Math.random().toString(36).substr(2,8),
            "title":"meld the flavors",
            "videoId":{"id":null,"title":"","youtubeId":"","url":""},
            "startSeconds":"",
            "pauseSeconds":"",
            "tags":[],
            "parent":"13376",
            "description":[],
            "likes":{"like":0,"dislike":0,"type":""},
            "code_snippet":[],
            "images":[],
            "score":null,
            "last_updated":"1574253401",
            "is_reviewed":false,
            "children":[
              {
              "stepId":"13436",
              "id" : Math.random().toString(36).substr(2,8),
              "author":"1",
              "title":"let mixture boil additional 2 minutes",
              "videoId":{
                "id":"3466",
                "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
                "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
              },
              "startSeconds":11,
              "pauseSeconds":15,
              "tags":[],
              "parent":"13411",
              "description":[],
              "likes":{"like":0,"dislike":0,"type":""},
              "code_snippet":[],
              "images":[],
              "score":null,
              "last_updated":"1574253212",
              "is_reviewed":false
            },
            {
              "stepId":"13441",
              "id" : Math.random().toString(36).substr(2,8),
              "author":"1",
              "title":"add 1\/2 cup milk to boiling tea \u0026 continue boiling",
              "videoId":
              {
                "id":"3471",
                "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
                "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
              },
              "startSeconds":29,
              "pauseSeconds":34,
              "tags":[{"id":"1421","name":"indian masala tea"}],
              "parent":"13411",
              "description":[],
              "likes":{"like":0,"dislike":0,"type":""},
              "code_snippet":[],
              "images":[],
              "score":null,
              "last_updated":"1574253336",
              "is_reviewed":false
            }],
            "expanded":false
          },
          {
            "stepId":"13416",
            "author":"1",
            "id" : Math.random().toString(36).substr(2,8),
            "title":"sprinkle 3\/4 teaspoon tea masala",
            "videoId":{"id":null,"title":"","youtubeId":"","url":""},
            "startSeconds":"",
            "pauseSeconds":"",
            "tags":[{"id":"1421","name":"indian masala tea"}],
            "parent":"13376",
            "description":[],
            "likes":{"like":0,"dislike":0,"type":""},
            "code_snippet":[],
            "images":[],
            "score":null,
            "last_updated":"1574252791",
            "is_reviewed":false
          },
          {
            "stepId":"13421",
            "author":"1",
            "id" : Math.random().toString(36).substr(2,8),
            "title":"continue boiling until foam builds a few inches",
            "videoId":{
              "id":"3461",
              "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
              "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
            },
            "startSeconds":32,
            "pauseSeconds":37,
            "tags":[{"id":"1421","name":"indian masala tea"}],
            "parent":"13376",
            "description":[],
            "likes":{"like":0,"dislike":0,"type":""},
            "code_snippet":[],
            "images":[],
            "score":null,
            "last_updated":"1574252856",
            "is_reviewed":false
          },
          {
            "stepId":"13426",
            "author":"1",
            "id" : Math.random().toString(36).substr(2,8),
            "title":"lower heat to a gentle boil for 1 more minute",
            "videoId":{"id":null,"title":"","youtubeId":"","url":""},
            "startSeconds":"",
            "pauseSeconds":"",
            "tags":[{"id":"1421","name":"indian masala tea"}],
            "parent":"13376",
            "description":[],
            "likes":{"like":0,"dislike":0,"type":""},
            "code_snippet":[],
            "images":[],
            "score":null,
            "last_updated":"1574252992",
            "is_reviewed":false
          },
        ],
          "expanded":false
    }
  ]

 variationLadders=[
  {
    "stepId":"13376",
    "id" : Math.random().toString(36).substr(2,8),
    "author":"1",
    "title":"make a hot, delicious drink",
    "videoId":{
      "id":null,
      "title":"",
      "youtubeId":"",
      "url":""
    },
    "startSeconds":"",
    "pauseSeconds":"",
    "tags":[{
      "id":"1421",
      "name":"indian masala tea"
    }],
    "parent":"0",
    "description":[],
    "likes":{
      "like":0,
      "dislike":0,
      "type":""
    },
    "code_snippet":[{
      "title":null,
      "code_snippet":null,
      "help_text":null
    }],
    "images":[],
    "score":null,
    "last_updated":"1574253401",
    "revision_exist":true,
    "is_reviewed":false,
    "children":[
      {
      "stepId":
      "13381",
      "id" : Math.random().toString(36).substr(2,8),
      "author":"1",
      "title":"boil 1\/2 cup pure water",
      "videoId":{
        "id":"3436",
        "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc","url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
      },
      "startSeconds":"",
      "pauseSeconds":"",
      "tags":[{
        "id":"1421",
        "name":"indian masala tea"
      }],
      "parent":"13376",
      "description":[],
      "likes":{
        "like":0,
        "dislike":0,
        "type":""
      },
      "code_snippet":[],
      "images":[],
      "score":null,
      "last_updated":"1574252156",
      "is_reviewed":false
    },
    {
      "stepId":"13386",
      "id" : Math.random().toString(36).substr(2,8),
      "author":"1",
      "title":"add the major flavors to the boiling water",
      "videoId":{
        "id":null,
        "title":"",
        "youtubeId":"",
        "url":""
      },
      "startSeconds":"",
      "pauseSeconds":"",
      "tags":[],
      "parent":"13376",
      "description":[],
      "likes":{
        "like":0,
        "dislike":0,
        "type":""
      },
      "code_snippet":[],
      "images":[],
      "score":null,
      "last_updated":"1574252667",
      "is_reviewed":false,
      "children":[
        {
        "stepId":"13391",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add 1 tablespoon tea leaves........\t",
        "videoId":{
          "id":"3441",
          "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc","url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
        },
        "startSeconds":"",
        "pauseSeconds":"",
        "tags":[{
          "id":"1421",
          "name":"indian masala tea"
        }],
        "parent":"13386",
        "description":[],
        "likes":{
          "like":0,
          "dislike":0,
          "type":""
        },
        "code_snippet":[],
        "images":[],
        "score":null,
        "last_updated":"1574252341",
        "updateAdd":true,
        "is_reviewed":false
      },
      {
        "stepId":"13396",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add 1 tablespoon ground coffee",
        "videoId":{
          "id":"3446",
          "title":"@Boiling Coffee using sand  :|#Made on hot sand |Like Turkish Cofee || Apple Street Food","youtubeId":"7WonfypKDpM",
          "url":"https:\/\/www.youtube.com\/watch?v=7WonfypKDpM"
        },
        "startSeconds":"",
        "pauseSeconds":"",
        "tags":[{
          "id":"1426",
          "name":"sand coffee"
        }],
        "parent":"13386",
        "description":[],
        "likes":{
          "like":0,
          "dislike":0,
          "type":""
        },
        "code_snippet":[],
        "images":[],
        "score":null,
        "last_updated":"1574252482",
        "is_reviewed":false
      },
      {
        "stepId":"13401",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add 1 tablespoon sugar",
        "videoId":{
          "id":"3451",
          "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
          "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{
            "id":"1421",
            "name":"indian masala tea"
          }],
          "parent":"13386",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252576",
          "is_reviewed":false
        },
        {
          "stepId":"13406",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"grate a thumb-sized piece of ginger into boiling tea",
          "videoId":{
            "id":"3456",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
            "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
          },
          "startSeconds":7,
          "pauseSeconds":11,
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13386",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252653",
          "is_reviewed":false}],
          "expanded":false
        },
        {
          "stepId":"13411",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"meld the flavors",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574253401",
          "is_reviewed":false,
          "children":[
            {
            "stepId":"13436",
            "id" : Math.random().toString(36).substr(2,8),
            "author":"1",
            "title":"let mixture boil additional 2 minutes",
            "videoId":{
              "id":"3466",
              "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
              "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
            },
            "startSeconds":11,
            "pauseSeconds":15,
            "tags":[],
            "parent":"13411",
            "description":[],
            "likes":{"like":0,"dislike":0,"type":""},
            "code_snippet":[],
            "images":[],
            "score":null,
            "last_updated":"1574253212",
            "is_reviewed":false
          },
          {
            "stepId":"13441",
            "id" : Math.random().toString(36).substr(2,8),
            "author":"1",
            "title":"add 1\/2 cup milk to boiling tea \u0026 continue boiling",
            "videoId":
            {
              "id":"3471",
              "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
              "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
            },
            "startSeconds":29,
            "pauseSeconds":34,
            "tags":[{"id":"1421","name":"indian masala tea"}],
            "parent":"13411",
            "description":[],
            "likes":{"like":0,"dislike":0,"type":""},
            "code_snippet":[],
            "images":[],
            "score":null,
            "last_updated":"1574253336",
            "is_reviewed":false
          }],
          "expanded":false
        },
        {
          "stepId":"13416",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"sprinkle 3\/4 teaspoon tea masala",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252791",
          "is_reviewed":false
        },
        {
          "stepId":"13421",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"continue boiling until foam builds a few inches",
          "videoId":{
            "id":"3461",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
            "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
          },
          "startSeconds":32,
          "pauseSeconds":37,
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252856",
          "is_reviewed":false
        },
        {
          "stepId":"13426",
          "author":"1",
          "id" : Math.random().toString(36).substr(2,8),
          "title":"lower heat to a gentle boil for 1 more minute",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252992",
          "is_reviewed":false
        },
        
      ],
        "expanded":false
  },
  {
    "stepId":"13376",
    "id" : Math.random().toString(36).substr(2,8),
    "author":"1",
    "title":"make a hot, delicious drink",
    "videoId":{
      "id":null,
      "title":"",
      "youtubeId":"",
      "url":""
    },
    "startSeconds":"",
    "pauseSeconds":"",
    "tags":[{
      "id":"1421",
      "name":"indian masala tea"
    }],
    "parent":"0",
    "description":[],
    "likes":{
      "like":0,
      "dislike":0,
      "type":""
    },
    "code_snippet":[{
      "title":null,
      "code_snippet":null,
      "help_text":null
    }],
    "images":[],
    "score":null,
    "last_updated":"1574253401",
    "revision_exist":true,
    "is_reviewed":false,
    "children":[
      {
      "stepId":
      "13381",
      "id" : Math.random().toString(36).substr(2,8),
      "author":"1",
      "title":"boil 1\/2 cup water",
      "videoId":{
        "id":"3436",
        "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc","url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
      },
      "startSeconds":"",
      "pauseSeconds":"",
      "tags":[],
      "parent":"13376",
      "description":[],
      "likes":{
        "like":0,
        "dislike":0,
        "type":""
      },
      "code_snippet":[],
      "images":[],
      "score":null,
      "last_updated":"1574252156",
      "is_reviewed":false,
      "updateDelete":true
    },
    {
      "stepId":"13386",
      "id" : Math.random().toString(36).substr(2,8),
      "author":"1",
      "title":"add the major flavors to the boiling water",
      "videoId":{
        "id":null,
        "title":"",
        "youtubeId":"",
        "url":""
      },
      "startSeconds":"",
      "pauseSeconds":"",
      "tags":[],
      "parent":"13376",
      "description":[],
      "likes":{
        "like":0,
        "dislike":0,
        "type":""
      },
      "code_snippet":[],
      "images":[],
      "score":null,
      "last_updated":"1574252667",
      "is_reviewed":false,
      "children":[
        {
        "stepId":"13391",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add 1 tablespoon tea leaves........\t",
        "videoId":{
          "id":"3441",
          "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc","url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
        },
        "startSeconds":"",
        "pauseSeconds":"",
        "tags":[{
          "id":"1421",
          "name":"indian masala tea"
        }],
        "parent":"13386",
        "description":[],
        "likes":{
          "like":0,
          "dislike":0,
          "type":""
        },
        "code_snippet":[],
        "images":[],
        "score":null,
        "last_updated":"1574252341",
        "is_reviewed":false
      },
      {
        "stepId":"13396",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add 1 tablespoon ground coffee",
        "videoId":{
          "id":"3446",
          "title":"@Boiling Coffee using sand  :|#Made on hot sand |Like Turkish Cofee || Apple Street Food","youtubeId":"7WonfypKDpM",
          "url":"https:\/\/www.youtube.com\/watch?v=7WonfypKDpM"
        },
        "startSeconds":"",
        "pauseSeconds":"",
        "tags":[{
          "id":"1426",
          "name":"sand coffee"
        }],
        "parent":"13386",
        "description":[],
        "likes":{
          "like":0,
          "dislike":0,
          "type":""
        },
        "code_snippet":[],
        "images":[],
        "score":null,
        "last_updated":"1574252482",
        "is_reviewed":false
      },
      {
        "stepId":"13401",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add 1 tablespoon sugar",
        "videoId":{
          "id":"3451",
          "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
          "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{
            "id":"1421",
            "name":"indian masala tea"
          }],
          "parent":"13386",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252576",
          "is_reviewed":false
        },
        {
          "stepId":"13406",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"grate a thumb-sized piece of ginger into boiling tea",
          "videoId":{
            "id":"3456",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
            "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
          },
          "startSeconds":7,
          "pauseSeconds":11,
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13386",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252653",
          "is_reviewed":false}],
          "expanded":false
        },
        {
          "stepId":"13411",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"meld the flavors",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574253401",
          "is_reviewed":false,
          "children":[
            {
            "stepId":"13436",
            "id" : Math.random().toString(36).substr(2,8),
            "author":"1",
            "title":"let mixture boil additional 2 minutes",
            "videoId":{
              "id":"3466",
              "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
              "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
            },
            "startSeconds":11,
            "pauseSeconds":15,
            "tags":[],
            "parent":"13411",
            "description":[],
            "likes":{"like":0,"dislike":0,"type":""},
            "code_snippet":[],
            "images":[],
            "score":null,
            "last_updated":"1574253212",
            "is_reviewed":false
          },
          {
            "stepId":"13441",
            "id" : Math.random().toString(36).substr(2,8),
            "author":"1",
            "title":"add 1\/2 cup milk to boiling tea \u0026 continue boiling",
            "videoId":
            {
              "id":"3471",
              "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
              "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
            },
            "startSeconds":29,
            "pauseSeconds":34,
            "tags":[{"id":"1421","name":"indian masala tea"}],
            "parent":"13411",
            "description":[],
            "likes":{"like":0,"dislike":0,"type":""},
            "code_snippet":[],
            "images":[],
            "score":null,
            "last_updated":"1574253336",
            "is_reviewed":false
          }],
          "expanded":false
        },
        {
          "stepId":"13416",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"sprinkle 3\/4 teaspoon tea masala",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252791",
          "is_reviewed":false
        },
        {
          "stepId":"13421",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"continue boiling until foam builds a few inches",
          "videoId":{
            "id":"3461",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
            "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
          },
          "startSeconds":32,
          "pauseSeconds":37,
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252856",
          "is_reviewed":false
        },
        {
          "stepId":"13426",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"lower heat to a gentle boil for 1 more minute",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252992",
          "is_reviewed":false
        },
      ],
    "expanded":false,
  },
  {
    "stepId":"13376",
    "id" : Math.random().toString(36).substr(2,8),
    "author":"1",
    "title":"make a hot, delicious drink",
    "videoId":{
      "id":null,
      "title":"",
      "youtubeId":"",
      "url":""
    },
    "startSeconds":"",
    "pauseSeconds":"",
    "tags":[{
      "id":"1421",
      "name":"indian masala tea"
    }],
    "parent":"0",
    "description":[],
    "likes":{
      "like":0,
      "dislike":0,
      "type":""
    },
    "code_snippet":[{
      "title":null,
      "code_snippet":null,
      "help_text":null
    }],
    "images":[],
    "score":null,
    "last_updated":"1574253401",
    "revision_exist":true,
    "is_reviewed":false,
    "children":[
      {
      "stepId":
      "13381",
      "id" : Math.random().toString(36).substr(2,8),
      "author":"1",
      "title":"boil 1\/2 cup water",
      "videoId":{
        "id":"3436",
        "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc","url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
      },
      "startSeconds":"",
      "pauseSeconds":"",
      "tags":[{
        "id":"1421",
        "name":"indian masala tea"
      }],
      "parent":"13376",
      "description":[],
      "likes":{
        "like":0,
        "dislike":0,
        "type":""
      },
      "code_snippet":[],
      "images":[],
      "score":null,
      "last_updated":"1574252156",
      "is_reviewed":false
      },
      {
        "stepId":"13386",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add the major flavors to the boiling water",
        "videoId":{
          "id":null,
          "title":"",
          "youtubeId":"",
          "url":""
        },
        "startSeconds":"",
        "pauseSeconds":"",
        "tags":[],
        "parent":"13376",
        "description":[],
        "likes":{
          "like":0,
          "dislike":0,
          "type":""
        },
        "code_snippet":[],
        "images":[],
        "score":null,
        "last_updated":"1574252667",
        "is_reviewed":false,
        "children":[
          {
          "stepId":"13391",
          "author":"1",
          "title":"add 1 tablespoon tea leaves........\t",
          "videoId":{
            "id":"3441",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc","url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
          },
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{
            "id":"1421",
            "name":"indian masala tea"
          }],
          "parent":"13386",
          "description":[],
          "likes":{
            "like":0,
            "dislike":0,
            "type":""
          },
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252341",
          "is_reviewed":false
        },
        {
          "stepId":"13396",
          "author":"1",
          "title":"add 1 tablespoon ground coffee",
          "videoId":{
            "id":"3446",
            "title":"@Boiling Coffee using sand  :|#Made on hot sand |Like Turkish Cofee || Apple Street Food","youtubeId":"7WonfypKDpM",
            "url":"https:\/\/www.youtube.com\/watch?v=7WonfypKDpM"
          },
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{
            "id":"1426",
            "name":"sand coffee"
          }],
          "parent":"13386",
          "description":[],
          "likes":{
            "like":0,
            "dislike":0,
            "type":""
          },
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252482",
          "is_reviewed":false
        },
        {
          "stepId":"13401",
          "author":"1",
          "title":"add 1 tablespoon sugar",
          "videoId":{
            "id":"3451",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
            "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"},
            "startSeconds":"",
            "pauseSeconds":"",
            "tags":[{
              "id":"1421",
              "name":"indian masala tea"
            }],
            "parent":"13386",
            "description":[],
            "likes":{"like":0,"dislike":0,"type":""},
            "code_snippet":[],
            "images":[],
            "score":null,
            "last_updated":"1574252576",
            "is_reviewed":false
          },
          {
            "stepId":"13406",
            "author":"1",
            "title":"grate a thumb-sized piece of ginger into boiling tea",
            "videoId":{
              "id":"3456",
              "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
              "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
            },
            "startSeconds":7,
            "pauseSeconds":11,
            "tags":[{"id":"1421","name":"indian masala tea"}],
            "parent":"13386",
            "description":[],
            "likes":{"like":0,"dislike":0,"type":""},
            "code_snippet":[],
            "images":[],
            "score":null,
            "last_updated":"1574252653",
            "is_reviewed":false}],
            "expanded":false
      },
      {
        "stepId":"13411",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"meld the flavors",
        "videoId":{"id":null,"title":"","youtubeId":"","url":""},
        "startSeconds":"",
        "pauseSeconds":"",
        "tags":[],
        "parent":"13376",
        "description":[],
        "likes":{"like":0,"dislike":0,"type":""},
        "code_snippet":[],
        "images":[],
        "score":null,
        "last_updated":"1574253401",
        "is_reviewed":false,
        "children":[
          {
          "stepId":"13436",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"let mixture boil additional 2 minutes",
          "videoId":{
            "id":"3466",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
            "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
          },
          "startSeconds":11,
          "pauseSeconds":15,
          "tags":[],
          "parent":"13411",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574253212",
          "is_reviewed":false
        },
        {
          "stepId":"13441",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"add 1\/2 cup milk to boiling tea \u0026 continue boiling",
          "videoId":
          {
            "id":"3471",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
            "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
          },
          "startSeconds":29,
          "pauseSeconds":34,
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13411",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574253336",
          "is_reviewed":false
        }],
        "expanded":false,
        "deleted":true,
      },
        {
          "stepId":"13416",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"sprinkle 3\/4 teaspoon tea masala",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252791",
          "is_reviewed":false
        },
        {
          "stepId":"13421",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"continue boiling until foam builds a few inches",
          "videoId":{
            "id":"3461",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
            "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
          },
          "startSeconds":32,
          "pauseSeconds":37,
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252856",
          "is_reviewed":false
        },
        {
          "stepId":"13426",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"lower heat to a gentle boil for 1 more minute",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252992",
          "is_reviewed":false
        },
        ],
        "expanded":false
  },
  {
    "stepId":"13376",
    "id" : Math.random().toString(36).substr(2,8),
    "author":"1",
    "title":"make a hot, delicious drink",
    "videoId":{
      "id":null,
      "title":"",
      "youtubeId":"",
      "url":""
    },
    "startSeconds":"",
    "pauseSeconds":"",
    "tags":[{
      "id":"1421",
      "name":"indian masala tea"
    }],
    "parent":"0",
    "description":[],
    "likes":{
      "like":0,
      "dislike":0,
      "type":""
    },
    "code_snippet":[{
      "title":null,
      "code_snippet":null,
      "help_text":null
    }],
    "images":[],
    "score":null,
    "last_updated":"1574253401",
    "revision_exist":true,
    "is_reviewed":false,
    "children":[
      {
      "stepId":
      "13381",
      "id" : Math.random().toString(36).substr(2,8),
      "author":"1",
      "title":"boil 1\/2 cup water",
      "videoId":{
        "id":"3436",
        "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc","url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
      },
      "startSeconds":"",
      "pauseSeconds":"",
      "tags":[{
        "id":"1421",
        "name":"indian masala tea"
      }],
      "parent":"13376",
      "description":[],
      "likes":{
        "like":0,
        "dislike":0,
        "type":""
      },
      "code_snippet":[],
      "images":[],
      "score":null,
      "last_updated":"1574252156",
      "is_reviewed":false
    },
    {
      "stepId":"13386",
      "id" : Math.random().toString(36).substr(2,8),
      "author":"1",
      "title":"add the major flavors to the boiling water",
      "videoId":{
        "id":null,
        "title":"",
        "youtubeId":"",
        "url":""
      },
      "startSeconds":"",
      "pauseSeconds":"",
      "tags":[],
      "parent":"13376",
      "description":[],
      "likes":{
        "like":0,
        "dislike":0,
        "type":""
      },
      "code_snippet":[],
      "images":[],
      "score":null,
      "last_updated":"1574252667",
      "is_reviewed":false,
      "children":[
        {
        "stepId":"13391",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add 1 tablespoon tea leaves........\t",
        "videoId":{
          "id":"3441",
          "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc","url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
        },
        "startSeconds":"",
        "pauseSeconds":"",
        "tags":[{
          "id":"1421",
          "name":"indian masala tea"
        }],
        "parent":"13386",
        "description":[],
        "likes":{
          "like":0,
          "dislike":0,
          "type":""
        },
        "code_snippet":[],
        "images":[],
        "score":null,
        "last_updated":"1574252341",
        "is_reviewed":false
      },
      {
        "stepId":"13396",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add 1 tablespoon ground coffee",
        "videoId":{
          "id":"3446",
          "title":"@Boiling Coffee using sand  :|#Made on hot sand |Like Turkish Cofee || Apple Street Food","youtubeId":"7WonfypKDpM",
          "url":"https:\/\/www.youtube.com\/watch?v=7WonfypKDpM"
        },
        "startSeconds":"",
        "pauseSeconds":"",
        "tags":[{
          "id":"1426",
          "name":"sand coffee"
        }],
        "parent":"13386",
        "description":[],
        "likes":{
          "like":0,
          "dislike":0,
          "type":""
        },
        "code_snippet":[],
        "images":[],
        "score":null,
        "last_updated":"1574252482",
        "is_reviewed":false
      },
      {
        "stepId":"13401",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add 1 tablespoon sugar",
        "videoId":{
          "id":"3451",
          "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
          "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{
            "id":"1421",
            "name":"indian masala tea"
          }],
          "parent":"13386",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252576",
          "is_reviewed":false
        },
        {
          "stepId":"13406",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"grate a thumb-sized piece of ginger into boiling tea",
          "videoId":{
            "id":"3456",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
            "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
          },
          "startSeconds":7,
          "pauseSeconds":11,
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13386",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252653",
          "is_reviewed":false}],
          "expanded":false
        },
        {
          "stepId":"13416",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"sprinkle 3\/4 teaspoon tea masala",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252791",
          "is_reviewed":false
        },
        {
          "stepId":"13421",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"continue boiling until foam builds a few inches",
          "videoId":{
            "id":"3461",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
            "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
          },
          "startSeconds":32,
          "pauseSeconds":37,
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252856",
          "is_reviewed":false
        },
        {
          "stepId":"13426",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"lower heat to a gentle boil for 1 more minute",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252992",
          "is_reviewed":false
        },
        {
          "stepId":"13431",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"pour through a strainer and enjoy!",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574253063",
          "is_reviewed":false,
          "added":true
        }],
        "expanded":false
  },
  {
    "stepId":"13376",
    "id" : Math.random().toString(36).substr(2,8),
    "author":"1",
    "title":"make a hot, delicious drink",
    "videoId":{
      "id":null,
      "title":"",
      "youtubeId":"",
      "url":""
    },
    "startSeconds":"",
    "pauseSeconds":"",
    "tags":[{
      "id":"1421",
      "name":"indian masala tea"
    }],
    "parent":"0",
    "description":[],
    "likes":{
      "like":0,
      "dislike":0,
      "type":""
    },
    "code_snippet":[{
      "title":null,
      "code_snippet":null,
      "help_text":null
    }],
    "images":[],
    "score":null,
    "last_updated":"1574253401",
    "revision_exist":true,
    "is_reviewed":false,
    "children":[
      {
      "stepId":
      "13381",
      "id" : Math.random().toString(36).substr(2,8),
      "author":"1",
      "title":"boil 1\/2 cup water",
      "videoId":{
        "id":"3436",
        "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc","url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
      },
      "startSeconds":"",
      "pauseSeconds":"",
      "tags":[{
        "id":"1421",
        "name":"indian masala tea"
      }],
      "parent":"13376",
      "description":[],
      "likes":{
        "like":0,
        "dislike":0,
        "type":""
      },
      "code_snippet":[],
      "images":[],
      "score":null,
      "last_updated":"1574252156",
      "is_reviewed":false
    },
    {
      "stepId":"13386",
      "id" : Math.random().toString(36).substr(2,8),
      "author":"1",
      "title":"add the major flavors to the boiling water",
      "videoId":{
        "id":null,
        "title":"",
        "youtubeId":"",
        "url":""
      },
      "startSeconds":"",
      "pauseSeconds":"",
      "tags":[],
      "parent":"13376",
      "description":[],
      "likes":{
        "like":0,
        "dislike":0,
        "type":""
      },
      "code_snippet":[],
      "images":[],
      "score":null,
      "last_updated":"1574252667",
      "is_reviewed":false,
      "children":[
        {
        "stepId":"13391",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add 1 tablespoon tea leaves........\t",
        "videoId":{
          "id":"3441",
          "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc","url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
        },
        "startSeconds":"",
        "pauseSeconds":"",
        "tags":[{
          "id":"1421",
          "name":"indian masala tea"
        }],
        "parent":"13386",
        "description":[],
        "likes":{
          "like":0,
          "dislike":0,
          "type":""
        },
        "code_snippet":[],
        "images":[],
        "score":null,
        "last_updated":"1574252341",
        "is_reviewed":false
      },
      {
        "stepId":"13396",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add 1 tablespoon ground coffee",
        "videoId":{
          "id":"3446",
          "title":"@Boiling Coffee using sand  :|#Made on hot sand |Like Turkish Cofee || Apple Street Food","youtubeId":"7WonfypKDpM",
          "url":"https:\/\/www.youtube.com\/watch?v=7WonfypKDpM"
        },
        "startSeconds":"",
        "pauseSeconds":"",
        "tags":[{
          "id":"1426",
          "name":"sand coffee"
        }],
        "parent":"13386",
        "description":[],
        "likes":{
          "like":0,
          "dislike":0,
          "type":""
        },
        "code_snippet":[],
        "images":[],
        "score":null,
        "last_updated":"1574252482",
        "is_reviewed":false
      },
      {
        "stepId":"13401",
        "id" : Math.random().toString(36).substr(2,8),
        "author":"1",
        "title":"add 1 tablespoon sugar",
        "videoId":{
          "id":"3451",
          "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
          "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{
            "id":"1421",
            "name":"indian masala tea"
          }],
          "parent":"13386",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252576",
          "is_reviewed":false
        },
        {
          "stepId":"13406",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"grate a thumb-sized piece of ginger into boiling tea",
          "videoId":{
            "id":"3456",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
            "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
          },
          "startSeconds":7,
          "pauseSeconds":11,
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13386",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252653",
          "is_reviewed":false}],
          "expanded":false
        },
        {
          "stepId":"13416",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"sprinkle 3\/4 teaspoon tea masala",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252791",
          "is_reviewed":false
        },
        {
          "stepId":"13421",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"continue boiling until foam builds a few inches",
          "videoId":{
            "id":"3461",
            "title":"Ginger Tea - Adrak Chai  Recipe - Indian Recipes by Archana\u0027s Kitchen","youtubeId":"jqUGraJZyUc",
            "url":"https:\/\/www.youtube.com\/watch?v=jqUGraJZyUc"
          },
          "startSeconds":32,
          "pauseSeconds":37,
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252856",
          "is_reviewed":false
        },
        {
          "stepId":"13426",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"lower heat to a gentle boil for 1 more minute",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574252992",
          "is_reviewed":false
        },
        {
          "stepId":"13431",
          "id" : Math.random().toString(36).substr(2,8),
          "author":"1",
          "title":"pour through a strainer and enjoy!",
          "videoId":{"id":null,"title":"","youtubeId":"","url":""},
          "startSeconds":"",
          "pauseSeconds":"",
          "tags":[{"id":"1421","name":"indian masala tea"}],
          "parent":"13376",
          "description":[],
          "likes":{"like":0,"dislike":0,"type":""},
          "code_snippet":[],
          "images":[],
          "score":null,
          "last_updated":"1574253063",
          "is_reviewed":false,
        }
      ],
        "expanded":false
  }
]
