#### Like/Dislike team requirements:

#### Feature: Like
 	Allow moderators to like any video according to its content.

#### Feature: Dislike.
   	Allow moderators to dislike any video according to its content. 

#### Feature: Like counts.
   	Allow moderators to see total likes of any video. 

#### Feature: Dislike counts.
   	Allow moderators to see total dislikes of any video. 

@See technical implementation: https://gitlab.com/onso-labs/step/blob/like_dislike_api/docs/technical_spec/ladder_like_dislike.md

