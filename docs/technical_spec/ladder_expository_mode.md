***Expository mode explanation:***

**TBD-001**: Show Grand Parent ladder title

**Prerequisite**: `<h1>` tag to display the First Ladder title.

**Steps**: The Grand parent ladder’s title will be displayed in `<h1>Grand parent title</h1>` tag.

**Comments**:

**TBD-002**: Show Parents ladder’s title

**Prerequisite**: `<h2>` tag to display parents ladder’s title

**Steps**: Parent Ladders’ titles will be displayed in `<h2>Parent title</h2>` tag.

**Comments**:

**TBD-003**: Show Child ladders’ Title

**Prerequisite**: `<h3>` tag to show children level ladders’ title.

**Steps**: Children level Ladders’ title will be displayed in `<h3>Child title</h3>` tag, if any children has other child/children then it’s title will be displayed in `<h4>Grand child title</h4>` tag and as follows up to `<h6>Title</h6>` tag.

**Comments**:

**TBD-004**: Showing description of ladder

**Prerequisite**: `<div>` tag to display the description section.

**Steps**: Each parent/child or main ladder’s description will be displayed on the left side after each ladder’s title if it is available otherwise message ‘No description found’ will be there.

**Comments**:

**TBD-005**: Showing video of each ladder

**Prerequisite**: `<div>` & `<video>` tags to display the video section.

**Steps**: Each parent/child or main ladder’s video will be displayed on the right side of the description section after each ladder’s title if it is available otherwise message ‘No Video found’ will be there.

**Comments**:

**TBD-006**: Showing code samples and help text of each ladder

**Prerequisite**: `<div>` &  `<code>` tags  to display the code sample and help text section and show a code under description section.

**Steps**: Each parent/child or main ladder’s code samples and help text will be displayed on the left side after each ladder’s title and it’ll be displayed in the tags if it is available otherwise message ‘No code samples found’ will be displayed.

**Comments**:

**TBD-007**: Showing screenshot of each ladder

**Prerequisite**: `<div>` & `<img>` tags to display the screenshot section

**Steps**: Each parent/child or main ladder’s screenshot will be displayed on the right side of the code Screenshot section after each ladder’s title if it is available otherwise message ‘No snapshot found’ will be there.

**Comments**:

**TBD-008**: Video play/pause

**Prerequisite**: To play/pause functionality show button over video to play/pause video.

**Steps**: User can play and pause video manually, If one video is playing and at that time if we played another video then first video will pause automatically. Basically at a time only one video will be played.

**Comments**:
