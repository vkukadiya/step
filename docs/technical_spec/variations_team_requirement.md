#### Variations team requirements

#### Feature : Main screen ladder
	If there is only one user of that ladder then it’ll show the original variant of that ladder, if there are multiple user and then it’ll show the most popular variant of the ladder, if there are multiple variant of the ladder but there is zero like for all the variants then it’ll show the original variant.

#### Feature : Edit ladder.
	Any of the user can edit the ladder and make variants of that ladder.

#### Feature :Variants bifurcation.
	If the different user edited the ladder then it will consider as a variant otherwise if same user edited the ladder multiple times then it’ll not be considered a variant.


#### Feature :Popular and Oldest Filter.
	Filter popular and oldest will be displayed if the ladder has more than one user and more than one variant.


#### Feature : Popular.
	Shows ladder variants based on popularity or we can say based on number of likes.

#### Feature :Oldest.
	Shows ladder variants based on time from oldest to latest.


@See technical implementation:https://gitlab.com/onso-labs/step/blob/like_dislike_api/docs/technical_spec/variations.md
