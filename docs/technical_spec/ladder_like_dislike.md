**Like/Dislike explanation**:

**TBD-001**: Like Icon

**Prerequisite**: `<icon>` tag with thumbs up icon to display

**Steps**: Like Icon will be displayed left side under the video.

**Comments**:


**TBD-002**: Dislike Icon

**Prerequisite**: `<icon>` tag with thumbs down icon to display

**Steps**: Dislike Icon will be displayed on the right side of the Like icon under the video.

**Comments**:

**TBD-003**: Like Icon click

**Prerequisite**: `<icon>` tag with thumbs up icon button

**Steps**: On the click of Like Icon it will increase the total likes of that particular video and color would be changed from gray to blue.

**Comments**:

**TBD-004**: Dislike Icon click

**Prerequisite**: `<icon>` tag with thumbs down icon button

**Steps**: On the click of Dislike Icon it will increase the total dislikes of that particular video and color would be changed from gray to blue.

**Comments**:

**TBD-005**: Dislike icon click on already liked video.

**Prerequisite**: `<icon>` tag with thumbs down icon button

**Steps**: On the click of dislike icon on already liked video will increase dislike and decrease like and also dislike icon will change the color from gray to blue same as like icon will change color from blue to gray.

**Comments**:

**TBD-006**: Like icon click on already disliked video.

**Prerequisite**: `<icon>` tag with thumbs up icon button

**Steps**: On the click of like icon on already disliked video will increase and decrease dislike and also like icon will change the color from gray to blue same as dislike icon will change color from blue to gray.

**Comments**:

**TBD-007**: Like icon click on already liked video.

**Prerequisite**: `<icon>` tag with thumbs up icon button

**Steps**: On the click of like icon of already liked video will decrease total likes of that video and icon color changes to gray again.

**Comments**:

**TBD-008**: Dislike icon click on already disliked video.

**Prerequisite**: `<icon>` tag with thumbs down icon button

**Steps**: On the click of dislike icon of already disliked video will decrease total dislikes of that video and icon color changes to gray again.

**Comments**:

**TBD-009**: Number Of Likes.

**Prerequisite**: `<span>` tag to show number of likes

**Steps**: Shows the total number of likes on the right side of the like icon.

**Comments**:

**TBD-010**: Number Of Dislikes.

**Prerequisite**: `<span>` tag to show number of dislikes

**Steps**: Shows the total number of dislikes on the right side of the dislike icon.

**Comments**:
