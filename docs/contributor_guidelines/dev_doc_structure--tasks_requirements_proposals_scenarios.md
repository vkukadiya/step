@working_agreement @proposal

# How should development teams capture, store and share the various critical pieces of information that determine the success of a project?

## match up documentation types to dev teams needs

- Some development documentation needs to `evolve` and some is `single-use`.
  - when it must `EVOLVE`
    - **Synch** docs with corresponding software version: if a piece of documentation needs to evolve, tagged releases are essential for keeping it in sync with the related version of the software.
    - If evolving documentation is mistaken for single use documentation, necessary information disappears from the development team's field of vision, making requirements harder to confirm.
    - Example: if you combine project requirements and tasks in tickets, the project requirements
  - when it serves a `SINGLE-USE`
    - **Archive** docs that are no longer needed: if the documentation serves a single use, archiving it once it has served its purpose helps keep the project documentation less cluttered.
    - Single-use documentation that continues to appear after its purpose is served creates unnecessary complexity.

- Some development documentation applies to a very **specific part of the project**, some to **many parts**, and some **throughout** the project.
  - for documentation that applies to a specific part
    - Limit its scope to help keep the project organized.
  - for documentation that applies to many parts
    - Make the information modifiable in just one location and display it anywhere it relates.
  - for documentation that applies universally
    - Make its scope unlimited.

Some development documentation is written collaboratively and some is largely produced by a single team member. When documentation requires collaboration, being able to share changes in realtime makes discussions more productive. When it is written by a single team member,

## proposed solution

Standardize on the format for each type of project documentation that best suits the above distinctions. See the distinctions among documentation needs and the recommendations for doc types below.

Advanced: tie each into scope, schedule, budget, signoffs and change orders (tho recommend adopting the following first, getting well familiar, then expanding to the advanced integrations).

### enable project requirements to `EVOLVE`

#### diagrams

- Create diagrams using draw.io; share via Google Docs & make editable by all with the link
- Export diagrams as both .html and .xml to make versioning a little more effective; add to repo

#### functional tests in the form of user-stories based on user acceptance criteria

- Write Behat tests for key end user expectations, capturing all important user acceptance criteria in the form of Features.
- Save in /features directory with the .features filename extension.
- Craft Scenarios for each key aspect of these requirements.
- Test these user acceptance criteria with Behat.

#### unit tests

- Write unit tests
  - [PHPUnit](https://www.drupal.org/docs/8/phpunit) to test PHP.
  - [NightWatch](https://www.drupal.org/docs/8/testing/javascript-testing-using-nightwatch) to test JavaScript. (The DL project will likely start with JEST for JavaScript testing as it's a more familiar framework, then transition later to NightWatch.)
- Save in \*/{ custom code directory }/{ project directory }/tests/\`/\*Test.php.

### enable user documentation to `EVOLVE`

### write tasks for `SINGLE-USE` and close them when complete

- Track tasks in a ticketing system - gitlab is current choice
- Only include information in tickets that will 'expire' when the ticket is complete
- Link tasks to each related document and each related ticket

### calculate project milestones for `SINGLE-USE` but adjust future dates as efficiencies or delays come up

### record development team working agreements `EVOLVE`
