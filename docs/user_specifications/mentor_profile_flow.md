# ﻿Mentor Profile Flow

1. Dashboard

- My Profile & Settings (Below there would be a button to manage my profile settings)

  - Profile Image
  - Designation {{ question from Kay: what does this word mean - something like a job description or title? }}
  - Country
  - Goal {{ question from Kay: what does this word mean - something like a reason for participating in the site? }}
  - Auto-generated record of my resources/ladders/tasks
  - My bookmarks
  - Auto-generated record of my conversations (Button to view all conversations)
  - Community guidelines.

- Profile: Click on  profile & settings to make changes in the profile.
  Left hand side panel, we can show, my dashboard, ladders (search option and a scroll bar) and conversations.

  - Account Information
    - Name
    - Email Id
    - Password
    - Language
    - Country
    - Contact {{ question from Kay: something like the default Drupal user contact forms and opt-in features? }}

  - Mentor Profile {{ question from Kay: would there be a way to request the user's permission and simply pull this information from other resume sites like Linked-in; alternatively perhaps we just to provide links to people's profiles on those sites? }}
    - What is your primary Industry
    - Your area of expertise
    - Years of experience
    - Professional experience and accomplishment.
    - You image
    - Website

Mentor login header: Image (profile settings & logout), ladder count (if possible) and Message (notification).
