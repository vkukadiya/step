# homepage featured ladders

## requirements

Homepage helps users discover content that is new to them whether they are visiting for the first time or they visit frequently.

## proposed solutions

- show 2 short lists on the homepage
- each list in its own tab
- limit each list to 20 ladders marked as "is ladder"

### highlight most active content

list most active content under an active tab on the homepage with the label 'hot'
- mvp: fake score count until there are enough users to generate meaningful activity levels
- mvp: sort by score in descending order (i.e. highest score is first)
- lts: aggregate upvotes, number of visits, number of edits, youtube video views

### highlight newest content

list newest content under an inactive tab on homepage with the label 'new'
- sort by most recently created/updated ladder in descending order (i.e. most recent is first)

### filters

when the user enters a tag as a filter, only items containing the tag show up in the list
when the user enter multiple tags as filters, only items containing all tags show up in the list
