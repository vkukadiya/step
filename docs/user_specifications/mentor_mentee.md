## Mentor-Mentee Flow

Mentee requests help from a Mentor through the website, via email, in a chat at the water cooler, when first meeting at a conference, etc.

Mentor or Mentee can invite each other to the website.

Once the Mentor creates a Ladder, the website notifies the Mentee.

The Mentor's version of the ladder will be the default version seen by the Mentee unless the Mentee changes it.

In MVP, everyone can see the Ladder, as they can all Ladders.

Anyone (including the Mentor and Mentee) can receive a notification of a new revision to a Ladder.

## proposed user profile data model
 {{ question from Kay: duplicate of profile breakout effort in ./mentor_profile_flow.md? perhaps we break it out as its own file? }}
First Name
Last Name
Email Id
Password
Agree terms and condition check box and submit.
