
# GraphQl Overview
---------------------
Graph QL module overview and extending modules

Module: https://www.drupal.org/project/graphql


## Supported Module
---------------------

- GraphQL Mutation

  - https://www.drupal.org/project/graphql_mutation

  - This module support Mutation for the drupal entities. Creat node, term, comment, user, etc. is possible using this module.

- GraphQL Views

  - https://www.drupal.org/project/graphql_views

  - This module support to build API using views. We can make API using views.

- GraphQL Meta tag (Dev version)

  - https://www.drupal.org/project/graphql_metatag

  - This module provides supports to meta tag to graphQL.

- GraphQL Redirect (Experimental module)

  - https://www.drupal.org/project/graphql_redirect

  - This module provides supports to with redirect module.


## Roadblock
---------------------

- Add entry for direct table is not possible.

- User login/logout might not possible.