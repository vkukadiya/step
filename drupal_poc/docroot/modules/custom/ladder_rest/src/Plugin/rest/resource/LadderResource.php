<?php

namespace Drupal\ladder_rest\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\duration_field\Service\DurationService;
use Drupal\node\Entity\Node;


/**
 * Provides a Ladder Resource
 *
 * @RestResource(
 *   id = "ladder_resource",
 *   label = @Translation("Ladder Resource"),
 *   uri_paths = {
 *     "canonical" = "/rest-api/ladder/{entity_id}"
 *   }
 * )
 */
class LadderResource extends ResourceBase { 

  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function get($entity_id = null) { 

    $entityResult = [];
    if(!empty($entity_id)) {
      // To check the ladder is the main ladder
      $query = \Drupal::entityQuery('node');
      $query->condition('status', 1);
      $query->condition('nid', $entity_id);
      // $query->condition('field_is_ladder', 1);
      $entityResult = $query->execute();
    }

    if(!empty($entityResult)) { 
      $data = array();

      // We call add row once and then it's called resursivly to fetch each child.
      \Drupal\ladder_rest\Plugin\rest\resource\LadderResource::_ladder_rest_add_row($entity_id, "0", $data);

      // Now turn the flat data into a multi-dimensional array.
      $tree = \Drupal\ladder_rest\Plugin\rest\resource\LadderResource::_ladder_rest_build_tree($data);

      // And inspect our tree.
      return new JsonResponse($tree);
    }
    else {
      // And inspect our tree.
      return new JsonResponse('Not a parent Ladder');
    }
  }

  /**
   * To add row in tree
   */
  function _ladder_rest_add_row($nid, $parentNid = 0, &$data) {

    // load node
    $node = Node::load($nid);

    $startTime = $endTime = $videoTargetId = '';
    $startingSeconds = $endingSeconds = $youtubeId = '';
    $like = [];
    $user_id = \Drupal::currentUser()->id();

    if (!empty($node)) {

      // Set the start time
      if(!empty($node->get('field_start_time'))) {
        $hours = $minutes = $seconds = '';
        $startTime = $node->get('field_start_time')->getValue();
        if(!empty($startTime)) {
          $interval = new \DateInterval($startTime[0]['value']);
          $startFormatTime =  $interval->format("%H:%I:%S");
          $startStrTime = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $startFormatTime);
          sscanf($startStrTime, "%d:%d:%d", $hours, $minutes, $seconds);
          $startingSeconds = $hours * 3600 + $minutes * 60 + $seconds;
        }
      }

      // Set the end time
      if(!empty($node->get('field_end_time'))) {
        $hours = $minutes = $seconds = '';
        $endTime = $node->get('field_end_time')->getValue();
        if(!empty($endTime)) {
          $interval = new \DateInterval($endTime[0]['value']);
          $endFormatTime =  $interval->format("%H:%I:%S");
          $endStrTime = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $endFormatTime);
          sscanf($endStrTime, "%d:%d:%d", $hours, $minutes, $seconds);
          $endingSeconds = $hours * 3600 + $minutes * 60 + $seconds;
        }
      }

      // Set the video
      if(!empty($node->get('field_remote_video_er'))) {
        $videoTargetId = $node->field_remote_video_er->target_id;

        $videoPath = '';
        if(!empty($videoTargetId)) {
          $file =  \Drupal::entityTypeManager()->getStorage("media")->load($videoTargetId);
          $mediaFile = $file->get('field_media_oembed_video')->getString();
          preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $mediaFile, $match);
          $youtubeId = $match[1];
        }
        $videpData = [
          'id' => $node->field_remote_video_er->target_id,
          'youtubeId' => $youtubeId
        ];
      }

      // Set the tags
      if(!empty($node->get('field_tags'))) {
        $tagsData = [];
        $tags = $node->get('field_tags')->getValue();
        foreach ($tags as $key => $value) {
          $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($value['target_id']);
          if(isset($term)) {
            $tagsData[$key]['id'] = $value['target_id'];
            $tagsData[$key]['name'] = $term->getName();
          }
        }
      }
      // Set the body
      if(!empty($node->get('body'))) {
        $description = $node->get('body')->getValue();
      }

      // Set the vote
      if(!empty($node->get('field_vote'))) {
        $like = $node->get('field_vote')->getValue();

        // Get the users who already clicked on this particular content.
        $users = json_decode($node->field_vote->clicked_by);
        if ($users == NULL) {
          $users = new \stdClass();
          $users->default = 'default';
        }

        $clicked =  array_search($user_id, array_keys((array) $users));
        if ($clicked) {
          $like[0]['action'] = $users->{$user_id};
        }
      }

      $codeSnippet = '';
      // Set the vote
      if(!empty($node->get('field_code_snippet'))) {
        $codeSnippet = $node->get('field_code_snippet')->getValue();
      }

      $arrImages = [];
      // Set the vote
      if(!empty($node->get('field_image_er'))) {
        // $images = $node->get('field_image_er')->getValue();
        $images = $node->get('field_image_er')->referencedEntities();

        foreach ($images as $key => $image) {

          $imageUrl = '';
          $mid = isset($image->mid) ? $image->mid->value : '';
          $name = isset($image->name) ? $image->name->value : '';
          $mediaImages = $image->get('field_media_image')->referencedEntities();

          foreach ($mediaImages as $key => $mediaImage) {
            // $imageId = isset($mediaImage->uri) ? $mediaImage->uri->value : '';
            $uri = isset($mediaImage->uri) ? $mediaImage->uri->value : '';
            if (!empty($uri)) {
              $imageUrl = file_create_url($uri);
            }
          }

          $arrImages [$key] = [
            'name' => $name,
            'url' => $imageUrl,
            // 'imageId' => $imageId,
            // 'mid' => $mid,
          ];
        }
      }

      // @TODO to add the data fields for the ladder steps for video and time
      $data[$node->id()] = array(
        'stepId' => $node->id(),
        'title' => $node->get('title')->getString(),
        'videoId' => $videpData,
        'startSeconds' => $startingSeconds,
        'pauseSeconds' => $endingSeconds,
        'tags' => $tagsData,
        'parent' => $parentNid,
        'description' => $description,
        'like' => $like,
        'code_snippet' => $codeSnippet,
        'images' => $arrImages,
      );

      $stepsValue = $node->get('field_steps_er')->getValue();
      $nid = $node->get('nid')->getString();
    }
    else {
      // @TODO to add the proper error message
      // And inspect our tree.
      return new JsonResponse('Empty values');
    }
    if(isset($stepsValue)) {
      foreach($stepsValue as $child) {
        \Drupal\ladder_rest\Plugin\rest\resource\LadderResource::_ladder_rest_add_row($child['target_id'], $nid, $data);
      }
    }
  }

  /**
   * To buid steps tree
   */
  function _ladder_rest_build_tree(array $elements, $parentId = 0) { 

    $uid = 0;
    $branch = [];

    foreach ($elements as $element) {

      if ($element['parent'] == $parentId) { 
        $children = \Drupal\ladder_rest\Plugin\rest\resource\LadderResource::_ladder_rest_build_tree($elements, $element['stepId']);

        // Node is reviewed or not
        $review = \Drupal\ladder_rest\Plugin\rest\resource\LadderResource::_ladder_rest_node_read($element['stepId'], $uid);
        $element['is_reviewed'] = $review;

        if ($review) {
          $element['is_reviewed'] = TRUE;
          $element['reviewed_id'] = $review;
        }

        if ($children) {
          $element['children'] = $children;
          $element['expanded'] = FALSE;
        }
        $branch[] = $element;
      }
    }
    return $branch;
  }

  /**
   * Check if node is read by user or not
   */
  function _ladder_rest_node_read($nid = 0, $uid = 0) {

    $isReviewed = FALSE;
    if (!empty($nid)) {

      // Query to check if node is readed or not
      $db = \Drupal::database();
      $query = $db->select('flagging', 'f');
      $query->fields('f', ['id'])
        ->condition('flag_id', 'bookmark')
        ->condition('entity_type', 'node')
        ->condition('uid', $uid)
        ->condition('entity_id', $nid);
      $flagId = $query->execute()->fetchField();

      if (!empty($flagId)) {
        $isReviewed = $flagId;
      }
    }
    return $isReviewed;
  }
}