(function ($, Drupal) {
  Drupal.behaviors.ladderRest = {
    attach: function (context, settings) {

      // Unset token
      if (settings.unset_token) {
        // var __token = { token_type:'basic', value: "" };
        localStorage.removeItem(settings.token_alias);
      }

      $("form#user-login-form").on("submit", (function(e) {
        var name = $("input[name='name']").val();
        var pass = $("input[name='pass']").val();
        var str = name + ':'  + pass;
        var __token = { token_type: 'basic', value: window.btoa(str) };
        localStorage.setItem(settings.token_alias, JSON.stringify(__token));
      }));
    }
  };
})(jQuery, Drupal);